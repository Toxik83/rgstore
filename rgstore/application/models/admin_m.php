<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function check($data) {
        $this->db->where('username', $data['username']);
        $this->db->where('password', $data['password']);
        $query = $this->db->get('users');

        if ($query->num_rows == 1) {
            return true;
        }
    }

}


