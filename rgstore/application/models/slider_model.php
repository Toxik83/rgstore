<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class slider_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getMaxId() {
        $this->db->select_max('id');
        $result = $this->db->get('slider');
        return $result->row();
    }

    public function setSliderData($sliderData = array()) {
        $this->db->insert('slider', $sliderData);
    }

    public function getSliderData($conditions = array(), $limit = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->select('*');
        $this->db->from('slider');
        $this->db->order_by('order_id');

        //Check For Limit
        if (is_array($limit)) {
            if (count($limit) == 1)
                $this->db->limit($limit[0]);
            else if (count($limit) == 2)
                $this->db->limit($limit[0], $limit[1]);
        }
        $result = $this->db->get();
        return $result;
    }

    public function updateSliderData($slideId, $sliderData = array()) {
        if (array_key_exists('image', $sliderData)) {
            $fields['image'] = $sliderData['image'];
        }
        if (array_key_exists('image300x100', $sliderData)) {
            $fields['image300x100'] = $sliderData['image300x100'];
        }

        $this->db->where('slider.id', $slideId);
        $this->db->update('slider', $sliderData);
    }

    public function deleteSliderData($slideId) {
        $this->db->where('slider.id', $slideId);
        $result = $this->db->delete('slider');
        return $result;
    }

    public function reorder($orderId, $slideId) {
        $fields = array(
            'order_id' => $orderId,
        );

        $this->db->where('slider.id', $slideId);
        $this->db->update('slider', $fields);
    }

}
