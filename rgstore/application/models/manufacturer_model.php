<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manufacturer_model extends CI_Model {
     public function show_Manufacturer($search_term ='default' , $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->like('man_name', $search_term);
        $this->db->order_by('order_id');
        $this->db->where('active', 0);
        $this->db->limit($limit, $offset);
        $data = $this->db->get('manufacturer')->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'data' => $data,
                'count' => $count
            );
        } else {
        
        }
    
    } 
    public function getMaxId() {
        $this->db->select_max('id');
        $result = $this->db->get('manufacturer');
        return $result->row();
    }

    public function show_Company() {
        $this->db->select('*');
        $this->db->where('active', 0);
        $this->db->from('manufacturer');
        $this->db->order_by('order_id');
        return $this->db->get()->result();
    }

    public function create($order_id,$name) {
        $company = array(
            'order_id' => $order_id,
            'man_name' => $name
        );
        $this->db->insert('manufacturer', $company);
    }

    public function delete($id) {
        $this->db->set('active', 1);
        $this->db->where('id', $id);
        $this->db->update('manufacturer');
    }

    public function find($id) {
        $this->db->where('id', $id);
        return $this->db->get('manufacturer')->row();
    }

    public function update($id, $name) {
        $this->db->set('man_name', $name);
        $this->db->where('id', $id);
        $this->db->update('manufacturer');
    }

    public function reorder($order_id, $id) {
        $sort_order = array(
            'order_id' => $order_id,
        );
        $this->db->where('id', $id);
        $this->db->update('manufacturer', $sort_order);
    }

    public function check_unique($name) {
        $this->db->select('*');
        $this->db->from('manufacturer');
        $this->db->where('man_name', $name);
        $this->db->where('active', 0);
        $exist = $this->db->get()->row();
        if ($exist) {
            return true;
        }
        return false;
    }
    
    public function countRecords($manid){
        $this->db->select('SQL_CALC_FOUND_ROWS *', false );
        $this->db->from('products');
        $this->db->join('manufacturer', 'products.man_id = manufacturer.id');
        $this->db->where('manufacturer.id', $manid);
        $this->db->where('products.active', 0);
        $result = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'result' => $result,
                'count' => $count
            );
        } else {
        
        }
        
        
    }
}
