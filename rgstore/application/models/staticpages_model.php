<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class staticpages_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function setPage($pageData = array()) {
        $this->db->insert('static_pages', $pageData);
    }

    public function getPage($conditions = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->select('*');
        $this->db->from('static_pages');
        $this->db->order_by('id', 'desc');
        $result = $this->db->get();
        return $result;
    }

    public function updatePage($pageId, $pageData = array()) {
        $this->db->where('id', $pageId);
        $this->db->update('static_pages', $pageData);
    }

    public function deletePage($id) {
        $this->db->where('static_pages.id', $id);
        $result = $this->db->delete('static_pages');
        return $result;
    }
    
    

}
