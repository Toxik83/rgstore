<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Address_model extends CI_Model {

    public function find_address($conditions = array()){
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        $this->db->select('*');
        $this->db->from('user_details');
        $this->db->join('country', 'user_details.country_id = country.id');

        $query = $this->db->get()->row();
        
        return $query;
    }
    
    public function find_names($id=0){

        $this->db->where('id', $id);
        
        return $this->db->get('users')->row();
    }
}

