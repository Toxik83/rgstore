<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Finishing_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getFinishings($search_term = 'default', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS * , products.id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('category', 'products.f_cat_id = category.id', 'left');
        $this->db->join('manufacturer', 'products.man_id = manufacturer.id', 'left');
        $this->db->group_by('products.id');
        $this->db->like('title', $search_term);
        $this->db->order_by('products.date_added', 'DESC');
        $this->db->where('cat_id',3);
        $this->db->where('products.active',0);
        $this->db->where('products_images.main_pic',1);

        $this->db->limit($limit, $offset);

        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }
    
    public function getFinishingsInactive($search_term = 'default', $offset = 0, $limit = 0){
        $this->db->select('SQL_CALC_FOUND_ROWS * , products.id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('category', 'products.f_cat_id = category.id', 'left');
        $this->db->join('manufacturer', 'products.man_id = manufacturer.id', 'left');
        $this->db->group_by('products.id');
        $this->db->like('title', $search_term);
        $this->db->order_by('products.date_added', 'DESC');
        $this->db->where('cat_id',3);
        $this->db->where('products.active',2);
        $this->db->where('products_images.main_pic',1);

        $this->db->limit($limit, $offset);

        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }

    public function getCatogories($search_term = 'default', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS * ', false);
        $this->db->from('category');
        $this->db->like('cat_name', $search_term);
        $this->db->order_by('order_id', 'ASC');
        $this->db->where('active', 0);

        $this->db->limit($limit, $offset);

        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }

    public function deleteData($slideId) {
        $this->db->set('active', 1);
        $this->db->where('id', $slideId);
        $result = $this->db->update('products');
        return $result;
    }

    public function createCategory($data) {
        $this->db->set('cat_name', $data['name']);
        $this->db->set('date', $data['date']);
        $this->db->insert('category');
    }

    public function createFinishing($data) {
        $this->db->set('title', $data['title']);
        $this->db->set('text', $data['text']);
        $this->db->set('price', $data['price']);
        $this->db->set('new_price', $data['new_price']);
        $this->db->set('f_cat_id', $data['f_cat_id']);
        $this->db->set('man_id', $data['man_id']);
        $this->db->set('date_added', $data['date_added']);
        $this->db->set('cat_id', 3);
        $this->db->insert('products');
    }

    public function Cat_deleteData($slideId) {
        $this->db->set('active', 1);
        $this->db->where('id', $slideId);
        $result = $this->db->update('category');
        return $result;
    }

    public function sources($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        return $this->db->get('category')->row();
    }

    public function sourcesCat() {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('active',0);
        $this->db->order_by('cat_name','ASC');
        $result = $this->db->get()->result();
        return $result;
    }

    public function sourcesFinish($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        return $this->db->get('products')->row();
    }

    public function getCategory($id) {
        $this->db->select('*, category.id ');
        $this->db->from('category');
        $this->db->join('products', 'category.id = products.f_cat_id', 'inner');
        $this->db->where('products.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    public function getManufacturer($id) {
        $this->db->select('*, manufacturer.id ');
        $this->db->from('manufacturer');
        $this->db->join('products', 'manufacturer.id = products.man_id', 'inner');
        $this->db->where('products.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function getAllCat() {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('active',0);
        $this->db->order_by('cat_name','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function check_unique($id, $name) {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('id !=', $id);
        $this->db->where('cat_name', $name);
        $this->db->where('active', 0);
        
        $exist = $this->db->get()->row();

        if ($exist) {
            return true;
        }

        return false;
    }
    public function check_unique_cat($name) {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('cat_name', $name);
        $this->db->where('active', 0);
        
        $exist = $this->db->get()->row();

        if ($exist) {
            return true;
        }

        return false;
    }

    public function check_unique_fin($id, $title) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('id !=', $id);
        $this->db->where('title', $title);
        $this->db->where('active', 0);
        $this->db->where('cat_id', 3);
        $exist = $this->db->get()->row();

        if ($exist) {
            return true;
        }

        return false;
    }

    public function editCategory($data) {
        $this->db->set('cat_name', $data['name']);
        $this->db->set('date', $data['date']);
        $this->db->where('id', $data['id']);

        $this->db->update('category');
    }

    public function editFinishing($data) {
        $this->db->set('title', $data['title']);
        $this->db->set('text', $data['text']);
        $this->db->set('price', $data['price']);
        $this->db->set('new_price', $data['new_price']);
        $this->db->set('f_cat_id', $data['f_cat_id']);
        $this->db->set('man_id', $data['man_id']);
        $this->db->where('id', $data['id']);

        $this->db->update('products');
    }

    public function showFlashDateById($id) {
        $this->db->select('id,fl_date_begin, fl_date_end');
        $this->db->where('id', $id);
        $query = $this->db->get('finishings');

        return $query->result();
    }

    public function setFlashdata($data) {
        $this->db->set('fl_date_begin', $data['fl_date_begin']);
        $this->db->set('fl_date_end', $data['fl_date_end']);
        $this->db->where('id', $data['id']);
        $this->db->update('products');
    }

    public function flashSource($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        return $this->db->get('products')->row();
    }

    public function deleteFlashData($id) {
        $this->db->set('fl_date_begin', NULL);
        $this->db->set('fl_date_end', NULL);
        $this->db->where('id', $id);
        $result = $this->db->update('products');
        return $result;
    }

    public function reorder($orderId, $slideId) {
        $fields = array(
            'order_id' => $orderId,
        );
        $this->db->where('id', $slideId);
        $this->db->update('category', $fields);
    }
    
    public function find($id){
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('products.id', $id);
        $this->db->where('cat_id',3);
        $this->db->where('main_pic',1);
        $this->db->join('products_images', 'products.id = products_images.product_id','LEFT');
        $this->db->where('products_images.main_pic',1);
        
        return $this->db->get()->row();
    }
    
    public function get_finishing(){
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('cat_id',3);
        $this->db->where('active',0);
        
        $query = $this->db->get();
        return $query->result();
    }
    
     public function check_unique3($title) {
        $this->db->select('title,active,cat_id');
        $this->db->from('products');
        $this->db->where('title', $title);
        $this->db->where('active', 0);
        $this->db->where('cat_id', 3);
        
        $exist = $this->db->get()->row();

        if ($exist) {
            return true;
        }

        return false;
    }
    
    public function newPriceRow($id){
        $this->db->select('new_price');
        $this->db->from('products');
        $this->db->where('id',$id);
        
        $query = $this->db->get();
        return $query->row();
    }
    
    public function setNewPrice($id,$newPrice){
        $this->db->set('new_price',$newPrice);
        $this->db->where('id',$id);
        $this->db->update('products');
    }
    
    public function makeInactive($id){
        $this->db->set('active', 2);
        $this->db->where('id', $id);
        $result = $this->db->update('products');
        return $result;
    }
    
    public function makeActive($id){
        $this->db->set('active', 0);
        $this->db->where('id', $id);
        $result = $this->db->update('products');
        return $result;
    }
     public function countRecords($catid){
        $this->db->select('SQL_CALC_FOUND_ROWS *', false );
        $this->db->from('products');
        $this->db->join('category', 'products.f_cat_id = category.id');
        $this->db->where('category.id', $catid);
        $this->db->where('products.active', 0);
        $result = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
      
        if ($count > 0) {
            return array(
                'result' => $result,
                'count' => $count
            );
        } else {
        
        }
        
        
    }

}
