<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manage_admin_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAdmins($search_term, $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->from('users');
        if ($search_term == true) {
            $this->db->like('username', $search_term);
            $this->db->or_like('fname', $search_term);
        }
        $this->db->order_by('registered_at', 'DESC');
        $this->db->limit($limit, $offset);
        $this->db->where('is_admin', 1);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;

        return array('data' => $data, 'count' => $count);
    }

    public function deleteData($slideId) {
        $this->db->where('id', $slideId);
        $result = $this->db->delete('users');
        return $result;
    }

    public function createAdmin($data){
        $this->db->set('username',$data['username']);
        $this->db->set('fname',$data['name']);
        $this->db->set('password',$data['password']);
        $this->db->set('registered_at',$data['date']);      
        $this->db->set('is_admin',1);      
        $this->db->set('online',0);      
        $this->db->insert('users');
    }
    
    public function sources($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->where('is_admin', 1);
        return $this->db->get('users')->row();
    }
    
    public function editAdmin($data){
        $this->db->set('username',$data['username']);
        $this->db->set('fname',$data['name']);
        $this->db->set('password',$data['password']);
        $this->db->where('id',$data['id']);
        $this->db->update('users');
    }
    
    public function editAdminWithoutPass($data){
        $this->db->set('username',$data['username']);
        $this->db->set('fname',$data['name']);
        $this->db->where('id',$data['id']);
        $this->db->update('users');
    }
    
    public function check_unique($id, $username) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id !=', $id);
        $this->db->where('username', $username);
        $exist = $this->db->get()->row();

        if ($exist) {
            return true;
        }

        return false;
    }
            

}