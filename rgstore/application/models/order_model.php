<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function setOrder($orderData = array())
    {
        $this->db->insert('orders', $orderData);
    }

    function insert_products($data) {
        $this->db->insert('products_ordered', $data);
    }

    public function getOrderData($conditions = array(), $fields = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        if (count($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }
        $this->db->from('orders AS o');
        $this->db->join('products_ordered AS po', 'o.order_id = po.order_id', 'left');
        $this->db->join('products AS p', 'po.product_id = p.id', 'left');
        $this->db->order_by('o.order_id', 'desc');
        $result = $this->db->get();
        return $result;
    }

    public function getOrders($conditions = array(), $limit = array(), $like = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        $this->db->select('*');
        $this->db->from('orders');

        //search for terms
        if (!empty($like)) {
            $this->db->like('email', $like);
        }
        
        //Check For Limit
        if (is_array($limit)) {
            if (count($limit) == 1)
                $this->db->limit($limit[0]);
            else if (count($limit) == 2)
                $this->db->limit($limit[0], $limit[1]);
        }
        $this->db->order_by('order_id', 'desc');
        
        $result = $this->db->get();
        return $result;
    }

//    public function getOrderData($orderId)
//    {
//        $sql = 'SELECT o.fname, o.lname, o.email, o.address, o.city, o.country_name, o.zip_code, o.phone, o.status, 
//                       po.quantity, po.price, po.total_price, po.date_added, po.product_id,
//                       p.title
//                FROM orders AS o
//                LEFT JOIN products_ordered AS po ON o.order_id = po.order_id
//                LEFT JOIN products AS p ON po.product_id = p.id
//                WHERE po.order_id = ?';
//        
//        $result = $this->db->query($sql, array($orderId));
//        
//        return $result;
//    }

    public function changeStatus($orderId, $status = array()) {
        $this->db->where('orders.order_id', $orderId);
        $this->db->update('orders', $status);
    }

    public function getBestSellers($conditions = array(), $fields = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        if (count($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }
        $this->db->from('orders AS o');
        $this->db->join('products_ordered AS po', 'o.order_id = po.order_id', 'left');
        $this->db->join('products AS p', 'po.product_id = p.id', 'left');
        $this->db->join('products_images AS pi', 'p.id = pi.id', 'left');
        $where = 'o.date_added >= DATE_SUB( CURDATE( ) , INTERVAL 30 DAY )';
        $this->db->where($where);
        $this->db->group_by('po.id');
        $result = $this->db->get()->result();
        return $result;
    }

    public function getFilamentCount() {
        $this->db->select('products.id');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('products_ordered', 'products.id = products_ordered.product_id', 'left');
        $this->db->join('orders', 'products_ordered.order_id = orders.order_id', 'left');
        $this->db->where('active', 0);
        $this->db->where('cat_id', 2);
        $this->db->where('products_images.main_pic', 1);
        $this->db->where('orders.date_added BETWEEN SYSDATE() - INTERVAL 30 DAY AND SYSDATE()');
        //$this->db->where('sum(products_ordered.quantity)>= ',3);
        //$this->db->order_by('products.id', 'DESC');
        $this->db->group_by('products.id ');
        $data = $this->db->get()->result_array();

        return $data;
    }

    public function bestSellers4() {
        $sql = "SELECT *,sum(products_ordered.quantity) as total
    FROM `orders`
    JOIN products_ordered ON orders.order_id = products_ordered.order_id
    JOIN products ON products_ordered.product_id = products.id
    JOIN products_images ON products.id = products_images.product_id
    WHERE year(orders.date_added) = YEAR( CURRENT_DATE - INTERVAL 1 MONTH)
    AND Month(orders.date_added) = Month( CURRENT_DATE - INTERVAL 1 MONTH)
    AND products_images.main_pic = 1 
    GROUP BY products.id
    HAVING total > 2
    LIMIT 4";
                
        $result = $this->db->query($sql, array());
        return $result;
    }

    public function bestSellers($offset = 0, $limit = 12) {
        $sql = "SELECT SQL_CALC_FOUND_ROWS 
            products.id, products.title, products.price, products.new_price, products.text,products.cat_id,
            products_images.name,
            products_ordered.quantity 
            ,sum(products_ordered.quantity) as total
    FROM `orders`
    JOIN products_ordered ON orders.order_id = products_ordered.order_id
    JOIN products ON products_ordered.product_id = products.id
    JOIN products_images ON products.id = products_images.product_id
    WHERE year(orders.date_added) = YEAR( CURRENT_DATE - INTERVAL 1 MONTH)
    AND Month(orders.date_added) = Month( CURRENT_DATE - INTERVAL 1 MONTH)
    AND products_images.main_pic = 1 
    GROUP BY products.id
    HAVING total > 2
    LIMIT $limit
    OFFSET $offset 
    ";
        
        $result = $this->db->query($sql, array())->result_array();
        
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        
        if ($count > 0) {
            return array(
                'data' => $result,
                'count' => $count
            );
        } else {
            
        }
    }

    public function getProductsOrdered($conditions = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        $this->db->select('*');
        $this->db->from('products_ordered as po');
        $this->db->join('products as p', 'po.product_id = p.id', 'left');
        $result = $this->db->get();
        return $result;
    }

    public function bestSellersCount() {
        $this->db->select('products.id');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('products_ordered', 'products.id = products_ordered.product_id', 'left');
        $this->db->join('orders', 'products_ordered.order_id = orders.order_id', 'left');
        $this->db->where('active', 0);
        $this->db->where('products_images.main_pic', 1);
        $this->db->having('sum(products_ordered.quantity) >= ', 2);
        $this->db->where('EXTRACT( YEAR_MONTH FROM orders.date_added ) = EXTRACT( YEAR_MONTH FROM CURDATE( ) - INTERVAL 1 MONTH ) ');
        $this->db->group_by('products.id ');
        $data = $this->db->get()->result_array();

        return $data;
    }

     public function allBestSellers($offset = 0,$limit = 0) {
        $this->db->select('products.title,products');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('products_ordered', 'products.id = products_ordered.product_id', 'left');
        $this->db->join('orders', 'products_ordered.order_id = orders.order_id', 'left');
        $this->db->where('products_images.main_pic', 1);
        $this->db->where('products.active', 0);
        $this->db->where('products.sold >',2);
        $this->db->where('EXTRACT( YEAR_MONTH FROM orders.date_added ) = EXTRACT( YEAR_MONTH FROM CURDATE() - INTERVAL 1 MONTH )');
          $this->db->limit($limit, $offset);
        $this->db->group_by('products.id ');
       $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
       $data = $this->db->get()->result_array();
       echo '<pre>';
       print_r($data);
       echo '</pre>';
       die();
        return array(
            'data' => $data,
            'count' => $count
        );
    }

}
