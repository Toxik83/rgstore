<?php
class Printers_m extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function showPrintersById($id, $fields = array() ) {
        if (count($fields)>0) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }
        $this->db->get('products');
        $this->db->where('id', $id);
        $this->db->where('cat_id', 1);
        $query = $this->db->get('products');
        return $query->row();
    }
    //make inactive
    public function delete($id) {
        $this->db->set('active', 1);
        $this->db->where('id', $id);
        $this->db->update('products');
    }

    public function find($id) {
        $this->db->select('*');
        $this->db->from('products as pr');
        $this->db->where('pr.id', $id);
        $this->db->where('pi.main_pic', 1);
        $this->db->join('products_images as pi', 'pr.id = pi.product_id', 'LEFT');
        return $this->db->get()->row();
    }

    public function insert($printer = array()) {
        $this->db->insert('products', $printer);
    }

    public function insertFlDate($id, $new_price, $fl_date_begin, $fl_date_end, $date_update ) {
        $printer = array(
            'new_price' => $new_price,
            'fl_date_begin' => $fl_date_begin,
            'fl_date_end' => $fl_date_end,           
            'date_update' => $date_update
        );
        $this->db->where('id', $id);
        $this->db->update('products', $printer);
    }

    public function showFlashDateById($id) {
        $this->db->select('id,fl_date_begin, fl_date_end, active');
        $this->db->where('id', $id);
        $query = $this->db->get('products');
        return $query->row();
    }

    public function update($id, $printer = array()) {
        $this->db->where('id', $id);
        $this->db->update('products', $printer);
    }

    //Pagination for admin $search
    public function get_results($search_term = '', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *, products.id', false);
        $this->db->from('products');
        $this->db->join('manufacturer', 'manufacturer.id = products.man_id');
        $this->db->join('products_images', 'products_images.product_id = products.id ', 'left');
        $this->db->join('print_tech', 'print_tech.id = products.print_id');
        $this->db->join('building_size', 'building_size.id = products.build_id');
        $this->db->join('accuracy', 'accuracy.id = products.acc_id');
        $this->db->where('cat_id', 1);
        $this->db->order_by('products.id', 'DESC');
        $this->db->group_by('products.id ');
        $this->db->where('products_images.main_pic', 1);
        $this->db->where('products.active', 0);
        $this->db->like('title', $search_term);
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'data' => $data,
                'count' => $count
            );
        } else {
            
        }
    }

    //for front home page -printlisting, pagination
    public function printersList($offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *, products.id,products_images.order_id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->where('active', 0);
        $this->db->where('cat_id', 1);
        $this->db->where('products_images.main_pic', 1);
        $this->db->order_by('products.id', 'DESC');
        $this->db->group_by('products.id ');
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result_array();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        return array(
            'data' => $data,
            'count' => $count
        );
    }

    public function printersListFlash() {
        $this->db->select('SQL_CALC_FOUND_ROWS *, products.id,products_images.order_id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->where('active', 0);
        $this->db->order_by('products.id', 'DESC');
        $this->db->group_by('products.id ');
        $data = $this->db->get()->result();
        return $data;
    }

    public function resetNewPrice($productId, $data = array()) {
        $this->db->where('id', $productId);
        $this->db->update('products', $data);
    }
    
    //for ptinters details / front
    public function printersById($printer_id) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->where('products.id', $printer_id);
        $this->db->order_by('products_images.order_id', 'ASC');
        $data = $this->db->get();
        return $data->result();
    }

    public function deleteFlDate($id) {
        $this->db->set('fl_date_begin', '0000-00-00 00:00:00');
        $this->db->set('fl_date_end', '0000-00-00 00:00:00');
        $this->db->set('new_price', 0);
        $this->db->set('date_update', date('Y-m-d H:i'));
        $this->db->where('id', $id);
        $this->db->update('products');
    }

    //for fast Price Edit
    public function editPromo($id, $new_price) {
        $this->db->set('new_price', $new_price);
        $this->db->set('date_update', date('Y-m-d H:i'));
        $this->db->where('id', $id);
        $query = $this->db->update('products');
    }

    //Pagination for admin $search
    public function get_resultsInactive($search_term = '', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *, products.id', false);
        $this->db->from('products');
        $this->db->join('manufacturer', 'manufacturer.id = products.man_id');
        $this->db->join('products_images', 'products_images.product_id = products.id ', 'left');
        $this->db->join('print_tech', 'print_tech.id = products.print_id');
        $this->db->join('building_size', 'building_size.id = products.build_id');
        $this->db->join('accuracy', 'accuracy.id = products.acc_id');
        $this->db->where('cat_id', 1);
        $this->db->order_by('products.id', 'DESC');
        $this->db->group_by('products.id ');
        $this->db->where('products_images.main_pic', 1);
        $this->db->where('products.active', 1);
        $this->db->like('title', $search_term);
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'data' => $data,
                'count' => $count
            );
        } else {
            
        }
    }

    public function makeActive($id) {
        $this->db->set('active', 0);
        $this->db->set('date_update', date('Y-m-d H:i'));
        $this->db->where('id', $id);
        $this->db->update('products');
    }
    //Soft Delete and Bulk Delete
    public function del($id) {
        $this->db->set('active', 2);
        $this->db->where('id', $id);
        $this->db->update('products');
    }

    function findNewPrice($id) {
        $this->db->select('id,new_price, price, title');
        $this->db->where('id', $id);
        $query = $this->db->get('products');
        return $query->result_array();
    }

    //for front
//    public function getFlashdate() {
//        $this->db->select('fl_date_begin,fl_date_end, title, price, new_price');
//        $this->db->from('products');
//        $this->db->where('cat_id', 1);
//        $query = $this->db->get();
//        return $query->result();
//    }

//    public function updateAjaxPrice($id, $price) {
//        $this->db->set('new_price', 0);
//        $this->db->where('id', $id);
//        $this->db->update('products');
//    }

    //for home page
    public function getHotDeals($currentTime) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->where('active', 0);
        $this->db->where('main_pic', 1);
        $this->db->where('products.fl_date_begin <=', $currentTime);
        $this->db->where('products.fl_date_begin !=', '0000-00-00 00:00:00');
        $this->db->order_by('products.id', 'random');
        $this->db->group_by('products.id ');
        $this->db->limit(4);
        $data = $this->db->get()->result();
        return $data;
    }

    public function getAllHotDeals($currentTime, $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *,products.id ', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->where('active', 0);
        $this->db->where('main_pic', 1);
        $this->db->where('products.fl_date_begin <=', $currentTime);
        $this->db->where('new_price !=', '0.00');
        $this->db->order_by('date_update', 'random');
        $this->db->group_by('products.id ');
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result_array();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        return array(
            'data' => $data,
            'count' => $count
        );
    }

    public function getSearchResult($currentTime, $limit = array(), $flag, $option1, $option2) {
        $this->db->select('* , products.id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('category', 'products.f_cat_id = category.id', 'left');
        $this->db->group_by('products.id');
        $this->db->where('products.active', 0);
        if ($flag == 1) {
            $this->db->where('products.sold >', '0');
        } else {
            $this->db->where('products.fl_date_begin <=', $currentTime);
            $this->db->where('products.new_price !=', '0.00');
        }

        $this->db->where('products_images.main_pic', 1);
        if($option1 =='price'){
        $option1 = '(case when new_price != 0 then new_price else price end) ';
        $this->db->order_by($option1, $option2);
        } else {
        $this->db->order_by($option1, $option2);
        }
        $this->db->order_by($option1, $option2);

        //Check For Limit
        if (is_array($limit)) {
            if (count($limit) == 1)
                $this->db->limit($limit[0]);
            else if (count($limit) == 2)
                $this->db->limit($limit[0], $limit[1]);
        }

        $result = $this->db->get();
        return $result;
    }

    // for best sellers
    public function getPrintersCount() {
        $this->db->select('products.id');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('products_ordered', 'products.id = products_ordered.product_id', 'left');
        $this->db->join('orders', 'products_ordered.order_id = orders.order_id', 'left');
        $this->db->where('active', 0);
        $this->db->where('cat_id', 1);
        $this->db->where('products_images.main_pic', 1);
        $this->db->having('sum(products_ordered.quantity) >= ', 2);
        $this->db->where('EXTRACT( YEAR_MONTH FROM orders.date_added ) = EXTRACT( YEAR_MONTH FROM CURDATE( ) - INTERVAL 1 MONTH ) ');
        $this->db->group_by('products.id ');
        $data = $this->db->get()->result_array();
        return $data;
    }
    //For Last Seen
    public function getLastSeen ($id) {        
            $this->db->select('products.id, products.title, products.price, products.new_price, products_images.name, products.cat_id');
            $this->db->from('products');
            $this->db->join('products_images ', 'products.id = products_images.product_id');
            $this->db->where('products.id', $id);
            $this->db->where('products_images.main_pic', 1);
            $result = $this->db->get()->result_array();
            return $result;
        }

}
