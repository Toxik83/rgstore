<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Price_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getPrices($offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->from('price_ranges');
        $this->db->order_by('order_id', 'ASC');
        $this->db->where('active', 0);
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }

    public function getWhatYouNeed() {
        $this->db->select('*');
        $this->db->from('price_ranges');
        $this->db->where('active', 0);
        $query = $this->db->get();
        return $query->result();
    }

    public function createRange($data = array()) {
        $this->db->set('min', $data['min']);
        $this->db->set('max', $data['max']);
        $this->db->set('date', $data['date']);
        $this->db->insert('price_ranges');
    }

    public function reorder($orderId, $slideId) {
        $fields = array(
            'order_id' => $orderId,
        );
        $this->db->where('id', $slideId);
        $this->db->update('price_ranges', $fields);
    }

    public function deleteData($slideId) {
        $this->db->set('active', 1);
        $this->db->where('id', $slideId);
        $result = $this->db->update('price_ranges');
        return $result;
    }

    public function check_unique($min) {
        $this->db->select('*');
        $this->db->from('price_ranges');
        $this->db->where('min', $min);
        $this->db->where('active', 0);
        $exist = $this->db->get()->row();
        if ($exist) {
            return true;
        }
        return false;
    }

    public function check_unique2($max) {
        $this->db->select('*');
        $this->db->from('price_ranges');
        $this->db->where('max', $max);
        $this->db->where('active', 0);
        $exist = $this->db->get()->row();
        if ($exist) {
            return true;
        }
        return false;
    }

    public function check_uniqueDub($id, $min) {
        $this->db->select('*');
        $this->db->from('price_ranges');
        $this->db->where('id !=', $id);
        $this->db->where('min', $min);
        $this->db->where('active', 0);
        $exist = $this->db->get()->row();
        if ($exist) {
            return true;
        }
        return false;
    }

    public function check_uniqueDub2($id, $max) {
        $this->db->select('*');
        $this->db->from('price_ranges');
        $this->db->where('max', $max);
        $this->db->where('id !=', $id);
        $this->db->where('active', 0);
        $exist = $this->db->get()->row();
        if ($exist) {
            return true;
        }
        return false;
    }

    public function sources($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        return $this->db->get('price_ranges')->row();
    }

    public function editRange($data) {
        $this->db->set('min', $data['min']);
        $this->db->set('max', $data['max']);
        $this->db->set('date', $data['date']);
        $this->db->where('id', $data['id']);
        $this->db->update('price_ranges');
    }

    public function getSidebarPrices() {
        $this->db->select('*');
        $this->db->from('price_ranges');
        $this->db->where('active', 0);
        $this->db->order_by('order_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    
    public function countRecords($minPrice, $maxPrice){
        $this->db->select('SQL_CALC_FOUND_ROWS *', false );
        $this->db->from('products');
        $this->db->where('price >=', $minPrice);
        $this->db->where('price <=', $maxPrice);
        $this->db->where('active', 0);
        $result = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'result' => $result,
                'count' => $count
            );

        } else {
        
        }
        
        
    }
    
    public function getPricesById($id) {
        $this->db->select('*');
        $this->db->from('price_ranges');
        $this->db->where('id', $id);
        $this->db->where('active', 0);
        $query = $this->db->get();
        return $query->result();
        ;
    }
}
