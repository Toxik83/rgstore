<?php

class Printersgallery_Model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function getalldata() {
        $this->db->select('*');
        $this->db->order_by('order_id', 'ASC');
        $this->db->from('products_images');
        return $this->db->get()->result();
    }

    public function save($url, $printer_id) {
        $this->db->set('name', $url);
        $this->db->set('product_id', $printer_id);
        $this->db->insert('products_images');
    }

    public function saveMainPic($file, $printer_id, $main_pic) {
        $this->db->set('name', $file);
        $this->db->set('main_pic', $main_pic);
        $this->db->set('product_id', $printer_id);
        $this->db->insert('products_images');
    }

    public function showImageByPrinterId($printer_id) {
        $this->db->select('*,products_images.product_id,products_images.id ');
        $this->db->from('products_images');
        $this->db->join('products', 'products_images.product_id = products.id', 'left');
        $this->db->where('products_images.product_id ', $printer_id);
        $this->db->order_by('products_images.order_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->where('main_pic', 0);
        $this->db->delete('products_images');
    }

    public function chekForMP($id, $product_id) {
        $this->db->select('id');
        $this->db->from('products_images');
        $this->db->where('product_id', $product_id);
        $this->db->where('main_pic', 1);
        $query = $this->db->get();
        return $query->row();
    }

    public function find($id) {
        $this->db->where('id', $id);
        return $this->db->get('products_images')->row();
    }

    //for front page
    public function showfirstimage() {
        $this->db->select('*');
        $this->db->from('products_images');
        $this->db->join('products', 'products_images.product_id = products.id', 'left');
        $this->db->order_by('products_images.order_id', 'ASC');
        $this->db->group_by('products.id ');
        $query = $this->db->get();
        return $query->result();
    }

    public function update($id, $url, $printer_id) {
        $this->db->set('name', $url);
        $this->db->set('product_id', $printer_id);
        $this->db->where('id', $id);
        $this->db->update('products_images');
    }

    //for front page
    public function showgallerybyId() {
        $this->db->select('products_images.name, products_images.product_id, products_images.main_pic, products_images.order_id, products_images.id');
        $this->db->from('products_images');
        $this->db->join('products', 'products_images.product_id = products.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    //for front page
    public function showfirstimagebyId($printer_id) {
        $this->db->select('products_images.name, products_images.product_id');
        $this->db->from('products_images');
        $this->db->join('products', 'products_images.product_id = products.id', 'left');
        $this->db->where('products.id ', $printer_id);
        $this->db->ordered_by('printers_images.order_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function reorder($order_id, $id) {
        $sort_order = array(
            'order_id' => $order_id,
        );
        $this->db->where('id', $id);
        $this->db->update('products_images', $sort_order);
    }

    public function mainPic($id) {
        $this->db->set('main_pic', 1);
        $this->db->set('order_id', 0);
        $this->db->where('id', $id);
        $this->db->update('products_images');
    }

    public function unMakemainPic($id, $product_id) {
        $this->db->set('main_pic', 0);
        $this->db->set('order_id', 1);
        $this->db->where('id !=', $id);
        $this->db->where('product_id', $product_id);
        $this->db->update('products_images');
    }

    public function getFirstPic($printer_id) {
        $this->db->select('*');
        $this->db->from('products_images');
        $this->db->where('product_id', $printer_id);
        return $this->db->get()->row();
    }
    
    public function getMaxId() {
        $this->db->select_max('id');
        $result = $this->db->get('products_images');
        return $result->row();
    }

}
