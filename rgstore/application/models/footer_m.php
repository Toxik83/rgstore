<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Footer_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function news($data) {
        $this->db->set('email', $data['email']);
        $this->db->set('date', $data['date']);
        $this->db->insert('newsletter');
    }

    public function getEmails($conditions = array('')) {

        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        $this->db->select('*');
        $this->db->from('newsletter');
        $result = $this->db->get();
        return $result;
    }

}
