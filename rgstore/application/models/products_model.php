<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class products_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function setProduct($bundleData = array()) {
        $this->db->insert('products', $bundleData);
    }

    public function setBundle($bundleData = array()) {
        $this->db->insert('bundle', $bundleData);
    }

    public function setBundleItems($bundleItems = array()) {
        $this->db->insert('bundle_items', $bundleItems);
    }

    public function getBundle($conditions = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->select('*');
        $this->db->from('products as p');
        $this->db->join('bundle as b', 'p.id = b.product_id', 'left');
        $result = $this->db->get();
        return $result;
    }

    public function getBundleData($conditions = array(), $fields = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        if (count($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }

        $this->db->from('bundle as b');
        $this->db->join('bundle_items as bi', 'b.product_id = bi.bundle_id', 'left');
        $this->db->join('products as p', 'bi.product_id = p.id', 'left');
        $this->db->join('products_images as pi', 'p.id = pi.product_id', 'left');
        $result = $this->db->get();
        return $result;
    }

    public function getRelatedBundleData($conditions = array(), $fields = array(), $start, $limit) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->limit($limit, $start);

        if (count($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }

        $this->db->from('bundle_items as bi');
        $this->db->join('bundle as b', 'bi.bundle_id = b.product_id', 'left');
        $this->db->join('products as p', 'b.product_id = p.id', 'left');
        $this->db->order_by('b.product_id', 'RANDOM');
        $result = $this->db->get();
        return $result;
    }

    public function getRandomBundle($conditions = array(), $start, $limit) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->limit($limit, $start);
        $this->db->select('*');
        $this->db->from('bundle');
        $this->db->join('products', 'bundle.product_id = products.id', 'left');
        $this->db->order_by('bundle.id', 'RANDOM');
        $result = $this->db->get();

        return $result;
    }

    public function getProductsExceptBundles($conditions = array()) {
        if (count($conditions) > 0) {
            $this->db->where_in('cat_id', $conditions);
        }

        $this->db->select('*');
        $this->db->from('products');
        $result = $this->db->get();
        return $result;
    }

    public function getProducts($conditions = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->select('*');
        $this->db->from('products ');
        $result = $this->db->get();
        return $result;
    }

    public function getProduct($conditions = array(), $fields = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        if (count($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }

        $this->db->from('products as p');
        $this->db->join('products_images as i', 'p.id = i.product_id', 'left');
        $result = $this->db->get();
        return $result;
    }

    public function getRelatedProducts($cat_id, $id) {
        $whereArray = array('p.cat_id' => $cat_id, 'p.id !=' => $id, 'p.active' => 0);
        $this->db->select(
                          'p.id,
                           pi.name,
                           pi.product_id,
                           p.fl_date_begin,
                           p.fl_date_end,
                           p.new_price,
                           p.title,
                           p.price,
                           p.active'
        );

        $this->db->from('products p');
        $this->db->join('products_images pi', 'p.id = pi.product_id');
        $this->db->order_by('id', 'RANDOM');
        $this->db->where($whereArray);
        $this->db->where('pi.main_pic', 1);
        $this->db->limit(4);
        $result = $this->db->get();
        
        return $result->result();
    }

    public function updateBundle($bundleData = array(), $bundleId) {
        $this->db->where('bundle.product_id', $bundleId);
        $this->db->update('bundle', $bundleData);
    }

    public function updateProduct($productData = array(), $productId) {
        $this->db->where('products.id', $productId);
        $this->db->update('products', $productData);
    }

    public function isProductInBundle($conditions = array(), $fields = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        if (count($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }

        $this->db->from('products as p');
        $this->db->join('bundle as b', 'p.id = b.product_id', 'left');
        $this->db->join('bundle_items as bi', 'b.product_id = bi.bundle_id', 'left');
        $result = $this->db->get();
        return $result;
    }

    public function deleteBundleItems($bundleId) {
        $this->db->where('bundle_items.bundle_id', $bundleId);
        $this->db->delete('bundle_items');
    }
}
