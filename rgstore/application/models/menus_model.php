<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class menus_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function setMenu($menuData = array()) {
        $this->db->insert('menus', $menuData);
    }

    public function setItem($menu_data = array()) {
        $this->db->insert('menu_items', $menu_data);
    }

    public function getMenuList($conditions = array()) {
        if (count($conditions) > 0) {
        $this->db->where($conditions);
        }
        $this->db->select('*');
        $this->db->from('menus');
        $result = $this->db->get();
        return $result;
    }

    public function getMenuItems($conditions = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        $this->db->select('*');
        $this->db->from('menus as m');
        $this->db->join('menu_items as mi', 'm.menus_id = mi.menu_id', 'left');
        $this->db->order_by('mi.order_id', 'asc');
        $result = $this->db->get();
        return $result;
    }

    public function getMenuHelper($conditions = array()) {
        $this->db->select('*');
        $this->db->from('menus as m');
        $this->db->join('menu_items as mi', 'm.menus_id = mi.menu_id');

        if (array_key_exists('keywords', $conditions)) {
            $this->db->where_in('keyword', $conditions['keywords']);
        }
        $this->db->order_by('mi.order_id', 'asc');
        $result = $this->db->get();
        return $result;
    }

    public function getMaxId() {
        $this->db->select_max('item_id');
        $result = $this->db->get('menu_items');
        return $result->row();
    }

    public function updateMenu($id, $contentData = array()) {
        $this->db->where('menus_id', $id);
        $this->db->update('menus', $contentData);
    }

    public function updateItem($itemId, $itemData = array()) {
        $this->db->where('item_id', $itemId);
        $this->db->update('menu_items', $itemData);
    }

    public function deleteItem($itemId) {
        $this->db->where('menu_items.item_id', $itemId);
        $result = $this->db->delete('menu_items');
        return $result;
    }

    public function deleteMenu($menuId) {
        $sql = "DELETE menus, menu_items
                FROM menus
                LEFT JOIN menu_items
                      ON menus.menus_id = menu_items.menu_id 
                WHERE menus.menus_id = ?";

        $result = $this->db->query($sql, array($menuId));
        return $result;
    }

    public function reorder($orderId, $itemId) {
        $fields = array(
            'order_id' => $orderId,
        );

        $this->db->where('menu_items.item_id', $itemId);
        $this->db->update('menu_items', $fields);
    }

}
