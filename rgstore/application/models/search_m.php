<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getPrinters($search_term = 'default', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->from('printers');
        $this->db->where('active', 0);
        $this->db->like('title', $search_term);
        $this->db->like('text', $search_term);
        $this->db->order_by('date_added', 'DESC');
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return $data;
    }
    
    public function getSearchResult($cat, $cat_sidebar, $limit = array(), $like = array(), $option1, $option2, $price_min, $price_max, $man, $tech, $size, $acc) {
        $this->db->select('* , products.id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('category', 'products.f_cat_id = category.id', 'left');
        $this->db->group_by('products.id');
        $this->db->where('cat_id', $cat);
        $this->db->where('products.active', 0);
        $this->db->where('products_images.main_pic', 1);
        if($option1 =='price'){
        $option1 = '(case when new_price != 0 then new_price else price end) ';
        $this->db->order_by($option1, $option2);
        } else {
        $this->db->order_by($option1, $option2);
        }

        if ($cat_sidebar != null) {
            $this->db->where('category.id', $cat_sidebar);
        }
        $this->db->where('products.active', 0);
        if ($price_max != null) {
            $pr = '(case when new_price = 0 then price else new_price end) ';
            $this->db->where($pr .'>= ', $price_min);
            $this->db->where($pr .' < ', $price_max);
        }
        if ($man != null) {
            $this->db->where('man_id', $man);
        }
        if ($tech != null) {
            $this->db->where('print_id', $tech);
        }
        if ($size != null) {
            $this->db->where('build_id', $size);
        }
        if ($acc != null) {
            $this->db->where('acc_id', $acc);
        }

        if (!empty($like)) {
            foreach ($like as $key => $value) {
                $this->db->like($key, $value);
            }
        }

        //Check For Limit
        if (is_array($limit)) {
            if (count($limit) == 1)
                $this->db->limit($limit[0]);
            else if (count($limit) == 2)
                $this->db->limit($limit[0], $limit[1]);
        }

        $result = $this->db->get();
        return $result;
    }
    
    public function getPrices() {
        $this->db->select('price,new_price');
        $this->db->from('products');
        $this->db->where('active', 0);
        $query = $this->db->get();
        return $query->result();
    }

    public function getQuantityForSearch($cat) {
        // for best sellers
        $this->db->select('products.id');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('products_ordered', 'products.id = products_ordered.product_id', 'left');
        $this->db->join('orders', 'products_ordered.order_id = orders.order_id', 'left');
        $this->db->where('active', 0);
        $this->db->where('cat_id', $cat);
        $this->db->where('products_images.main_pic', 1);
        $this->db->having('sum(products_ordered.quantity) >= ',2);
        $this->db->where('EXTRACT( YEAR_MONTH FROM orders.date_added ) = EXTRACT( YEAR_MONTH FROM CURDATE( ) - INTERVAL 1 MONTH ) ');        

        $this->db->group_by('products.id ');
        $data = $this->db->get()->result_array();
        return $data;
    }
    
    public function getSearchResult2($option,$option2,$limit=array()) {
        $this->db->select('products.id, products.title, products.price, products.new_price,products.sold,products.active,products.cat_id,'
                . 'products_images.name,products_images.product_id,'
                . 'products_ordered.product_id,products_ordered.order_id, products_images.main_pic,'
                . 'orders.order_id,orders.date_added');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('products_ordered', 'products.id = products_ordered.product_id', 'left');
        $this->db->join('orders', 'products_ordered.order_id = orders.order_id', 'left');
        $this->db->where('products_images.main_pic', 1);
        $this->db->where('products.active', 0);
        $this->db->where('products.sold >= ', 2);
        $this->db->where('EXTRACT( YEAR_MONTH FROM orders.date_added ) = EXTRACT( YEAR_MONTH FROM CURDATE( ) - INTERVAL 1 MONTH ) ');
        $this->db->group_by('products.id ');
        if($option =='price'){
        $option = '(case when products.new_price != 0 then products.new_price else products.price end) ';
        $this->db->order_by($option, $option2);
        } else {
        $this->db->order_by($option, $option2);
        }
         if ($option == 'title' && $option2 == 'ASC') {

            $this->db->order_by('products.title', 'asc');
        }
        if ($option == 'title' && $option2 == 'DESC') {

            $this->db->order_by($option, $option2);
        }
        if ($option == 'price' && $option2 == 'ASC') {
            
            $this->db->order_by('products.price', 'asc');
        }
        if ($option == 'price' && $option2 == 'DESC') {

            $this->db->order_by('products.price', 'desc');
        }
         if (is_array($limit)) {
            if (count($limit) == 1){
                
                $this->db->limit($limit[0]);
            }
            else if (count($limit) == 2){
              
                $this->db->limit($limit[0], $limit[1]);
            }
        }


        // Check For Limit


        $result = $this->db->get();
        
        return $result;
    }
}
