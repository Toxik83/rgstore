<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accuracy_model extends CI_Model {
    
    public function show_Admin_Accuracy($keyword = null, $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS * ', false);
        $this->db->from('accuracy');
        $this->db->where('active', 0);
        $this->db->like('accuracy_name',$keyword);
        $this->db->order_by('order_id');
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;        
        return array('data' => $data, 'count' => $count);
    }
    
    public function show_Accuracy() {
        $this->db->select('*');
        $this->db->from('accuracy');
        $this->db->where('active', 0);
        $this->db->order_by('order_id');
        return $this->db->get()->result();
    }

    public function create($name) {
        $size = array(
            'accuracy_name' => $name
        );
        $this->db->insert('accuracy', $size);
    }

    public function delete($id) {
        $this->db->set('active', 1);
        $this->db->where('id', $id);
        $this->db->update('accuracy');
    }

    public function find($id) {
        $this->db->where('id', $id);
        return $this->db->get('accuracy')->row();
    }

    public function update($id, $name) {
        $this->db->set('accuracy_name', $name);
        $this->db->where('id', $id);
        $this->db->update('accuracy');
    }

    public function check_unique($name) {
        $this->db->select('*');
        $this->db->from('accuracy');
        $this->db->where('accuracy_name', $name);
        $this->db->where('active', 0);
        $exist = $this->db->get()->row();
        if ($exist) {
            return true;
        }
        return false;
    }

    public function reorder($order_id, $id) {
        $sort_order = array(
            'order_id' => $order_id,
        );
        $this->db->where('id', $id);
        $this->db->update('accuracy', $sort_order);
    }

    public function countRecords($id){
        $this->db->select('SQL_CALC_FOUND_ROWS *', false );
        $this->db->from('products');
        $this->db->join('accuracy', 'products.acc_id = accuracy.id');
        $this->db->where('accuracy.id', $id);
        $this->db->where('products.active', 0);
        $result = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'result' => $result,
                'count' => $count
            );
        } else {
        
        }
        
        
    }
}
