<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gallery_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getImages($id) {
        $this->db->select('*,products_images.id');
        $this->db->from('products_images');
        $this->db->join('products', 'products_images.product_id = products.id', 'left');
        $this->db->where('product_id', $id);
        $this->db->where('products.cat_id', 3);
        $this->db->order_by('order_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function deleteImage($id) {
        $this->db->select('*');
        $this->db->from('products_images');
        $this->db->where('products_images.id', $id);
        $result = $this->db->delete();
        return $result;
    }

    public function checkImage($id) {
        $this->db->select('*');
        $this->db->from('products_images');
        $this->db->where('products_images.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function save($image, $id) {
        $this->db->set('name', $image);
        $this->db->set('product_id', $id);
        $this->db->insert('products_images');
    }

    public function saveMain($image, $id) {
        $this->db->set('name', $image);
        $this->db->set('product_id', $id);
        $this->db->set('main_pic', 1);
        $this->db->insert('products_images');
    }

    public function reorder($orderId, $slideId) {
        $fields = array(
            'order_id' => $orderId,
        );
        $this->db->where('id', $slideId);
        $this->db->update('products_images', $fields);
    }

    public function mainPic($id) {
        $this->db->set('main_pic', 1);
        $this->db->set('order_id', 0);
        $this->db->where('id', $id);
        $this->db->update('products_images');
    }

    public function unMakemainPic($id, $product_id) {
        $this->db->set('main_pic', 0);
        $this->db->set('order_id', 1);
        $this->db->where('id !=', $id);
        $this->db->where('product_id', $product_id);
        $this->db->update('products_images');
    }

}
