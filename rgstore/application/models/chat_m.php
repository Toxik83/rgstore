<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Chat_m extends CI_Model {

    public function __construct() {

        parent::__construct();
    }

    // function that returns admin information for accessing the chat itself
    function getAdminInfo($conditions = array(), $fields = '') {

        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        $this->db->from('users');
        $this->db->order_by("users.id", "asc");

        if ($fields != '') {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
            $this->db->where('username', 'admin');
        }

        $result = $this->db->get()->result();

        return $result;
    }

    // Getting all users including admins .
    public function getChatUsers($search_term = 'default', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *',false);
        $this->db->from('users');
        $this->db->like('fname', $search_term);
        $this->db->order_by("id", "asc");
        
        $this->db->limit($limit, $offset);
        
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }
    
    // Function that returns chat history  . 
    public function getChatHistory($id,$search_term = 'default', $offset = 0, $limit = 0,$option) {
        $this->db->select('SQL_CALC_FOUND_ROWS users.id,chat.message,chat.from,chat.to,chat.sent',false);
        $this->db->from('users');
        $this->db->join('chat', 'users.fname = chat.to OR users.fname = chat.from','left');
        $this->db->where('users.id', $id);
        
        // This "if" clause is using for search field. If there isn`t
        // select property like from/to/message it returns by default
        // chat.message .
        
        if($option != null){
            $this->db->like('chat.'.$option, $search_term);
        }else{
            $this->db->like('chat.message', $search_term);
        }
        
        $this->db->order_by("chat.sent", "ASC");
        
        
        $this->db->limit($limit, $offset);
        
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }
    
    // Simple function that returns selected user information
    public function chatName($id) {
        $this->db->select('id,fname');
        $this->db->from('users');
        $this->db->where('id', $id);

        $result = $this->db->get()->row();
        return $result;
    }
    
    // Well setting the status to "Online" when admin is logged in :) 
    public function setOnline() {
        $this->db->set('online', 1);
        $this->db->where('username', $this->session->userdata('username'));
        $this->db->update('users');
    }
    
    public function setOffline() {
        $this->db->set('online', 0);
        $this->db->where('username', $this->session->userdata('username'));
        $this->db->update('users');
    }

    public function setOnlineUser() {
        $this->db->set('online', 1);
        $this->db->where('id', $this->session->userdata('userId'));
        $this->db->update('users');
    }

    public function setOfflineUser() {
        $this->db->set('online', 0);
        $this->db->where('id', $this->session->userdata('userId'));
        $this->db->update('users');
    }

}
