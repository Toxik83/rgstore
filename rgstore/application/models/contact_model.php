<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class contact_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function setMessage($messageData = array()) {
        $this->db->insert('inbox', $messageData);
    }

    public function getMessage($conditions = array(), $limit = array(), $like = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->select('*');
        $this->db->from('inbox');
        $this->db->order_by('sent_at', 'desc');

        //search for terms
        if (!empty($like)) {
            foreach ($like as $key => $value) {
                $this->db->like($key, $value);
            }
        }

        //Check For Limit
        if (is_array($limit)) {
            if (count($limit) == 1)
                $this->db->limit($limit[0]);
            else if (count($limit) == 2)
                $this->db->limit($limit[0], $limit[1]);
        }

        $result = $this->db->get();

        return $result;
    }

    public function updateMessage($messageId, $data = array()) {
        $this->db->where('inbox.id', $messageId);
        $this->db->update('inbox', $data);
    }

    public function deleteMessage($messageId) {
        $this->db->where('inbox.id', $messageId);
        $result = $this->db->delete('inbox');

        return $result;
    }

}
