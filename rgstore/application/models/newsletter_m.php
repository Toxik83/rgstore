<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletter_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getNewsletter($search_term = 'default', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->from('newsletter');
        $this->db->like('email', $search_term);
        $this->db->order_by('date', 'DESC');
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('newsletter');
    }

    public function deleteData($slideId) {
        $this->db->where('newsletter.id', $slideId);
        $result = $this->db->delete('newsletter');
        return $result;
    }

    public function getRecords() {
        $this->db->select('email');
        $this->db->from('newsletter');
        $query = $this->db->get()->result_array();
        return $query;
    }

}
