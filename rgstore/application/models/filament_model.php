<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Filament_model extends CI_Model {

    public function get_filament() {
        $this->db->where('cat_id', 2);
        $this->db->where('active', 0);
        return $this->db->get('products')->result();
    }

    public function getFilamentById($id) {
        $this->db->select('products.id,products.title,products_images.name,products.price,products.new_price,manufacturer.man_name,category.cat_name,products.fl_date_begin,products.fl_date_end,products.text,'
                . 'products.date_added,products.man_id,products_images.product_id,manufacturer.id as man_id,category.id as category_id,products.f_cat_id,products_images.main_pic,products.active,products.cat_id');
        $this->db->from('products');
        $this->db->join('manufacturer', 'manufacturer.id = products.man_id');
        $this->db->join('products_images', 'products_images.product_id = products.id ', 'left');
        $this->db->join('category', 'products.f_cat_id = category.id', 'left');
        $this->db->where('products.id', $id);
        $this->db->where('products.cat_id', 2);
        $this->db->where('products.active', 0);
        $this->db->where('products_images.main_pic', 1);
         $this->db->order_by('products_images.order_id');
        $data = $this->db->get();

        return $data;
    }
    public function getPicsById($id) {
        $this->db->select('products_images.name');
        $this->db->from('products');
        $this->db->join('products_images', 'products_images.product_id = products.id ', 'left');
        $this->db->where('products.id', $id);
        $this->db->where('products.cat_id', 2);
        $this->db->where('products.active', 0);
        $this->db->order_by('products_images.order_id');
        //$this->db->where('products_images.main_pic', 1);
        $data = $this->db->get()->result();

        return $data;
    }

    public function showFilamentById($id) {
        $this->db->select('*');
        $this->db->get('products');
        $this->db->where('id', $id);
        $this->db->where('cat_id', 2);
        $query = $this->db->get('products');
        return $query->row();
    }

    //Proverqva dali ima product sas sashtoto ime
    public function unique($title) {
        $this->db->select('title,active,cat_id');
        $this->db->from('products');
        $this->db->where('title', $title);
        $this->db->where('active', 0);
        $this->db->where('cat_id', 2);

        $exist = $this->db->get()->row();

        if ($exist) {
            return true;
        }

        return false;
    }

    //sazdava filament product
    public function create($title, $f_cat_id, $text, $price, $new_price, $manufacturer) {
        $filament = array(
            'title' => $title,
            'f_cat_id' => $f_cat_id,
            'text' => $text,
            'price' => $price,
            'new_price' => $new_price,
            'man_id' => $manufacturer,
            'date_added' => date('Y-m-d H:i'),
            'cat_id' => 2
        );

        $this->db->insert('products', $filament);
    }

    //Update promo price
    public function editPromo($id, $new_price) {
        $this->db->set('new_price', $new_price);
        $this->db->set('date_update', date('Y-m-d H:i'));
        $this->db->where('id', $id);
        $query = $this->db->update('products');
    }

    //update Filament product
    public function update($id, $title, $text, $price, $new_price, $manufacturer, $category, $date_update) {
        $filament = array(
            'title' => $title,
            'text' => $text,
            'price' => $price,
            'new_price' => $new_price,
            'man_id' => $manufacturer,
            'f_cat_id' => $category,
            'date_update' => date('Y-m-d H:i')
        );
        $this->db->where('id', $id);
        $this->db->update('products', $filament);
    }

    public function find($id) {
        $this->db->select('pr.id,pr.cat_id,pr.title,pr.price,pr.new_price,pr.active,pi.name');
        $this->db->from('products as pr');
        $this->db->where('pr.id', $id);
        $this->db->where('cat_id', 2);
         $this->db->where('pi.main_pic', 1);
        $this->db->join('products_images as pi', 'pr.id = pi.product_id', 'LEFT');
        return $this->db->get()->row();
    }

    public function getCategories() {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('active', 0);
        $data = $this->db->get()->result();

        return $data;
    }

    public function filamentsList($offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *, products.id,products_images.order_id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('category', 'products.f_cat_id = category.id', 'left');
        $this->db->where('cat_id', 2);
        $this->db->where('products.active', 0);
        $this->db->where('products_images.main_pic', 1);
        $this->db->order_by('products.id', 'DESC');
        $this->db->group_by('products.id ');
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result_array();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        return array(
            'data' => $data,
            'count' => $count
        );
    }

    //Displays all filament products 
    public function get_results($search_term = 'default', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS * , products.id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('category', 'products.f_cat_id = category.id', 'left');
        $this->db->join('manufacturer', 'products.man_id = manufacturer.id', 'left');
        $this->db->group_by('products.id');
        $this->db->like('title', $search_term);
        $this->db->order_by('products.date_added', 'DESC');
        $this->db->where('cat_id', 2);
        $this->db->where('products.active', 0);
        $this->db->where('products_images.main_pic', 1);
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();

        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;

        return array('data' => $data, 'count' => $count);
    }

    public function mainPic($id) {
        $this->db->set('main_pic', 1);
        $this->db->where('id', $id);
        $this->db->update('products_images');
    }

    public function unMakemainPic($id, $product_id) {
        $this->db->set('main_pic', 0);
        $this->db->where('id !=', $id);
        $this->db->where('product_id', $product_id);
        $this->db->update('products_images');
    }

    public function delete($id) {
        $this->db->set('active', 1);
        $this->db->where('id', $id);
        $this->db->update('products');
    }

    public function newPriceRow($id) {
        $this->db->select('new_price');
        $this->db->from('products');
        $this->db->where('id', $id);

        $query = $this->db->get();
        return $query->row();
    }

    //Update flashdate
    public function setFlashdata($data) {
        $this->db->set('fl_date_begin', $data['fl_date_begin']);
        $this->db->set('fl_date_end', $data['fl_date_end']);
        $this->db->where('id', $data['id']);
        $this->db->update('products');
    }

    public function flashSource($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        return $this->db->get('products')->row();
    }



    public function idBestsellers() {
        $sql = "SELECT products.id,sum(products_ordered.quantity) as total
    FROM `orders`
    JOIN products_ordered ON orders.order_id = products_ordered.order_id
    JOIN products ON products_ordered.product_id = products.id
    JOIN products_images ON products.id = products_images.product_id
    WHERE year(orders.date_added) = YEAR( CURRENT_DATE - INTERVAL 1 MONTH)
    AND Month(orders.date_added) = Month( CURRENT_DATE - INTERVAL 1 MONTH)
    AND products_images.main_pic = 1 AND products.cat_id=2
    GROUP BY products.id
    HAVING total > 1    
    LIMIT 12";

        $result = $this->db->query($sql, array());
        return $result;
    }

    //
    public function deleteData($slideId) {
        $this->db->set('active', 1);
        $this->db->where('id', $slideId);
        $result = $this->db->update('products');
        return $result;
    }
     public function setPromo($id,$newPrice){
        $this->db->set('new_price',$newPrice);
        $this->db->where('id',$id);
        $this->db->update('products');
    }
 public function deleteFlash($id) {
        $this->db->set('fl_date_begin', '0000-00-00 00:00:00');
        $this->db->set('fl_date_end', '0000-00-00 00:00:00');
        $this->db->set('new_price', 0);
        $this->db->set('date_update', date('Y-m-d H:i'));
        $this->db->where('id', $id);
        $this->db->update('products');
    }
     public function get_resultsInactive($search_term = '', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *, products.id', false);
        $this->db->from('products');
        $this->db->join('manufacturer', 'manufacturer.id = products.man_id');
        $this->db->join('products_images', 'products_images.product_id = products.id ', 'left');
        $this->db->where('cat_id', 2);
        $this->db->order_by('products.id', 'DESC');
        $this->db->group_by('products.id ');
        $this->db->where('products_images.main_pic', 1);
        $this->db->where('products.active', 1);
        $this->db->like('title', $search_term);
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'data' => $data,
                'count' => $count
            );
        } else {
            
        }
    }
     public function makeActive($id) {
        $this->db->set('active', 0);
        $this->db->set('date_update', date('Y-m-d H:i'));
        $this->db->where('id', $id);
        $this->db->update('products');
    }
    public function inactivePrice($id,$new_price) {
        $this->db->set('new_price', $new_price);
        $this->db->set('date_update', date('Y-m-d H:i'));
        $this->db->where('id', $id);
        $this->db->update('products');
    }
     public function deleteInactive($id) {
        $this->db->set('active', 2);
        $this->db->where('id', $id);
        $this->db->update('products');
    }
}
