<?php

class Filamentsgallery_Model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function save($url,$id) {
        $this->db->set('name', $url);
        $this->db->set('product_id', $id); 
        $this->db->insert('products_images');
    }
   
    public function saveMainPic($url, $filament_id, $main_pic) {
        $this->db->set('name', $url);
        $this->db->set('main_pic', $main_pic);
        $this->db->set('product_id', $filament_id);
        $this->db->insert('products_images');
    }

    public function showImageByFilament_Id($filament_id) {
        $this->db->select('*');
        $this->db->from('products as pr');
        $this->db->join('products_images as pi', 'pi.product_id = pr.id');
        $this->db->where('pr.id ', $filament_id);
        //$this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }

    public function find($id) {
        $this->db->where('id', $id);
        return $this->db->get('products_images')->row();
    }
    public function update($id, $url, $filament_id) {
        $this->db->set('name', $url);
        $this->db->set('product_id', $filament_id);
        $this->db->where('id', $id);
        $this->db->update('products_images');
    }

    public function mainPic($id) {
        $this->db->set('main_pic', 1);
         $this->db->set('order_id', 0);
        $this->db->where('id', $id);
        $this->db->update('products_images');
    }

    public function unMakemainPic($id, $product_id) {
        $this->db->set('main_pic', 0);
         $this->db->set('order_id', 1);
        $this->db->where('id !=', $id);
        $this->db->where('product_id', $product_id);
        $this->db->update('products_images');
    }
     public function reorder($orderId, $slideId) {
        $fields = array(
            'order_id' => $orderId,
        );
        $this->db->where('id', $slideId);
        $this->db->update('products_images', $fields);
    }
    
     public function deleteImage($id) {
        $this->db->select('*');
        $this->db->from('products_images');
        $this->db->where('products_images.id', $id);
        $result = $this->db->delete();
        return $result;
    }
     public function check($id) {
        $this->db->select('*');
        $this->db->from('products_images');
        $this->db->where('products_images.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    

}
