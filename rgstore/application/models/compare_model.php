<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Compare_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getProduct($id) {
        $this->db->select('*, products.id,products_images.product_id');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('manufacturer', 'products.man_id = manufacturer.id');
        $this->db->join('print_tech', 'print_tech.id = products.print_id');
        $this->db->join('building_size', 'building_size.id = products.build_id');
        $this->db->join('accuracy', 'accuracy.id = products.acc_id');
        $this->db->where('products.active', 0);
        $this->db->where('products.id', $id);
        $this->db->where('products_images.main_pic', 1);
        $this->db->group_by('products.id ');
        $query = $this->db->get();
        return $query->row();
    }

    public function getProductDetails($conditions = array(), $fields = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        if (count($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }

        $this->db->from('products as p');
        $this->db->join('products_images as pi', 'p.id = pi.product_id', 'left');
        $this->db->join('manufacturer as m', 'p.man_id = m.id', 'left');
        $this->db->join('print_tech as pt', 'pt.id = p.print_id', 'left');
        $this->db->join('building_size as bs', 'bs.id = p.build_id', 'left');
        $this->db->join('accuracy as a', 'a.id = p.acc_id', 'left');

        $result = $this->db->get();

        return $result;
    }

}
