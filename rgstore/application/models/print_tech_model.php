<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Print_tech_model extends CI_Model {

    public function show_PrintTech($search_term ='default' , $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->like('print_name', $search_term);
        $this->db->order_by('order_id');
        $this->db->where('active', 0);
        $this->db->limit($limit, $offset);
        $data = $this->db->get('print_tech')->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'data' => $data,
                'count' => $count
            );
        } else {
            
        }
    
    } 

    public function show_Print_tech() {
        $this->db->select('*');
        $this->db->order_by('order_id');
        $this->db->where('active', 0);
        $data = $this->db->get('print_tech')->result();
        return  $data;   
    } 

    public function create($name, $orderId) {
        $print_tech = array(
            'print_name' => $name, 
            'order_id' =>$orderId
        );
        $this->db->insert('print_tech', $print_tech);
    }

    public function delete($id) {
        $this->db->set('active', 1);
        $this->db->where('id', $id);
        $this->db->update('print_tech');
    }

    public function find($id) {
        $this->db->where('id', $id);
        return $this->db->get('print_tech')->row();
    }

    public function update($id, $name) {
        $this->db->set('print_name', $name);
        $this->db->where('id', $id);
        $this->db->update('print_tech');
    }

    public function check_unique($name) {
        $this->db->select('*');
        $this->db->from('print_tech');
        $this->db->where('print_name', $name);
        $this->db->where('active', 0);
        $exist = $this->db->get()->row();
        if ($exist) {
            return true;
        }
        return false;
    }

    public function reorder($order_id, $id) {
        $sort_order = array(
            'order_id' => $order_id,
        );

        $this->db->where('id', $id);
        $this->db->update('print_tech', $sort_order);
    }
    
    public function lastId() {
        $this->db->select_max('id');
        $this->db->where('active', 0);
        $result = $this->db->get('print_tech');
        return $result ->row();
    }
    
    public function countRecords($id) {
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->from('products');
        $this->db->join('print_tech', 'products.print_id = print_tech.id');
        $this->db->where('print_tech.id', $id);
        $this->db->where('products.active', 0);
        $result = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'result' => $result,
                'count' => $count
            );
        } else {
            
        }
    }

}

