<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Finishing_front_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getFinishings($offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS * , products.id,', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->group_by('products.id');
        $this->db->order_by('products.date_added', 'DESC');
        $this->db->where('cat_id', 3);
        $this->db->where('active', 0);
        $this->db->where('products_images.main_pic', 1);


        $this->db->limit($limit, $offset);

        $data = $this->db->get()->result_array();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }

    public function getFinishingsByCategory($id, $search_term = 'default', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS * , products.id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('category', 'products.f_cat_id = category.id', 'left');
        $this->db->where('category.id', $id);
        $this->db->group_by('products.id');
        $this->db->like('title', $search_term);
        $this->db->order_by('products.date_added', 'DESC');
        $this->db->where('cat_id', 3);
        $this->db->where('active', 0);


        $this->db->limit($limit, $offset);

        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }

    public function getFinishingsByDetail($id, $search_term = 'default', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS * , products.id', false);
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->where('products.id', $id);
        $this->db->where('products.cat_id', 3);
        $this->db->group_by('products.id');
        $this->db->like('title', $search_term);
        $this->db->order_by('products.date_added', 'DESC');
        $this->db->limit($limit, $offset);

        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }

    public function getImages($id) {
        $this->db->select('*');
        $this->db->from('products_images');
        $this->db->where('product_id', $id);
        $this->db->order_by('order_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getCatogories() {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->order_by('order_id', 'ASC');
        $this->db->where('active',0);

        $data = $this->db->get()->result();
        return $data;
    }
    
    public function autoSearch($title,$id){
        $this->db->like('title',$title,'both');
        $this->db->limit(3);
        $this->db->where('active',0);
        $this->db->where('cat_id',$id);
        return $this->db->get('products')->result();
    }
    // for best sellers
    public function getFinishingCount() {
        $this->db->select('products.id');
        $this->db->from('products');
        $this->db->join('products_images', 'products.id = products_images.product_id', 'left');
        $this->db->join('products_ordered', 'products.id = products_ordered.product_id', 'left');
        $this->db->join('orders', 'products_ordered.order_id = orders.order_id', 'left');
        $this->db->where('active', 0);
        $this->db->where('cat_id', 3);
        $this->db->where('products_images.main_pic', 1);
        $this->db->having('sum(products_ordered.quantity) >= ',3);
        $this->db->where('EXTRACT( YEAR_MONTH FROM orders.date_added ) = EXTRACT( YEAR_MONTH FROM CURDATE( ) - INTERVAL 1 MONTH ) ');        
        $this->db->group_by('products.id ');
        $data = $this->db->get()->result_array();
        return $data;
    }
}
