<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class users_model extends CI_Model {

    public function __construct() {

        parent::__construct();
    }

    public function setUserData($userData = array()) {
        $this->db->insert('users', $userData);
    }

    public function setUserDetails($userDetails = array()) {
        $this->db->insert('user_details', $userDetails);
    }
    
    public function setBillingAddress($userData = array())
    {
        $this->db->insert('billing_addresses', $userData);
    }
    
    public function setTransactionData($transactionData = array())
    {  

        $this->db->insert('transactions_paypal', $transactionData);
    }

    public function setTransactionComplete($paymentId, $data = array()) {
        $this->db->where('payment_id', $paymentId);
        $this->db->update('transactions_paypal', $data);
    }
    
    public function getBillingAddresses($conditions = array(), $fields = array())
    {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        
        if (count($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }
        
        $this->db->from('billing_addresses as ba');
        $this->db->join('country as c', 'ba.b_country_id = c.id', 'left');
        
        $result = $this->db->get();
        
        return $result;
    }

    public function getUserByHash($cookieHash)
    {
        $this->db->select('*', false);
        $this->db->from('users');
        $this->db->where("SHA1(id)", $cookieHash);
        $result = $this->db->get();
        return $result;
    }

    public function getPaymentId($conditions = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->select('payment_id');
        $this->db->from('transactions_paypal');
        $result = $this->db->get();
        return $result;
    }

    public function getUserData($conditions = array('')) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->select('*');
        $this->db->from('users');
        $result = $this->db->get();
        return $result;
    }

    public function getAllUserData($conditions = array(), $fields = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        if (count($fields)) {
            $this->db->select($fields);
        } else {
            $this->db->select('*');
        }

        $this->db->from('users as u');
        $this->db->join('user_details as ud', 'u.id = ud.user_id', 'left');
        $this->db->join('country as c', 'ud.country_id = c.id', 'left');
        $result = $this->db->get();
        return $result;
    }

    public function getCurrentPassword($conditions = array()) {
        $sql = "SELECT password FROM users
                WHERE id = ? AND password = ?";

        $result = $this->db->query($sql, array($conditions['id'], $conditions['password']));
        return $result;
    }

    public function getCountry($conditions = array()) {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }

        $this->db->select('*');
        $this->db->from('country');
        $result = $this->db->get();
        return $result;
    }
    
    public function updateBillingAddress($addressId, $updateData = array())
    {
        $this->db->where('billing_addresses.id', $addressId);
        $this->db->update('billing_addresses', $updateData);
    }
    
    public function updateAddressDetails($addressId, $data = array())
    {
       $this->db->where('user_details.detail_id', $addressId);
       $this->db->update('user_details', $data); 
    }
   
    public function updateUserData($email, $data = array())
    {
        $this->db->where('users.email', $email);
        $this->db->update('users', $data);
    }

    public function social($api, $tokenColumn, $token, $api_id, $email = null, $data = array()) {
        //First we check if there's exsisting social user
        $check_api_id = $this->db->get_where('users', array($api => $api_id));
        if ($check_api_id->num_rows() == 0) {
            //Checking if there's exsisting email
            $check_email = $this->db->get_where('users', array('email' => $email));
            if ($check_email->num_rows() == 0) {
                if ($api == 'twitter_id') {
                    return 'No Email';
                } else {
                    //insert in to db
                    $this->db->insert('users', $data);

                    return 'New Registration';
                }
            } elseif ($check_email->num_rows() == 1) {
                //Linking the account to the exsisting account
                $this->db->where('email', $email);
                $this->db->update('users', array($api => $api_id, $tokenColumn => $token));
                return 'Update Registration';
            }
        } else {
            if ($email != null) {
                $check_email_api = $this->db->get_where('users', array($api => $api_id, 'email' => $email));

                if ($check_email_api->num_rows() == 1) {
                    $this->db->where($api, $api_id);
                    $this->db->update('users', array($tokenColumn => $token));

                    return 'Social Login';
                } else {
                    return 'Existing Account';
                }
            } else {
                $query = $this->db->get_where('users', array($api => $api_id));

                if ($query->num_rows() == 1) {
                    $this->db->where($api, $api_id);
                    $this->db->update('users', array($tokenColumn => $token));

                    return 'Social Login';
                }
            }
        }
    }

}
