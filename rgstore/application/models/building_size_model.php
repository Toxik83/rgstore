<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Building_size_model extends CI_Model {
    
    public function show_Admin_Size($keyword = null ,$offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS * ', false);
        $this->db->from('building_size');
        $this->db->where('active', 0);
        $this->db->like('build_name',$keyword);
        $this->db->order_by('order_id');
        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        
        return array('data' => $data, 'count' => $count);
    }
    
    public function show_Size() {
        $this->db->select('*');
        $this->db->where('active', 0);
        $this->db->from('building_size');
        $this->db->order_by('order_id');
        
        return $this->db->get()->result();
    }

    public function create($name) {
        $size = array(
            'build_name' => $name
        );
        $this->db->insert('building_size', $size);
    }

    public function delete($id) {
        $this->db->set('active', 1);
        $this->db->where('id', $id);
        $this->db->update('building_size');
    }

    public function find($id) {
        $this->db->where('id', $id);
        return $this->db->get('building_size')->row();
    }

    public function update($id, $name) {
        $this->db->set('build_name', $name);
        $this->db->where('id', $id);
        $this->db->update('building_size');
    }


    public function check_unique($name) {
        $this->db->select('*');
        $this->db->from('building_size');
        $this->db->where('build_name', $name);
        $this->db->where('active', 0);
        $exist = $this->db->get()->row();

        if ($exist) {
            return true;
        }

        return false;
    }

    public function reorder($order_id, $id) {
        $sort_order = array(
            'order_id' => $order_id
        );

        $this->db->where('id', $id);
        $this->db->update('building_size', $sort_order);
    }
    
    public function countRecords($id){
        $this->db->select('SQL_CALC_FOUND_ROWS *', false );
        $this->db->from('products');
        $this->db->join('building_size', 'products.build_id = building_size.id');
        $this->db->where('building_size.id', $id);
        $this->db->where('products.active', 0);
        $result = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if ($count > 0) {
            return array(
                'result' => $result,
                'count' => $count
            );
        } else {
        
        }
        
        
    }
}
