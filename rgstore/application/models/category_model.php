<?php

class Category_Model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getFilaments($search_term = 'default', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS * , category.id', false);
        $this->db->from('category');
        $this->db->group_by('category.id');
        $this->db->like('cat_name', $search_term);
        $this->db->limit($limit, $offset);

        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count ;')->row()->count;
        return array('data' => $data, 'count' => $count);
    }

    public function delete($id) {
        $this->db->delete('category', array('id' => $id));
    }

    public function find($id) {
        $this->db->where('id', $id);
        return $this->db->get('category')->row();
    }

    public function insert($name) {
        $category = array(
            'cat_name' => $name
        );
        $this->db->insert('category', $category);
    }

    public function update($id, $name) {
        $this->db->set('cat_name', $name);
        $this->db->where('id', $id);
        $this->db->update('category');
    }

    public function getCategories() {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('active', 0);
        $this->db->order_by('order_id');
        return $this->db->get()->result();
    }

    public function categories() {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->order_by('cat_name', 'ASC');
        $result = $this->db->get()->result();
        return $result;
    }

    public function get_Num_rows() {
        $query = $this->db->query('SELECT * FROM category');
        return $query->num_rows();
    }

    public function show_Categories() {
        $this->db->get('category');
        return $this->db->get('category')->result();
    }

    public function reorder($orderId, $slideId) {
        $fields = array(
            'order_id' => $orderId,
        );
        $this->db->where('slider.id', $slideId);
        $this->db->update('slider', $fields);
    }

    public function get_results($search_term = '', $offset = 0, $limit = 0) {
        $this->db->select('SQL_CALC_FOUND_ROWS *,category.id', false);
        $this->db->from('category');

        $this->db->order_by('category.id', 'DESC');

        $this->db->group_by('category.id ');
        $this->db->like('cat_name', $search_term);

        $this->db->limit($limit, $offset);
        $data = $this->db->get()->result();
        $count = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        if ($count > 0) {
            return array(
                'data' => $data,
                'count' => $count
            );
        } else {
            
        }
    }

    public function unique($id, $name) {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('id !=', $id);
        $this->db->where('cat_name', $name);
        $exist = $this->db->get()->row();

        if ($exist) {
            return true;
        }

        return false;
    }
    

}
