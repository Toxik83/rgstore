<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class PayPalPayment extends MY_Controller_Front{
    
    public function __construct() {
        parent::__construct();
        
        require __DIR__ . '/../../vendor/autoload.php';
        $this->load->model('users_model');
        $this->load->model('products_model');
        $this->load->model('address_model');
        $this->load->model('order_model');
        $this->load->library('cart');
        
    }
    
    public function index()
    {             
        // API 
        $api = new PayPal\Rest\ApiContext(       
                new PayPal\Auth\OAuthTokenCredential(
                        'AcrLzFTFouNeIf5WszPCzORoHGCTE1PQHvAbhrzRZx8b5eIRH-iXr0u4k9ZXql2lXm1h0weaDcxEF3cz',
                        'ELZnTvbeqxrXe74z-jwSBcVby0t9YXTvu46N729SfE3zvycrZ2ewNkr32rljXNowck8lSxeokFPpxlpu'
                )
               );       

        if ($this->input->get('isApproved') == 'approved') {

            $payerId = $this->input->get('PayerID');
            
            $conditions = array(
                'hash' => $this->session->userdata('paypal_hash'),
            );

            $data['payment_id'] = $this->users_model->getPaymentId($conditions)->row();

            $paymentId = $data['payment_id']->payment_id;
            
            $payment = PayPal\Api\Payment::get($paymentId, $api);
            
            $paymentToArray = $payment->toArray();
            $shipping = 0;
            $subtotal = $this->cart->total();
            
            if (array_key_exists('shipping', $paymentToArray['transactions'][0]['amount']['details'])) {
                $shipping = $paymentToArray['transactions'][0]['amount']['details']['shipping'];
            }

            $execution = new PayPal\Api\PaymentExecution();
            $execution->setPayerId($payerId);
            
            $payment->execute($execution, $api);

            $data = array(
                'complete' => 1
            );
            $this->users_model->setTransactionComplete($paymentId, $data);

            // Add the order in the database.
            $userId = $this->session->userdata('userId');
            
            $whereConditions = array(
            'user_id' => $userId,
            'is_main' => 1
            );
            $data['address'] = $this->address_model->find_address($whereConditions);

            //get the current billing address
            $billingAddresConditions = array(
                'ba.user_id' => $userId,
                'ba.is_main' => 1,
                'ba.is_deleted' => 0
            );
            $billingAddresFields = array(
                'ba.user_id', 
                'ba.b_address', 
                'ba.b_city', 
                'ba.id', 
                'ba.b_zip_code', 
                'ba.b_phone', 
                'ba.b_phone_code', 
                'c.display_name'
            );
            $data['billingAddress'] = $this->users_model->getBillingAddresses($billingAddresConditions, $billingAddresFields)->row();
            $data['details'] = $this->address_model->find_names($userId);
            $date = date("Y-m-d H:i:s");

            //insert the new order
            $data['orderData'] = array(
                'user_id' => $userId,
                'title' => $data['details']->title,
                'fname' => $data['details']->fname,
                'lname' => $data['details']->lname,
                'email' => $data['details']->email,
                'address' => $data['address']->address,
                'city' => $data['address']->city,
                'zip_code' => $data['address']->zip_code,
                'country_name' => $data['address']->display_name,
                'phone' => $data['address']->phone,
                'phone_code' => $data['address']->phonecode,
                'order_price' => $shipping + $subtotal,
                'date_added' => $date,
                'b_address' => $data['billingAddress']->b_address,
                'b_city' => $data['billingAddress']->b_city,
                'b_zip_code' => $data['billingAddress']->b_zip_code,
                'b_country_name' => $data['billingAddress']->display_name,
                'b_phone' => $data['billingAddress']->b_phone,
                'b_phone_code' => $data['billingAddress']->b_phone_code
            );
            
            $this->order_model->setOrder($data['orderData']);
            $orderId = $this->db->insert_id();
            
            //insert products in the products_ordered table
            $colour = '';
            foreach ($this->cart->contents() as $product) {

                if (array_key_exists('options', $product)) {
                    if (array_key_exists('Colour', $product['options'])) {
                        $colour = $product['options']['Colour'];
                    }
                }

                if (array_key_exists('qty', $product)) {
                    $quantity = $product['qty'];
                }  

                $products_data = array(
                    'price' => $product['price'],
                    'quantity' => $product['qty'],
                    'product_id' => $product['id'],
                    'total_price' => $product['subtotal'],
                    'options' => $colour,
                    'order_id' => $orderId
                );

                $this->order_model->insert_products($products_data);
            }
            
            $orderConditions = array(
            'po.order_id' => $orderId
            );
            $orderedProducts = $this->order_model->getProductsOrdered($orderConditions)->result();    

            foreach ($orderedProducts as $key => $product){
                $updateFields = array(
                    'sold' => $product->sold + $product->quantity
                );
                $this->products_model->updateProduct($updateFields, $product->id);
            }

            $status = array(
                'status' => 3
            );

            $this->order_model->changeStatus($orderId, $status);
            
            //destroying the cart content after successfull payment
            $this->cart->destroy();
            
            $this->session->set_flashdata('success', 'Your purchase has been made successfully');
            redirect('profile/orders');
        } else {
            $this->session->set_flashdata('success', 'Your order has been canceled');
            redirect('profile/orders');
        }
    }
    
}
