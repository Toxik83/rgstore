<?php

use OAuth\OAuth2\Service\Google;
use OAuth\OAuth2\Service\Facebook;
use OAuth\OAuth1\Service\Twitter;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;

class oAuthController extends CI_Controller 
{
    public function __construct() {
        parent::__construct();
        
        $this->load->model('users_model');
    }
    public function hash($string) 
    {
        return hash("sha1", $string . config_item('encryption_key'));
    }

    public function setPassword() 
    {
        $conditions = array(
            'email' => $this->session->userdata('email'),
            'password' => ''
        );
        $query = $this->users_model->getUserData($conditions);
        $passNumRows = $query->num_rows();
        // Only users that are logged and don't have password will be able to get access to this page
        if ($this->session->userdata('logged_in') == true && $passNumRows == 1) {
            $this->breadcrumb->populate(array(
                'Home' => '',
                'Login/Register' => 'login',
                'Set Password'
            ));

            $data = array();
            $data['postData'] = array(
                'password' => '',
                'searchBy' => ''
            );

            $this->form_validation->set_rules('password', 'Password', 'required|trim|matches[passconf]|xss_clean');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|trim|xss_clean');

            if ($this->input->post()) {
                
                if ($this->form_validation->run()) {
                    $email = $this->session->userdata('email');
                    $data['userData'] = array(
                        'password' => $this->hash($this->input->post('password'))
                    );

                    $query = $this->users_model->updateUserData($email, $data['userData']);

                    $this->session->set_flashdata('success', 'You\'ve now set your password');

                    redirect('/');
                }
            }

            $terms = $this->input->get('search');
            $data['search'] = $terms;
            $data['title'] = 'Set Password';

            $this->load->view('login/enterPassword_view', $data);
        } else {
            $this->session->set_flashdata('error', 'You don\'t have access to this page. You\'re either not logged in or you already have password.');
            //? 
            redirect('/');
        }
    }

    public function google() 
    {
        /**
         * Load the credential for the different services
         */
        require_once __DIR__ . '/../config/init.php';
        
        /**
         * Create a new instance of the URI class with the current URI, stripping the query string
         */
        $uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
        $currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
        $currentUri->setQuery('');

        // Session storage
        $storage = new Session();

        //Callback URL
        $callbackUrl = 'http://rgintern.www.demo.rewardgateway.com/login/google';

        // Setup the credentials for the requests
        $credentials = new Credentials(
                $servicesCredentials['google']['key'], $servicesCredentials['google']['secret'], $callbackUrl
        );

        // Instantiate the Google service using the credentials, http client and storage mechanism for the token
        /** @var $googleService Google */
        $googleService = $serviceFactory->createService('google', $credentials, $storage, array('userinfo_email', 'userinfo_profile'));

        if (!empty($_GET['code'])) {
            // retrieve the CSRF state parameter
            $state = isset($_GET['state']) ? $_GET['state'] : null;

            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken($_GET['code'], $state);
            
            // Send a request with it
            $result = json_decode($googleService->request('userinfo'), true);

            //Setting up credentials for inserting records in DB
            ($result['gender'] == 'male') ? $gender = 'Mr' : $gender = 'Ms';
            $api = 'google_id';
            $tokenColumn = 'google_acc_token';
            $accToken = $token->getAccessToken();
            $google_id = $result['id'];
            if ($this->session->userdata('email') != null) {
                $email = $this->session->userdata('email');
            } else {
                $email = $result['email'];
            }
            $data = array(
                'email' => $email,
                'title' => $gender,
                'fname' => $result['given_name'],
                'lname' => $result['family_name'],
                'google_id' => $google_id,
                'google_acc_token' => $accToken
            );
            
            //Inserting credentials in to db
            $login_query = $this->users_model->social($api, $tokenColumn, $accToken, $google_id, $email, $data);
            
            // Login the person and setting up session
            if ($login_query == 'Update Registration' || $login_query == 'Social Login' || $login_query == 'New Registration') {
                $conditions = array('email' => $email);
                $query = $this->users_model->getUserData($conditions);

                $userData = $query->row();
                if ($query->num_rows() > 0) {
                    $sessionData = array(
                        'userId' => $userData->id,
                        'email' => $userData->email,
                        'logged_in' => true,
                    );

                    $this->session->set_userdata($sessionData);
                    if ($login_query == 'New Registration') {
                        redirect('login/set-password');
                    }else{
                        if ($login_query == 'Update Registration') {
                            $this->session->set_flashdata('success', 'Congratulations, you\'ve linked your Google account');
                        }
                        redirect('/');
                    }
                }
            } elseif ($login_query == 'Existing Account') {
                $this->session->set_flashdata('error', 'This Google account is already taken by another user.');
                redirect('profile');
            }
        } elseif (!empty($_GET['go']) && $_GET['go'] === 'go') {
            $url = $googleService->getAuthorizationUri();
            header('Location: ' . $url);
        } else {
            redirect('/login');
        }
    }

    public function facebook() 
    {
        /**
         * Load the credential for the different services
         */
        require_once __DIR__ . '/../config/init.php';

        /**
         * Create a new instance of the URI class with the current URI, stripping the query string
         */
        $uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
        $currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
        $currentUri->setQuery('');

        // Session storage
        $storage = new Session();

        //Callback URL
        $callbackUrl = 'http://rgintern.www.demo.rewardgateway.com/login/facebook';
        // Setup the credentials for the requests
        $credentials = new Credentials(
                $servicesCredentials['facebook']['key'], $servicesCredentials['facebook']['secret'], $callbackUrl
        );

        // Instantiate the Facebook service using the credentials, http client and storage mechanism for the token
        /** @var $facebookService Facebook */
        $facebookService = $serviceFactory->createService('facebook', $credentials, $storage, array('public_profile', 'email'));

        if (!empty($_GET['code'])) {
            // retrieve the CSRF state parameter
            $state = isset($_GET['state']) ? $_GET['state'] : null;

            // This was a callback request from facebook, get the token
            $token = $facebookService->requestAccessToken($_GET['code'], $state);

            // Send a request with it
            $result = json_decode($facebookService->request('/me?fields=email,name,id,gender'), true);
            
            //Setting up credetials for the insert to DB
            ($result['gender'] == 'male') ? $gender = 'Mr' : $gender = 'Ms';
            $names = explode(' ', trim($result['name']));
            $api = 'facebook_id';
            $tokenColumn = 'facebook_acc_token';
            $accToken = $token->getAccessToken();
            $facebook_id = $result['id'];
            if ($this->session->userdata('email') != null) {
                $email = $this->session->userdata('email');
            } else {
                $email = $result['email'];
            }
            $data = array(
                'email' => $email,
                'title' => $gender,
                'fname' => $names[0],
                'lname' => $names[1],
                'facebook_id' => $facebook_id,
                'facebook_acc_token' => $accToken
            );
            
            //Inserting credentials in to db
            $login_query = $this->users_model->social($api, $tokenColumn, $accToken, $facebook_id, $email, $data);
           
            //Login the user and setting up a session
            if ($login_query == 'Update Registration' || $login_query == 'Social Login' || $login_query == 'New Registration') {
                $conditions = array('email' => $email);
                $query = $this->users_model->getUserData($conditions);

                $userData = $query->row();
                if ($query->num_rows() > 0) {
                    $sessionData = array(
                        'userId' => $userData->id,
                        'email' => $userData->email,
                        'logged_in' => true,
                    );

                    $this->session->set_userdata($sessionData);
                    if ($login_query == 'New Registration') {
                        redirect('login/set-password');
                    }else{
                        if ($login_query == 'Update Registration') {
                            $this->session->set_flashdata('success', 'Congratulations, you\'ve linked your Facebook account');
                        }
                        redirect('/');
                    }
                }
            } elseif ($login_query == 'Existing Account') {
                $this->session->set_flashdata('error', 'This Facebook account is already taken by another user.');
                redirect('profile');
            }
        } elseif (!empty($_GET['go']) && $_GET['go'] === 'go') {
            $url = $facebookService->getAuthorizationUri();
            header('Location: ' . $url);
        } else {
            redirect('login');
        }
    }

    public function twitter() {
        /**
         * Load the credential for the different services
         */
        require_once __DIR__ . '/../config/init.php';

        /**
         * Create a new instance of the URI class with the current URI, stripping the query string
         */
        $uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
        $currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
        $currentUri->setQuery('');

        // Session storage
        $storage = new Session();

        //Callback URL
        $callbackUrl = 'http://rgintern.www.demo.rewardgateway.com/login/twitter';
        // Setup the credentials for the requests
        $credentials = new Credentials(
            $servicesCredentials['twitter']['key'], $servicesCredentials['twitter']['secret'], $currentUri->getAbsoluteUri()
        );

        // Instantiate the Facebook service using the credentials, http client and storage mechanism for the token
        /** @var $facebookService Facebook */
        $twitterService = $serviceFactory->createService('twitter', $credentials, $storage);

        if (!empty($_GET['oauth_token'])) {

            $token = $storage->retrieveAccessToken('Twitter');

            // This was a callback request from twitter, get the token
            $twitterService->requestAccessToken(
                    $_GET['oauth_token'], $_GET['oauth_verifier'], $token->getRequestTokenSecret()
            );
            // Send a request with it
            $result = json_decode($twitterService->request('account/verify_credentials.json'));
            
            //Setting up credentials for the registration
            $api = 'twitter_id';
            $tokenColumn = 'twitter_acc_token';
            $accToken = $token->getAccessToken();
            $twitter_id = $result->id;

            if ($this->session->userdata('email') != null) {
                $email = $this->session->userdata('email');
            } else {
                $email = null;
            }

            $data = array(
                'twitter_id' => $twitter_id,
                'twitter_acc_token' => $accToken
            );
            
            //Inserting credentials in to db
            $login_query = $this->users_model->social($api, $tokenColumn, $accToken, $twitter_id, $email, $data);
            
            //Login the person and setting up session
            if ($login_query == 'No Email') {
                $this->session->set_flashdata('error', 'In order to login with Twitter, you need to have linked account.');
                redirect('/');
            } elseif ($login_query == 'Update Registration' || $login_query == 'Social Login') {

                ($email != null) ? $conditions = array('email' => $email) : $conditions = array('twitter_id' => $twitter_id);
                $query = $this->users_model->getUserData($conditions);

                $userData = $query->row();
                if ($query->num_rows() > 0) {
                    $sessionData = array(
                        'userId' => $userData->id,
                        'email' => $userData->email,
                        'logged_in' => true,
                    );
                    
                    $this->session->set_userdata($sessionData);
                    if ($login_query == 'Update Registration') {
                        $this->session->set_flashdata('success', 'Congratulations, you\'ve linked your Twitter account!');
                    }
                    redirect('profile');
                }
            } elseif ($login_query == 'Existing Account') {
                $this->session->set_flashdata('error', 'This Twitter account is already taken by another user.');
                redirect('profile');
            }
        } elseif (!empty($_GET['go']) && $_GET['go'] === 'go') {
            // extra request needed for oauth1 to request a request token :-)
            $token = $twitterService->requestRequestToken();
            $url = $twitterService->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));
            header('Location: ' . $url);
        } else {
            redirect('login');
        }
    }

}
