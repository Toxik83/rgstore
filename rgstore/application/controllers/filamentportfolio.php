<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Filamentportfolio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('filament_model');
        $this->load->model('search_m');
        $this->load->model('order_model');
        $this->load->model('products_model');
        $this->load->model('printers_m');

    }
   
    public function filamentsDetail($filaments_id=0){
        $this->breadcrumb->populate(array(
            'Home' => '', 
            'Filament'=>'filamentportfolio',
            'Filament Details'
        ));
        $this->load->model('filament_model');
       
        $this->load->library('cart');
        $filaments_id = $this->uri->segment(3);
        
        $data['title'] = 'Filaments Details';
        $data['filament'] = $this->filament_model->getFilamentById($filaments_id)->row();
       
        $data['pics'] = $this->filament_model->getPicsById($filaments_id);
      //  $data['related']=$this->filament_model->get_random_filaments();
       
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $data['postData'] = array(
            'searchBy' => ''
        );
        
        //For last seen - 2 products
        
        $idl = $this->session->userdata('idl');
        $idl[] = $filaments_id;
        if (is_array($idl)) {
            $twoLastSeen = array_unique(array_slice($idl, -2, 2));
            $last = array(
                'idl' => $twoLastSeen
            );
            $this->session->set_userdata($last);
        }
        //Related Filaments
        
        $data['current_time'] = date("Y-m-d H:i:s");
        $category_id = $data['filament']->cat_id;
        $data['relatedProducts'] = $this->products_model->getRelatedProducts($category_id, $filaments_id);
        
        $this->load->view('filamentdetails', $data);
    }
     
     public function index($offset = 0){
        $this->breadcrumb->populate(array(
            'Home' => '', 
            'Filament'=>'Filament'
            
        ));
        $this->load->library('pagination');
        $data = array();
        $title = 'Filaments Portfolio';
        $data['title'] = $title;    
      
        $data['filamentlist'] = $this->filament_model->filamentsList($offset,12);
      
       $ids=array();
       $ids=$this->filament_model->idBestsellers()->result_array();
       
       $count=count($ids);
       
       

       foreach ($data['filamentlist']['data'] as $key => $val) {
          
            $data['filamentlist']['data'][$key]['flag'] = 0;
                foreach ($ids as $k2 => $v2) { 
                    if(($val['id']== $v2['id'])){    
                        $data['filamentlist']['data'][$key]['flag'] = 1;
                }   
            }
        }  
//       for($i = 0; $i < $count; $i++){
//          $id=$ids[$i]['id'];
//         
//           foreach ($data['filamentlist'] as $fil){
//               if(in_array($id, $fil)){
//                   echo 'yes';
//                   
//                   
//                   
//               }
//               else{
//                   echo 'No';
//                   
//               }
//           }
//           
//       }
       // echo '<pre>';
       // print_r($ids);
       // echo '</pre>';
       // die();
       
  
       
        $config = array();
        $config['base_url'] =  base_url() . 'index.php/filamentportfolio/index';       
        $config['total_rows'] = $data['filamentlist']['count'];
        $config['per_page'] = 12;
        $config['num_links'] = 9;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
            if($this->uri->segment(3) == 0)
            {
                $offset = 0;
            }
            else
            {
                $offset = ($config['per_page'])*($this->uri->segment(3)-1);
            }
            
        $links = $this->pagination->create_links();


        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );

        checkFlashDate();
        $this->load->view('filamentlisting', array(
            'filamentlist' =>$data['filamentlist']['data'],
            'postData' =>$data['postData'],
            'search' =>$terms,
            'title' => $title,
            'links' =>$links
            ));


        
    }
    public function showLtoH($offset = 0){
        $data['results'] = $this->filament_model->lowToHight();   
        $this->load->library('pagination');
        $data = array();
        $title = 'Printers Portfolio';
        $data['title'] = $title;        
        
        $data['filamentlist'] = $this->filament_model->lowToHight($offset, 5);
        $config = array();
        $config['base_url'] =  base_url() . 'index.php/filamentportfolio/showLtoH';       
        $config['total_rows'] = $data['printerlist']['count'];
        $config['per_page'] = 5;
        $config['num_links'] = 9;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
            if($this->uri->segment(3) == 0)
            {
                $offset = 0;
            }
            else
            {
                $offset = ($config['per_page'])*($this->uri->segment(3)-1);
            }
            
        $links = $this->pagination->create_links();
         
         
        
        
        
        $this->load->view('filamentlisting', array(
            'filament' =>$data['filamentlist']['data'],
            'title' => $title,
            'links' =>$links
            ));
        
    }
    public function showHtoL($offset = 0){
        $data['results'] = $this->printers_m->lowToHight();   
        $this->load->library('pagination');
        $data = array();
        $title = 'Printers Portfolio';
        $data['title'] = $title;        
        
        $data['printerlist'] = $this->printers_m->highToLow($offset, 5);
        //print_r($data);
        $config = array();
        $config['base_url'] =  base_url() . 'index.php/printersportfolio/showHtoL';       
        $config['total_rows'] = $data['printerlist']['count'];
        $config['per_page'] = 5;
        $config['num_links'] = 9;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
            if($this->uri->segment(3) == 0)
            {
                $offset = 0;
            }
            else
            {
                $offset = ($config['per_page'])*($this->uri->segment(3)-1);
            }
            
        $links = $this->pagination->create_links();      
        $this->load->view('printerslisting', array(
            'printerlist' =>$data['printerlist']['data'],
            'title' => $title,
            'links' =>$links
            ));
        
    }

      public function search() {
        $this->load->library('pagination');
        
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Filament' => ''
        ));
        $cat = 0;
        
        if($this->uri->segment(1) == 'finishing'){
            $cat = 3;
        }elseif($this->uri->segment(1) == 'filamentportfolio'){
            $cat = 2;
        }elseif($this->uri->segment(1) == 'printersportfolio'){
            $cat = 1;
        }

        $data = array();

        $filterable = array(
                'min',
                'max',
                'per',
                'searchBy',
                'catName',
                'orderBy',
                'search',
                'man',
                'tech',
                'size',
                'acc',
                
            );
        
        $allqueryparams = $this->input->get();
        
        $actualfilters = array();
        
        foreach($filterable as $key){
            if (array_key_exists($key, $allqueryparams)){
                $actualfilters[$key] = $allqueryparams[$key];
            }
        }
        
        if(!array_key_exists('min', $this->input->get())){
            $actualfilters['min'] = null;
        }
        if(!array_key_exists('max', $this->input->get())){
            $actualfilters['max'] = null;
        }
        if(!array_key_exists('catName', $this->input->get())){
            $actualfilters['catName'] = null;
        }
        if(!array_key_exists('man', $this->input->get())){
            $actualfilters['man'] = null;
        }
         if(!array_key_exists('tech', $this->input->get())){
            $actualfilters['tech'] = null;
        }
        if(!array_key_exists('size', $this->input->get())){
            $actualfilters['size'] = null;
        }
        if(!array_key_exists('acc', $this->input->get())){
            $actualfilters['acc'] = null;
        }
        
        $like = array();

        if ($this->input->get()) {
            
            switch ($this->input->get('orderBy')) {
                case 1: $option = 'price';
                    $option2 = 'ASC';
                    break;
                case 2: $option = 'price';
                    $option2 = 'DESC';
                    break;
                case 3: $option = 'title';
                    $option2 = 'ASC';
                    break;
                case 4: $option = 'title';
                    $option2 = 'DESC';
                    break;
                default : $option = 'date_added';
                    $option2 = 'DESC';
            }
        }      

         $total = $this->search_m->getSearchResult(
                    $cat,$actualfilters['catName'],
                    null,
                    $like,
                    $option,
                    $option2,
                    $actualfilters['min'],
                    $actualfilters['max'],
                    $actualfilters['man'],
                    $actualfilters['tech'],
                    $actualfilters['size'],
                    $actualfilters['acc']
                )->num_rows();  
        
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1; 
        
        $pageLimit = 12;
        if(array_key_exists('per', $this->input->get())){
            $pageLimit = $this->input->get('per');
        }
        
        $max = array($pageLimit, ($page - 1) * $pageLimit);

         $messages = $this->search_m->getSearchResult(
                    $cat,
                    $actualfilters['catName'],
                    $max,
                    $like,
                    $option,
                    $option2,
                    $actualfilters['min'],
                    $actualfilters['max'],
                    $actualfilters['man'],
                    $actualfilters['tech'],
                    $actualfilters['size'],
                    $actualfilters['acc']
                )->result_array();
        
        
        $data['messageData'] = $messages;
         $bestsells = $this->filament_model->idBestsellers()->result_array();
         
        $current_time = time();
        foreach ($data['messageData'] as $key => $val) {
            $data['messageData'][$key]['flag'] = 0;
            if((($val['fl_date_begin'] == 0) && ($val['fl_date_end'] == 0))||($current_time < strtotime($val['fl_date_begin']))  )  {
            
                foreach ($bestsells as $k2 => $v2) {
                    if(($val['id']== $v2['id'])){ 
                        $data['messageData'][$key]['flag'] = 1;
                }   
            }
        }
        }
      
        //setting the $config array for the pagination
        $config = array();
        $config['base_url'] = base_url() . 'filamentportfolio/search';

        if ($this->input->get() != false) {
            $config['suffix'] = '?' . http_build_query($this->input->get(), '', '&');
            $config['first_url'] = base_url() . 'filamentportfolio/search/1' . $config['suffix'];

        }

        $config['total_rows'] = $total;
        $config['per_page'] = $pageLimit;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = true;

        $config['first_link'] = false;
        $config['last_link'] = false;

        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';

        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        //initializing pagination
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();


        $data['title'] = 'Filament';

        $terms = $this->input->get('search');

        $data['search'] = $terms;   
        $data['postData'] = array(
            'searchBy' => ''
        );
        
        $data['category'] = $this->input->get('catName');
        $data['min'] = $this->input->get('min');
        $data['max'] = $this->input->get('max');
        $data['man'] = $this->input->get('man');
        $data['tech'] = $this->input->get('tech');
        $data['size'] = $this->input->get('size');
        $data['acc'] = $this->input->get('acc');
        $current_time = time();
        $data['current_time'] = $current_time;
        $this->load->view('sort/product_list', $data);
    }

    
}

