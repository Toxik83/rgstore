<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class adminInbox extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('contact_model');
    }
    
    public function index()
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Inbox' => ''
        ));
        $data = array();
        
        //setting the get parrameters
        $terms = $this->input->get('search');
        $option = $this->input->get('searchBy');

        if ($this->input->get() != false) {      
            //check if searchBy and search keys exist
            $option = (array_key_exists('searchBy', $this->input->get())) ? $this->input->get('searchBy') : '';
            $terms = (array_key_exists('search', $this->input->get())) ? $this->input->get('search') : '';
            
        }
        
        $like = array();
        
        //setting the $like condition. If no terms are selected $like is an empty array
        if ($this->input->get()) {
            if ($option == '' || $terms == '') {
                $like = array();
            } else {
                //chech the option value and handle it
                switch ($this->input->get('searchBy')) {
                    case 1: $option = 'From';
                        break;
                    case 3: $option = 'Subject';
                        break;
                    case 4: $option = 'Message';
                        break;
                    default : $option = 'From';
                }
                $like = array(
                    $option => $terms
                );    
            }
        }
        
        $conditions = array();
        $this->load->library('pagination');
        $total = $this->contact_model->getMessage($conditions, null, $like)->num_rows();
        
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1; //start page for pagination
        
        $pageLimit = 5;
        $max = array($pageLimit, ($page - 1) * $pageLimit);
        
        $messages = $this->contact_model->getMessage(null, $max, $like)->result();
        $data['messageData'] = $messages;

        //setting the $config array for the pagination
        $config = array();
        $config['base_url'] = base_url() . 'index.php/admin/admin-inbox';
        
        if ($this->input->get() != false) {
            $config['suffix'] = '?'.http_build_query($this->input->get(), '', '&');
            $config['first_url'] = base_url().'index.php/admin/admin-inbox/1'. $config['suffix'];
        }
       
        $config['total_rows'] = $total;
        $config['per_page'] = $pageLimit;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = true;
        $config['first_link'] = false;
        $config['last_link'] = false;

        //initializing pagination
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
    
        $data['search'] = $terms;
        $data['filter'] = $option;
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Inbox';
        
        $this->load->view('admin/inbox/listing', $data);
    }
    
    public function readMessage($id)
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Inbox' => 'admin/admin-inbox',
            'Message' => ''
        ));
        $data = array();
        
        $conditions = array(
            'id' => $id
        );
        $messageData = $this->contact_model->getMessage($conditions, null)->row();
        
        if ($messageData == null) {
            redirect('admin/admin-inbox');
        }
        
        $data['setToRead'] = array(
            'status' => 1
        );
        
        $this->contact_model->updateMessage($id, $data['setToRead']);
        
        $data['messageData'] = $messageData;
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Inbox';
        $this->load->view('admin/inbox/message', $data);
    }
    
    public function reply($id)
    {
        $conditions = array(
            'id' => $id,
        );
        $messageData = $this->contact_model->getMessage($conditions, null)->row();

        if ($messageData == null) {
            redirect('admin/admin-inbox');
        }
        
        $this->breadcrumb->populate(array
        (
            'Home' => 'admin/administration',
            'Inbox' => 'admin/admin-inbox',
            'Message' => 'admin/admin-inbox/message/'.$id,
            'Reply' => ''
        ));
        $data = array();
        $data['postData'] = array(
            'id' => $messageData->id,
            'from' => '',
            'to' => $messageData->from,
            'subject' => $messageData->subject,
            'message' => ''
        );
        
        $this->form_validation->set_rules('from', 'From', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('subject', 'Subject', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('message', 'Message', 'required|trim|min_length[3]');
        
        if ($this->input->post()) {
            $data['postData'] = array_merge($this->input->post() + array('id' =>$messageData->id));
            
            if ($this->form_validation->run()) {
                
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'rgatewaytest@gmail.com',
                    'smtp_pass' => 'rgatewaytest1q2w3e4r',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

                $this->email->from('rgatewaytest@gmail.com', $this->input->post('from'));
                $this->email->to($messageData->from);
                $this->email->subject($this->input->post('subject'));
                $this->email->message($this->input->post('message'));

                $result = $this->email->send();

                $this->session->set_flashdata('success', 'You have sent the email');
                redirect('admin/admin-inbox');
            }
        }
        
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Inbox';
        $this->load->view('admin/inbox/reply', $data);
    }

    public function ajaxDelete()
    {
        $id = $this->input->post('id');
        
        $conditions = array(
            'id' => $id
        );
        
        if ($this->contact_model->deleteMessage($id)) {
            $fields = array(
                'success' => 1
            );
            
        } elseif (!$this->contact_model->deleteMessage($id)) {
            $fields = array(
                'success' => 0
            );
        }
               
        echo json_encode($fields);
        die;
    }
    
    public function deleteMessage($messageId)
    {                      
        $conditions = array(
            'id' => $messageId
        );
        $limit = array();
        $like = array();
        $messageData = $this->contact_model->getMessage($conditions, $limit, $like)->row();
        
        if (empty($messageData)) {
            redirect('admin/admin-inbox');
        }
        
        $this->contact_model->deleteMessage($messageId);
        $this->session->set_flashdata('success', 'You have successfully deteleted the message');
        redirect('admin/admin-inbox');
    }
    
    public function bulkSelect() 
    {  
        $ids = $this->input->post('bulkSelect');
        
        if (!isset($ids) && empty($ids)) {
            redirect('admin/admin-inbox');
        }
        
        //on selected option delete
        if ($this->input->post('option') == 'Delete') {
            foreach ($ids as $key => $id) {
                $this->contact_model->deleteMessage($id);
                $this->session->set_flashdata('success', 'You have successfuly deleted the messages');
            }
            redirect('admin/admin-inbox');
        }
        
        //on selected option mark as read
        if ($this->input->post('option') == 'Mark as read') {
            foreach ($ids as $key => $id) {
                $data['setToRead'] = array(
                'status' => 1
                );
                
                $this->contact_model->updateMessage($id, $data['setToRead']);           
                $this->session->set_flashdata('success', 'Selected messages successfully set to read');
            }
            redirect('admin/admin-inbox');
        }
        
        //on selected option mark as unread
        if ($this->input->post('option') == 'Mark as unread') {
            foreach ($ids as $key => $id) {
                $data['setToUnread'] = array(
                'status' => 0
                );
                
                $this->contact_model->updateMessage($id, $data['setToUnread']);           
                $this->session->set_flashdata('success', 'Selected messages successfully set to unread');
            }
            redirect('admin/admin-inbox');
        }
    }
    
}
