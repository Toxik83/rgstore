<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Administration extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
        $data['title'] = 'Administration';
        $data['newMessages'] = $this->data['newMessages'];
        
        $this->load->view('admin/administration_v', $data);
    }

}
