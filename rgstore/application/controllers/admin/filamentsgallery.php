<?php

class Filamentsgallery extends MY_Controller {

    public $original_path;
    public $resized_path;
    public $thumbs_path;
    public $resized_path1;
    public $resized_path2;

    public function __construct() {
        parent::__construct();
        $this->load->model('filamentsgallery_model');
        $this->load->helper(array('form', 'url'));
        $this->original_path = realpath(APPPATH . '../upload/original');
        $this->resized_path3 = realpath(APPPATH . '../upload/images500x500');
        $this->resized_path = realpath(APPPATH . '../upload/images280x280');
        $this->thumbs_path = realpath(APPPATH . '../upload/thumbs');
        $this->resized_path1 = realpath(APPPATH . '../upload/images155x155');
        $this->resized_path2 = realpath(APPPATH . '../upload/images60x70');
    }

    public function index() {
        $this->load->helper('form', 'url');
        $this->load->library('form_validation');
    }

    public function do_upload() {
        $this->load->library('image_lib');
        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png',
            'max_size' => 2048,
            'upload_path' => $this->original_path,
            'file_name' => sha1(time())
        );

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            $this->session->set_flashdata('success', $this->upload->display_errors());
            return redirect('admin/filamentsgallery/loadGalleryPage/' . $this->input->post('filament_id'));
        } else {
            $data = array('upload_data' => $this->upload->data());
            $image_data = $data['upload_data'];
            $this->load->view('upload_success', $data);
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path,
                'maintain_ratio' => false,
                'width' => 280,
                'height' => 280
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path3,
                'maintain_ratio' => false,
                'width' => 500,
                'height' => 500
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->thumbs_path,
                'maintain_ratio' => false,
                'width' => 65,
                'height' => 65
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path1,
                'maintain_ratio' => false,
                'width' => 155,
                'height' => 155
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path2,
                'maintain_ratio' => false,
                'width' => 60,
                'height' => 70
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            return $this->upload->file_name;
        }
        return "";
    }

    public function upload_image() {
        $this->load->library('image_lib');
        $this->load->model('filamentsgallery_model');
        $this->load->view('admin/filamentsgallery');
        $this->$this->do_upload();
    }
	 public function edit() {
        $url = $this->do_upload();
        $id = $this->input->post('pic_id');
        $filament_id = $this->input->post('filament_id');
        $this->filamentsgallery_model->update($id, $url, $filament_id);
        redirect('admin/filamentsgallery/loadGalleryPage/' . $this->input->post('filament_id'));
    }

    public function loadGalleryPage() {
        $data['title'] = 'Filaments Gallery';
        $this->load->model('filamentsgallery_model');
        $filament_id = $this->uri->segment(4);
        $data['images'] = $this->filamentsgallery_model->showImageByFilament_Id($filament_id);
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/filaments_gallery', $data);
    }

    public function save($id = 0, $name = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('userfile', 'Userfile', 'required|jpg|jpeg|gif|png');
        if ($this->form_validation->run() == TRUE) {
            $this->session->set_flashdata('key', 'Your file type is not alowed to upload!');
            return redirect('admin/filamentsgallery/loadGalleryPage/' . $this->input->post('filament_id'));
        } else {
            $this->load->model('filamentsgallery_model');
           
            $id = $this->input->post('filament_id');
            $name = $this->do_upload();
            if ($name == '') {
                $this->session->set_flashdata('key', 'Upload only alowed file types: jpg, jpeg, gif, png!');
                return redirect('admin/filamentsgallery/loadGalleryPage/' . $this->input->post('filament_id'));
            } else {
                $this->session->set_flashdata('key', 'Your image file was successfully created');
              
                $this->filamentsgallery_model->save($name, $id);
                return redirect('admin/filamentsgallery/loadGalleryPage/' . $this->input->post('filament_id'));
            }
        }
    }

    public function update($id) {
        $data['title'] = 'Update Picture';
        $this->load->model('filamentsgallery_model');
        $data['images'] = $this->filamentsgallery_model->find($id);
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/filamentsgallery_edit', $data);
    }
    public function make_main_pic() {
        if ($this->input->post()) {
            $product_id = $this->input->post('product_id');
            $id = $this->input->post('pic_id');
            $this->filamentsgallery_model->mainPic($id);
            $this->filamentsgallery_model->unMakemainPic($id, $product_id);
            $this->session->set_flashdata('success', 'Your main picture was sucessfully changed!');
            redirect('admin/filamentsgallery/loadGalleryPage/' . $this->input->post('product_id'));
        }
    }

    public function ajaxReorder() {
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->filamentsgallery_model->reorder($key, $value);
            }
        } else {
            return false;
        }
        die;
    }

    public function ajaxDelete() {
        $id = $this->input->post('id');
        $check = $this->filamentsgallery_model->check($id);
        if ((int) $check->main_pic == 1) {
            $fields = array(
                'success' => 0
            );
        } else {
            $this->filamentsgallery_model->deleteImage($id);
            $fields = array(
                'success' => 1
            );
        }

        echo json_encode($fields);
        die;
    }

}
