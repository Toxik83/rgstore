<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Filament_category extends MY_Controller {

    public function __construct() {
        parent::__construct();
        //if (isLogged() == false) {
        //redirect('admin/home');
        //}
        $this->load->model('category_model');
    }

    public function index($offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Filament Category' => 'admin/filament_category',
            'Edit Filament Category' => ''
        ));

        $this->load->library('pagination');

        $search_term = $this->input->get('search');
        $data = array();
        $title = 'Categories';
        $data['title'] = $title;
        //$data['results'] = $result;
        $data['categories'] = $this->category_model->get_results($search_term, $offset, 6);

        $config = array();
        $config['base_url'] = base_url() . 'index.php/admin/filament_category/index';
        $config['total_rows'] = $data['categories']['count'];
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = 6;
        $config['num_links'] = 9;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        if ($this->uri->segment(4) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(4) - 1);
        }

        $links = $this->pagination->create_links();

        $this->load->view('admin/category', array(
            'categories' => $data['categories']['data'],
            'links' => $links,
            'title' => $title,
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term

                //'result' => $result
        ));
    }

    public function add($category_id = 0, $data = 0) {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Category Name', 'required|trim|xss_clean');
        $data = array(
            'name' => $this->input->post('name')
        );

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('category_key', 'Please fill Category field!');
            return redirect('admin/filament_category');
        } else {
            $this->load->model('category_model');
            $name = $this->input->post('name');

            $this->category_model->insert($name);
            $this->session->set_flashdata('my_key', '<h3><b>A New Category has been added </b></h3>');
            return redirect('admin/filament_category');
        }
    }

    public function update($id = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Filament Category' => 'admin/filament_category',
            'Edit Filament Category' => ''
        ));
        $this->load->model('category_model');
        $data['category'] = $this->category_model->find($id);

        $title = 'Edit Category';
        $this->load->view('admin/filament_category_edit', array(
            'title' => $title,
            'newMessages' => $this->data['newMessages'],
            'category' => $data['category']

                //'result' => $result
        ));
    }

    public function delete($id) {
        $this->load->model('category_model');
        $this->category_model->delete($id);
        return redirect('admin/filament_category');
    }

    public function edit_category() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Category Name', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('my key2', 'Please Edit Category Field!');
            return redirect('admin/filament_category/update/' . $this->input->post('id'));
        } else {
            $this->load->model('category_model');
            $id = $this->input->post('id');
            $name = $this->input->post('name');
            $this->category_model->update($id, $name);
            $this->session->set_flashdata('my_key2', '<h3><b>The category has been updated </b></h3>');
            return redirect('admin/filament_category');
        }
    }

    public function ajaxReorder() {
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->category_model->reorder($key, $value);
            }
        } else {
            return false;
        }

        die;
    }

}
