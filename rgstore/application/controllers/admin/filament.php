<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Filament extends MY_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->model('filament_model');
         $this->load->model('category_model');
        $this->load->model('manufacturer_model');
        $this->load->model('products_model');
         $this->load->model('filamentsgallery_model');
        $this->original_path = realpath(APPPATH . '../upload/original');
        $this->resized_path3 = realpath(APPPATH . '../upload/images500x500');
        $this->resized_path = realpath(APPPATH . '../upload/images280x280');
        $this->thumbs_path = realpath(APPPATH . '../upload/thumbs');
        $this->resized_path1 = realpath(APPPATH . '../upload/images155x155');
        $this->resized_path2 = realpath(APPPATH . '../upload/images60x70');
    }
    
    public function index($offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Filament'
        ));

        $this->load->library('pagination');
        $search_term = $this->input->get('search');
        $data = array();
        $title = 'Filaments';
        $data['title'] = $title;
        $data['filaments'] = $this->filament_model->get_results($search_term, $offset, 6);

        $config = array();
        $config['base_url'] = base_url() . '/admin/filament/index';
        $config['total_rows'] = $data['filaments']['count'];
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = 6;
        $config['num_links'] = 9;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['uri_segment'] = 4;
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if ($this->uri->segment(4) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(4) - 1);
        }
        
        $links = $this->pagination->create_links();
        $this->load->view('admin/filament', array(
            'filaments' => $data['filaments']['data'],
            'links' => $links,
            'title' => $title,
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term
        ));
    }
    //Dobavq filament product
    public function addFilament($filament_id = 0, $url = '') {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Filament' => 'admin/filament',
            'Add Filament'
        ));
        $data['title'] = 'Add Filament';
        $data['companies'] = $this->manufacturer_model->show_Company();
        $data['categories'] = $this->category_model->getCategories();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required|trim');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric|xss_clean|');
        $this->form_validation->set_rules('new_price', 'New Price', 'trim|numeric|xss_clean|');
        //IF Begins
        if ($this->input->post()) {
            //IF for validation
            if ($this->form_validation->run() == FALSE) {
                redirect('admin/filament/addFilament');
            }
            //else if
            else {
                $title = $this->input->post('title');
                $text = $this->input->post('text');
                $price = $this->input->post('price');
                $new_price = $this->input->post('new_price');
                $manufacturer = $this->input->post('manufacturer');
                $f_cat_id = $this->input->post('cat_id');
                $this->filament_model->create($title, $f_cat_id, $text, $price, $new_price, $manufacturer);
                $filament_id = $this->db->insert_id();
                $this->session->set_flashdata('my_key', '<h3><b>A New Filament has been added </b></h3>');
            }
            //end else
            $this->load->library('form_validation');
            $this->form_validation->set_rules('userfile', 'Userfile', 'required|jpg|jpeg|gif|png');
            //IF for upload image
            if ($this->input->post()) {
                if ($this->form_validation->run() == TRUE) {
                    $this->session->set_flashdata('my key', 'Your file type is not alowed to upload!');
                    $this->load->view('admin/addFilament');
                }
                //else begins
                else {
                    $url = $this->do_upload();
                    if ($url == '') {
                        $this->session->set_flashdata('my key', 'Upload only alowed file types: jpg, jpeg, gif, png!');
                        return redirect('admin/filament/addFilament');
                    }
                    //else for success
                    else {
                        $this->session->set_flashdata('my_key', 'Your image file was successfully created');
                        $main_pic = 1;
                        $this->filamentsgallery_model->saveMainPic($url, $filament_id, $main_pic);
                    }
                    redirect('admin/filament');
                }
                //else ends
            }
            //end for upload
            $data['newMessages'] = $this->data['newMessages'];
            $this->load->view('admin/addFilament', $data);
        }
        //IF ENDS
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/addFilament', $data);
    }

    //Update a filament product
    public function updateFilament($id) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Filament' => 'admin/Filament',
            'Edit Filament'
        ));
        $data['title'] = 'Edit Filament';
        $this->load->model('manufacturer_model');
        $this->load->model('category_model');
        $data['newMessages'] = $this->data['newMessages'];
        $data['companies'] = $this->manufacturer_model->show_Company();
        $data['categories'] = $this->category_model->getCategories();
       
        $data['filament'] = $this->filament_model->showFilamentById($id);
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required|trim');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric|xss_clean|');
        $this->form_validation->set_rules('new_price', 'New Price', 'trim|numeric|xss_clean|');
        //IF begins
        if ($this->input->post()) {
            $data['postData'] = $this->input->post();
            if ($this->form_validation->run() == FALSE) {
              $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
                redirect('admin/filament/updateFilament/' . $this->input->post('id'));
            }
            //else begins
            else {
                $id = $data['postData']['id'];
                $title = $data['postData']['title'];
                $text = $data['postData']['text'];
                $price = $data['postData']['price'];
                $new_price = $data['postData']['new_price'];
                $manufacturer = $data['postData']['manufacturer'];
                $category = $data['postData']['category'];
                $date_update = date('Y-m-d H:i');
                $this->filament_model->update($id, $title, $text, $price, $new_price, $manufacturer, $category, $date_update);
                $this->session->set_flashdata('success', 'You have successfully edited '
                        . $this->input->post('title') . ' with ID : '
                        . $this->input->post('id') . ' !');
               
                if ($data['filament']->active == 0) {
                    
                    redirect('admin/filament');
                } else {
                    redirect('admin/filament/showInactive');
                }
            }
            //else ends
        }
        //IF Ends
        $this->load->view('admin/filament_edit', $data);
    }
    //Upload a picture
    public function do_upload() {
        $this->load->library('image_lib');
        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png',
            'max_size' => 2048,
            'upload_path' => $this->original_path,
            'file_name' => sha1(time())
        );
        $this->load->library('upload', $config);
        //if begins
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('admin/addprinter_v', $error);
        }
        //if ends
        else {
            $data = array('upload_data' => $this->upload->data());
            $image_data = $data['upload_data'];
            $this->load->view('upload_success', $data);
            //config  for 280 * 280
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path,
                'maintain_ratio' => false,
                'width' => 280,
                'height' => 280
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            //confif for 500*500
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path3,
                'maintain_ratio' => false,
                'width' => 500,
                'height' => 500
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            //config for 65*65
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->thumbs_path,
                'maintain_ratio' => false,
                'width' => 65,
                'height' => 65
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            //config for 155*155
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path1,
                'maintain_ratio' => false,
                'width' => 155,
                'height' => 155
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            //config for 60*70
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path2,
                'maintain_ratio' => false,
                'width' => 60,
                'height' => 70
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            return $this->upload->file_name;
        }
        return "";
    }

    public function delete($filament_id = 0) {
        $this->load->model('filament_model');
        $filament_id = (int) $filament_id;
        
        if (!$filament_id) {
            redirect('/admin/filament');
        }
        $this->filament_model->delete($filament_id);
        $this->session->set_flashdata('deleted', 'The product is inactive ');
        redirect('admin/filament');
    }

    //Skriva producta ot viewto ,no ne go iztriva ot bazata
    public function softdelete($id) {
       
       
        $conditions = array(
            'p.active' => 0,
            'bi.product_id' => $id,
            'p.cat_id' => 4,
        );
        $fields = array('b.product_id');
        $bundleItems = $this->products_model->isProductInBundle($conditions, $fields)->result();
        if (empty($bundleItems)) {
            $this->filament_model->deleteInactive($id);
            $this->session->set_flashdata('success', 'You have successfully deleted the product');
        } else {
            $this->session->set_flashdata('success', 'This product is part of a Bundle. Edit the bundle before continuing.');
        }
        redirect('admin/filament');
    }

    public function flashdate() {
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Filament' => 'admin/filament',
            'Add Flash Date' => ''
        ));
        $data = array();
        $data['title'] = 'Flash Date';
        $id = $this->uri->segment(4);
        //IF begins
        if ($this->input->post()) {
            $this->form_validation->set_rules('new_price', 'New price', 'trim|numeric|xss_clean');
            $data = array(
                'id' => $this->input->post('id'),
                'fl_date_begin' => $this->input->post('fl_date_begin'),
                'fl_date_end' => $this->input->post('fl_date_end')
            );
           
            $newPrice = $this->input->post('new_price');
            $this->filament_model->editPromo($data['id'], $newPrice);
            $data['newprice'] = $this->filament_model->newPriceRow($data['id']);
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
                redirect('admin/filament/flashdate/' . $data['id']);
            } else if ($data['fl_date_begin'] == '' || $data['fl_date_end'] == '') {
                $this->session->set_flashdata('error', 'Empty field is not allowed !');
                redirect('admin/filament/flashdate/' . $data['id']);
            } else if ($data['fl_date_begin'] > $data['fl_date_end']) {
                $this->session->set_flashdata('error', 'Flash Date (begin) cannot be lower than Flash Date (end) !');
                redirect('admin/filament/flashdate/' . $data['id']);
            } else if ($data['newprice']->new_price == "0.00") {

                $this->session->set_flashdata('error', 'You cannot create flashdate . First need to add New Price ! ');
                redirect('admin/filament/flashdate_edit/' . $data['id']);
            }
            $id=$this->input->post('id');
            $data['filament']=$this->filament_model->showFilamentById($id);
            $this->filament_model->setFlashdata($data);
            $this->session->set_flashdata('success', 'You have successfully  add flashdate to filament with id: ' . $this->input->post('id'));
            if ($data['filament'] ->active == 0) {
            redirect('admin/filament');
            } else {
                redirect('admin/filament/showInactive');
            }
        }
        //IF Ends
        $data['sources'] = $this->filament_model->showFilamentById($id);
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/filament_flashdate_v', $data);
    }

    //Update a flashdate
    public function flashdate_edit() {
        $data = array();
        $data['title'] = 'Flash Date';
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Filament' => 'admin/filament',
            'Edit Flash Date' => ''
        ));
        $id = $this->uri->segment(4);
        //if begins
        if ($this->input->post()) {
            $data = array(
                'id' => $this->input->post('id'),
                'fl_date_begin' => $this->input->post('fl_date_begin'),
                'fl_date_end' => $this->input->post('fl_date_end')
            );
             $newPrice = $this->input->post('new_price');
            $this->filament_model->setPromo($data['id'],$newPrice);
            $data['newprice'] = $this->filament_model->newPriceRow($data['id']);
         
            if ($data['fl_date_begin'] == '' || $data['fl_date_end'] == '') {
                $this->session->set_flashdata('success', 'Empty field is not allowed !');
                redirect('admin/filament/flashdate_edit/' . $data['id']);
            } else if ($data['fl_date_begin'] > $data['fl_date_end']) {
                $this->session->set_flashdata('success', 'Flash Date (begin) cannot be lower than Flash Date (end) !');
                redirect('admin/filament/flashdate_edit/' . $data['id']);
            } else if ($data['newprice']->new_price == "0.00") {
                $this->session->set_flashdata('success', 'You cannot create flashdate . First need to add New Price ! ');
                redirect('admin/filament/flashdate_edit/' . $data['id']);
            }
            $this->filament_model->setFlashdata($data);
            $this->session->set_flashdata('success', 'You have successfully  updated flashdate to filament with id: ' . $this->input->post('id'));
            redirect('admin/filament');
        }
        //if ends
        $data['sources'] = $this->filament_model->flashSource($id);
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/filament_flashdate_edit_v', $data);
    }
    //iztriva s ajax
    public function ajaxDelete() {
        $id = $this->input->post('id');
        $conditions = array(
            'p.active' => 0,
            'bi.product_id' => $id,
            'p.cat_id' => 4,
        );
        $fields = array('b.product_id');
        $bundleItems = $this->products_model->isProductInBundle($conditions, $fields)->result();
        if (empty($bundleItems)) {
            if ($this->filament_model->deleteData($id)) {
                $fields = array(
                    'success' => 1
                );
            } elseif (!$this->filament_model->deleteData($id)) {
                $fields = array(
                    'success' => 0
                );
            }
        } else {
            $fields = array(
                'success' => 2
            );
        }
        echo json_encode($fields);
        die;
    }

    //Iztriva vsi4ki izbrani 
    public function bulkDelete() {
       
        $ids = $this->input->post('bulkDelete');
       
        if ($ids == false) {
            $this->session->set_flashdata('success', 'You need to choose items first !');
            redirect('admin/filament');
        }
        foreach ($ids as $key => $id) {
            $this->filament_model->deleteData($id);
            $this->session->set_flashdata('success', 'You have successfully deleted selected filament products');
        }
        redirect('admin/filament');
    }
    public function new_Price() {
        $id = $this->input->post('new_price_id');
        $newPrice = $this->input->post('new_price');
        $this->form_validation->set_rules('new_price', 'New price', 'trim|numeric|xss_clean');
        $filament = $this->filament_model->showFilamentById($id);
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('success', validation_errors());
            redirect('admin/filament');
        } else if ($filament->fl_date_begin != '0000-00-00 00:00:00' && ($newPrice == '0.00' || $newPrice == '')) {
            $this->session->set_flashdata('success', 'You cannot set New Price to 0.00 when there is Flash Date !');
            redirect('admin/filament');
        } else {
            $this->filament_model->editPromo($id, $newPrice);
            $this->session->set_flashdata('success', 'You have successfully  updated new price to filament with id: ' . $id);
            redirect('admin/filament');
        }
    }
     public function flashDelete($id) {
        $id = $this->uri->segment(4); 
        $this->filament_model->deleteFlash($id);
        $this->session->set_flashdata('success', 'Flash Date was deleted successfully');
        redirect('admin/filament');
    }
    public function showInactive($offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Filament (Active Filament)' => 'admin/filament',
            'Show Inactive Filament'
        ));
        $this->load->library('pagination');
        $search_term = $this->input->get('search');
        $data = array();
        $title = 'Filament Page Inactive Products';
        $data['title'] = $title;
        $data['filaments'] = $this->filament_model->get_resultsInactive($search_term, $offset, 6);
        
        $config = array();
        $config['base_url'] = base_url() . 'index.php/admin/filament/inactive';
        $config['total_rows'] = $data['filaments']['count'];
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = 6;
        $config['num_links'] = 9;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        if ($this->uri->segment(4) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(4) - 1);
        }
        $links = $this->pagination->create_links();
        $this->load->view('admin/filament_inactive', array(
            'filaments' => $data['filaments']['data'],
            'links' => $links,
            'title' => $title,
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term
        ));
    }
    
    public function active($id) {
        $this->filament_model->makeActive($id);
        redirect('admin/filament/showInactive');
    }
     public function editPromoPrice() {
        $id = $this->input->post('filament_id');
        $new_price = $this->input->post('new_price');
        $data['filament'] = $this->filament_model->showFilamentById($id);
        $this->filament_model->editPromo($id, $new_price);
            if($data['filament']->active == 0){
                    redirect('admin/filament');
            } else {
                redirect('admin/filament/showInactive');
            }
    }
}
