<?php

class Building_size extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('building_size_model');
    }

    public function index($offset = 0) {
        $this->load->library('pagination');
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Add Building size'
        ));
        
        $data['title'] = 'Building Size ';
        $keyword = $this->input->get('keyword');
        $data['sizes'] = $this->building_size_model->show_Admin_Size($keyword,$offset,8);
        
        $config = array();
        $config['base_url'] = base_url() . 'admin/building_size/index';
        $config['suffix'] = '?search=' . $keyword;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['total_rows'] = $data['sizes']['count'];
        $config['per_page'] = 8;
        $config['num_links'] = 8;
        $config['uri_segment'] = 4;

        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();
        $data['newMessages'] = $this->data['newMessages'];
        
        $this->load->view('admin/building_size_add', array(
            'title' => $data['title'],
            'sizes' => $data['sizes']['data'],
            'newMessages' => $data['newMessages'],
            'links' => $data['links'],
        ));
    }

    public function add() {
        $this->load->library('form_validation');
        $name = $this->input->post('build_name');
        $is_duplicate = $this->building_size_model->check_unique($name);
        $this->form_validation->set_rules('build_name', 'Size', 'required|trim|xss_clean');
        if ($this->input->post()) {
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('build_key', 'Please fill building size!');
                redirect('admin/building_size');
            } elseif ($is_duplicate) {
                $this->session->set_flashdata('build_key', 'This size already exist! ');
                redirect('admin/building_size');
            } else {
                $this->building_size_model->create($name);
                $this->session->set_flashdata('build_key', 'Success! New size has been added!');
                redirect('admin/building_size');
            }
        }
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/building_size_add', $data);
    }

    public function update($id) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Building size' => 'admin/building_size',
            'Edit Building size'
        ));
        $data['sizes'] = $this->building_size_model->find($id);
        $data['title'] = 'Edit Building Size';
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/building_size_edit', $data);
    }

    public function delete($id) {
        $data['result'] = $this->building_size_model->countRecords($id);
            if($data['result']['count'] == 0) {
                $this->building_size_model->delete($id);
                $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                redirect('admin/building_size');
            } else {
                $this->session->set_flashdata('error', 'You have products in your category!');
                redirect('admin/building_size');
            }
    }

    public function edit_building_size() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('build_name', 'Sizes', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'Please Edit Building Size!');
            return redirect('admin/building_size/update/' . $this->input->post('build_id'));
        } else {
            $id = $this->input->post('build_id');
            $name = $this->input->post('build_name');
            $this->building_size_model->update($id, $name);
            $this->session->set_flashdata('success', 'Success! The accuracy has been edited!');
            
            return redirect('admin/building_size');
        }
    }

    public function ajaxReorder() {
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->building_size_model->reorder($key, $value);
            }
        } else {
            return false;
        }

        die;
    }
    public function bulkDelete() {
        $ids = $this->input->post('bulkDelete');
        
        if ($ids == false) {
            $this->session->set_flashdata('error', 'You need to choose items first !');
            redirect('admin/building_size');
        } else {
            foreach ($ids as $id) {
                
            $data['result'] = $this->building_size_model->countRecords($id);
                if($data['result']['count'] == 0) {
                    $this->building_size_model->delete($id);
                    $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                    redirect('admin/building_size');
                } else {
                    $this->session->set_flashdata('error', 'You have products in your category!');
                    redirect('admin/building_size');
                }
            }
        }
    }

}
