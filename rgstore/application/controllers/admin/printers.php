<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Printers extends MY_Controller {
    
        protected $original_path;
        protected $resized_path;
        protected $thumbs_path;
        protected $resized_path1;
        protected $resized_path2;
        protected $resized_path3;
        protected $resized_path5;
        
    public function __construct() {
        parent::__construct();
        $this->load->model('printers_m');
        $this->load->model('printersgallery_model');
        $this->load->model('products_model');
        $this->load->model('manufacturer_model');
        $this->load->model('print_tech_model');
        $this->load->model('building_size_model');
        $this->load->model('accuracy_model');
        $this->load->model('price_m');
        $this->load->helper(array('form', 'url'));
        $this->original_path = realpath(APPPATH . '../upload/original');
        $this->resized_path3 = realpath(APPPATH . '../upload/images500x500');
        $this->resized_path = realpath(APPPATH . '../upload/images280x280');
        $this->thumbs_path = realpath(APPPATH . '../upload/thumbs');
        $this->resized_path1 = realpath(APPPATH . '../upload/images155x155');
        $this->resized_path2 = realpath(APPPATH . '../upload/images60x70');
    }
    
    public function index($offset = 0) { 
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers'
        ));

        $price_range = $this->price_m->getWhatYouNeed();
        $companies = $this->manufacturer_model->show_Company();       
        $print_tech = $this->print_tech_model->show_Print_tech();        
        $sizes = $this->building_size_model->show_Size();
        $accuracies = $this->accuracy_model->show_Accuracy();        
        $this->load->library('pagination');
        $search_term = $this->input->get('search');
        $data = array();
        $title = 'Printers Page Active Products';
        $data['title'] = $title;
        $data['printers'] = $this->printers_m->get_results($search_term, $offset, 6);    
        $config = array();
        $config['base_url'] = base_url() . 'index.php/admin/printers/index';
        $config['total_rows'] = $data['printers']['count'];
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = 6;
        $config['num_links'] = 9;
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        if ($this->uri->segment(4) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(4) - 1);
        }
        $links = $this->pagination->create_links();
//        $this->createUploadFolder();
        $this->load->view('admin/printers_v', array(
            'printers' => $data['printers']['data'],
            'links' => $links,
            'title' => $title,
            'prices' =>$price_range,
            'companies'=>$companies,
            'print_tech'=> $print_tech,
            'sizes' => $sizes,
            'accuracies' => $accuracies,
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term
        ));
    }

    public function input($printer_id = 0, $file = '') {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Add Printer'
        ));
        $data['title'] = 'Add Printer';
        $data['companies'] = $this->manufacturer_model->show_Company();
        $data['print_tech'] = $this->print_tech_model->show_Print_tech();
        $data['sizes'] = $this->building_size_model->show_Size();
        $data['accuracies'] = $this->accuracy_model->show_Accuracy();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required|trim');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric|greater_than[0]| xss_clean|');
        $this->form_validation->set_rules('new_price', 'New Price', 'trim|numeric|xss_clean|');
        $this->form_validation->set_rules('manufacturer', 'Manufacturer', 'trim|required|xss_clean|');
        $this->form_validation->set_rules('print_tech', 'Printers Technology', 'trim|required|xss_clean|');
        $this->form_validation->set_rules('building_size', 'Building Size', 'trim|required|xss_clean|');
        $this->form_validation->set_rules('accuracy', 'Accuracy', 'trim|required|xss_clean|');
        if ($this->input->post()) {
            if ($this->form_validation->run() == FALSE) {    
                
            } else {
                $title = $this->input->post('title');
                $text = $this->input->post('text');
                $price = $this->input->post('price');
                $new_price = $this->input->post('new_price');
                $manufacturer = $this->input->post('manufacturer');
                $print_tech = $this->input->post('print_tech');
                $build_sizes = $this->input->post('building_size');  
                $accuracy = $this->input->post('accuracy');
                $cat = 1;
                
                $printer = array(
                            'title' => $title,
                            'text' => $text,
                            'price' => $price,
                            'new_price' => $new_price,
                            'man_id' => $manufacturer,
                            'print_id' => $print_tech,
                            'build_id' => $build_sizes,
                            'acc_id' => $accuracy,
                            'date_added' => date('Y-m-d H:i'),
                            'cat_id' => $cat
                            );
                $this->printers_m->insert($printer);
                $printer_id = $this->db->insert_id();
                if ($this->do_upload() === "") {
                    $this->session->set_flashdata('error', $this->upload->display_errors('<div class="error">', '</div>'));
                    redirect('admin/printers/input');
                }
                
                $file = $this->do_upload();
                $main_pic = 1;
                $this->printersgallery_model->saveMainPic($file, $printer_id, $main_pic);
                $this->session->set_flashdata('success', 'New printer has been added');        
                redirect('admin/printers');
            }
        }
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/addprinter_v', $data);
    
    }
    
//    public function createUploadFolder(){
//        $file = '/var/www/rgstore/upload';
//
//        if(!is_dir($file)){
//            mkdir($file, 0777, true);
//
//            }
//        $pathToUpload = '/var/www/rgstore/upload/original';
//        $pathToUpload1 = '/var/www/rgstore/upload/images500x500';
//        $pathToUpload2 = '/var/www/rgstore/upload/images280x280';
//        $pathToUpload3 = '/var/www/rgstore/upload/thumbs';
//        $pathToUpload4 = '/var/www/rgstore/upload/images155x155';
//        $pathToUpload5 = '/var/www/rgstore/upload/images60x70';
//        $pathToUpload6 = '/var/www/rgstore/upload/home-slider';
//
//        if (!file_exists($pathToUpload) || ! file_exists($pathToUpload1) || ( !file_exists($pathToUpload2) || ! file_exists($pathToUpload3)) || ( !file_exists($pathToUpload4) || ! file_exists($pathToUpload5) || ! file_exists($pathToUpload6))) {
//               $create1 = mkdir($pathToUpload, 777);
//               $create2 = mkdir($pathToUpload1, 777);
//               $create3 = mkdir($pathToUpload2, 777);
//               $create4 = mkdir($pathToUpload3, 777);
//               $create5 = mkdir($pathToUpload4, 777);
//               $create6 = mkdir($pathToUpload5, 777);
//               $create7 = mkdir($pathToUpload6, 777);
//        }
//
//        return;
//    }

    public function do_upload() {
        $this->load->library('image_lib');
        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png',
            'max_size' => 2048,
            'upload_path' => $this->original_path,
            'file_name' => sha1(time())
        );
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            $this->session->set_flashdata('error_validation', $this->upload->display_errors('<div class="error">', '</div>'));
            redirect('admin/printers/input');
        } else {
            $data = array('upload_data' => $this->upload->data());
            $image_data = $data['upload_data'];
            $this->load->view('upload_success', $data);
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path,
                'maintain_ratio' => false,
                'width' => 280,
                'height' => 280
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path3,
                'maintain_ratio' => false,
                'width' => 500,
                'height' => 500
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->thumbs_path,
                'maintain_ratio' => false,
                'width' => 65,
                'height' => 65
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path1,
                'maintain_ratio' => false,
                'width' => 155,
                'height' => 155
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path2,
                'maintain_ratio' => false,
                'width' => 60,
                'height' => 70
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
            return $this->upload->file_name;
        }

        return "";
    }

    public function delete($id) {
        $this->printers_m->delete($id);
        redirect('admin/printers');
    }

    public function update($id) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Edit Printer'
        ));
        $data['title'] = 'Edit Printer';

        $data['newMessages'] = $this->data['newMessages'];
        $data['companies'] = $this->manufacturer_model->show_Company();
        $data['print_tech'] = $this->print_tech_model->show_Print_tech();
        $data['sizes'] = $this->building_size_model->show_Size();
        $data['accuracies'] = $this->accuracy_model->show_Accuracy();
        
        $fields = array(
                'id', 'man_id', 'print_id', 'acc_id','build_id', 'price',
                'new_price','text', 'title'
                );
        $data['printer'] = $this->printers_m->showPrintersById($id, $fields);
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required|trim |xss_clean|');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric|greater_than[0]|xss_clean|');
        $this->form_validation->set_rules('new_price', 'New Price', 'trim|numeric|xss_clean|');

        if ($this->input->post()) {
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $id = $this->input->post('printer_id');
                $title = $this->input->post('title');
                $text = $this->input->post('text');
                $price = $this->input->post('price');
                $new_price = $this->input->post('new_price');
                $manufacturer = $this->input->post('manufacturer');
                $print_tech = $this->input->post('print_tech');
                $build_sizes = $this->input->post('building_size');
                $accuracy = $this->input->post('accuracy');
                $date_update = date('Y-m-d H:i');
                $printer = array();
                $printer = array(
                                'title' => $title,
                                'text' => $text,
                                'price' => $price,
                                'new_price' => $new_price,
                                'man_id' => $manufacturer,
                                'print_id' => $print_tech,
                                'build_id' => $build_sizes,
                                'acc_id' => $accuracy,
                                'date_update' => $date_update
                            );
                $this->printers_m->update($id, $printer);
                $this->session->set_flashdata('success', 'You have successfully edited 
                    product with ID : ' . $id . ' !');
                    if($data['printer']->active == 0){
                        redirect('admin/printers');
                    } else {
                        redirect('admin/printers/showInactive');
                    }

                
            }
        }
        $this->load->view('admin/printers_edit', $data);
    }

    public function flashdate() {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Add Flash date'
        ));
        $id = $this->uri->segment(4);
        $data['flashdates'] = $this->printers_m->showFlashDateById($id);
        $data['newprice'] = $this->printers_m->findNewPrice($id);
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Add Flash Date';
        $this->load->view('admin/flashdate_view', $data);
    }
    
    public function inputFlashDate() {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Add Flash date'
        ));
        $id = $this->input->post('printer_id');
        $new_price = $this->input->post('new_price');
        $fl_date_begin = $this->input->post('fl_date_begin');
        $fl_date_end = $this->input->post('fl_date_end');
        $date_update = date('Y-m-d H:i');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fl_date_begin', 'Flash date begin', 'required');
        $this->form_validation->set_rules('fl_date_end', 'Flash date end', 'required|callback__check_lessthan');
        $this->form_validation->set_rules('new_price', 'New Price', 'trim|required|numeric|greater_than[0]|xss_clean|');
        if ($this->input->post()) {
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
                redirect('admin/printers/flashdate/'.$this->input->post('printer_id'));
            } elseif 
                 (($fl_date_begin != '') && ($fl_date_end > $fl_date_begin)) {
            $this->printers_m->insertFlDate($id, $new_price, $fl_date_begin, $fl_date_end, $date_update );
            $this->session->set_flashdata('success', 'Flash Date was created properly');
            $fields = array('active');
            $data['printer'] = $this->printers_m->showPrintersById($id, $fields);
            if ($data['printer'] ->active == 0) {
            redirect('admin/printers');
            } else {
                redirect('admin/printers/showInactive');
            }
            }
        
        }        
        $data['newprice'] = $this->printers_m->findNewPrice($id);
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Add Flash Date';
        $this->load->view('admin/flashdate_view', $data);
    }

    public function flashdate_edit() {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Edit Flash Date'
        ));
        $data = array();
        $id = $this->uri->segment(4);
        
        $data['newprice'] = $this->printers_m->findNewPrice($id);
        $data['flashdates'] = $this->printers_m->showFlashDateById($id);
        $data['title'] = 'Edit Flash Date';
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/flashdate_edit', $data);
    }

    
  
    public function updateFlDate(){
        $id = $this->input->post('printer_id');
        $new_price = $this->input->post('new_price');
        $fl_date_begin = $this->input->post('fl_date_begin');
        $fl_date_end = $this->input->post('fl_date_end');
        $date_update = date('Y-m-d H:i');
        $data['flashdates'] = $this->printers_m->showFlashDateById($id);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fl_date_begin', 'Flash date begin', 'required');
        $this->form_validation->set_rules('fl_date_end', 'Flash Date End', 'required|callback__check_lessthan');
        $this->form_validation->set_rules('new_price', 'New price', 'trim|required|numeric|greater_than[0]|xss_clean|');
        
        if ($this->input->post()) {
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
                redirect('admin/printers/flashdate_edit/'.$id);
            } else {
                $this->printers_m->insertFlDate($id,$new_price, $fl_date_begin, $fl_date_end,  $date_update);
                $this->session->set_flashdata('success', 'You have successfully  updated flashdate to printer with id: ' .$id);
                if($data['flashdates']->active ==0) {
                redirect('admin/printers');
                } else {
                    redirect('admin/printers/showInactive');
                }
            }
        }
    }
  
    public function flDateDelete($id) {
        $id = $this->uri->segment(4);
        $this->printers_m->deleteFlDate($id);
        $this->session->set_flashdata('success', 'Flash Date was deleted successfully');
        redirect('admin/printers');
    }

    public function editPromoPrice() {
        $id = $this->input->post('printer_id');
        $new_price = $this->input->post('new_price');
        $data['flashdates'] = $this->printers_m->showFlashDateById($id);
        $this->printers_m->editPromo($id, $new_price);
            if($data['flashdates']->active == 0){
                    redirect('admin/printers');
            } else {
                redirect('admin/printers/showInactive');
            }
    }

    public function showInactive($offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers (Active Printers)' => 'admin/printers',
            'Show Inactive Printers'
        ));
        $this->load->library('pagination');
        $search_term = $this->input->get('search');
        $data = array();
        $title = 'Printers Page Inactive Products';
        $data['title'] = $title;
        $data['printers'] = $this->printers_m->get_resultsInactive($search_term, $offset, 6);
        $config = array();
        $config['base_url'] = base_url() . 'index.php/admin/printers/inactive';
        $config['total_rows'] = $data['printers']['count'];
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = 6;
        $config['num_links'] = 9;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        if ($this->uri->segment(4) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(4) - 1);
        }
        $links = $this->pagination->create_links();
        $this->load->view('admin/printers_v_inactive', array(
            'printers' => $data['printers']['data'],
            'links' => $links,
            'title' => $title,
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term
        ));
    }

    public function active($id) {
        $this->printers_m->makeActive($id);
        redirect('admin/printers/showInactive');
    }

    public function softdelete($id) {
        $conditions = array(
            'p.active' => 0,
            'bi.product_id' => $id,
            'p.cat_id' => 4,
        );
        
        $fields = array('b.product_id');
        
        $bundleItems = $this->products_model->isProductInBundle($conditions, $fields)->result();
        
        if (empty($bundleItems)) {
            $this->printers_m->del($id);
            $this->session->set_flashdata('success', 'You have successfully deleted the product');
        } else {
            $this->session->set_flashdata('success', 'This product is part of a Bundle. Edit the bundle before continuing.');
        }
        
        redirect('admin/printers');
    }
    
    public function bulkDelete() {
        $ids = $this->input->post('bulkDelete');

        if ($ids == false) {
            $this->session->set_flashdata('error', 'You need to choose items first !');
            redirect('admin/printers');
        }

        foreach ($ids as $key => $id) {
            $this->printers_m->del($id);
            $this->session->set_flashdata('success', 'You have successfully deleted selected products');
        }
            $fields = array('active');
            $results = $this->printers_m->showPrintersById($id, $fields);
            if($results->active == 0){
            redirect('admin/printers');
            } else {
                redirect('admin/printers/showInactive');
            }
    }

    public function _check_lessthan() {
        if (($this->input->post('fl_date_begin'))>($this->input->post('fl_date_end'))) {
            $this->form_validation->set_message('_check_lessthan', 'Flash Date End cannot be lower than Flash Date Begin!');
            return false;
        } else {
            return true;
        }
    }
}
