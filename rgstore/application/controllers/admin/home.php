<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();     
        $this->load->model('admin_m');
        $this->load->model('chat_m');
    }

    public function index() {

        $data = array();
        $data['title'] = 'Login';

        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );

        $this->load->view('admin/admin', $data);
    }

    public function logout() {
        $this->chat_m->setOffline();
        $this->session->sess_destroy();
        redirect('admin');
    }

    public function login() {

        $this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('pass', 'Password', 'required|trim|xss_clean');

        //Ако не успее да се стартира , препраща към страницата за log in- а 
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('empty_f', 'The username and password fields are required.');
            $this->index();

        } else {
            
            $data['title'] = 'Admin panel';

            $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->hash($this->input->post('pass'))
            );


            $query = $this->admin_m->check($data);

            if ($query) {
                $userData = array(
                    'username' => $this->input->post('username'),
                    'is_admin' => TRUE
                );
                
                $this->session->set_userdata($userData);
                $this->chat_m->setOnline();
                redirect('admin/administration');
            } else {
                $this->session->set_flashdata('error', 'Invalid username or password.');
                redirect('admin');
            }
        }
    }

    public function hash($string) {
        return hash("sha1", $string . config_item('encryption_key'));
    }

}
