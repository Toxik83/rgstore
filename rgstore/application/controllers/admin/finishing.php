<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Finishing extends MY_Controller {

    protected $original_path;
    protected $resized_path;
    protected $thumbs_path;
    protected $resized_path1;
    protected $resized_path2;
    protected $resized_path3;

    public function __construct() {
        parent::__construct();
        $this->load->model('finishing_m');
        $this->load->model('price_m');
        $this->load->model('gallery_m');
        $this->load->model('manufacturer_model');
        $this->load->model('products_model');
        $this->load->model('printers_m');

        $this->original_path = realpath(APPPATH . '../upload/original');
        $this->resized_path = realpath(APPPATH . '../upload/images280x280');
        $this->thumbs_path = realpath(APPPATH . '../upload/thumbs');
        $this->resized_path1 = realpath(APPPATH . '../upload/images155x155');
        $this->resized_path2 = realpath(APPPATH . '../upload/images60x70');
        $this->resized_path3 = realpath(APPPATH . '../upload/images500x500');
    }

    public function index($offset = 0) {

        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Finishing' => ''
        ));

        $data = array();
        $data['title'] = 'Finishing';
        $this->load->library('pagination');

        $currentTime = date("Y-m-d H:i:s");
        $search_term = $this->input->get('search');

        $data['finishings'] = $this->finishing_m->getFinishings($search_term, $offset, 6);

        $config = array();
        $config['base_url'] = base_url() . 'admin/finishing/index/';
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['total_rows'] = $data['finishings']['count'];
        $config['per_page'] = 6;
        $config['num_links'] = 6;
        $config['uri_segment'] = 4;

        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';


        $currentTime = date("Y-m-d H:i:s");
        
        $products = $this->printers_m->printersListFlash(); 
        
        foreach ($products as $product) {
            if((($product->fl_date_begin) != 0 ) && ($product->fl_date_end != 0 )){
            if ($currentTime >= $product->fl_date_end) {
                $field = array(
                    'new_price' => '0.00',
                    'fl_date_begin' => 0,
                    'fl_date_end' => 0
                );
                $this->printers_m->resetNewPrice($product->id, $field);
                }
            }
        }

        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();

        $data['newMessages'] = $this->data['newMessages'];

        $this->load->view('admin/finishing/finishing_v', array(
            'title' => $data['title'],
            'finishings' => $data['finishings']['data'],
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term,
            'links' => $links,
        ));
    }
    
    public function inactive($offset = 0){
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Finishing' => 'admin/finishing',
            'Inactive' => ''
        ));

        $data = array();
        $data['title'] = 'Inactive';
        $this->load->library('pagination');

        $currentTime = date("Y-m-d H:i:s");
        $search_term = $this->input->get('search');

        $data['finishings'] = $this->finishing_m->getFinishingsInactive($search_term, $offset, 6);

        $config = array();
        $config['base_url'] = base_url() . 'admin/finishing/inactive/';
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['total_rows'] = $data['finishings']['count'];
        $config['per_page'] = 6;
        $config['num_links'] = 6;
        $config['uri_segment'] = 4;

        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        
        $products = $this->printers_m->printersListFlash(); 
        
        foreach ($products as $product) {
            if((($product->fl_date_begin) != 0 ) && ($product->fl_date_end != 0 )){
            if ($currentTime >= $product->fl_date_end) {
                $field = array(
                    'new_price' => '0.00',
                    'fl_date_begin' => 0,
                    'fl_date_end' => 0
                );
                $this->printers_m->resetNewPrice($product->id, $field);
                }
            }
        }

        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();

        $data['newMessages'] = $this->data['newMessages'];

        $this->load->view('admin/finishing/finishing_inactive_v', array(
            'title' => $data['title'],
            'finishings' => $data['finishings']['data'],
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term,
            'links' => $links,
        ));
    }
    
    public function makeInactive(){
        $id = $this->uri->segment(4);

        $this->finishing_m->makeInactive($id);
        $this->session->set_flashdata('success', 'You have successfully make finishing with id : ' . $id . " Inactive !");
        redirect('admin/finishing');

    }
    public function makeActive(){
        $id = $this->uri->segment(4);

        $this->finishing_m->makeActive($id);
        $this->session->set_flashdata('success', 'You have successfully make finishing with id : ' . $id . " Active !");
        redirect('admin/finishing/inactive');

    }

    public function createFinishing() {
        
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Finishing' => 'admin/finishing',
            'Create Finishing' => ''
        ));

       
        $data = array();
        $data['title'] = 'Create Finishing';

        $data['postData'] = array(
            'title' => '',
            'text' => '',
            'price' => '',
            'new_price' => '',
            'cat_id' => '',
            'man_id' => ''
        );

        $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]|max_length[88]|xss_clean');
        $this->form_validation->set_rules('text', 'Text', 'trim|required|xss_clean|');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric|xss_clean|');
        $this->form_validation->set_rules('new_price', 'New price', 'trim|numeric|xss_clean');
        $this->form_validation->set_rules('cat_id', 'Finishing Category', 'required');

        $is_dublicate = $this->finishing_m->check_unique3($this->input->post('title'));
        
        // Cheching if "post" request is set
        if ($this->input->post()) {
            
            $data['postData'] = $this->input->post();
            
            
            if ($this->form_validation->run() == FALSE) {
                
            } else if ($is_dublicate) {
                $this->session->set_flashdata('error', 'This title already exist.');
                redirect('/admin/finishing/createFinishing');
            } else {
                $data = array(
                    'title' => $this->input->post('title'),
                    'text' => $this->input->post('text'),
                    'price' => $this->input->post('price'),
                    'new_price' => $this->input->post('new_price'),
                    'f_cat_id' => $this->input->post('cat_id'),
                    'man_id' => $this->input->post('man_id'),
                    'date_added' => date("Y-m-d H:i:s")
                );
               
                if ($this->upload() === "") {
                    
                    $this->session->set_flashdata('error_validation', $this->upload->display_errors('<div class="error">', '</div>'));
                    redirect('admin/finishing/createFinishing');
                }

                $this->finishing_m->createFinishing($data);
                $id = $this->db->insert_id();
                $image = $this->upload();
                $this->gallery_m->saveMain($image, $id);

                $this->session->set_flashdata('success', 'New finishing has been added :) ');
                redirect('admin/finishing');
            }
        }

        $data['companies'] = $this->manufacturer_model->show_Company();
        $data['categories'] = $this->finishing_m->getAllCat();

        $this->load->view('admin/finishing/finishing_create_v', array(
            'postData' => $data['postData'],
            'newMessages' => $this->data['newMessages'],
            'title' => $data['title'],
            'categories' => $data['categories'],
            'companies' => $data['companies'],
        ));
    }

    public function upload() {

        $this->load->library('image_lib');

        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png',
            'max_size' => 2048,
            'upload_path' => $this->original_path,
            'file_name' => sha1(time())
        );

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('image')) {

            $data = array('upload_data' => $this->upload->data());
            $image_data = $data['upload_data'];
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path,
                'maintain_ratio' => false,
                'width' => 280,
                'height' => 280
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->thumbs_path,
                'maintain_ratio' => false,
                'width' => 65,
                'height' => 65
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path1,
                'maintain_ratio' => false,
                'width' => 155,
                'height' => 155
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path2,
                'maintain_ratio' => false,
                'width' => 60,
                'height' => 70
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path3,
                'maintain_ratio' => false,
                'width' => 500,
                'height' => 500
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();


            return $this->upload->file_name;
        } else {
            return "";
        }
    }

    public function createCategory($offset = 0) {
        $bred_name = '';
        $bred_action = '';
        if ($_SERVER['HTTP_REFERER'] == 'http://rgstore.local/admin/finishing') {
            $bred_name = 'Finishing';
            $bred_action = 'admin/finishing';
        } else if ($_SERVER['HTTP_REFERER'] == 'http://rgstore.local/admin/filament') {
            $bred_name = 'Filament';
            $bred_action = 'admin/filament';
        } else if ($_SERVER['HTTP_REFERER'] == 'http://rgstore.local/admin/printers') {
            $bred_name = 'Printers';
            $bred_action = 'admin/printers';
        }
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            $bred_name => $bred_action,
            'Create Category' => '',
        ));

        $data = array();
        $data['title'] = 'Categories';

        $this->load->library('pagination');

        $search_term = $this->input->get('search');

        $data['categories'] = $this->finishing_m->getCatogories($search_term, $offset, 8);


        $config = array();
        $config['base_url'] = base_url() . 'admin/finishing/createCategory/';
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['total_rows'] = $data['categories']['count'];
        $config['per_page'] = 8;
        $config['num_links'] = 8;
        $config['uri_segment'] = 4;

        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';



        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();

        $data['postData'] = array(
            'username' => '',
            'name' => ''
        );

        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|max_length[12]|xss_clean');
        
        $name = $this->input->post('name');
        
        $is_duplicate = $this->finishing_m->check_unique_cat($name);
        
        if ($this->input->post()) {
            $data['postData'] = $this->input->post();

            if ($is_duplicate) {
                $this->session->set_flashdata('error', 'This Name Exist');
                redirect('admin/category');
            } 
            
            if ($this->form_validation->run() == FALSE) {

            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'date' => date("Y-m-d H:i:s")
                );
                $this->finishing_m->createCategory($data);

                $this->session->set_flashdata('success', 'New category has been added :) ');
                redirect('admin/category');
            }
        }

        $this->load->view('admin/finishing/finishing_cat_v', array(
            'title' => $data['title'],
            'postData' => $data['postData'],
            'categories' => $data['categories']['data'],
            'search_term' => $search_term,
            'links' => $links,
            'newMessages' => $this->data['newMessages']
        ));
    }

    public function editCat($id) {
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Finishing' => 'admin/finishing',
            'Create Category' => 'admin/finishing/createCategory',
            'Edit Category' => ' ',
        ));
        
        $data = array();
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Edit';
        $data['source'] = $this->finishing_m->sources($id);
        $this->load->view('admin/finishing/finishing_catEdit_v', $data);
    }

    public function editFinishing() {
        $data = array();

        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Finishing' => 'admin/finishing',
            'Edit' => ' ',
        ));

        $data['postData'] = array(
            'title' => '',
            'text' => '',
            'price' => '',
            'new_price' => '',
            'cat_id' => '',
            'man_id' => ''
        );

        if ($this->input->post()) {
            $data['postData'] = $this->input->post();

            $data = array(
                'id' => $this->input->post('id'),
                'title' => $this->input->post('title'),
                'text' => $this->input->post('text'),
                'price' => $this->input->post('price'),
                'new_price' => $this->input->post('new_price'),
                'man_id' => $this->input->post('man_id'),
                'f_cat_id' => $this->input->post('cat_id')
            );

            $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]|max_length[88]|xss_clean');
            $this->form_validation->set_rules('text', 'Text', 'trim|required|xss_clean|');
            $this->form_validation->set_rules('price', 'Text', 'trim|required|numeric|xss_clean|');
            $this->form_validation->set_rules('new_price', 'New price', 'trim|numeric|xss_clean');

            $is_duplicate = $this->finishing_m->check_unique_fin($data['id'], $data['title']);

            $source = $this->finishing_m->sourcesFinish($data['id']);
            
            if ($is_duplicate) {
                $this->session->set_flashdata('error', 'This Title Exist');
                redirect('admin/finishing/edit/' . $this->input->post('id'));
            } elseif ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
                redirect('admin/finishing/edit/' . $this->input->post('id'));
            } elseif($source->fl_date_begin != "0000-00-00 00:00:00"){
                if($data['new_price'] == "0.00"){
                    $this->session->set_flashdata('error', 'You cannot set New Price to 0.00 because this product is in flashsale !');
                    redirect('admin/finishing/edit/' . $this->input->post('id'));
                }
            }
            $this->finishing_m->editFinishing($data);
            $this->session->set_flashdata('success', 'You have successfully edited '
                    . $this->input->post('title') . ' with ID : '
                    . $this->input->post('id') . ' !');
            redirect('admin/finishing');
        }
        
        $id = $this->uri->segment(4);
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Edit';
        $data['source'] = $this->finishing_m->sourcesFinish($id);
        $data['sourceOne'] = $this->finishing_m->sourcesCat();
        $data['sourceCat'] = $this->finishing_m->getCategory($id);
        $data['sourceMan'] = $this->finishing_m->getManufacturer($id);
        $data['companies'] = $this->manufacturer_model->show_Company();

        $this->load->view('admin/finishing/finishing_edit_v', $data);
    }

    public function editCategory() {

        $data = array();
        $data = array(
            'id' => $this->input->post('id'),
            'name' => $this->input->post('name'),
            'date' => $this->input->post('date')
        );

        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|max_length[12]|xss_clean');

        $is_duplicate = $this->finishing_m->check_unique($data['id'], $data['name']);

        if ($is_duplicate) {
            $this->session->set_flashdata('error', 'This Name Exist');
            redirect('admin/category/edit/' . $this->input->post('id'));
        } elseif ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
            redirect('admin/category/edit/' . $this->input->post('id'));
        }
        $this->finishing_m->editCategory($data);
        $this->session->set_flashdata('success', 'You have successfully edited  ' . $this->input->post('name') . ' !');
        redirect('admin/category');
    }

    public function CatajaxDelete() {
        $id = $this->input->post('id');
       
        $data['result']['count'] = $this->finishing_m->countRecords($id);
        
        if (count($data['result'])>0){
         
             $fields = array(
                'error' => 1
            );
        }
        else{
           
        $conditions = array(
            'id' => $id
        );

        if ($this->finishing_m->Cat_deleteData($id)){
            $fields = array(
                'success' => 1
            );
        
        } 
        elseif (!$this->finishing_m->Cat_deleteData($id)) {
            $fields = array(
                'success' => 0
            );
        }
        }
        echo json_encode($fields);
        die;
    }

    public function ajaxDelete() {
        $id = $this->input->post('id');
        $conditions = array(
            'p.active' => 0,
            'bi.product_id' => $id,
            'p.cat_id' => 4,
        );

        $fields = array('b.product_id');
        
        $bundleItems = $this->products_model->isProductInBundle($conditions, $fields)->result();

        if (empty($bundleItems)) {           
        
            if ($this->finishing_m->deleteData($id)) {
                $fields = array(
                    'success' => 1
                );
            } elseif (!$this->finishing_m->deleteData($id)) {
                $fields = array(
                    'success' => 0
                );
            }
            
        } else {
            $fields = array(
                'success' => 2
            );
        }
        
        echo json_encode($fields);
        die;
    }

    public function FlashajaxDelete() {
        $id = $this->input->post('id');
        
        $this->finishing_m->deleteFlashData($id);
        $this->session->set_flashdata('success', 'You have successfully deleted flashdata for finishing with id :' . $id);
        redirect('admin/finishing');
    }

    public function bulkDelete() {
        $ids = $this->input->post('bulkDelete');

        if ($ids == false) {
            $this->session->set_flashdata('success', 'You need to choose items first !');
            redirect('admin/finishing');
        }

        foreach ($ids as $key => $id) {
            $this->finishing_m->deleteData($id);
            $this->session->set_flashdata('success', 'You have successfully deleted selected finishing products');
        }
        redirect('admin/finishing');
    }

    public function Cat_bulkDelete() {
        $ids = $this->input->post('bulkDelete');

        if ($ids == false) {
            $this->session->set_flashdata('success', 'You need to choose items first !');
            redirect('admin/finishing/createCategory');
        }

        foreach ($ids as $key => $id) {
            $data['result'] = $this->finishing_m->countRecords($id);
             if($data['result']['count'] == 0) {
                    $this->finishing_m->Cat_deleteData($id);
                    $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                    redirect('admin/finishing/createCategory');
                } else {
                    $this->session->set_flashdata('error', 'You have products in your category!');
                    redirect('admin/finishing/createCategory');
                }
           // $this->finishing_m->Cat_deleteData($id);
           // $this->session->set_flashdata('success', 'You have successfuly deleted selected Categories');
        }
        
    }

    public function flashdate() {
        
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Finishing' => 'admin/finishing',
            'Add Flash Date' => ''
        ));
        
        
        $data = array();
        $data['title'] = 'Flash Date';
        $id = $this->uri->segment(4);
        
        if ($this->input->post()) {
            
            $this->form_validation->set_rules('new_price', 'New price', 'trim|numeric|xss_clean');

            $data = array(
                'id' => $this->input->post('id'),
                'fl_date_begin' => $this->input->post('fl_date_begin'),
                'fl_date_end' => $this->input->post('fl_date_end')
            );
            
            $newPrice = $this->input->post('new_price');
            $this->finishing_m->setNewPrice($data['id'],$newPrice);
            $data['newprice'] = $this->finishing_m->newPriceRow($data['id']);
            
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
                redirect('admin/finishing/flashdate/' . $data['id']);
            }
            else if ($data['fl_date_begin'] == '' || $data['fl_date_end'] == ''){
                $this->session->set_flashdata('error', 'Empty field is not allowed !');
                redirect('admin/finishing/flashdate/' . $data['id']);
                
            }else if($data['fl_date_begin'] > $data['fl_date_end']){
                
                $this->session->set_flashdata('error', 'Flash Date (begin) cannot be lower than Flash Date (end) !');
                redirect('admin/finishing/flashdate/' . $data['id']);
            }else if($data['newprice']->new_price == "0.00"){
                    
                $this->session->set_flashdata('error', 'You cannot create flashdate . First need to add New Price ! ');
                redirect('admin/finishing/flashdate/' . $data['id']) ;
            }
            
            $this->finishing_m->setFlashdata($data);
            $this->session->set_flashdata('success', 'You have successfully  add flashdate to finishing with id: ' . $this->input->post('id'));
            redirect('admin/finishing');
        }
        

        $data['sources'] = $this->finishing_m->sourcesFinish($id);
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/finishing/finishing_flashdate_v', $data);
    }

    public function flashdate_edit() {
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Finishing' => 'admin/finishing',
            'Edit Flash Date' => ''
        ));

        
        $data = array();
        $data['title'] = 'Flash Date';
        $id = $this->uri->segment(4);

        if ($this->input->post()) {
            $this->form_validation->set_rules('new_price', 'New price', 'trim|numeric|xss_clean');
            
            $data = array(
                'id' => $this->input->post('id'),
                'fl_date_begin' => $this->input->post('fl_date_begin'),
                'fl_date_end' => $this->input->post('fl_date_end')
            );
            
            $newPrice = $this->input->post('new_price');
            $this->finishing_m->setNewPrice($data['id'],$newPrice);
            $data['newprice'] = $this->finishing_m->newPriceRow($data['id']);
            
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
                redirect('admin/finishing/flashdate_edit/' . $data['id']);
            }else if ($data['fl_date_begin'] == '' || $data['fl_date_end'] == ''){
                
                $this->session->set_flashdata('error', 'Empty field is not allowed !');
                redirect('admin/finishing/flashdate_edit/' . $data['id']) ;
            }else if($data['fl_date_begin'] > $data['fl_date_end']){
                
                $this->session->set_flashdata('error', 'Flash Date (begin) cannot be lower than Flash Date (end) !');
                redirect('admin/finishing/flashdate_edit/' . $data['id']) ;
            }else if($data['newprice']->new_price == "0.00"){
                
                $this->session->set_flashdata('error', 'You cannot create flashdate . First need to add New Price ! ');
                redirect('admin/finishing/flashdate_edit/' . $data['id']) ;
            }

            $this->finishing_m->setFlashdata($data);
            $this->session->set_flashdata('success', 'You have successfully  updated flashdate to finishing with id: ' . $this->input->post('id'));
            redirect('admin/finishing');
        }

        $data['sources'] = $this->finishing_m->flashSource($id);

        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/finishing/finishing_flashdate_edit_v', $data);
    }
    
    public function newPriceEdit() {
        $id = $this->input->post('new_price_id');
        $newPrice = $this->input->post('new_price');
        
        //Need for checking last url and redirecting right .
        $lastURL = $_SERVER['HTTP_REFERER'];

        $this->form_validation->set_rules('new_price', 'New price', 'trim|numeric|xss_clean');

        $source = $this->finishing_m->sourcesFinish($id);
        
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
            if($lastURL == site_url('admin/finishing/inactive')){
                redirect('admin/finishing/inactive');
            }else{
                redirect('admin/finishing');
            }
        }else if($source->fl_date_begin != '0000-00-00 00:00:00'  && ($newPrice == '0.00' || $newPrice == '')) {
            $this->session->set_flashdata('error', 'You cannot set New Price to 0.00 when there is Flash Date !');
            if($lastURL == site_url('admin/finishing/inactive')){
                redirect('admin/finishing/inactive');
            }else{
                redirect('admin/finishing');
            }
        }else {
            $this->finishing_m->setNewPrice($id, $newPrice);
            $this->session->set_flashdata('success', 'You have successfully  updated new price to finishing with id: ' . $id);
            if($lastURL == site_url('admin/finishing/inactive')){
                redirect('admin/finishing/inactive');
            }else{
                redirect('admin/finishing');
            }
        }
    }

    public function ajaxReorder() {
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->finishing_m->reorder($key, $value);
            }
        } else {
            return false;
        }

        die;
    }

}
