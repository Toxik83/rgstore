<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

/* Order status
 * 0 - pending
 * 1 - completed
 * 2 - canceled
 * 3 - paid
 */
 

class adminOrders extends MY_Controller {
    
   public function __construct() 
    {
        parent::__construct();
        $this->load->model('order_model');
        $this->load->model('products_model');
    }

    public function index()
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Orders'
        ));
        
        $terms = $this->input->get('search');
        
        if ($this->input->get()) {      
            //check if search keys exist      
            $terms = (array_key_exists('search', $this->input->get())) ? $this->input->get('search') : ''; 
        }
        
        $like = array();
        if ($this->input->get()) {
            if ($terms != '') {
               $like = $terms;  
            }
        }
        
        $data = array();
        $conditions = array();
        
        $this->load->library('pagination');
        $total = $this->order_model->getOrders($conditions, null, $like)->num_rows();

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1; //start page for pagination
        
        $pageLimit = 5;
        $max = array($pageLimit, ($page - 1) * $pageLimit);

        $orders = $this->order_model->getOrders($conditions, $max, $like)->result();

        $config = array();
        $config['base_url'] = base_url() . 'admin/orders';
        $config['total_rows'] = $total;
        $config['per_page'] = $pageLimit;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = true;

        if ($this->input->get()) {
            $config['suffix'] = '?'.http_build_query($this->input->get(), '', '&');
            $config['first_url'] = base_url().'admin/orders/1'. $config['suffix'];
        }
        
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['search'] = $terms;
        $data['orders'] = $orders;
        $data['title'] = 'Orders';
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/orders/listing' ,$data);
    }
    
    public function orderDetails($id)
    {
        $data = array();
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Orders' => 'admin/orders',
            'Details' => ''
        ));
        $conditions = array(
            'po.order_id' => $id
        );
        $fields = array(
                    'o.order_id', 'o.fname', 'o.lname', 'o.email', 'o.address', 'o.city', 'o.country_name', 'o.zip_code', 'o.phone', 'o.status', 'o.order_price', 
                    'po.quantity', 'po.price', 'po.total_price', 'po.product_id', 
                    'p.title', 'p.new_price'
                );
        
        $orderData = $this->order_model->getOrderData($conditions, $fields)->result();
              
        if (empty($orderData)) {
            redirect('admin/orders');
        }   
        
        $productsPrice = ''; 
        $orderPrice = '';
        foreach ($orderData as $key => $details) {
            $productsPrice += $details->total_price;
            $orderPrice = $details->order_price;
        }
        $data['shipping'] = $orderPrice - $productsPrice;
        $data['orderPrice'] = $orderPrice;
        $data['orderData'] = $orderData;
        $data['title'] = 'Order Details';
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/orders/details', $data);
    }

    public function confirmOrder($orderId)
    {
        $status = array(
            'status' => 1
        );
        
        $this->order_model->changeStatus($orderId, $status);
        $this->session->set_flashdata('success', 'You have successfully completed the order');
        redirect('admin/orders');
    }
    
    public function cancelOrder($orderId)
    {
        $conditions = array(
            'po.order_id' => $orderId
        );        
        $orderedProducts = $this->order_model->getProductsOrdered($conditions)->result();
        
        foreach ($orderedProducts as $key => $product){
            $updateFields = array(
                'sold' => $product->sold - $product->quantity
            );
            $this->products_model->updateProduct($updateFields, $product->product_id);
        }    

        $status = array(
            'status' => 2
        );       
        $this->order_model->changeStatus($orderId, $status);
        $this->session->set_flashdata('success', 'You have successfully canceled the order');
        redirect('admin/orders');
    }
    
    public function setToPaid($orderId)
    {
        $conditions = array(
            'po.order_id' => $orderId
        );
        $orderedProducts = $this->order_model->getProductsOrdered($conditions)->result();    

        foreach ($orderedProducts as $key => $product){
            $updateFields = array(
                'sold' => $product->sold + $product->quantity
            );
            $this->products_model->updateProduct($updateFields, $product->id);
        }
        
        $status = array(
            'status' => 3
        );
        
        $this->order_model->changeStatus($orderId, $status);
        $this->session->set_flashdata('success', 'The order is set to paid');
        redirect('admin/orders');
    }
 
}
