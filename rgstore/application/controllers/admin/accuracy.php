<?php
class Accuracy extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
       $this->load->model('accuracy_model');
    }

     public function index($offset = 0){
        $this->load->library('pagination');
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Accuracy sizes'
             ));
        
        $data['title']='Accuracy -  Sizes ';
        $keyword  =  $this->input->get('keyword');
        $data['accuracies']= $this->accuracy_model->show_Admin_Accuracy($keyword, $offset, 8);
        $data['newMessages'] =  $this->data['newMessages'];        
        $config = array();
        $config['base_url'] = base_url() . 'admin/accuracy/index';
        $config['suffix'] = '?search=' . $keyword;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['total_rows'] = $data['accuracies']['count'];
        $config['per_page'] = 8;
        $config['num_links'] = 8;
        $config['uri_segment'] = 4;
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';



        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();
        
        $this->load->view('admin/accuracy_add', array(
            'title' => $data['title'],
            'accuracies' => $data['accuracies']['data'],
            'newMessages' => $data['newMessages'],
            'links' => $data['links'],
        ));
    }

    public function add(){
        $this->load->library('form_validation');
        $name = $this->input->post('accuracy_name');
        $is_duplicate = $this->accuracy_model->check_unique($name);
        $this->form_validation->set_rules('accuracy_name','Printers Accuracy name', 'required|trim|xss_clean');
        if ($this->input->post()) {
            if ($this->form_validation->run() == FALSE){
                $this->session->set_flashdata('accuracy_name', 'Please fill accuracy name!');
                redirect('admin/accuracy');
            } elseif ($is_duplicate) {
                $this->session->set_flashdata('accuracy_name', 'This accuracy already exist');
               redirect('admin/accuracy');                    
                } else{
        $this->accuracy_model->create($name);
        $this->session->set_flashdata('accuracy_name', 'Success! New accuracy has been added!');
       redirect('admin/accuracy');
    }
        }
        $data['newMessages'] =  $this->data['newMessages'];            
        $this->load->view('admin/accuracy_add', $data);     
    }

    public function update($id) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Accuracy sizes' => 'admin/accuracy',
            'Edit Accuracy size'
             ));
        $data['accuracies'] = $this->accuracy_model->find($id);
        $data['title']='Edit Accuracy Size';
        $data['newMessages'] =  $this->data['newMessages'];
        $this->load->view('admin/accuracy_edit', $data);
    }
    public function delete($id){
        
        $data['result'] = $this->accuracy_model->countRecords($id);
            if($data['result']['count'] == 0) {
                $this->accuracy_model->delete($id);
                $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                redirect('admin/accuracy');
            } else {
                $this->session->set_flashdata('error', 'You have products in your category!');
                redirect('admin/accuracy');
            }
        
    }
     
     public function edit_accuracy() {
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('accuracy_name','Accuracy', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error','Please Edit Accuracy Size!');
            return redirect('admin/accuracy/update/'.$this->input->post('accuracy_id'));
        }
        else{
            $id = $this->input->post('accuracy_id');
            $name = $this->input->post('accuracy_name');
            $this->accuracy_model->update($id, $name);
            $this->session->set_flashdata('success', 'Success! The accuracy has been edited!');
            return redirect('admin/accuracy');

        }
    }
    
    public function ajaxReorder()
    {      
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->accuracy_model->reorder($key, $value);
            }
        } else {
            return false;
        }    
        
        die;
    }
    
    public function bulkDelete() {
        $ids = $this->input->post('bulkDelete');
        
        if ($ids == false) {
            $this->session->set_flashdata('error', 'You need to choose items first !');
            redirect('admin/accuracy');
        } else {
            foreach ($ids as $id) {
                $data['result'] = $this->accuracy_model->countRecords($id);
                    if($data['result']['count'] == 0) {
                        $this->accuracy_model->delete($id);
                        $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                        redirect('admin/accuracy');
                    } else {
                        $this->session->set_flashdata('error', 'You have products in your category!');
                        redirect('admin/accuracy');
                    }
                }
        }
    }
}
