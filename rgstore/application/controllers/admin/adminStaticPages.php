<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class adminStaticPages extends MY_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('staticpages_model');
    }
    
    public function index() 
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Static Pages' => '',
        ));
        
        $data = array();
        $conditions = array();
        $data['pageData'] = $this->staticpages_model->getPage($conditions)->result();
        
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Static pages';
        $this->load->view('admin/static-pages/listing', $data);
    }
    
    public function createPage()
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Static Pages' => 'admin/static-pages',
            'Create page'
        ));
        $data = array();
        $data['postData'] = array(
            'title' => '',
            'slug' => '',
            'link_name' => '',
            'content' => ''
        );
        
        $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('slug', 'Slug', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('link_name', 'Link name', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('content', 'Content', 'required|trim|min_length[3]');
        
        if ($this->input->post()) {
            $data['postData'] = $this->input->post();
            if ($this->form_validation->run()) {
                $data['pageData'] = array(
                    'title' => $this->input->post('title'),
                    'slug' => $this->input->post('slug'),
                    'link_name' => $this->input->post('link_name'),
                    'link_to_page' => site_url('page/'.$this->input->post('slug')),
                    'content' => $this->input->post('content')
                );
                
                $this->staticpages_model->setPage($data['pageData']);
                $this->session->set_flashdata('success', 'The content has been successfully saved');
                redirect('admin/static-pages');
            }
        }
        
        $data['title'] = 'Create page';
        $data['newMessages'] = $this->data['newMessages'];
        
        $this->load->view('admin/static-pages/create', $data);
    }
    
    public function editPage($id)
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Static Pages' => 'admin/static-pages',
            'Edit page'
        ));
        
        $data = array();
        
        $conditions = array(
            'id' => $id
        );

        $data['pageData'] = $this->staticpages_model->getPage($conditions)->row();
        $pageData = $data['pageData']; 
        
        if ($pageData == null) {
            redirect('admin/static-pages');
        }

        $data['postData'] = array(
            'title' => $pageData->title,
            'slug' => $pageData->slug,
            'link_name' => $pageData->link_name,
            'content' => $pageData->content,
        );

        $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('slug', 'Slug', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('link_name', 'Link name', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('content', 'Content', 'required|trim|min_length[3]');
        
        if ($this->input->post()) {
            $data['postData'] = $this->input->post();
            
            if ($this->form_validation->run()) {
                $data['pageData'] = array(
                    'title' => $this->input->post('title'),
                    'slug' => $this->input->post('slug'),
                    'link_name' => $this->input->post('link_name'),
                    'link_to_page' => site_url('page/'.$this->input->post('slug')),
                    'content' => $this->input->post('content'),
                );
                
                $this->staticpages_model->updatePage($id, $data['pageData']);
                $this->session->set_flashdata('success', 'Your page was successfully edited');
                redirect('admin/static-pages');
            }
            
        }
        
        $data['title'] = 'Edit page';
        $data['newMessages'] = $this->data['newMessages'];
        
        $this->load->view('admin/static-pages/create', $data);
    }
    
    public function bulkDelete()
    {      
        $ids = $this->input->post('bulkDelete');

        if (!isset($ids) && empty($ids)) {
            redirect('admin/static-pages');
        }
        
        foreach ($ids as $key => $id) {
            $this->staticpages_model->deletePage($id);
            $this->session->set_flashdata('success', 'You have successfuly deleted the pages');
        }
        redirect('admin/static-pages');
    }
    
    public function ajaxDelete()
    {
        $id = $this->input->post('id');
        
        $conditions = array(
            'id' => $id
        );
        
        if ($this->staticpages_model->deletePage($id)) {
            $fields = array(
                'success' => 1
            );
            
        } elseif (!$this->staticpages_model->deletePage($id)) {
            $fields = array(
                'success' => 0
            );
        }
               
        echo json_encode($fields);
        die;
    }
    
    public function deletePage($pageId)
    {               
        $conditions = array(
            'id' => $pageId
        );
        $pageData = $this->staticpages_model->getPage($conditions)->row();
        
        if (empty($pageData)) {
            redirect('admin/static-pages');
        }
        
        $this->staticpages_model->deletePage($pageId);
        $this->session->set_flashdata('success', 'You have successfully deteleted the page');
        redirect('admin/static-pages');
    }
}
