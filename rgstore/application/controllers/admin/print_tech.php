<?php

class Print_tech extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('print_tech_model');
    }

    public function index($offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Printers Technology'
        ));
        $this->load->library('pagination');
        $data = array();
        $title = 'Printer\'s Technology';
        $data['title'] = $title;
        $search_term = $this->input->get('keyword');
        $data['print_tech'] = $this->print_tech_model->show_PrintTech($search_term, $offset, 8);
        $config = array();
        $config['base_url'] = base_url() . 'index.php/admin/print_tech/index';
        $config['total_rows'] = $data['print_tech']['count'];
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = 8;
        $config['num_links'] = 9;
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        if ($this->uri->segment(4) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(4) - 1);
        }
        $links = $this->pagination->create_links();

//        $this->createUploadFolder();
        $this->load->view('admin/print_tech_add', array(
            'print_tech' => $data['print_tech']['data'],
            'links' => $links,
            'title' => $title,
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term
        ));       

    }

    public function add() {
        $this->load->library('form_validation');
        $lastId = $this->print_tech_model->lastId();
        $orderId = (int)$lastId->id +1;
        $name = $this->input->post('print_name');
        $is_duplicate = $this->print_tech_model->check_unique($name);
        $this->form_validation->set_rules('print_name', 'Printers Technology name', 'required|trim|xss_clean');
        if ($this->input->post()) {
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', 'Please fill techology name!');
                redirect('admin/print_tech');
            } elseif ($is_duplicate) {
                $this->session->set_flashdata('error', 'This technology already exist');
                redirect('admin/print_tech');
            } else {
                $this->print_tech_model->create($name, $orderId);
                $this->session->set_flashdata('success', 'Success! New technology has been added!');
                redirect('admin/print_tech');
            }
        }
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/print_tech_add', $data);
    }

    public function update($id) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Printers Technology' => 'admin/print_tech',
            'Edit Printers Technology'
        ));
        $data['print_tech'] = $this->print_tech_model->find($id);
        $data['title'] = 'Edit Printers Technology';
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/print_tech_edit', $data);
    }

    public function delete($id) {
        $data['result'] = $this->print_tech_model->countRecords($id);
            if($data['result']['count'] == 0) {
                $this->print_tech_model->delete($id);
                $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                redirect('admin/print_tech');
            } else {
                $this->session->set_flashdata('error', 'You have products in your category!');
                redirect('admin/print_tech');
            }
    }

    public function edit_print_tech() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('print_name', 'Printers Technology', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'Please Edit Printer Technology name!');
            return redirect('admin/print_tech/update/' . $this->input->post('print_id'));
        } else {
            $id = $this->input->post('print_id');
            $name = $this->input->post('print_name');
            $this->print_tech_model->update($id, $name);
            return redirect('admin/print_tech');
        }
    }

    public function ajaxReorder() {
        $maxId = $this->print_tech_model->lastId();
        
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->print_tech_model->reorder($key, $value);
            }
        } else {
            return false;
        }

        die;
    }
    
    public function bulkDelete() {
        $ids = $this->input->post('bulkDelete');

        if ($ids == false) {
            $this->session->set_flashdata('error', 'You need to choose items first !');
            redirect('admin/print_tech');
        } else {

        foreach ($ids as $key => $id) {
            
        $data['result'] = $this->print_tech_model->countRecords($id);
                if($data['result']['count'] == 0) {
                    $this->print_tech_model->delete($id);
                    $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                    redirect('admin/print_tech');
                } else {
                    $this->session->set_flashdata('error', 'You have products in your category!');
                    redirect('admin/print_tech');
                }
            }
        }
    }
}
