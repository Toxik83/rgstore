<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class adminBundle extends MY_Controller{
    
     public function __construct() {
        parent::__construct();
        $this->load->model('products_model');
        $this->load->model('printers_m');
    }
    
    public function index()
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Bundles' => ''
        ));
        
        $conditions = array(
            'p.cat_id' => 4,
            'p.active' => 0
        );
        
        $data['bundles'] = $this->products_model->getBundle($conditions)->result();

        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Bundle';
        $this->load->view('admin/bundles/listing', $data);
    }
    
    public function createBundle()
    {      
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Bundles' => 'admin/bundle',
            'Create' => ''
        ));
                
        $conditions = array(1,2,3);      
        $data['products'] = $this->products_model->getProductsExceptBundles($conditions)->result();

        $data['postData'] = array(
            'name' => '',
            'total_price_hidden' => '',
            'discount_price_hidden' => '',
            'bundle' => array(''),
            'discount_hidden' => ''
        );
        $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('bundle', 'Products', 'required');
        $this->form_validation->set_rules('discount', 'Discount', 'required|callback__check_discount');
        
        if ($this->input->post()) {
            $data['postData'] = $this->input->post();

            if ($this->form_validation->run()) {
                
                //insert the bundle as product in products table
                $data['productData'] = array(
                    'cat_id' => 4,
                    'title' => $this->input->post('name'),
                    'price' => $this->input->post('discount_price_hidden')
                );
                $this->products_model->setProduct($data['productData']);
                $bundleId = $this->db->insert_id();
                
                //insert the bundle in the bundle table
                $data['bundleData'] = array(
                        'product_id' => $bundleId,
                        'bundle_name' => $this->input->post('name'),                       
                        'combined_price' => $this->input->post('total_price_hidden'),
                        'discount_price' => $this->input->post('discount_price_hidden'),
                        'discount' => $this->input->post('discount'),
                    );
                
                $this->products_model->setBundle($data['bundleData']);
                
                //foreach bundle[] as key / value product_id => value. Insert the products in the bundle in bundle_items table
                foreach ($this->input->post('bundle') as $key => $productId) {                 
                    $data['bundleProducts'] = array(
                        'product_id' => $productId,
                        'bundle_id' => $bundleId
                    );
                    $this->products_model->setBundleItems($data['bundleProducts']);                    
                }
                
                $this->session->set_flashdata('success', 'You have successfully created a new bundle');
                redirect('admin/bundle');
            }
        }        
        
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Bundle';
        $this->load->view('admin/bundles/createBundle', $data);
    }
    
    public function editBundle($bundleId)
    {
        $data = array();
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Bundles' => 'admin/bundle',
            'Edit' => ''
        ));
        
        $conditions = array(
            'b.product_id' => $bundleId
        );
        
        $fields = array('b.bundle_name', 'b.discount', 'b.discount_price', 'b.combined_price', 'p.title', 'p.id', 'p.price');
        
        $data['bundleData'] = $this->products_model->getBundleData($conditions, $fields)->result();
        
        if (empty($data['bundleData'])) {
            redirect('admin/bundle');
        }
        
        $data['productIds'] = array();
        foreach ($data['bundleData'] as $key => $item) {
            array_push($data['productIds'], $item->id);
        }
        
        $data['postData'] = array(
            'total_price_hidden' => $data['bundleData'][0]->combined_price,
            'discount_price_hidden' => $data['bundleData'][0]->discount_price,
            'bundle' => $data['productIds'],
            'discount' => $data['bundleData'][0]->discount,
            'bundle_name' => $data['bundleData'][0]->bundle_name
        );
        
        $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('bundle', 'Products', 'required');
        $this->form_validation->set_rules('discount', 'Discount', 'required|callback__check_discount');
        
        if ($this->input->post()) {
            
            $data['postData'] = $this->input->post();

            if ($this->form_validation->run()) {
                $data['bundleData'] = array(
                        'bundle_name' => $this->input->post('name'),                       
                        'combined_price' => $this->input->post('total_price_hidden'),
                        'discount_price' => $this->input->post('discount_price_hidden'),
                        'discount' => $this->input->post('discount'),
                    );
                    
                $this->products_model->updateBundle($data['bundleData'], $bundleId);
                
                $this->products_model->deleteBundleItems($bundleId);
                
                foreach ($this->input->post('bundle') as $key => $productId) {                 
                    $data['bundleProducts'] = array(
                        'product_id' => $productId,
                        'bundle_id' => $bundleId
                    );
                    $this->products_model->setBundleItems($data['bundleProducts']);                    
                }
                
                $data['productData'] = array(
                    'price' => $this->input->post('discount_price_hidden'),
                    'title' => $this->input->post('name'),
                    );
                $this->products_model->updateProduct($data['productData'], $bundleId);
                
                $this->session->set_flashdata('success', 'You have successfully edited the bundle');
                redirect('admin/bundle');
            }
        }
        
        $productCategories = array(1,2,3);      
        $data['products'] = $this->products_model->getProductsExceptBundles($productCategories)->result();
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Edit Bundle';
        $this->load->view('admin/bundles/editBundle', $data);
    }
    
    public function softDelete($bundleId)
    {
        $conditions = array(
            'product_id' => $bundleId
        );
        
        $bundleData = $this->products_model->getBundle($conditions)->row();
        
        if (empty($bundleData)) {
            redirect('admin/bundle');
        }
        
        $this->printers_m->delete($bundleId);
        
        $this->session->set_flashdata('success', 'You have successfully deleted the bundle.');
        redirect('admin/bundle');
    }

    public function calculatePrice()
    {
        if (empty($this->input->post('id'))) {
            $price = 0;           
        } else {
            $productId = $this->input->post('id');
            $price = '';
            foreach ($productId as $key => $id){
                $conditios = array(
                    'id' => $id
                );
                $product = $this->products_model->getProducts($conditios)->row();
                $price += $product->price;
            }
        }
        
        $discount = $this->input->post('discount');
        $discountPrice = $price - ($price*($discount/100));
         
        $details = array(
            'price' => $price,
            'discountPrice' => $discountPrice
        );
        echo json_encode($details);
        die;
    }
    
    public function calculateDiscount()
    {
        $price = 0;
        if (!empty($this->input->post('price'))) {
            $price = $this->input->post('price');
        }
        
        $discount = 0;
        if (!empty($this->input->post('discount'))) {
            $discount = $this->input->post('discount');
        }
        
        $price = $price - ($price*($discount/100));
        
        echo json_encode($price);
        die;
    }
    
    public function _check_discount()
    {
        if ($this->input->post('discount') == '') {
            $this->form_validation->set_message('_check_discount', 'Please select discount.');
            return false;
        } else {
            return true;
        }
    }
    
}
