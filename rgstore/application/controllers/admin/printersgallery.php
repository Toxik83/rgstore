<?php

class Printersgallery extends MY_Controller {

        protected $original_path;
        protected $resized_path;
        protected $thumbs_path;
        protected $resized_path1;
        protected $resized_path2;
        protected $resized_path3;
        protected $resized_path5;

    public function __construct() {
        parent::__construct();
        $this->load->model('printersgallery_model');
        $this->load->helper(array('form', 'url'));
        $this->original_path = realpath(APPPATH . '../upload/original');
        $this->resized_path3 = realpath(APPPATH . '../upload/images500x500');
        $this->resized_path = realpath(APPPATH . '../upload/images280x280');
        $this->thumbs_path = realpath(APPPATH . '../upload/thumbs');
        $this->resized_path1 = realpath(APPPATH . '../upload/images155x155');
        $this->resized_path2 = realpath(APPPATH . '../upload/images60x70');
        $this->resized_path5 = realpath(APPPATH . '../upload/images5');
    }

    public function index() {
        $this->load->helper('form', 'url');
        $this->load->library('form_validation');
    }

    public function do_upload() {

        $this->load->library('image_lib');
        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png',
            'max_size' => 2048,
            'upload_path' => $this->original_path,
            'file_name' => sha1(time())
        );
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {            
            $this->session->set_flashdata('error_validation', $this->upload->display_errors('<div class="error">', '</div>'));
            redirect('admin/printersgallery/loadGalleryPage/'.$this->input->post('printer_id'));
        } else {
            $data = array('upload_data' => $this->upload->data());
            $image_data = $data['upload_data'];
            $this->load->view('upload_success', $data);
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path,
                'maintain_ratio' => FALSE,
                'width' => 280,
                'height' => 280
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path3,
                'maintain_ratio' => FALSE,
                'width' => 500,
                'height' => 500
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->thumbs_path,
                'maintain_ratio' => FALSE,
                'width' => 65,
                'height' => 65
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path1,
                'maintain_ratio' => FALSE,
                'width' => 155,
                'height' => 155
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path2,
                'maintain_ratio' => FALSE,
                'width' => 60,
                'height' => 70
            );
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            
            return $this->upload->file_name;
        }

        return "";
    }

    public function loadGalleryPage() {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Printers Gallery'
        ));
        $data['title'] = 'Printers Gallery';
        $printer_id = $this->uri->segment(4);
        $data['images'] = $this->printersgallery_model->showImageByPrinterId($printer_id);
        if(empty($data['images'])){
            redirect ('admin/printers');
        }
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/printersgallery_add', $data);
    }

    public function save($printer_id = 0, $url = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('userfile', 'Userfile', 'required|jpg|jpeg|gif|png');
        $printer_id = $this->input->post('printer_id');
        if ($this->input->post()) {
            if ($this->form_validation->run()) {
                $this->session->set_flashdata('error_validation', $this->upload->display_errors('<div class="error">', '</div>'));
                redirect('admin/printersgallery/loadGalleryPage/' . $this->input->post('printer_id'));
            } else {
                $url = $this->do_upload();
                if ($url == '') {
                    $this->session->set_flashdata('error_validation', $this->upload->display_errors('<div class="error">', '</div>'));
                    redirect('admin/printersgallery/loadGalleryPage/' . $this->input->post('printer_id'));
                } else {
                    $this->session->set_flashdata('success', 'Your image file was sucessfully created');
                    $picOne = $this->printersgallery_model->getFirstPic($printer_id);
                    if (empty($picOne)) {
                        $main_pic = 1;
                        $this->printersgallery_model->saveMainPic($url, $printer_id, $main_pic);
                    } else {
                        $this->printersgallery_model->save($url, $printer_id);
                    }
                }
                redirect('admin/printersgallery/loadGalleryPage/' . $this->input->post('printer_id'));
            }
        }
    }


    public function update($id) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Printer\'s Gallery' => 'admin/printersgallery/loadGalleryPage/'.$this->uri->segment(5),
            'Edit Gallery'
        ));
        $data['title'] = 'Update Picture';
        $data['images'] = $this->printersgallery_model->find($id);
        if(empty($data['images'])){
            redirect('admin/printers');
        }
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/printersgallery_edit', $data);
    }
    //for delete of main pic
    public function delete($id) {
        $id = $this->uri->segment(4);
        $product_id = $this->uri->segment(5);
        $this->printersgallery_model->delete($id);
        $mainPicId = $this->printersgallery_model->chekForMP($id, $product_id);
        if($id == $mainPicId->id){
        $this->session->set_flashdata('error', 'Your can\'t delete main picture! First make onather picture main!');
        redirect('admin/printersgallery/loadGalleryPage/' . $product_id);
        }
        $this->session->set_flashdata('success', 'Picture was deleted');
        redirect('admin/printersgallery/loadGalleryPage/' . $product_id);
    }

    //for front page
    public function portfolioimages() {
        $this->load->model('portfolio_main_model');
        $data['firstimage'] = $this->printersgallery_model->showfirstimage();
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('home', $data);
    }

    public function editImage() {
        $url = $this->do_upload();
        $id = $this->input->post('pic_id');
        $printer_id = $this->input->post('printer_id');
        $this->printersgallery_model->update($id, $url, $printer_id);
        redirect('admin/printersgallery/loadGalleryPage/' . $this->input->post('printer_id'));
    }

    public function ajaxReorder() {
        if ($this->input->post()) {            
            foreach ($this->input->post('order') as $key => $value) {
                $this->printersgallery_model->reorder($key, $value);
            }
        } else {
            return false;
        }
        die;
    }

    public function make_main_pic() {
        if ($this->input->post()) {
            $product_id = $this->input->post('printer_id');            
            $id = $this->input->post('image_check');
            $this->printersgallery_model->mainPic($id);
            $this->printersgallery_model->unMakemainPic($id, $product_id);
            $this->session->set_flashdata('success', 'Your main picture was sucessfully changed!');
            redirect('admin/printersgallery/loadGalleryPage/' . $this->input->post('printer_id'));
        }
    }

}
