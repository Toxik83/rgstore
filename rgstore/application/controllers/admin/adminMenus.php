<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class adminMenus extends MY_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('menus_model');
        $this->load->model('staticpages_model');
    }
    
    public function index() 
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Menus' => ''
        ));
               
        $data = array();

        $conditions = array();
        $data['menuData'] = $this->menus_model->getMenuList($conditions)->result();
        
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Menus';
        $this->load->view('admin/menus/listing', $data);
    }
    
    public function addLink()
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Menus' => 'admin/menus',
            'Preview' => 'admin/menus/view'
        ));
        
        $data = array();
        $data['postData'] = array(
            'name' => '',
            'page_link' => '',
            'link_name' => '',
        );
        
        $this->form_validation->set_rules('page_link', 'Page Link', 'trim|min_length[3]|required|callback__check_valid_url');
        $this->form_validation->set_rules('link_name', 'Link Name', 'trim|min_length[3]|required');

        if ($this->input->post()) {
            $data['postData'] = $this->input->post();
 
            //Setting the type of the link (internal or external);
            $internal = 0;
            $pageLink = $this->input->post('page_link');
            $inputLink = parse_url($this->input->post('page_link'), PHP_URL_HOST);
           
            $siteUrl = parse_url(site_url(), PHP_URL_HOST);
            
            if ($inputLink == $siteUrl) {
                $internal = 1;
                $pageLink = parse_url($this->input->post('page_link'), PHP_URL_PATH);
            }
            
            if ($this->form_validation->run()) {    
                $maxId = $this->menus_model->getMaxId();
                $data['maxId'] = (int) $maxId->item_id + 1;

                //get data about the current menu
                $menuId = $this->input->post('menu_id');
                $conditions = array(
                    'menus_id' => $menuId
                );                
                $menuData = $this->menus_model->getMenuList($conditions)->row();
                if (empty($menuData)) {
                    redirect('admin/menus');
                }
                
                //if the menu is undeletable, the link will also be undeletable.
                $deletable = 1;
                if ($menuData->is_deletable == 0) {
                    $deletable = 0;
                }
                
                $data['menuItem'] = array(
                    'menu_id' => $this->input->post('menu_id'),
                    'page_link' => $pageLink,
                    'link_name' => $this->input->post('link_name'),
                    'internal' => $internal,
                    'order_id' => $data['maxId'],
                    'is_deletable' => $deletable,
                );
                
                $this->menus_model->setItem($data['menuItem']);
                $this->session->set_flashdata('success', 'You have successfully added new item');
                redirect('admin/menus/add/'.$this->input->post('menu_id'));
            }
        }
        
        $data['menuData'] = $this->menus_model->getMenuList()->result();     
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Add items';
        $data['option'] = $this->uri->segment(4);
        $this->load->view('admin/menus/addItems', $data);
    }
    
    public function editLink($id)
    {        
        $data = array();
        $conditions = array(
            'item_id' => $id
        );      
        
        $itemData = $this->menus_model->getMenuItems($conditions)->row();

        if (empty($itemData)) {
            redirect('admin/menus');
        }
        
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            $itemData->menu_name => 'admin/menus/view/'.$itemData->menus_id,
            'Edit item'
        ));

    
        $pageLink = $itemData->page_link;
        $inputLink = parse_url($pageLink, PHP_URL_SCHEME);

        //if the link is internal
        if ($inputLink == null) {
            
            $data['postData'] = array(
                'page_link' => site_url($pageLink),
                'link_name' => $itemData->link_name,
                'internal' => $itemData->internal,           
            );
            
        } else {
            
            $data['postData'] = array(
                'page_link' => $pageLink,
                'link_name' => $itemData->link_name,
                'internal' => $itemData->internal,           
            );
        }

        
        $this->form_validation->set_rules('page_link', 'Page Link', 'required|trim|min_length[3]|callback__check_valid_url');
        $this->form_validation->set_rules('link_name', 'Link Name', 'required|trim|min_length[3]');
        
        if ($this->input->post()) {          
            $data['postData'] = array_merge($this->input->post() + array('menu_id' => $itemData->menu_id));
            
            //Setting the type of the link (internal or external)
            $internal = 0;
            $pageLink = $this->input->post('page_link');
            $inputLink = parse_url($this->input->post('page_link'), PHP_URL_HOST);
            $siteUrl = parse_url(site_url(), PHP_URL_HOST);
            
            if ($inputLink == $siteUrl) {
                $internal = 1;
                $pageLink = parse_url($this->input->post('page_link'), PHP_URL_PATH);
            }
            
            if ($this->form_validation->run()) {  
                
                $data['itemData'] = array(
                    'page_link' => $pageLink,
                    'link_name' => $this->input->post('link_name'),
                    'internal' => $internal,
                );
                
                $this->menus_model->updateItem($id, $data['itemData']);
                $this->session->set_flashdata('success', 'Your item was successfully edited');
                redirect('admin/menus/view/'.$itemData->menus_id);
            }
            
        }
        
        $data['menuData'] = $this->menus_model->getMenuList()->result();
        $data['title'] = 'Edit item';
        $data['newMessages'] = $this->data['newMessages'];
        
        $this->load->view('admin/menus/editItem', $data);
    }

    public function viewMenu($id)
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Menus' => 'admin/menus',
            'Preview'=> ''
        ));
                
        $data = array();

        $conditions = array(
            'menus_id' => $id
        );
        $data['menuData'] = $this->menus_model->getMenuItems($conditions)->result();

        if ($data['menuData'] == null) {
            redirect('admin/menus');
        }
        $data['title'] = 'Menus';
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/menus/view', $data);
    }

    public function createMenu()
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Menus' => 'admin/menus',
            'Create menu' => ''
        ));
        
        $data = array();
        $data['postData'] = array(
            'name' => '',
            'keyword' => '',
        );

        $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('keyword', 'Keyword', 'required|trim|min_length[3]');
        
        if ($this->input->post()) {
            $data['postData'] = $this->input->post();
                        
            if (array_key_exists('is_deletable', $data['postData'])) {
                $deletable = 1;
            } else {
                $deletable = 0;
            }
            
            if ($this->form_validation->run()) {
                $data['menuData'] = array(          
                    'menu_name' => $this->input->post('name'),
                    'keyword' => $this->input->post('keyword'),
                    'is_deletable' => $deletable,
                );
                
                $this->menus_model->setMenu($data['menuData']);
                $this->session->set_flashdata('success', 'The content has been successfully saved');
                redirect('admin/menus');
            }
        }
        
        $data['title'] = 'Create menu';
        $data['newMessages'] = $this->data['newMessages'];
        
        $this->load->view('admin/menus/create', $data);
    }
    
    public function editMenu($menuId)
    {
        $data = array();
        $conditions = array(
            'menus_id' => $menuId
        );      
        
        $menuData = $this->menus_model->getMenuList($conditions)->row();

        if (empty($menuData)) {
            redirect('admin/menus');
        }
        
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            $menuData->menu_name => 'admin/menus/view/'.$menuData->menus_id,
            'Edit menu'
        ));
    
        $data['postData'] = array(
            'name' => $menuData->menu_name,
            'keyword' => $menuData->keyword,
        );       
                
        $this->form_validation->set_rules('name', 'Name', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('keyword', 'Keyword', 'required|trim|min_length[3]');
        
        if ($this->input->post()) {
            $data['postData'] = $this->input->post();
            
            if ($this->form_validation->run()) {
                $data['menuData'] = array(          
                    'menu_name' => $this->input->post('name'),
                    'keyword' => $this->input->post('keyword'),
                );
                
                $this->menus_model->updateMenu($menuId, $data['menuData']);
                $this->session->set_flashdata('success', 'The menu has been successfully updated');
                redirect('admin/menus');
            }
        }
      
        $data['title'] = 'Edit menu';
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/menus/editMenu', $data);
    }
    
    public function ajaxDelete()
    {
        $id = $this->input->post('id');
        $fields = array();
        $conditions = array(
            'item_id' => $id
        );
        $itemData = $this->menus_model->getMenuItems($conditions)->row();
        
        if ($itemData == array('')) {
            $fields = array(
                'success' => 0
            );
        }
        
        if ($itemData->is_deletable == 1) {

            if ($this->menus_model->deleteItem($id)) {
                $fields = array(
                    'success' => 1
                );
            }          
        } else {
                $fields = array(
                    'success' => 0
                );
            }
            
        echo json_encode($fields);
        die;
    }
    
    public function ajaxReorder()
    {         
        if ($this->input->post()) {
            
            foreach ($this->input->post('order') as $new_order_id => $item_id) {
                $this->menus_model->reorder($new_order_id, $item_id);
            }
            
        } else {
            return false;
        }    
        
        die;
    }
    
    public function ajaxDeleteMenu()
    {
        $id = $this->input->post('id');
        $conditions = array(
            'menus_id' => $id
        );
        $menuData = $this->menus_model->getMenuList($conditions)->row();
        
        if ($menuData == array('')) {
            $fields = array(
                'success' => 0
            );
        }
        
        if ($menuData->is_deletable == 1) {
                  
            if ($this->menus_model->deleteMenu($id)) {
                $fields = array(
                    'success' => 1
                );    
            } 
        
        } else {
            $fields = array(
                'success' => 0
            );
        }
        
        echo json_encode($fields);
        die;
        
    }
    
    public function deleteMenu($menuId)
    {        
        $conditions = array(
            'menus_id' => $menuId
        );
        $menuData = $this->menus_model->getMenuList($conditions)->row();;
        
        if (empty($menuData)) {
            redirect('admin/menus');
        }
        
        if ($menuData->is_deletable != 1) {
            redirect('admin/menus');
        }
        
        $this->menus_model->deleteMenu($menuId);
        $this->session->set_flashdata('success', 'You have successfully deteleted the menu');
        redirect('admin/menus');
    }
    
    public function deleteItem($itemId)
    {        
        $conditions = array(
            'item_id' => $itemId
        );
        $menuData = $this->menus_model->getMenuItems($conditions)->row();
        
        if (empty($menuData)) {
            redirect('admin/menus');
        }
        
        if ($menuData->is_deletable != 1) {
            redirect('admin/menus');
        }
        
        $this->menus_model->deleteItem($itemId);
        $this->session->set_flashdata('success', 'You have successfully deteleted the item');
        redirect('admin/menus/view/'.$menuData->menu_id);
    }
    
    public function _check_valid_url($input)
    {     
        if (filter_var($input, FILTER_VALIDATE_URL) == true) {
            return true;
        } else {
            $this->form_validation->set_message('_check_valid_url', 'Please enter a valid url.');
            return false;
        }
    }
}

