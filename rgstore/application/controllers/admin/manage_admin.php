<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manage_admin extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('manage_admin_m');
    }

    public function index($offset = 0) {

        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Manage admins' => ''
        ));

        $data = array();
        $data['title'] = 'Manage admins';
        $this->load->library('pagination');

        $search_term = $this->input->get('search');

        $data['admins'] = $this->manage_admin_m->getAdmins($search_term, $offset, 6);

        $config = array();
        $config['base_url'] = base_url() . 'index.php/admin/manage_admin/index/';
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['total_rows'] = $data['admins']['count'];
        $config['per_page'] = 6;
        $config['num_links'] = 6;
        $config['uri_segment'] = 4;

        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();


        $data['newMessages'] = $this->data['newMessages'];

        $this->load->view('admin/manage/manage_admin_v', array(
            'title' => $data['title'],
            'admins' => $data['admins']['data'],
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term,
            'links' => $links
        ));
    }

    public function createAdmin() {
        $data = array();

        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Manage admins' => 'admin/manage_admin',
            'Create admin' => ''
        ));

        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|xss_clean|is_unique[users.username]');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|max_length[12]|xss_clean|is_unique[users.fname]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|matches[password]|required');


        $data['postData'] = array(
            'username' => '',
            'name' => ''
        );

        if ($this->input->post()) {
            $data['postData'] = $this->input->post();

            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $data = array(
                    'username' => $this->input->post('username'),
                    'name' => $this->input->post('name'),
                    'password' => $this->hash($this->input->post('password')),
                    'date' => date("Y-m-d H:i:s")
                );
                $this->manage_admin_m->createAdmin($data);


                $this->session->set_flashdata('success', 'New admin has been added :) ');
                redirect('admin/manage_admin');
            }
        }
        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Create admin';
        $this->load->view('admin/manage/manage_admin_create_v', $data);
    }

    public function edit($id) {
        $data = array();

        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Manage admins' => 'admin/manage_admin',
            'Edit' => ' '
        ));


        $data['title'] = 'Edit';
        $data['newMessages'] = $this->data['newMessages'];
        $data['source'] = $this->manage_admin_m->sources($id);
        $this->load->view('admin/manage/manage_admin_edit_v', $data);
    }

    public function editAdmin() {
        $data = array();
        $data = array(
            'id' => $this->input->post('id'),
            'username' => $this->input->post('username'),
            'name' => $this->input->post('name'),
            'password' => $this->hash($this->input->post('password'))
        );

        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[12]|xss_clean');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|max_length[12]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|matches[password]');

        $is_duplicate = $this->manage_admin_m->check_unique($data['id'], $data['username']);

        if ($is_duplicate) {
            $this->session->set_flashdata('error', 'This Username Exist');
            redirect('admin/manage-admin/edit/' . $this->input->post('id'));
        } elseif ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
            redirect('admin/manage-admin/edit/' . $this->input->post('id'));
        } elseif ($this->input->post('password') == '') {
            $this->manage_admin_m->editAdminWithoutPass($data);
            $this->session->set_flashdata('success', 'You have successfully edited  ' . $this->input->post('username') . ' !');
            redirect('admin/manage-admin');
        }
        
        $this->manage_admin_m->editAdmin($data);
        $this->session->set_flashdata('success', 'You have successfully edited  ' . $this->input->post('username') . ' !');
        redirect('admin/manage-admin');
    }

    public function ajaxDelete() {
        $id = $this->input->post('id');

        $conditions = array(
            'id' => $id
        );

        if ($this->manage_admin_m->deleteData($id)) {
            $fields = array(
                'success' => 1
            );
        } elseif (!$this->manage_admin_m->deleteData($id)) {
            $fields = array(
                'success' => 0
            );
        }

        echo json_encode($fields);
        die;
    }

    public function bulkDelete() {
        $ids = $this->input->post('bulkDelete');
        if (!isset($ids) && empty($ids)) {
            redirect('admin/manage_admin');
        }

        foreach ($ids as $key => $id) {
            $this->manage_admin_m->deleteData($id);
            $this->session->set_flashdata('success', 'You have successfuly deleted the admins');
        }
        redirect('admin/manage_admin');
    }

    public function hash($string) {
        return hash("sha1", $string . config_item('encryption_key'));
    }

}
