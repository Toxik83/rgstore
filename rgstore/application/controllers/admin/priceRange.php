<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class PriceRange extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('price_m');
    }

    public function index($offset = 0) {
        $bred_name = '';
        $bred_action = '';

        if ($_SERVER['HTTP_REFERER'] == 'http://rgstore.local/admin/finishing') {
            $bred_name = 'Finishing';
            $bred_action = 'admin/finishing';
        }else if($_SERVER['HTTP_REFERER'] == 'http://rgstore.local/admin/filament'){
            $bred_name = 'Filament';
            $bred_action = 'admin/filament';
        }else if($_SERVER['HTTP_REFERER'] == 'http://rgstore.local/admin/printers'){
            $bred_name = 'Printers';
            $bred_action = 'admin/printers';
        }

        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            $bred_name => $bred_action,
            'Price Range' => ''
        ));


        $data = array();
        $data['title'] = 'Price Range';

        $this->load->library('pagination');


        $data['prices'] = $this->price_m->getPrices($offset, 6);
        
        
        $data['postData'] = array(
            'min' => '',
            'max' => ''
        );

        $config = array();
        $config['base_url'] = base_url() . 'admin/priceRange/index/';
        $config['total_rows'] = $data['prices']['count'];
        $config['per_page'] = 6;
        $config['num_links'] = 6;
        $config['uri_segment'] = 4;

        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();

        $this->form_validation->set_rules('min', 'Min', 'trim|required|min_length[1]|max_length[12]|xss_clean|is_natural');
        $this->form_validation->set_rules('max', 'Max', 'trim|required|min_length[1]|max_length[12]|xss_clean|is_natural');

        $min = $this->input->post('min');
        $max = $this->input->post('max');
                       
        
        $is_duplicate = $this->price_m->check_unique($min);
        $is_duplicate2 = $this->price_m->check_unique2($max);

        if ($this->input->post()) {
            $data['postData'] = $this->input->post();
                
            $price = $this->price_m->getWhatYouNeed();

            if($min > $max){
                $this->session->set_flashdata('error', 'This is not a valid price range !!! MAX always should be bigger number than MIN :0');
                redirect('admin/price-range'); 
            }
            
            foreach ($price as $value) {
                if($min > $value->min && $max < $value->max){
                    $this->session->set_flashdata('error', 'There si already price range like this !!! ');
                    redirect('admin/price-range');
                }else if($min > $value->min && $min < $value->max){
                    $this->session->set_flashdata('error', 'There is already price range with this MIN value !!! ');
                    redirect('admin/price-range'); 
                }
            }

            if ($this->form_validation->run() == FALSE) {
                
            } else if ($min > $max) {
                $this->session->set_flashdata('error', 'Your MAX price cannot be lower then you min price !!!');
                redirect('admin/price-range');
            } else if ($is_duplicate) {

                $this->session->set_flashdata('error', 'There is already same MIN price .');
                redirect('admin/price-range');
            } else if ($is_duplicate2) {

                $this->session->set_flashdata('error', 'There is already same MAX price .');
                redirect('admin/price-range');
            } else {
                $data = array(
                    'min' => $this->input->post('min'),
                    'max' => $this->input->post('max'),
                    'date' => date("Y-m-d H:i:s")
                );
                $this->price_m->createRange($data);

                $this->session->set_flashdata('success', 'New price range has been successfully created :)');
                redirect('admin/price-range');
            }
        }


        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/price/priceRange_v', array(
            'title' => $data['title'],
            'prices' => $data['prices']['data'],
            'postData' => $data['postData'],
            'links' => $links,
            'newMessages' => $this->data['newMessages']
        ));
    }

    public function ajaxReorder() {
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->price_m->reorder($key, $value);
            }
        } else {
            return false;
        }
        die;
    }

    public function ajaxDelete() {
        $id = $this->input->post('id');
        $conditions = array(
            'id' => $id
        );
        $data['prices'] = $this->price_m->getPricesById($id);
        foreach ($data['prices'] as $key =>$prices){
            $minPrice = $prices ->min;
            $maxPrice = $prices->max;
            $data['result'] = $this->price_m->countRecords($minPrice,$maxPrice);
        }
        if($data['result']['count'] != 0){
            $fields = array(
                'success' => 0
            );
        } else {
            $this->price_m->deleteData($id);
            $fields = array(
                'success' => 1
            );
        }

        

        echo json_encode($fields);
        die;
    }
    
    public function edit($id) {
        $data = array();

        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Price Range' => 'admin/priceRange',
            'Edit Price Range' => ''
        ));
        


        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Edit';
        $data['source'] = $this->price_m->sources($id);
        
        $this->load->view('admin/price/priceRange_edit_v', $data);
    }
    
    public function editRange() {

        $data = array();

        

        $this->form_validation->set_rules('min', 'Min', 'trim|required|min_length[1]|max_length[12]|xss_clean|numeric');
        $this->form_validation->set_rules('max', 'Max', 'trim|required|min_length[1]|max_length[12]|xss_clean|numeric');

        $data = array(
            'id' => $this->input->post('id'),
            'min' => $this->input->post('min'),
            'max' => $this->input->post('max'),
            'date' => $this->input->post('date')
        );
        
        $min = $this->input->post('min');
        $max = $this->input->post('max');

        $id = $this->input->post('id');
        $is_duplicate = $this->price_m->check_uniqueDub($id, $min);
        $is_duplicate2 = $this->price_m->check_uniqueDub2($id, $max);


        $price = $this->price_m->getWhatYouNeed();

            if($min > $max){
                $this->session->set_flashdata('error', 'This is not a valid price range !!! MAX always should be bigger number then MIN :0');
                redirect('admin/price-range/edit/' . $this->input->post('id'));
            }
            
            foreach ($price as $value) {
                if($min > $value->min && $max < $value->max){
                    $this->session->set_flashdata('error', 'There si already price range like this !!! ');
                    redirect('admin/price-range/edit/' . $this->input->post('id'));
                }else if($min > $value->min && $min < $value->max){
                    $this->session->set_flashdata('error', 'There is already price range with this MIN value !!! ');
                    redirect('admin/price-range/edit/' . $this->input->post('id'));
                }
            }
        
        if ($this->form_validation->run() == FALSE) {
            
        } else if ($min > $max) {
            $this->session->set_flashdata('error', 'Your MAX price cannot be lower then you min price !!!');
            redirect('admin/price-range/edit/' . $this->input->post('id'));
        } else if ($is_duplicate) {
            $this->session->set_flashdata('error', 'There is already same MIN price .');
            redirect('admin/price-range/edit/' . $this->input->post('id'));
        } else if ($is_duplicate2) {

            $this->session->set_flashdata('error', 'There is already same MAX price .');
            redirect('admin/price-range/edit/' . $this->input->post('id'));
        }

        $this->price_m->editRange($data);
        $this->session->set_flashdata('success', 'You have successfully edited price range with id : ' . $this->input->post('id') . ' !');
        redirect('admin/price-range');
    }
    
//    public function bulkDelete() {
//        $ids = $this->input->post('bulkDelete');
//
//        if ($ids == false) {
//            $this->session->set_flashdata('success', 'You need to choose items first !');
//            redirect('admin/priceRange');
//        } else {
//            foreach ($ids as $key => $id) {
//                $this->price_m->deleteData($id);
//                $this->session->set_flashdata('success', 'You have successfully deleted selected finishing products');
//                redirect('admin/priceRange');
//            }
//        }
//    }
    
    public function bulkDelete1() {
        $ids = $this->input->post('bulkDelete');
        if ($ids == false) {
            $this->session->set_flashdata('success', 'You need to choose items first!');
            redirect('admin/priceRange');
        } 
        foreach ($ids as $key=>$id){
            $data['prices'] = $this->price_m->getPricesById($id);
        }
        $data['prices'] = $this->price_m->getPricesById($id);
        foreach ($data['prices'] as $key =>$prices){
            $minPrice = $prices ->min;
            $maxPrice = $prices->max;
            $data['result'] = $this->price_m->countRecords($minPrice,$maxPrice );
        }
                
                
                if($data['result']['count'] == 0) {
                    $this->price_m->deleteData($id);
                    $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                    redirect('admin/priceRange');
                } else {
                    $this->session->set_flashdata('error', 'You have products in your category!');
                    redirect('admin/priceRange');
                }
            }

}
