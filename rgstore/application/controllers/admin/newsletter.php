<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletter extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('newsletter_m');
    }

    public function index($offset = 0) {
        
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Newsletter' => ''
        ));
        
        $data = array();
        $data['title'] = 'Newsletter';
        $this->load->library('pagination');

        $search_term = $this->input->get('search');

        $data['newsletters'] = $this->newsletter_m->getNewsletter($search_term, $offset, 6);

        $config = array();
        $config['base_url'] = base_url() . 'admin/newsletter/index/';
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['total_rows'] = $data['newsletters']['count'];
        $config['per_page'] = 6;
        $config['num_links'] = 6;
        $config['uri_segment'] = 4;

        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();
        $data['newMessages'] = $this->data['newMessages'];
        
        
        $this->load->view('admin/newsletter_v', array(
            'title' => $data['title'],
            'newsletters' => $data['newsletters']['data'],
            'search_term' => $search_term,
            'links' => $links,
            'newMessages' => $this->data['newMessages']
        ));
    }

    public function deleteNewsletter() {
        $id = $this->uri->segment(4);
        $this->newsletter_m->delete($id);
        $this->session->set_flashdata('success', 'You have successfully deleted newsletter !');
        redirect('admin/newsletter');
    }

    public function bulkDelete() {
      
        $ids = $this->input->post('bulkDelete');
        
        if ($ids == false) {
            $this->session->set_flashdata('error', 'You need to choose items first !');
            redirect('admin/newsletter');
        }

        foreach ($ids as $key => $id) {
            $this->newsletter_m->deleteData($id);
            $this->session->set_flashdata('success', 'You have successfuly deleted the emails');
        }
        redirect('admin/newsletter');
    }

    public function ajaxDelete() {
        $id = $this->input->post('id');

        $conditions = array(
            'id' => $id
        );

        if ($this->newsletter_m->deleteData($id)) {
            $fields = array(
                'success' => 1
            );
        } elseif (!$this->newsletter_m->deleteData($id)) {
            $fields = array(
                'success' => 0
            );
        }

        echo json_encode($fields);
        die;
    }

    public function email() {
        $this->form_validation->set_rules('text', 'Email', 'required');        
        $text = $this->input->post('text');
        
        if ($this->form_validation->run()) {
            $recordsDB = $this->newsletter_m->getRecords();
            $records = array();
            foreach ($recordsDB as $key => $value) {
                $records[] = $value['email'];
            }
            
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'ssl://smtp.googlemail.com';
            $config['smtp_port'] = '465';
            $config['smtp_timeout'] = '30';
            $config['smtp_user'] = 'rgatewaytest@gmail.com';
            $config['smtp_pass'] = 'rgatewaytest1q2w3e4r';
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';

            $this->load->library('email', $config);

            $this->email->from('rgatewaytest@gmail.com', 'Reward Gateway');
            $this->email->to($records);
            $this->email->subject('Newsletter Mail');
            $this->email->message($text);

            if ($this->email->send()) {
                $this->session->set_flashdata('success', 'You have successfuly send email :)');
            } else if(!$recordsDB) {
                $this->session->set_flashdata('error', 'There is no subsribers yet :( ');
            } else {
                $this->session->set_flashdata('error', 'Something went wrong :( ');
            }
            redirect('admin/newsletter');
        } else {
            $this->session->set_flashdata('error', 'Email content cannot be empty ! ');
            redirect('admin/newsletter');
        }
    }

}
