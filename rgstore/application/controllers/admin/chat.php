<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Chat extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('chat_m');
    }

    public function index($offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Live chat' => ''
        ));

        $data['title'] = 'Chat Room';
        $this->load->library('session');
        $this->load->library('pagination');

        $search_term = $this->input->get('search');

        $sessionUserID = $this->session->userdata('username');
        if (!$sessionUserID){
            redirect();
        }
        $data['listOfUsers'] = $this->chat_m->getChatUsers($search_term, $offset, 20);
        $config = array();
        $config['base_url'] = base_url() . 'admin/chat/index/';
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['total_rows'] = $data['listOfUsers']['count'];
        $config['per_page'] = 20;
        $config['num_links'] = 6;
        $config['uri_segment'] = 4;

        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();


        $data['newMessages'] = $this->data['newMessages'];

        $this->load->view('chat/chatRoom', array(
            'title' => $data['title'],
            'listOfUsers' => $data['listOfUsers']['data'],
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term,
            'links' => $links,
        ));
    }

    public function chatListing() {
        
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Live chat' => 'admin/chat',
            'Chat history' => ''
        ));

        $data['title'] = 'Chat History';
        $this->load->library('pagination');

        $id = $this->uri->segment(4);
        $offset = $this->uri->segment(6);
        $search_term = $this->input->get('search');
        $searchBy = $this->input->get('searchBy');
        $option = '';

        //Checking if "get" request is set
        if ($this->input->get()) {
            switch ($searchBy) {

                case 1: $option = 'from';
                    break;
                case 2: $option = 'to';
                    break;
                case 3: $option = 'message';
                    break;
                default : $option = 'from';
            }
        }

        $data['listOfUsers'] = $this->chat_m->getChatHistory($id, $search_term, $offset, 40, $option);
        $config = array();
        $config['base_url'] = base_url() . 'admin/chat/chatListing/' . $id . '/index/';
        $config['suffix'] = '?searchBy=' . $searchBy . '&search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['total_rows'] = $data['listOfUsers']['count'];
        $config['per_page'] = 40;
        $config['num_links'] = 6;
        $config['uri_segment'] = 6;

        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        
       
        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();

        $data['chatName'] = $this->chat_m->chatName($id);
        $data['newMessages'] = $this->data['newMessages'];

        $this->load->view('chat/chatHistory', array(
            'title' => $data['title'],
            'listOfUsers' => $data['listOfUsers']['data'],
            'newMessages' => $this->data['newMessages'],
            'chatName' => $data['chatName'],
            'search_term' => $search_term,
            'links' => $links,
        ));
    }

}
