<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class FinishingGallery extends MY_Controller {

    protected $original_path;
    protected $resized_path;
    protected $thumbs_path;
    protected $resized_path1;
    protected $resized_path2;
    protected $resized_path3;

    public function __construct() {
        parent::__construct();
        $this->load->model('gallery_m');

        $this->original_path = realpath(APPPATH . '../upload/original');
        $this->resized_path = realpath(APPPATH . '../upload/images280x280');
        $this->thumbs_path = realpath(APPPATH . '../upload/thumbs');
        $this->resized_path1 = realpath(APPPATH . '../upload/images155x155');
        $this->resized_path2 = realpath(APPPATH . '../upload/images60x70');
        $this->resized_path3 = realpath(APPPATH . '../upload/images500x500');
    }

    public function getImages($id = 0) {

        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Finishing' => 'admin/finishing',
            'Gallery' => ''
        ));

        $data = array();
        $data['title'] = 'Gallery';
        $data['newMessages'] = $this->data['newMessages'];
        $data['images'] = $this->gallery_m->getImages($id);

        if ($this->input->post()) {
            $id = $this->input->post('id');
            
            if ($this->upload() == "") {
                $this->session->set_flashdata('error_validation', $this->upload->display_errors('<div class="error">', '</div>'));
                redirect('admin/finishing-gallery/images/' . $id);
            }

            $image = $this->upload();
            $this->gallery_m->save($image, $id);
            $this->session->set_flashdata('success', 'Your image file was successfully created');
            redirect('admin/finishing-gallery/images/' . $id);
        }
        $this->load->view('admin/finishing/gallery/listing', $data);
    }

    public function upload() {
        
        $this->load->library('image_lib');

        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png',
            'max_size' => 2048,
            'upload_path' => $this->original_path,
            'file_name' => sha1(time())
        );

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('image')) {

            $data = array('upload_data' => $this->upload->data());
            $image_data = $data['upload_data'];
            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path,
                'maintain_ratio' => false,
                'width' => 280,
                'height' => 280
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->thumbs_path,
                'maintain_ratio' => false,
                'width' => 65,
                'height' => 65
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path1,
                'maintain_ratio' => false,
                'width' => 155,
                'height' => 155
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path2,
                'maintain_ratio' => false,
                'width' => 60,
                'height' => 70
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();

            $config = array(
                'source_image' => $image_data['full_path'],
                'new_image' => $this->resized_path3,
                'maintain_ratio' => false,
                'width' => 500,
                'height' => 500
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize();


            return $this->upload->file_name;
        } else {
            return "";
        }
    }
    
    public function ajaxDelete() {
        $id = $this->input->post('id');
        
        $check = $this->gallery_m->checkImage($id);
       
        if((int)$check->main_pic == 1){
             $fields = array(
                'success' => 0
            );
        }else {
            $this->gallery_m->deleteImage($id);
            $fields = array(
                'success' => 1
            );
        } 

        echo json_encode($fields);
        die;
    }

    public function ajaxReorder() {
        
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->gallery_m->reorder($key, $value);
            }
        } else {
            return false;
        }
        die;
    }
    
    public function make_main_pic() {

        $product_id = $this->input->post('product_id');
        $id = $this->input->post('pic_id');

        if ($id == false) {
            $this->session->set_flashdata('success', 'You need to choose Main picture first !');
            redirect('admin/finishing-gallery/images/' . $this->input->post('product_id'));
        }

        $this->gallery_m->mainPic($id);
        $this->gallery_m->unMakemainPic($id, $product_id);
        $this->session->set_flashdata('success', 'Your Main picture has been successfully changed !');
        redirect('admin/finishing-gallery/images/' . $this->input->post('product_id'));
    }

}
