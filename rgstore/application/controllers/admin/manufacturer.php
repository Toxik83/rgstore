<?php

class Manufacturer extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('manufacturer_model');
    }

  public function index($offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Manufacturer'
        ));
        $this->load->library('pagination');
        $data = array();
        $title = 'Manufacturer';
        $data['title'] = $title;
        $search_term = $this->input->get('keyword');
        $data['companies'] = $this->manufacturer_model->show_Manufacturer($search_term, $offset, 8);
        $config = array();
        $config['base_url'] = base_url() . 'index.php/admin/manufacturer/index';
        $config['total_rows'] = $data['companies']['count'];
        $config['suffix'] = '?search=' . $search_term;
        $config['first_url'] = $config['base_url'] . $config['suffix'];
        $config['per_page'] = 8;
        $config['num_links'] = 9;
        $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
        $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="first_page">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li class="prev_page"><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li class="next_page"><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li class="last_page">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        if ($this->uri->segment(4) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(4) - 1);
        }
        $links = $this->pagination->create_links();

//        $this->createUploadFolder();
        $this->load->view('admin/manufacturer_add', array(
            'companies' => $data['companies']['data'],
            'links' => $links,
            'title' => $title,
            'newMessages' => $this->data['newMessages'],
            'search_term' => $search_term
        ));       

    }

    public function add() {
        $this->load->library('form_validation');
        $name = $this->input->post('man_name');
        $is_duplicate = $this->manufacturer_model->check_unique($name);
        $this->form_validation->set_rules('man_name', 'Company', 'required|trim|xss_clean');

        if ($this->input->post()) {
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_validation', validation_errors('<div class="error">', '</div>'));
                redirect('admin/manufacturer');
            } elseif ($is_duplicate) {
                $this->session->set_flashdata('error', 'This Name Exist');
                redirect('admin/manufacturer');
            } else {
                $this->load->model('manufacturer_model');
                $maxId = $this->manufacturer_model->getMaxId(); 
                $order_id = (int) $maxId->id + 1;
                $name = $this->input->post('man_name');
                $this->manufacturer_model->create($order_id,$name);
                $this->session->set_flashdata('success', 'You have successfully created a category   !');
                redirect('admin/manufacturer');
            }
        }
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/manufacturer_add', $data);
    }

    public function update($id) {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Printers' => 'admin/printers',
            'Manufacturer' => 'admin/manufacturer',
            'Edit Manufacturer'
        ));
        $this->load->model('manufacturer_model');
        $data['companies'] = $this->manufacturer_model->find($id);
        $data['title'] = 'Edit Manufacturer';
        $data['newMessages'] = $this->data['newMessages'];
        $this->load->view('admin/manufacturer_edit', $data);
    }

    public function delete($id) {
        $data['result'] = $this->manufacturer_model->countRecords($id);
            if($data['result']['count'] == 0) {
                $this->manufacturer_model->delete($id);
                $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                redirect('admin/manufacturer');
            } else {
                $this->session->set_flashdata('error', 'You have products in your category!');
                redirect('admin/manufacturer');
            }
        
    }

    public function edit_manufacturer() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('man_name', 'Company', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('man_key', 'Please Edit Company name!');
            return redirect('admin/manufacturer/update/' . $this->input->post('man_id'));
        } else {
            $id = $this->input->post('man_id');
            $name = $this->input->post('man_name');
            $this->manufacturer_model->update($id, $name);
             $this->session->set_flashdata('success', 'You have successfully edited  ' . $this->input->post('name') . ' !');
            return redirect('admin/manufacturer');
        }
    }

    public function ajaxReorder() {
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->manufacturer_model->reorder($key, $value);
            }
        } else {
            return false;
        }

        die;
    }
      public function bulkDelete() {
          
        $ids = $this->input->post('bulkDelete');
        
        if ($ids == false) {
            $this->session->set_flashdata('error', 'You need to choose items first !');
            redirect('admin/manufacturer');
        } else {

        foreach ($ids as $key => $id) {
            
            $data['result'] = $this->manufacturer_model->countRecords($id);
            if($data['result']['count'] == 0) {
                $this->manufacturer_model->delete($id);
                $this->session->set_flashdata('success', 'You have successfully deleted a category!');
                redirect('admin/manufacturer');
            } else {
                $this->session->set_flashdata('error', 'You have products in your category!');
                redirect('admin/manufacturer');
                }
            }
        }
    }
}
