<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class adminSlider extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('slider_model');
    }

    public function index() {

        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Slider'
        ));

        $sliderData = $this->slider_model->getSliderData()->result();
        $data['sliderData'] = $sliderData;

        $data['newMessages'] = $this->data['newMessages'];
        $data['title'] = 'Listing';    

        $this->load->view('admin/slider/listing', $data);
    }   

    public function createSlide() 
    {
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Slider' => 'admin/admin-slider',
            'Create Slide'
        ));
        $data = array();

        $maxId = $this->slider_model->getMaxId();
        $data['maxId'] = (int) $maxId->id + 1;

        $data['postData'] = array(
            'title' => '',
            'description' => '',
            'url' => '',
            'image' => '',
        );

        $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('description', 'Description', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('url', 'URL', 'required|trim|min_length[3]|callback__check_valid_url');
        
        $error = array();
        $config = array();
        $config['upload_path'] = './upload/home-slider/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048';
        $config['max_width'] = '1920';
        $config['max_height'] = '1080';
        $config['file_name'] = sha1(time());

        $this->load->library('upload', $config);

        if ($this->input->post()) {
            $data['postData'] = $this->input->post();
            
            if ($this->form_validation->run()) {
                
                //Setting the type of the link (internal or external);
                $internal = 0;

                $inputLink = parse_url($this->input->post('url'), PHP_URL_HOST);
                $siteUrl = parse_url(site_url(), PHP_URL_HOST);

                if ($inputLink == $siteUrl) {
                    $internal = 1;
                }

                if ($this->upload->do_upload('image')) {
                    $fileData = $this->upload->data();
                    $fileName = $fileData['file_name'];
                    $fileNameThumb = '1400x400_' . $fileName;
                    $listImage = '300x100_' . $fileName;

                    $config_resize = array();
                    $config_resize['image_library'] = 'gd2';
                    $config_resize['create_thumb'] = false;
                    $config_resize['maintain_ratio'] = false;
                    $config_resize['quality'] = "100%";
                    $config_resize['source_image'] = $fileData['full_path'];
                    $config_resize['new_image'] = './upload/home-slider/' . $fileNameThumb;
                    $config_resize['height'] = 400;
                    $config_resize['width'] = 1400;                   
                    $this->load->library('image_lib', $config_resize);
                    $this->image_lib->initialize($config_resize);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }

                    $config_resize['new_image'] = './upload/home-slider/' . $listImage;
                    $config_resize['height'] = 100;
                    $config_resize['width'] = 300;
                    $this->load->library('image_lib', $config_resize);
                    $this->image_lib->initialize($config_resize);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }

                    $data['sliderData'] = array(
                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description'),
                        'url' => $this->input->post('url'),
                        'order_id' => $data['maxId'],
                        'internal' => $internal,
                        'image' => $fileNameThumb,
                        'image300x100' => $listImage,
                        'created_at' => date("Y-m-d H:i:s")
                    );

                    $this->slider_model->setSliderData($data['sliderData']);
                    $this->session->set_flashdata('success', 'Your slide was successfully created');
                    redirect('admin/admin-slider');
                } else {
                    $error = array('error' => $this->upload->display_errors());
                }
            }
        }
            
        $data['newMessages'] = $this->data['newMessages'];
        $data['error'] = $error;
        $data['title'] = 'Create Slide';
        $this->load->view('admin/slider/createSlide', $data);
    }

    public function editSlide() 
    {      
        $this->breadcrumb->populate(array(
            'Home' => 'admin/administration',
            'Slider' => 'admin/admin-slider',
            'Edit Slide'
        ));
        
        $data = array();
        $id = ($this->uri->segment(4)) ? (int) $this->uri->segment(4) : 0;
        if ($id == 0) {
            redirect('admin/admin-slider');
        }
        
        $conditions = array(
            'id' => $id
        );

        $data['sliderData'] = $this->slider_model->getSliderData($conditions)->row();
        $sliderData = $data['sliderData']; 
        
        if ($sliderData == null) {
            redirect('admin/admin-slider');
        }
        
        $data['postData'] = array(
            'title' => $sliderData->title,
            'description' => $sliderData->description,
            'url' => $sliderData->url,
            'image' => $sliderData->image,
            'image300x100' => $sliderData->image300x100,
        );

        $error = array();
        $config = array();
        $config['upload_path'] = './upload/home-slider/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048';
        $config['max_width'] = '1920';
        $config['max_height'] = '1080';
        $config['file_name'] = sha1(time());

        $this->load->library('upload', $config);
        $this->load->library('form_validation');

        if ($this->input->post()) {
            $data['postData'] = $this->input->post();

            $this->form_validation->set_rules('title', 'Title', 'required|trim|min_length[3]');
            $this->form_validation->set_rules('description', 'Description', 'required|trim|min_length[3]');
            $this->form_validation->set_rules('url', 'URL', 'required|trim|min_length[3]');

            if ($this->form_validation->run()) {

            //Setting the type of the link (internal or external);
            $internal = 0;
            
            $inputLink = parse_url($this->input->post('url'), PHP_URL_HOST);
            $siteUrl = parse_url(site_url(), PHP_URL_HOST);
            
            if ($inputLink == $siteUrl) {
                $internal = 1;
            }
            
                //If not selected new slider image
                if ($_FILES['image']['name'] == '') {
                    $data['slideData'] = array(
                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description'),
                        'url' => $this->input->post('url'),
                        'internal' => $internal,
                        'updated_at' => date("Y-m-d H:i:s"),
                    );
                    
                    $this->slider_model->updateSliderData($id, $data['slideData']);
                    $this->session->set_flashdata('success', 'You have successfully updated the slide.');
                    redirect('admin/admin-slider');
                }

                //If new slider image is selected -> do image upload and resize
                if ($this->upload->do_upload('image')) {
                    $fileData = $this->upload->data();
                    $fileName = $fileData['file_name'];
                    $fileNameThumb = '1400x400_' . $fileName;
                    $listImage = '300x100' . $fileName;

                    $config_resize = array();
                    $config_resize['image_library'] = 'gd2';
                    $config_resize['create_thumb'] = false;
                    $config_resize['maintain_ratio'] = false;
                    $config_resize['quality'] = "100%";
                    $config_resize['source_image'] = $fileData['full_path'];
                    $config_resize['new_image'] = './upload/home-slider/' . $fileNameThumb;
                    $config_resize['height'] = 400;
                    $config_resize['width'] = 1400;                   
                    $this->load->library('image_lib', $config_resize);
                    $this->image_lib->initialize($config_resize);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    
                    $config_resize['new_image'] = './upload/home-slider/' . $listImage;
                    $config_resize['height'] = 100;
                    $config_resize['width'] = 300;
                    $this->load->library('image_lib', $config_resize);
                    $this->image_lib->initialize($config_resize);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }

                    $data['postData'] = array(
                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description'),
                        'url' => $this->input->post('url'),
                        'internal' => $internal,
                        'image' => $fileNameThumb,
                        'image300x100' => $listImage,
                        'updated_at' => date("Y-m-d H:i:s")
                    );

                    $this->slider_model->updateSliderData($id, $data['postData']);
                    $this->session->set_flashdata('success', 'You have successfully updated the slide.');
                    redirect('admin/admin-slider');
                } else {
                    $error = array('error' => $this->upload->display_errors());
                }
            }
        }
        $data['newMessages'] = $this->data['newMessages'];
        $data['error'] = $error;
        $data['title'] = 'Edit Slide';
        $this->load->view('admin/slider/createSlide', $data);
    }

    public function deleteSlide($sliderId)
    {               
        $conditions = array(
            'id' => $sliderId
        );
        $limit = array();
        $sliderData = $this->slider_model->getSliderData($conditions, $limit)->row();

        if (empty($sliderData)) {
            redirect('admin/admin-slider');
        }
        
        $this->slider_model->deleteSliderData($sliderId);
        $this->session->set_flashdata('success', 'You have successfully deteleted the slide');
        redirect('admin/admin-slider');
    }
    
    public function ajaxDelete()
    {
        $id = $this->input->post('id');
        
        if ($this->slider_model->deleteSliderData($id)) {
            $fields = array(
                'success' => 1
            );
            
        } elseif (!$this->slider_model->deleteSliderData($id)) {
            $fields = array(
                'success' => 0
            );
        }
               
        echo json_encode($fields);
        die;
    }

    public function bulkDelete() 
    {
        $ids = $this->input->post('bulkDelete');
        if (!isset($ids) && empty($ids)) {
            redirect('admin/admin-slider');
        }
        
        foreach ($ids as $key => $id) {
            $this->slider_model->deleteSliderData($id);
            $this->session->set_flashdata('success', 'You have successfuly deleted the slides');
        }
        redirect('admin/admin-slider');
    }

    public function ajaxReorder()
    {      
        if ($this->input->post()) {
            foreach ($this->input->post('order') as $key => $value) {
                $this->slider_model->reorder($key, $value);
            }
        } else {
            return false;
        }    
        
        die;
    }
}
