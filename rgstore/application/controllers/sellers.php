<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sellers extends MY_Controller_Front {

    public function __construct() {
        parent::__construct();
        $this->load->model('filament_model');
        $this->load->model('printers_m');
        $this->load->model('order_model');
        $this->load->model('search_m');
    }

    public function products($offset = 0) {

        $this->breadcrumb->populate(array(
            'Home' => '',
            'All Bestsellers'
        ));
        $this->load->library('pagination');
        $data = array();
        $title = 'Bestsellers';
        $data['title'] = $title;
        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $this->load->model('filament_model');
        $this->load->model('order_model');

        //$currentTime = date("Y-m-d H:i:s");
        $data['bestsellerslist'] = $this->order_model->bestSellers($offset, 12);
      
        $config = array();
        $config['base_url'] = base_url() . 'sellers/products';
        $config['total_rows'] = $data['bestsellerslist']['count'];
        $config['per_page'] = 12;
        $config['num_links'] = 9;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        if ($this->uri->segment(3) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(3) - 1);
        }
        $links = $this->pagination->create_links();
        $this->load->view('productslisting', array(
            'bestsellerslist' => $data['bestsellerslist']['data'],
            'title' => $title,
            'links' => $links,
            'search' => $terms,
            'postData' => array('searchBy' => '')
        ));
    }

  

    public function search() {
        $this->load->library('pagination');

        $this->breadcrumb->populate(array(
            'Home' => '',
            'Best Seller' => ''
        ));
        $checkURL = $this->uri->segment(1);
        
        $data = array();
        $currentTime = date("Y-m-d H:i:s");

        if ($checkURL == 'sellers') {
            $flag = 1;
        } else {
            $flag = 0;
        }

        $like = array();
       
        if ($this->input->get()) {

            switch ($this->input->get('orderBy')) {

                case 1: $option = 'price';
                    $option2 = 'ASC';
                    break;
                case 2: $option = 'price';
                    $option2 = 'DESC';
                    break;
                case 3: $option = 'title';
                    $option2 = 'ASC';
                    break;
                case 4: $option = 'title';
                    $option2 = 'DESC';
                    break;
                default : $option = 'date_added';
                    $option2 = 'DESC';
            }
        }
        

        $total = $this->search_m->getSearchResult2(
                         $option, $option2,null
                )->num_rows();
      
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

         $pageLimit = 12;
        if(array_key_exists('per', $this->input->get())){
            $pageLimit = $this->input->get('per');
        }
       
        $max = array($pageLimit, ($page - 1) * $pageLimit);
           
        $bestSellers = $this->search_m->getSearchResult2($option, $option2,$max)->result_array();
        
        $data['messageData'] = $bestSellers;

        //setting the $config array for the pagination
        $config = array();
        $config['base_url'] = base_url() . 'sellers/search';

        if ($this->input->get() != false) {
            $config['suffix'] = '?' . http_build_query($this->input->get(), '', '&');
            $config['first_url'] = base_url() . 'sellers/search/1' . $config['suffix'];
        }

        $config['total_rows'] = $total;
        $config['per_page'] = $pageLimit;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = true;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        //initializing pagination
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['title'] = 'Sellers';

        $terms = $this->input->get('search');

        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );

        $current_time = time();
        $data['current_time'] = $current_time;
        $this->load->view('sort/product_list', $data);
    }

}

