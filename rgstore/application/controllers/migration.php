<?php
class Migration extends CI_Controller {

    public function _construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->library('migration');
        $this->migration->latest();
        if (!$this->migration->latest()) {
            show_error($this->migration->error_string());
        } else {
            echo "Migration worked";
        }
    }

}
