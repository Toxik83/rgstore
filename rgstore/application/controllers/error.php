<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Error extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {

        $data['title'] = 'Page not found';

        $terms = $this->input->get('search');

        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );

        $this->load->view('not_found', $data);
    }

}
