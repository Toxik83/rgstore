<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class login extends MY_Controller_Front {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('chat_m');
    }

    public function validate()
    {           
        if (isLogged()) {
            $this->session->set_flashdata('success', 'You are already logged in');
            redirect('');
        }
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Login/Register',
        ));
        
        $data = array();
        $data['postData'] = array(
            'email' => '',
            'searchBy' => '',
        );
        
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valud_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->input->post()) {       
            $data['postData'] = array_merge($this->input->post(), array('searchBy' => ''));
            
            if ($this->form_validation->run()) {
                $conditions = array(
                    'email' => $this->input->post('email'),
                    'password' => $this->hash($this->input->post('password'))
                );
                
                $query = $this->users_model->getUserData($conditions);              
                $userData = $query->row();
                if ($query->num_rows() > 0) {
                    
                    $sessionData = array(
                        'userId' => $userData->id,
                        'username' => $userData->fname,
                        'email' => $userData->email,
                        'logged_in' => true,
                    );
                    
                    if ($this->input->post('remember') == 1) {
                        setcookie('test', hash('sha1', $userData->id), time()+(60*60*24*30), '/');                      
                    }
                    
                    $this->session->set_userdata($sessionData);      
                    $this->chat_m->setOnlineUser();
                    redirect();
                } else {          
                    $this->session->set_flashdata('validation_failed', 'Incorrect login data.');
                    redirect('login');
                }
            }       
        }

        $data['title'] = 'Login';
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        
        $this->load->view('login/login_view', $data);
    }
    
    public function logout()
    {
        $this->chat_m->setOfflineUser();
        $this->session->sess_destroy();
        if (isset($_COOKIE['test'])) {
            setcookie('test', '', 1, '/');
        }
        redirect();  
    }
    
    public function forgottenPassword()
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Login/Register' => 'login',
            'Forgot Password'
        ));
        
        $data = array();
        $data['postData'] = array(
            'email' => '',
            'searchBy' => ''
        );
        
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valud_email|callback__check_email');
        
        if ($this->input->post()) {
            $data['postData'] = $this->input->post() + array('searchBy' => '');

            if ($this->form_validation->run()) {
               $reset_token = $this->hash($this->input->post('email') . date("Y-m-d H:i:s"));
               
               $email = $this->input->post('email');
               
               $values = array(             
                   'reset_token' => $reset_token,
                   'reset_at' => date("Y-m-d H:i:s"),
               );
               $this->users_model->updateUserData($email, $values);
               
               $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'rgatewaytest@gmail.com',
                    'smtp_pass' => 'rgatewaytest1q2w3e4r',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

                $this->email->from('rgatewaytest@gmail.com', 'Administrator');
                $this->email->to($email);
                $this->email->subject('Password reset');
                $this->email->message('Your reset link is: '.site_url('login/reset/'.$reset_token));

                $result = $this->email->send();

                $this->session->set_flashdata('success', 'We have sent you an email with a link where you can set up a new password');
                redirect('login/validate');
            }
        }
        $terms = $this->input->get('search');       
        $data['search'] = $terms;
        $data['title'] = 'Forgotten password';
        $this->load->view('login/forgottenPassword_view', $data);
    }

    public function reset()
    {   
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Login/Register'
        ));
        
        $data = array();
        $token = $this->uri->segment(3);
        
        $conditions = array(
            'reset_token' => $token
        );
        $tokenData = $this->users_model->getUserData($conditions)->row();
        
        //проверка за логнат потребител. Ако си логнат..не можеш да използваш линка за ресване на паролата
        if (isLogged()) {
            $this->session->set_flashdata('validation_failed', 'You cannot reset your password while logged in');
            redirect('login');
        }
        
        //проверка дали има такъв токен в таблицата users, ако няма ->set_flashdata('', 'Error')       
        if ($tokenData->reset_token != $token) {
            $this->session->set_flashdata('validation_failed', 'Your reset token is not valid');
            redirect('login');
        }
        
        //да направя сравнение между сегашната дата и датата на ресетване на паролата. Ако е по-малка от 1 ден
        //Ако е над 1 ден да значи токена е изтекъл.
        $date1 = new DateTime(date('Y-m-d H:i:s', strtotime($tokenData->reset_at)));

        $currentDate = date('Y-m-d H:i:s');
        $date2 = new DateTime(date('Y-m-d H:i:s', strtotime($currentDate)));
        
        if ($date1->diff($date2)->days > 1) {             
            $this->session->set_flashdata('token_request', 'Your reset token has expired. Please request a new one.');
            redirect('login');
        }
        
        $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|min_length[3]|callback__check_confirmation');
        if ($this->input->post()) {               
            if ($this->form_validation->run()) { 
                
                $email = $tokenData->email;
                $newUserData = array(
                    'password' => $this->hash($this->input->post('password')),
                    'reset_token' => null,
                    'reset_at' =>null
                ); 
                
                $this->users_model->updateUserData($email, $newUserData);
                $this->session->set_flashdata('success', 'Your password has been changed successfully. You can login using your new password below.');
                redirect('login/confirmation');
            }
        }

        $terms = $this->input->get('search');       
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $data['title'] = 'Reset password';
        $data['token'] = $tokenData->reset_token;
        $this->load->view('login/resetPassword_view', $data);
    }
    
    public function confirmation()
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Login/Register' => 'login',
            'Confirmation'
        ));
        $data = array();
        $terms = $this->input->get('search');       
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $data['title'] = 'Confirmation';       
        $this->load->view('login/confirmation_view', $data);
    }

    public function _check_email($email)
    {
        $conditions = array(
            'email' => $email
        );
        $emailData = $this->users_model->getUserData($conditions)->num_rows();

        if ($emailData == 0) {
            $this->form_validation->set_message('_check_email', 'This email is does not exist');
            return false;
        } else {
            return true;
        }
    }

    public function _check_confirmation($passconf)
    {
        $passconf = $this->input->post('passconf');
        if ($this->input->post('password') == $passconf) {
            return true;
        } else {
            $this->form_validation->set_message('_check_confirmation', 'The confirmation password and the password does not match.');
            return false;
        }
    }
    
    public function hash($string) 
    {
        return hash("sha1", $string . config_item('encryption_key'));
    } 
    
}
