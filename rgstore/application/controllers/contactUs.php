<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class contactUs extends MY_Controller_Front{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('contact_model');
    }
    
    public function index()
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Contact us' => ''
        ));
        $data = array();
        $data['postData'] = array(
            'from' => '',
            'subject' => '',
            'message' => '',
            'searchBy' => ''
        );
        
        $data['title'] = 'Contact us';
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $this->load->view('contactUs_view', $data);  
    }
    
    public function sendMessage()
    {        
        $data = array();
        $this->form_validation->set_rules('from', 'From', 'required|trim|min_length[3]|valid_email');
        $this->form_validation->set_rules('subject', 'Subject', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('message', 'Message', 'required|trim|min_length[3]');

        if ($this->input->post()) {          
            $data['postData'] = array_merge($this->input->post(), array('searchBy' => ''));
            
            if ($this->form_validation->run()) {
                
                //saving the message into the inbox table
                $data['messageData'] = array(
                    'from' => $this->input->post('from'),
                    'subject' => $this->input->post('subject'),
                    'message' => $this->input->post('message'),
                    'sent_at' =>  date('Y-m-d H:i:s')
                    );
                
                $this->contact_model->setMessage($data['messageData']);
                
                //sending an email to the supports email
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'rgatewaytest@gmail.com',
                    'smtp_pass' => 'rgatewaytest1q2w3e4r',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

                $this->email->from('rgatewaytest@gmail.com', 'Support-RGinterns');
                $this->email->to('rgatewaytest@gmail.com');
                $this->email->subject($this->input->post('subject'));
                $this->email->message($this->input->post('message'));

                $result = $this->email->send();
                
                $this->session->set_flashdata('success', 'Your message has been sent successfully.');
                redirect('contact-us');
            }
        }
        
        $data['title'] = 'Contact us';
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $this->load->view('contactUs_view', $data);  
    }
    
}
