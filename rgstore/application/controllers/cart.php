<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart extends MY_Controller_Front {

    public function __construct() {
        parent::__construct();

        require __DIR__ . '/../../vendor/autoload.php';
        $this->load->model('users_model');

        $this->load->model('footer_m');
        $this->load->library('cart');
        $this->load->model('printers_m');
        $this->load->model('filament_model');
        $this->load->model('products_model');
        $this->load->model('shipping_model');
        $this->load->model('address_model');
    }

    public function buyWithPayPal() {
        
        if ($this->input->post() == false) {
            $this->session->set_flashdata('error', 'Please select your type of shipping and agree to the terms of use');
            redirect('cart/checkout');
        }
        
        $this->form_validation->set_rules('shipping', 'Shipping', 'required');
        $this->form_validation->set_rules('terms', 'Terms', 'callback__check_terms');

        if ($this->form_validation->run()) {
            // API 
            $api = new PayPal\Rest\ApiContext(
                    new PayPal\Auth\OAuthTokenCredential(
                    'AcrLzFTFouNeIf5WszPCzORoHGCTE1PQHvAbhrzRZx8b5eIRH-iXr0u4k9ZXql2lXm1h0weaDcxEF3cz', 'ELZnTvbeqxrXe74z-jwSBcVby0t9YXTvu46N729SfE3zvycrZ2ewNkr32rljXNowck8lSxeokFPpxlpu'
                    )
            );

            $data = array();
            $userId = $this->session->userdata('userId');
            $conditions = array(
                'id' => $userId
            );
            $data['userData'] = $this->users_model->getUserdata($conditions)->row();

            $payer = new \PayPal\Api\Payer();
            $details = new \PayPal\Api\Details();
            $amount = new PayPal\Api\Amount();
            $transaction = new \PayPal\Api\Transaction();
            $payment = new \PayPal\Api\Payment();
            $redirectUrls = new PayPal\Api\RedirectUrls();

            // Payer
            $payer->setPaymentMethod('paypal');

            // Details
            $shipping = (int) $this->input->post('shipping');
            $subtotal = $this->cart->total();
            $details->setShipping($shipping)
                    ->setTax(0)
                    ->setSubtotal($subtotal);

            // Amount
            $total = $subtotal + $shipping;
            $amount->setCurrency('GBP')
                    ->setTotal($total)
                    ->setDetails($details);

            // Transaction
            $transaction->setAmount($amount)
                    ->setDescription('Sale of product');

            // Payment
            $payment->setIntent('Sale')
                    ->setPayer($payer)
                    ->setTransactions([$transaction]);

            // Redirect URL's
            $redirectUrls->setReturnUrl('http://rgstore.local/paypal-payment?isApproved=approved')
                    ->setCancelUrl('http://rgstore.local/paypal-payment?isApproved=canceled');

            $payment->setRedirectUrls($redirectUrls);

            try {
                $payment->create($api);

                // Generate and store hash
                $hash = md5($payment->getId());
                $sessionData = array(
                    'paypal_hash' => $hash
                );
                $this->session->set_userdata($sessionData);

                // Prepare execute transaction
                $transactionData = array(
                    'user_id' => $userId,
                    'payment_id' => $payment->getId(),
                    'hash' => $hash
                );

                $this->users_model->setTransactionData($transactionData);
            } catch (PayPal\Exception\PayPalConnectionException $ex) {

                echo $ex;
                die();
            }

            foreach ($payment->getLinks() as $key => $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirectUrl = $link->getHref();
                }
            }

            redirect($redirectUrl);
        }

        $userId = $this->session->userdata('userId');
        $data['shipping'] = $this->shipping_model->show_shipping();
        
        $conditions = array(
            'user_id' => $userId,
            'is_main' => 1
        );
        $data['address'] = $this->address_model->find_address($conditions);
        if (empty($data['address'])) {
            $this->session->set_flashdata('accDetails', 'You need to set up your account details before proceeding to checkout.');
            redirect('profile/personal-details');
        }

        $whereConditions = array(
            'ba.user_id' => $userId,
            'ba.is_main' => 1,
            'ba.is_deleted' => 0
        );
        $fieldsToSelect = array(
            'ba.user_id', 'ba.b_address', 'ba.b_city', 'ba.id', 'ba.b_zip_code', 'ba.b_phone', 'ba.b_country_id', 'ba.b_phone_code', 'c.display_name');
        $data['billingAddress'] = $this->users_model->getBillingAddresses($whereConditions, $fieldsToSelect)->row();  
        if (empty($data['billingAddress'])) {
            $this->session->set_flashdata('accDetails', 'You need to set up your billing address before proceeding to checkout.');
            redirect('profile/billing-address');
        }
        
        $data['names'] = $this->address_model->find_names($userId);
        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array('searchBy' => '');
        if ($this->input->post()) {
            $data['postData'] = array_merge(array('searchBy' => ''), $this->input->post());
        }
        $data['title'] = 'Checkout';
        $this->load->view('checkout', $data);
    }

    public function buy($id = 0) {
        $this->load->model('slider_model');
        $this->load->model('printers_m');
        $this->load->library('cart');
        
        $sliderData = $this->slider_model->getSliderData()->result();
        $page['sliderData'] = $sliderData;
        $page['title'] = 'Home Page';
        $id = $this->input->post('printer_id');

        // $printer = $this->printers_m->find($id);
        $quantity = $this->input->post('quantity');
        if ($quantity > 0) {
            $printer = $this->printers_m->find($id);

            if ($printer->new_price == 0) {
                $price = $printer->price;
            } else {
                $price = $printer->new_price;
            }

            $data = array(
                'id' => $id,
                'qty' => $quantity,
                'name' => $printer->title,
                'price' => $price,
                'options' => array('Cat_id' => 1, 'Image' => $printer->name)
            );

            $this->session->set_flashdata('success', 'Printer was successfully added to basket.');
            $this->cart->insert($data);
            redirect('printersportfolio/printersdetail/' . $id);
        }
        $page['sliderData'] = $sliderData;
        $page['title'] = 'Home Page';

        $this->session->set_flashdata('error_q', 'Choose quantity.');
        redirect('printersportfolio/printersdetail/' . $id);

        //$this->load->view('printerdetails', $data);
    }

    public function buyProduct($id = 0) {
        $this->load->model('slider_model');
        $this->load->model('products_model');
        $this->load->library('cart');
        
        $id = $this->input->post('id');
        $conditions = array(
            'p.id' => $id
        );
        $fields = array('p.id', 'p.title', 'p.price', 'p.cat_id', 'i.name');
        $product = $this->products_model->getProduct($conditions, $fields)->row();

        // $printer = $this->printers_m->find($id);
        
        $data = array(
            'id' => $product->id,
            'qty' => 1,
            'name' => $product->title,
            'price' => $product->price,
            'options' => array('Cat_id' => $product->cat_id, 'Image' => $product->name)
        );
        $this->cart->insert($data);
        $this->session->set_flashdata('success', 'Product was successfully added to basket.');    //$this->load->view('printerdetails', $data);
    }

    public function show() {
        $this->load->library('cart');
        $data['cart'] = $this->cart->contents();
        print_r($data);
    }

    public function basket() {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Basket'
        ));
        //var_dump($this->cart->contents());
        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $this->load->view('basket', array(
            'postData' => $data['postData'],
            'search' => $terms,
            'title' => 'Basket'
        ));
    }

    public function destroy() {

        $this->cart->destroy();
    }

    public function checkout() {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Basket' => 'cart/basket',
            'Checkout'
        ));

        if (!isLogged()) {
            $this->session->set_flashdata('authentication', 'You need to login and set up your account details before proceeding to checkout.');
            redirect('login/validate');
        }
        $this->load->library('cart');

        if ($this->cart->_cart_contents['total_items'] == 0) {
            $this->session->set_flashdata('error_b', 'Your basket is empty. You cannot proceed to checkout.');
            redirect('cart/basket');
        }
        $this->load->model('shipping_model');
        $this->load->model('address_model');

        $userId = $this->session->userdata('userId');
        $data['shipping'] = $this->shipping_model->show_shipping();
        
        $conditions = array(
            'user_id' => $userId,
            'is_main' => 1
        );
        $data['address'] = $this->address_model->find_address($conditions);
        if (empty($data['address'])) {
            $this->session->set_flashdata('accDetails', 'You need to set up your account details before proceeding to checkout.');
            redirect('profile/personal-details');
        }

        $whereConditions = array(
            'ba.user_id' => $userId,
            'ba.is_main' => 1,
            'ba.is_deleted' => 0
        );
        $fieldsToSelect = array(
            'ba.user_id', 'ba.b_address', 'ba.b_city', 'ba.id', 'ba.b_zip_code', 'ba.b_phone', 'ba.b_country_id', 'ba.b_phone_code', 'c.display_name');
        $data['billingAddress'] = $this->users_model->getBillingAddresses($whereConditions, $fieldsToSelect)->row();  
        if (empty($data['billingAddress'])) {
            $this->session->set_flashdata('accDetails', 'You need to set up your billing address before proceeding to checkout.');
            redirect('profile/billing-address');
        }
        
        $data['names'] = $this->address_model->find_names($userId);
        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $data['title'] = 'Checkout';
        $this->load->view('checkout', $data);
    }

    public function calculate_Order() {
        $this->load->library('cart');
        $total = $this->cart->total();

        if ($this->input->post('shipping') != 'free') {
            $total = $total + $this->input->post('shipping');
        }

        echo json_encode($total);
        die;
    }

    public function send_Order($quantity = 0) {
        $this->load->library('cart');
        $this->load->library('form_validation');
        $this->load->model('address_model');


        $this->load->model('order_model');
        $userId = $this->session->userdata('userId');

        $this->form_validation->set_rules('shipping', 'Shipping', 'required');
        $this->form_validation->set_rules('terms', 'Terms', 'callback__check_terms');
        if ($this->form_validation->run() == FALSE) {
            if ($this->input->post('terms') == null) {
                $this->session->set_flashdata('terms', 'Please agree with our terms of use.');
            }

            if ($this->input->post('shipping') == null) {
                $this->session->set_flashdata('shipping', 'Please choose your shipping.');
            }
            redirect("cart/checkout");
        }

        $sh = $this->input->post('shipping');

        $price = $this->cart->total() + $sh;

        $conditions = array(
            'ba.user_id' => $userId,
            'ba.is_main' => 1,
            'ba.is_deleted' => 0
        );
        $fields = array(
            'ba.user_id', 
            'ba.b_address', 
            'ba.b_city', 
            'ba.id', 
            'ba.b_zip_code', 
            'ba.b_phone', 
            'ba.b_phone_code', 
            'c.display_name'
        );
        
        $addressConditions = array(
            'user_id' => $userId,
            'is_main' => 1
        );
        $data['address'] = $this->address_model->find_address($addressConditions);
        $data['details'] = $this->address_model->find_names($userId);
        $data['billingAddress'] = $this->users_model->getBillingAddresses($conditions, $fields)->row();
        $date = date("Y-m-d H:i:s");

        $orderData = array(
            'user_id' => $userId,
            'title' => $data['details']->title,
            'fname' => $data['details']->fname,
            'lname' => $data['details']->lname,
            'email' => $data['details']->email,
            'address' => $data['address']->address,
            'city' => $data['address']->city,
            'zip_code' => $data['address']->zip_code,
            'country_name' => $data['address']->display_name,
            'phone' => $data['address']->phone,
            'phone_code' => $data['address']->phonecode,
            'order_price' => $price,
            'date_added' => $date,
            'b_address' => $data['billingAddress']->b_address,
            'b_city' => $data['billingAddress']->b_city,
            'b_zip_code' => $data['billingAddress']->b_zip_code,
            'b_country_name' => $data['billingAddress']->display_name,
            'b_phone' => $data['billingAddress']->b_phone,
            'b_phone_code' => $data['billingAddress']->b_phone_code
        );
        
        $this->order_model->setOrder($orderData);
        $insert_id = $this->db->insert_id();

        $colour = '';
        foreach ($this->cart->contents() as $product) {

            if (array_key_exists('options', $product)) {
                if (array_key_exists('Colour', $product['options'])) {
                    $colour = $product['options']['Colour'];
                }
            }
            if (array_key_exists('qty', $product)) {
                $quantity = $product['qty'];
            }          
            $products_data = array(
                'price' => $product['price'],
                'quantity' => $product['qty'],
                'product_id' => $product['id'],
                'total_price' => $product['subtotal'],
                'options' => $colour,
                'order_id' => $insert_id
            );

            $this->order_model->insert_products($products_data);
        }

        $this->cart->destroy();
        $this->session->set_flashdata('success', 'Your order was successfully completed. You can check the status of your orders in the Order History.');
        redirect('profile/orders');
    }

    function check() {
        // Get the total number of items in cart
        $total = $this->cart->total_items();

        // Retrieve the posted information
        $item = $this->input->post('rowid');
        $qty = $this->input->post('qty');

        // Cycle true all items and  them
        for ($i = 0; $i < $total; $i++) {
            // Create an array with the products rowid's and quantities. 
            $data = array(
                'rowid' => $item[$i],
                'qty' => $qty[$i]
            );

            // Update the cart with the new information
            $this->cart->update($data);
            redirect('cart/basket');
        }
    }

    public function delete($rowid) {
        $this->load->library('cart');
        $this->cart->update(array('rowid' => $rowid, 'qty' => 0));
        $this->session->set_flashdata('success', 'Item was successfully removed from basket.');
        redirect('cart/basket');
    }

    public function updateCart() {
        $id = $this->input->post('id');
        $qty = $this->input->post('qty');
        $this->cart->update(array('rowid' => $id, 'qty' => $qty));
    }

    public function ajaxBuyFilament() {
        $this->load->model('filament_model');
        $id = $this->input->post('id');
        $fil = $this->filament_model->find($id);
        $colours = $this->input->post('colors2');
        $count = count($colours);

        for ($row = 0; $row < $count; $row++) {
            $data = array(
                'id' => $id,
                'qty' => $colours[$row]['qty'],
                'name' => $fil->title,
                'price' => $fil->price,
                'options' => array('Cat_id' => 2, 'Colour' => $colours[$row]['color'], 'Image' => $fil->name)
            );

            $this->cart->insert($data);
        }

        $this->session->set_flashdata('success_filament', 'Filament was successfully added to basket.');
    }

    public function buyById($id = 0) {
        $this->load->model('slider_model');
        $this->load->model('filament_model');

        $page['title'] = 'Home Page';
        $id = $this->input->post('filament_id');
       
       
        // $printer = $this->printers_m->find($id);
        $quantity = $this->input->post('quantity');
        
        if ($quantity > 0) {
            $filament = $this->filament_model->find($id);

            $colour = $this->input->post('colour');

            if ($colour == false) {
                $this->session->set_flashdata('error_q', 'Choose colour.');
                redirect('filamentportfolio/filamentsdetail/' . $id);
            }
            if ($filament->new_price == 0) {
                $price = $filament->price;
            } else {
                $price = $filament->new_price;
            }
            $data = array(
                'id' => $filament->id,
                'qty' => $quantity,
                'name' => $filament->title,
                'price' => $price,
                'options' => array('Cat_id' => 2, 'Image' => $filament->name, 'Colour' => $colour)
            );

            $this->session->set_flashdata('success', 'Filament was successfully added to basket.');
            $this->cart->insert($data);
            redirect('filamentportfolio/filamentsdetail/' . $id);
        }

        $page['title'] = 'Home Page';
        $this->session->set_flashdata('error_q', 'Choose quantity.');
        redirect('filamentportfolio/filamentsdetail/' . $id);
        //$this->load->view('printerdetails', $data);
    }

    public function buyBundle($bundleId) {
        $conditons = array(
            'product_id' => $bundleId
        );
        $bundle = $this->products_model->getBundle($conditons)->row();

        if (empty($bundle)) {
            $this->session->set_flashdata('success', 'No such bundle');
            redirect();
        }

        $cartContent = array(
            'id' => $bundle->product_id,
            'qty' => 1,
            'name' => $bundle->bundle_name,
            'price' => $bundle->discount_price,
            'options' => array('Cat_id' => 4, 'Image' => 'bundle.jpg')
        );

        $this->cart->insert($cartContent);
        $this->session->set_flashdata('success', 'Bundle was successfully added to basket.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    // Finishing cart functions

    public function buyFinishing($id = 0) {
        $this->load->model('finishing_m');
        $id = $this->input->post('id');
        $quantity = $this->input->post('quantity');
        if ($quantity > 0) {
            $finishing = $this->finishing_m->find($id);
            $colour = $this->input->post('colour');

            if ($colour == false) {
                $this->session->set_flashdata('error_q', 'Choose colour.');
                redirect('finishing/details/' . $id);
            }

            if ($finishing->new_price == 0) {
                $price = $finishing->price;
            } else {
                $price = $finishing->new_price;
            }
            $data = array(
                'id' => $finishing->id,
                'qty' => $quantity,
                'name' => $finishing->title,
                'price' => $price,
                'options' => array('cat_id' => 3, 'Image' => $finishing->name, 'Colour' => $colour)
            );

            $this->session->set_flashdata('success', 'Finishing was successfully added to basket.');
            $this->cart->insert($data);
            redirect('/finishing/details/' . $id);
        }

        $this->session->set_flashdata('error_q', 'Choose quantity.');
        redirect('/finishing/details/' . $id);
    }

    public function buyFinishingFromPrinters() {
        $this->load->model('finishing_m');
        $this->load->model('order_model');
        $quantity = $this->input->post('quantity');
        $id = $this->input->post('finishing_category');
        $color = $this->input->post('colour');
        $finishing = $this->finishing_m->find($id);
        $colour = $this->input->post('colour');

        if ($id == null) {
            $this->session->set_flashdata('error_q', 'Choose finishing.');
            redirect($_SERVER['HTTP_REFERER']);
        }
        if ($color == false) {
            $this->session->set_flashdata('error_q', 'Choose colour.');
            redirect($_SERVER['HTTP_REFERER']);
        }
        if ($quantity == 0) {
            $this->session->set_flashdata('error_q', 'Choose quantity.');
            redirect($_SERVER['HTTP_REFERER']);
        }
        $data = array(
            'id' => $finishing->id,
            'qty' => $quantity,
            'name' => $finishing->title,
            'price' => $finishing->price,
            'options' => array('Cat_id' => 3, 'Image' => $finishing->name, 'Colour' => $colour)
        );

        $this->cart->insert($data);
        $this->session->set_flashdata('success', 'Finishing was successfully added to basket.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function _check_terms() {
        if ($this->input->post('terms') == false) {
            $this->form_validation->set_message('_check_terms', 'Please agree with our terms of use');
            return FALSE;
        } else {
            return true;
        }
    }

}
