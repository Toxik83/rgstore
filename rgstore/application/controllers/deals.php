<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Deals extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('printers_m');
    }

    public function products($offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'All Hot Deals'
        ));
        $this->load->library('pagination');
        $data = array();
        $current_time = time();
        $title = 'Hot Deals';
        $data['title'] = $title;
        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $currentTime = date("Y-m-d H:i:s");
        //getting all products from db
        $data['printerlist'] = $this->printers_m->getAllHotDeals($currentTime, $offset, 12);
        //getting product most sold 1 month before
        $bestsells = $this->printers_m->getPrintersCount();
        //check for bestsellers // extracting products in bestsellers list  and compare ids with list of all products
        $current_time = time();
        foreach ($data['printerlist']['data'] as $key => $val) {
            $data['printerlist']['data'][$key]['flag'] = 0;
            //if product has flashdate, it is not in bestsellers list
            if((($val['fl_date_begin'] == 0) && ($val['fl_date_end'] == 0))||($current_time < strtotime($val['fl_date_begin']))  )  {           
                foreach ($bestsells as $k => $product) {
                    if(($val['id']== $product['id'])){ 
                        $data['printerlist']['data'][$key]['flag'] = 1;
                }   
            }
        }
        }
        $config = array();
        $config['base_url'] = base_url() . 'deals/products';
        $config['total_rows'] = $data['printerlist']['count'];
        $config['per_page'] = 12;
        $config['num_links'] = 9;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['uri_segment'] = 3;
        
        $this->pagination->initialize($config);
        if ($this->uri->segment(3) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(3) - 1);
        }
        checkFlashDate();
        $links = $this->pagination->create_links();
        $this->load->view('productslisting', array(
            'printerlist' => $data['printerlist']['data'],
            'title' => $title,
            'current_time' => $current_time,
            'links' => $links,
            'search' => $terms,
            'postData' => array('searchBy' => '')
        ));
    }

    public function search() {
        $this->load->library('pagination');
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Hot Deals' => ''
        ));

        $checkURL = $this->uri->segment(1);
        $data = array();
        $currentTime = date("Y-m-d H:i:s");
        if ($checkURL == 'sellers') {
            $flag = 1;
        } else {
            $flag = 0;
        }

        $like = array();
        if ($this->input->get()) {
            switch ($this->input->get('orderBy')) {
                case 1: $option = 'price';
                    $option2 = 'ASC';
                    break;
                case 2: $option = 'price';
                    $option2 = 'DESC';
                    break;
                case 3: $option = 'title';
                    $option2 = 'ASC';
                    break;
                case 4: $option = 'title';
                    $option2 = 'DESC';
                    break;
                default : $option = 'date_added';
                    $option2 = 'DESC';
            }
        }

        $total = $this->printers_m->getSearchResult(
                        $currentTime, null, $flag, $option, $option2
                )->num_rows();

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $pageLimit = 12;
        if (array_key_exists('per', $this->input->get())) {
            $pageLimit = $this->input->get('per');
        }
        $max = array($pageLimit, ($page - 1) * $pageLimit);
        //getting all products from db
        $messages = $this->printers_m->getSearchResult(
                        $currentTime, $max, $flag, $option, $option2
                )->result_array();
        //getting product most sold 1 month before
        $bestsells = $this->printers_m->getPrintersCount();   
        //check for bestsellers // extracting products in bestsellers list  and compare ids with list of all products
        $current_time = time();
        foreach ($messages as $key => $val) {
            $messages[$key]['flag'] = 0;
            //if product has flashdate, it is not in bestsellers list
            if((($val['fl_date_begin'] == 0) && ($val['fl_date_end'] == 0))||($current_time < strtotime($val['fl_date_begin']))  )  {
            
                foreach ($bestsells as $k => $product) {
                    if(($val['id']== $product['id'])){ 
                        $messages[$key]['flag'] = 1;
                }   
            }
        }
        }
        $data['messageData'] = $messages;
        //setting the $config array for the pagination
        $config = array();
        $config['base_url'] = base_url() . 'deals/search';
        if ($this->input->get() != false) {
            $config['suffix'] = '?' . http_build_query($this->input->get(), '', '&');
            $config['first_url'] = base_url() . 'deals/search/1' . $config['suffix'];
        }

        $config['total_rows'] = $total;
        $config['per_page'] = $pageLimit;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = true;

        $config['first_link'] = false;
        $config['last_link'] = false;

        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';

        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        //initializing pagination
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['title'] = 'Finishings';

        $terms = $this->input->get('search');

        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );

        $current_time = time();
        $data['current_time'] = $current_time;
        $this->load->view('sort/product_list', $data);
    }

}
