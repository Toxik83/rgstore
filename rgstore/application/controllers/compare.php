<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Compare extends MY_Controller_Front {

    public function __construct() {
        parent::__construct();
        $this->load->model('compare_model');
    }
    
    public function ajaxCompare() {
        $itemId = $this->input->post('compare');
           
        $ids = $this->session->userdata('ids');

        if ($ids == false) {
            $userData = $this->session->all_userdata();

            $userData = array_merge($userData, array('ids' => array()));
            
            $this->session->set_userdata($userData);
            $ids = $this->session->userdata('ids');
        }

        if (!in_array($itemId, $ids)) {  
            if (count($ids) >= 3) {
                $returnData = array(
                    'error' => 1
                );   
            } else {
                array_push($ids, $itemId);
                $returnData = array(
                    'error' => 0,
                    'itemsCount' => count($ids)
                );
            }                     
        } else {
            foreach ($ids as $key => $id) {
                if ($itemId == $id) {
                    unset($ids[$key]);
                }
            }
            
            $returnData = array(              
                'itemsCount' => count($ids)
            );
        }

        $compareId = array(
            'ids' => $ids
        );       
        $this->session->set_userdata($compareId);
      
        echo json_encode($returnData);
        die;
    }
     
    public function compareItems()
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Compare list'
        ));
        $ids = $this->session->userdata('ids');
 
        $fields = array(
            'p.id', 'p.title', 'p.price', 'p.new_price', 'p.cat_id', 'm.man_name', 'pt.print_name', 
            'bs.build_name', 'a.accuracy_name', 'pi.name'
        );
        
        $data['compareItems'] = array();

        if ($ids != false) {
            foreach ($ids as $key => $id) {
                $conditions = array(
                    'p.id' => $id,
                    'p.active' => 0,
                    'pi.main_pic' => 1,
                );
                $returnData = $this->compare_model->getProductDetails($conditions, $fields)->row();     
                array_push($data['compareItems'], $returnData);
            }
        }      
        
        $data['title'] = 'Compare list';
        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $this->load->view('compare', $data);
    }
    
    public function AjaxRemoveItems()
    {
        $id = (int)$this->input->post('remcompare');
        $ids = $this->session->userdata('ids');       
        $key = array_search($id,$ids);
        if($key!==false){
            unset($ids[$key]);       
        }
        $compareId = array(
            'ids' => $ids
        );       
        $this->session->set_userdata($compareId);

        die;
    }
    
}
