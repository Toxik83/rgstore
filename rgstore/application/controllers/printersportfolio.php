<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Printersportfolio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('printers_m');
        $this->load->model('search_m');
        $this->load->model('finishing_m');
        $this->load->model('products_model');
    }

    public function index($offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Printers'
        ));
        $this->load->library('pagination');
        $data = array();
        $title = 'Printers Portfolio';
        $data['title'] = $title;
        //getting all products from db
        $data['printerlist'] = $this->printers_m->printersList($offset, 12);
        $bestsells = $this->printers_m->getPrintersCount();
        //getting product most sold 1 month before
        $current_time = time();
        //check for bestsellers // extracting products in bestsellers list  and compare ids with list of all products
        foreach ($data['printerlist']['data'] as $key => $val) {
            $data['printerlist']['data'][$key]['flag'] = 0;
            //if product has flashdate, it is not in bestsellers list
            if ((($val['fl_date_begin'] == 0) && ($val['fl_date_end'] == 0)) || ($current_time < strtotime($val['fl_date_begin']))) {
                foreach ($bestsells as $k => $product) {
                    if (($val['id'] == $product['id'])) {
                        $data['printerlist']['data'][$key]['flag'] = 1;
                    }
                }
            }
        }
        $config = array();
        $config['base_url'] = base_url() . 'printersportfolio/index';
        $config['total_rows'] = $data['printerlist']['count'];
        $config['per_page'] = 12;
        $config['num_links'] = 9;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        if ($this->uri->segment(3) == 0) {
            $offset = 0;
        } else {
            $offset = ($config['per_page']) * ($this->uri->segment(3) - 1);
        }
        //function from flashdate_helper
        //when we call it, it check in db for finished flashdates and setting time for flashdate. New Promo price become 0
        checkFlashDate();
        $data['current_time'] = $current_time;
        $links = $this->pagination->create_links();
        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $this->load->view('printerslisting', array(
            'printerlist' => $data['printerlist']['data'],
            'title' => $title,
            'links' => $links,
            'search' => $terms,
            'current_time' => $current_time,
            'postData' => array('searchBy' => '')
        ));
    }

    public function printersDetail($printer_id = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Printers' => 'printersportfolio',
            'Printer details'
        ));

        //For last seen -  last seen 2 products
        $printer_id = $this->uri->segment(3);
        $idl = $this->session->userdata('idl');
        $idl[] = $printer_id;
        if (is_array($idl)) {
            //takes the last 2 ids of products  in array
            $twoLastSeen = array_unique(array_slice($idl, -2, 2));
            //and write them in session array
            $last = array(
                'idl' => $twoLastSeen
            );
            $this->session->set_userdata($last);
        }
        $this->load->model('filament_model');
        $this->load->library('cart');
        $data['title'] = 'Printers Details';
        $data['printers'] = $this->printers_m->printersById($printer_id);
        
            if(empty($data['printers'])){
                redirect('printersportfolio');
            }
        $data['filaments'] = $this->filament_model->get_filament();
        $data['finishings'] = $this->finishing_m->get_finishing();
        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );

        //Get Related Printers
        $data['current_time'] = date("Y-m-d H:i:s");
        $category_id = $data['printers'][0]->cat_id;
        $data['relatedProducts'] = $this->products_model->getRelatedProducts($category_id, $printer_id);
        $this->load->view('printerdetails', $data);
    }

    public function flDateReset($id) {
        $this->load->model('printers_m');
        $id = $this->input->post('id');
        $fl_date_begin = $this->input->post('fl_date_begin');
        $fl_date_end = $this->input->post('fl_date_end');
        $this->printers_m->deleteFlDate($id, $fl_date_begin, $fl_date_end);
    }

    public function search() {
        $this->load->library('pagination');

        $this->breadcrumb->populate(array(
            'Home' => '',
            'Printers'
        ));
        $cat = 0;


        if ($this->uri->segment(1) == 'finishing') {
            $cat = 3;
        } elseif ($this->uri->segment(1) == 'filamentportfolio') {
            $cat = 2;
        } elseif ($this->uri->segment(1) == 'printersportfolio') {
            $cat = 1;
        }

        $data = array();

        $filterable = array(
            'min',
            'max',
            'per',
            'searchBy',
            'catName',
            'orderBy',
            'search',
            'man',
            'tech',
            'size',
            'acc',
        );

        $allqueryparams = $this->input->get();

        $actualfilters = array();

        foreach ($filterable as $key) {
            if (array_key_exists($key, $allqueryparams)) {
                $actualfilters[$key] = $allqueryparams[$key];
            }
        }

        if (!array_key_exists('min', $this->input->get())) {
            $actualfilters['min'] = null;
        }
        if (!array_key_exists('max', $this->input->get())) {
            $actualfilters['max'] = null;
        }
        if (!array_key_exists('catName', $this->input->get())) {
            $actualfilters['catName'] = null;
        }
        if (!array_key_exists('man', $this->input->get())) {
            $actualfilters['man'] = null;
        }
        if (!array_key_exists('tech', $this->input->get())) {
            $actualfilters['tech'] = null;
        }
        if (!array_key_exists('size', $this->input->get())) {
            $actualfilters['size'] = null;
        }
        if (!array_key_exists('acc', $this->input->get())) {
            $actualfilters['acc'] = null;
        }

        $like = array();

        if ($this->input->get()) {

            switch ($this->input->get('orderBy')) {
                case 1: $option = 'price';
                    $option2 = 'ASC';
                    break;
                case 2: $option = 'price';
                    $option2 = 'DESC';
                    break;
                case 3: $option = 'title';
                    $option2 = 'ASC';
                    break;
                case 4: $option = 'title';
                    $option2 = 'DESC';
                    break;
                default : $option = 'date_added';
                    $option2 = 'DESC';
            }
        }

        $total = $this->search_m->getSearchResult(
                        $cat, $actualfilters['catName'], null, $like, $option, $option2, $actualfilters['min'], $actualfilters['max'], $actualfilters['man'], $actualfilters['tech'], $actualfilters['size'], $actualfilters['acc']
                )->num_rows();

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $pageLimit = 12;
        if (array_key_exists('per', $this->input->get())) {
            $pageLimit = $this->input->get('per');
        }

        $max = array($pageLimit, ($page - 1) * $pageLimit);

        
        $messages = $this->search_m->getSearchResult(
                        $cat, $actualfilters['catName'], $max, $like, $option, $option2, $actualfilters['min'], $actualfilters['max'], $actualfilters['man'], $actualfilters['tech'], $actualfilters['size'], $actualfilters['acc']
                )->result_array();
        if(empty($messages)){
            redirect('printersportfolio');
        }
        //for best sellers
        //getting all products from db
        $data['messageData'] = $messages;

        
        //getting product most sold 1 month before
        $bestsells = $this->printers_m->getPrintersCount();
        //check for bestsellers // extracting products in bestsellers list  and compare ids with list of all products
        $current_time = time();
        foreach ($data['messageData'] as $key => $val) {
            $data['messageData'][$key]['flag'] = 0;
            //if product has flashdate, it is not in bestsellers list
            if ((($val['fl_date_begin'] == 0) && ($val['fl_date_end'] == 0)) || ($current_time < strtotime($val['fl_date_begin']))) {
                foreach ($bestsells as $k => $product) {
                    if (($val['id'] == $product['id'])) {
                        $data['messageData'][$key]['flag'] = 1;
                    }
                }
            }
        }
        //setting the $config array for the pagination
        $config = array();
        $config['base_url'] = base_url() . 'printersportfolio/search';

        if ($this->input->get() != false) {
            $config['suffix'] = '?' . http_build_query($this->input->get(), '', '&');
            $config['first_url'] = base_url() . 'printersportfolio/search/1' . $config['suffix'];
        }

        $config['total_rows'] = $total;
        $config['per_page'] = $pageLimit;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = true;

        $config['first_link'] = false;
        $config['last_link'] = false;

        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';

        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        //initializing pagination
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['title'] = 'Printers';

        $terms = $this->input->get('search');

        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $data['category'] = $this->input->get('catName');
        $data['min'] = $this->input->get('min');
        $data['max'] = $this->input->get('max');
        $data['man'] = $this->input->get('man');
        $data['tech'] = $this->input->get('tech');
        $data['size'] = $this->input->get('size');
        $data['acc'] = $this->input->get('acc');
        $data['current_time'] = $current_time;
        $this->load->view('sort/product_list', $data);
    }

}
