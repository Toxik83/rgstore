<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class staticPages extends MY_Controller_Front {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('staticpages_model');
    }
    
    public function view($slug)
    {
        $data = array();
        
        $conditions = array(
            'slug' => $slug
        );
        
        $data['pageData'] = $this->staticpages_model->getPage($conditions)->row();
        
        if ($data['pageData'] == null) {
            show_404();
        }
        
        $this->breadcrumb->populate(array(
            'Home' => '',
            $data['pageData']->link_name,
        ));
        
        $data['title'] = $data['pageData']->link_name;
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $data['postData'] = array(
            'searchBy' => ''
        );
        $this->load->view('staticPages_view', $data);
    }
}
