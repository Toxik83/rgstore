<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller_Front {

    public function __construct() {
        parent::__construct();
        $this->load->model('footer_m');
        $this->load->model('staticpages_model');
        $this->load->model('search_m');
        $this->load->model('products_model');
        $this->load->model('order_model');
    }

    public function index() {
        $this->load->model('slider_model');
        $this->load->model('manage_admin_m');
        $this->load->model('contact_model');
        $this->load->model('search_m');
        $this->load->model('printers_m');
        $sliderData = $this->slider_model->getSliderData()->result();

        $data['sliderData'] = $sliderData;
        $data['title'] = 'Home Page';
        $data['title'] = 'Home Page';

        $conditions = array(
            'active' => 0,
            'cat_id' => 4
        );

        $bundleData = $this->products_model->getRandomBundle($conditions, $start = '', $limit = 1)->row();
        $currentTime = date("Y-m-d H:i:s");
        $data['hotdeals'] = $this->printers_m->getHotDeals($currentTime);
        $data['bestsellers'] = $this->order_model->bestsellers4()->result();
        

        if (!empty($bundleData)) {
            $data['bundle'] = $bundleData;
        }
        checkFlashDate();
        /* Следните 3 реда във всеки index ,че да не гърми search-a :3 */
        $current_time = time();
        $data['current_time'] = $current_time;
        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        $this->news();
        /* Край на онее 3 реда дето ви трябват :D */

        $this->load->view('home', $data);


        //}
    }

    public function search() {

        $this->breadcrumb->populate(array(
            'Home' => '',
            'Search' => ''
        ));
        $data = array();
        $data['title'] = 'Search';

        $filterable = array(
            'min',
            'max',
            'per',
            'searchBy',
            'catName',
            'orderBy',
            'search',
            'man',
            'tech',
            'size',
            'acc',
        );

        $allqueryparams = $this->input->get();

        $actualfilters = array();

        foreach ($filterable as $key) {
            if (array_key_exists($key, $allqueryparams)) {
                $actualfilters[$key] = $allqueryparams[$key];
            }
        }

        if (!array_key_exists('min', $this->input->get())) {
            $actualfilters['min'] = null;
        }
        if (!array_key_exists('max', $this->input->get())) {
            $actualfilters['max'] = null;
        }
        if (!array_key_exists('catName', $this->input->get())) {
            $actualfilters['catName'] = null;
        }
        if (!array_key_exists('man', $this->input->get())) {
            $actualfilters['man'] = null;
        }
        if (!array_key_exists('tech', $this->input->get())) {
            $actualfilters['tech'] = null;
        }
        if (!array_key_exists('size', $this->input->get())) {
            $actualfilters['size'] = null;
        }
        if (!array_key_exists('acc', $this->input->get())) {
            $actualfilters['acc'] = null;
        }

        $terms = $this->input->get('search');

        $data['postData'] = array(
            'searchBy' => ''
        );

        if ($this->input->get()) {
            $data['postData'] = $this->input->get();
        }

        $like = array();
        $cat = 0;
        //setting the $like condition. If no terms are selected $like is an empty array
        if ($this->input->get()) {

            $like = array();
            //chech the option value and handle it
            switch ($this->input->get('searchBy')) {
                case 1: $option = 'title';
                    $cat = 1;
                    break;
                case 2: $option = 'title';
                    $cat = 2;
                    break;
                case 3: $option = 'title';
                    $cat = 3;
                    break;
                default : $option = 'title';
                    $cat = 1;
            }
            $like = array(
                $option => $terms
            );
            switch ($this->input->get('orderBy')) {
                case 1: $option1 = 'price';
                    $option2 = 'ASC';
                    break;
                case 2: $option1 = 'price';
                    $option2 = 'DESC';
                    break;
                case 3: $option1 = 'title';
                    $option2 = 'ASC';
                    break;
                case 4: $option1 = 'title';
                    $option2 = 'DESC';
                    break;
                default : $option1 = 'date_added';
                    $option2 = 'DESC';
            }
        }

        $this->load->library('pagination');

        $total = $this->search_m->getSearchResult(
                        $cat, $actualfilters['catName'], null, $like, $option1, $option2, $actualfilters['min'], $actualfilters['max'], $actualfilters['man'], $actualfilters['tech'], $actualfilters['size'], $actualfilters['acc']
                )->num_rows();

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

        $pageLimit = 12;
        if (array_key_exists('per', $this->input->get())) {
            $pageLimit = $this->input->get('per');
        }

        $max = array($pageLimit, ($page - 1) * $pageLimit);

        $messages = $this->search_m->getSearchResult(
                        $cat, $actualfilters['catName'], $max, $like, $option1, $option2, $actualfilters['min'], $actualfilters['max'], $actualfilters['man'], $actualfilters['tech'], $actualfilters['size'], $actualfilters['acc']
                )->result_array();

        $data['messageData'] = $messages;
            //for best sellers
        $bestsells = $this->search_m->getQuantityForSearch($cat);
        $current_time = time();
        foreach ($data['messageData'] as $key => $val) {
            $data['messageData'][$key]['flag'] = 0;
            if((($val['fl_date_begin'] == 0) && ($val['fl_date_end'] == 0))||($current_time < strtotime($val['fl_date_begin']))  )  {
            
                foreach ($bestsells as $k2 => $v2) {
                    if(($val['id']== $v2['id'])){ 
                        $data['messageData'][$key]['flag'] = 1;
                }   
            }
        }
        }
        //setting the $config array for the pagination
        $config = array();
        $config['base_url'] = base_url() . 'home/search';
        if ($this->input->get() != false) {
            $config['suffix'] = '?' . http_build_query($this->input->get(), '', '&');
            $config['first_url'] = base_url() . 'home/search/1' . $config['suffix'];
        }
        $config['total_rows'] = $total;
        $config['per_page'] = $pageLimit;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = true;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        //initializing pagination
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $current_time = time();
        $data['current_time'] = $current_time;
        $data['search'] = $terms;
        $data['filter'] = $option;
        $data['search_category'] = $this->input->get('searchBy');
        $data['search_terms'] = $this->input->get('search');
        $data['tech'] = $this->input->get('tech');
        $data['size'] = $this->input->get('size');
        $data['acc'] = $this->input->get('acc');

        $this->load->view('sort/product_list', $data);
    }

}
