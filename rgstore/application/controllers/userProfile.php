<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class userProfile extends MY_Controller_Front{
    public function __construct() 
    {
        parent::__construct();
        if (!isLogged()) {
            redirect();
        }
        $this->load->model('users_model');
        $this->load->model('order_model');
    }
    
    public function index()
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Profile' => ''
        ));
        
        $data = array();
        
        //chech if this user exists
        $userId = $this->session->userdata('userId'); 
        $conditions = array(
            'u.id' => $userId,
        );     
        $fields = array(
            'u.id', 'u.email', 'u.title', 'u.fname', 'u.lname','google_id','facebook_id','twitter_id', 'ud.detail_id','ud.address', 'ud.city', 'ud.phone', 'ud.phonecode', 'ud.zip_code', 'c.display_name'
        );
        
        $userData = $this->users_model->getAllUserData($conditions, $fields)->row();

        if (empty($userData)) {
            redirect($userData);
        }
        
        //Data to pass to the users menu
        $filedsToSelect = array(
            'ud.detail_id', 'u.email', 'u.title', 'u.fname', 'u.lname', 'ud.address', 'ud.city', 'ud.phone', 'ud.phonecode', 'ud.zip_code', 'c.display_name'
        );
        $whereConditions = array(
            'u.id' => $userId,
            'ud.is_main' => 1,
            'ud.is_deleted' => 0
        );

        $data['userData'] = $this->users_model->getAllUserData($whereConditions, $filedsToSelect)->row();
        
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $data['postData'] = array(
            'searchBy' => ''
        );
        $data['userDetails'] = $userData;
        $data['title'] = 'User profile';
        $this->load->view('users/userDetails', $data);
    }
    
    public function editUserInfo($id)
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Profile' => 'profile',
            'Edit details' => ''
        ));
        
        $data = array();
        $conditions = array(
            'id' => $id
        );
        $userData = $this->users_model->getUserData($conditions)->row();
        
        if (empty($userData)) {;
            redirect('profile');
        } 
    
        $fields = array(
            'u.id', 'u.email', 'u.title', 'u.fname', 'u.lname', 'ud.address', 'ud.city', 'ud.phone', 'ud.phonecode', 'ud.zip_code', 'c.display_name'
        );
        $queryConditions = array(
            'u.id' => $id
        );
        $data['userData'] = $this->users_model->getAllUserData($queryConditions, $fields)->row();
        
        $data['postData'] = array(
            'id' => $userData->id,
            'title' => $userData->title,
            'fname' => $userData->fname,
            'lname' => $userData->lname,
            'email' => $userData->email,
            'searchBy' => ''
        );
        $data['email'] = array(
            'email' => $userData->email
        );
        
        $this->form_validation->set_rules('title', 'Title', 'required|callback__check_title');
        $this->form_validation->set_rules('fname', 'First name', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('lname', 'Last name', 'required|trim|min_length[3]');
               
        if ($this->input->post('password') != '') {
            $this->form_validation->set_rules('currPassword', 'Current Password', 'required|trim|min_length[3]|callback__check_current_password');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|trim|callback__check_confirmation');
        }
          
        //check if email is changed. If it is ---> set rules
        if ($userData->email != $this->input->post('email')) {
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback__check_email');
        }
 
        if ($this->input->post()) {
            $data['postData'] = array_merge($this->input->post(), array('id' => $userData->id, 'searchBy' => ''));
            
            if ($this->form_validation->run()) {
                
                 if ($this->input->post('title') == 0) {
                    $title = 'Ms';
                } elseif ($this->input->post('title') == 1) {
                    $title = 'Mr';
                }
                
                $data['newUserData'] = array(
                    'title' => $title,
                    'fname' => $this->input->post('fname'),
                    'lname' => $this->input->post('lname'),
                    'email' => $this->input->post('email'),
                );
                
                if ($this->input->post('password') != '') {
                    $data['newUserData'] += array('password' => $this->hash($this->input->post('password')));
                }
                
                $this->users_model->updateUserData($userData->email, $data['newUserData']);
                $this->session->set_flashdata('success', 'You have successfully updated your profile.');
                redirect('profile');
            }
        }
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $data['title'] = 'Update user information';
        $this->load->view('users/editUserDetails', $data);
    }
    
    public function viewPersonalDetails()
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Profile' => ''
        ));
        
        $data = array();
        $userId = $this->session->userdata('userId'); 
        $fields = array(
            'ud.detail_id', 'u.email', 'u.title', 'u.fname', 'u.lname', 'ud.address', 'ud.city', 'ud.phone', 'ud.phonecode', 'ud.zip_code', 'c.display_name'
        );
        $conditions = array(
            'u.id' => $userId,
            'ud.is_main' => 1,
            'ud.is_deleted' => 0
        );
        
        //Get the user main delivery address
        $data['userData'] = $this->users_model->getAllUserData($conditions, $fields)->row();

        $whereConditions = array(
            'u.id' => $userId,
            'ud.is_main' => 0,
            'ud.is_deleted' => 0
        );
        
        //Get all other delivery addresses
        $data['deliveryAddresses'] = $this->users_model->getAllUserData($whereConditions, $fields)->result();
        
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $data['postData'] = array(
            'searchBy' => ''
        );
        $data['title'] = 'Personal Details';
        $this->load->view('users/personalDetails', $data);
    }

    public function addPersonalDetails()
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Profile' => 'profile',
            'Personal Details' => ''
        ));
        
        $data = array();
        $userId = $this->session->userdata('userId');
        
        $county = $this->users_model->getCountry()->result();
        if (empty($county)) {
            $this->session->set_flashdata('success', 'Please insert the country list');
            redirect('login');
        }
        
        $data['countryData'] = $county;
        
        $data['postData'] = array(
            'address' => '',
            'city' => '',
            'zip_code' => '',
            'country_id' => '',
            'countryIdHidden' => '',
            'phone' => '',
            'phoneCode' => '',
            'searchBy' => ''
        );
        
        $this->form_validation->set_rules('address', 'Address', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('city', 'City', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('zip_code', 'Zip code', 'required|trim|min_length[3]|numeric');
        $this->form_validation->set_rules('country_id', 'Country', 'required|trim|callback__check_country');
        $this->form_validation->set_rules('phone', 'Phone', 'required|trim|min_length[3]|numeric');
        
        if ($this->input->post()) {
            $data['postData'] = array_merge($this->input->post(), array('searchBy' => '', 'display_name' => ''));
            
            if ($this->form_validation->run()) {
                $conditions = array(
                    'id' => $this->input->post('country_id')
                );
                $countryData = $this->users_model->getCountry($conditions)->row();
                if (!empty($countryData)) {
                    $phoneCode = $countryData->phonecode;
                }
                $data['userDetails'] = array(
                    'user_id' => $userId,
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'zip_code' => $this->input->post('zip_code'),
                    'country_id' => $this->input->post('country_id'),
                    'phone' => $this->input->post('phone'),
                    'phonecode' => $phoneCode,
                );
                
                //chech if there is already main address, if no make this one main
                $whereConditions = array(
                    'ud.user_id' => $userId,
                    'ud.is_main' => 1
                );

                $mainAddress = $this->users_model->getAllUserData($whereConditions);
                if ($mainAddress->num_rows == 0) {
                    $data['userDetails'] += array('is_main' => 1);
                }

                $this->users_model->setUserDetails($data['userDetails']);
                
                $this->session->set_flashdata('success', 'You have successfully set you personal details.');
                redirect('profile/personal-details');
            }
        }
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $data['title'] = 'Add your address';
        $this->load->view('users/addPersonalDetails', $data);
    }
    
    public function editPersonalDetails($addressId)
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Profile' => 'profile',
            'Edit details' => ''
        ));
        
        $data = array();
        
        $fields = array(
            'ud.detail_id', 'u.email', 'u.title', 'u.fname', 'u.lname', 'ud.address', 'ud.city', 'ud.phone', 'ud.phonecode', 'ud.zip_code', 'c.display_name', 'ud.country_id'
        );
        $conditions = array(
            'ud.detail_id' => $addressId,
        );
        
        //Get selected delivery address details
        $data['userData'] = $this->users_model->getAllUserData($conditions, $fields)->row();

        if (empty($data['userData'])) {
            redirect('profile/personal-details');
        }
        $userDetails = $data['userData'];
        
        $county = $this->users_model->getCountry()->result();
        if (empty($county)) {
            $this->session->set_flashdata('success', 'Please insert the country list');
            redirect();
        }
        
        $data['countryData'] = $county;        
        
        $data['postData'] = array(
            'address' => $userDetails->address,
            'city' => $userDetails->city,
            'zip_code' => $userDetails->zip_code,
            'country_id' => $userDetails->country_id,
            'countryIdHidden' => $userDetails->country_id,
            'phone' => $userDetails->phone,
            'display_name' => $userDetails->display_name,
            'phoneCode' => $userDetails->phonecode,
            'searchBy' => ''
        );      
        
        $this->form_validation->set_rules('address', 'Address', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('city', 'City', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('zip_code', 'Zip code', 'required|trim|min_length[3]|numeric');
        $this->form_validation->set_rules('country_id', 'Country', 'trim|callback__check_country');
        $this->form_validation->set_rules('phone', 'Phone', 'required|trim|min_length[3]|numeric');
        
        if ($this->input->post()) {        
            $data['postData'] = $this->input->post() + array('searchBy' => '');
            
            if ($this->form_validation->run()) {

                $conditions = array(
                    'id' => $this->input->post('country_id')
                );
                $countryData = $this->users_model->getCountry($conditions)->row();
                if (!empty($countryData)) {
                    $phoneCode = $countryData->phonecode;
                }
                
                $data['userDetails'] = array(
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'zip_code' => $this->input->post('zip_code'),
                    'country_id' => $this->input->post('country_id'),
                    'phone' => $this->input->post('phone'),
                    'phonecode' => $phoneCode
                );

                $this->users_model->updateAddressDetails($addressId, $data['userDetails']);
                $this->session->set_flashdata('success', 'You have successfully updated your personal details.');
                redirect('profile/personal-details');
            }
        }
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $data['title'] = 'Edit details';
        $this->load->view('users/addPersonalDetails', $data);
    }
    
    public function viewBillingAddresses() 
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Profile' => 'profile',
            'Billing Address' => ''
        ));

        $data = array();
        
        //get the main billing address
        $userId = $this->session->userdata('userId'); 
        $whereConditions = array(
            'ba.user_id' => $userId,
            'ba.is_main' => 1,
            'ba.is_deleted' => 0
        );
        $fieldsToSelect = array(
            'ba.user_id', 'ba.b_address', 'ba.b_city', 'ba.id', 'ba.b_zip_code', 'ba.b_phone', 'ba.b_country_id', 'ba.b_phone_code', 'c.display_name');
        $mainAddress = $this->users_model->getBillingAddresses($whereConditions, $fieldsToSelect)->row();

        $data['mainAddress'] = array();
        if (!empty($mainAddress)) {
           $data['mainAddress'] = $mainAddress; 
        }
        
        //get all billing addresses for this account
        $conditions = array(
            'ba.is_main' => 0,
            'ba.is_deleted' => 0,
            'ba.user_id' => $userId,
        );
        $billingAddresses = $this->users_model->getBillingAddresses($conditions, $fieldsToSelect)->result();

        $data['billingAddresses'] = array();
        if (!empty($billingAddresses)) {
           $data['billingAddresses'] = $billingAddresses; 
        }
        
        //get userData to pass to the users menu
        $conditions = array(
            'u.id' => $userId,
            'ud.is_main' => 1
        );
        $fields = array(
            'u.id', 'u.email', 'u.title', 'u.fname', 'u.lname', 'ud.detail_id', 'ud.address', 'ud.city', 'ud.phone', 'ud.phonecode', 'ud.zip_code', 'c.display_name'
        );
        $data['userData'] = $this->users_model->getAllUserData($conditions, $fields)->row();

        $data['postData'] = array(
            'searchBy' => ''
        );
        $data['title'] = 'Billing Address';
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $this->load->view('users/billingAddress', $data);
    }
        
    public function addBillingAddress() 
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Profile' => 'profile',
            'Add Address' => ''
        ));
        $userId = $this->session->userdata('userId');
        $data = array();
        
        $data['postData'] = array(
            'searchBy' => '',
            'address' => '',
            'city' => '',
            'zip_code' => '',
            'phone' => '',
            'phoneCode' => '',
            'countryIdHidden' => ''
        );
        
        //Get the country list for select
        $county = $this->users_model->getCountry()->result();
        if (empty($county)) {
            $this->session->set_flashdata('success', 'Please insert the country list');
            redirect();
        }
        
        $data['countryData'] = $county;  
        
        //set validations rules
        $this->form_validation->set_rules('address', 'Address', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('city', 'City', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('zip_code', 'Zip code', 'required|trim|min_length[3]|numeric');
        $this->form_validation->set_rules('country_id', 'Country', 'trim|callback__check_country');
        $this->form_validation->set_rules('phone', 'Phone', 'required|trim|min_length[3]|numeric');
        
        if ($this->input->post()) {
            $data['postData'] = $this->input->post() + array('searchBy' => '');

            //if the input falidation is successfull
            if ($this->form_validation->run()) {
                
                //get phonecode for the selected country
                $conditions = array(
                    'id' => $this->input->post('country_id')
                );
                $countryData = $this->users_model->getCountry($conditions)->row();
                
                if (!empty($countryData)) {
                    $phoneCode = $countryData->phonecode;
                }
                
                //data to be inserted in the db
                $userData = array(
                    'user_id' => $userId,
                    'b_address' => $this->input->post('address'),
                    'b_city' => $this->input->post('city'),
                    'b_zip_code' => $this->input->post('zip_code'),
                    'b_country_id' => $this->input->post('country_id'),
                    'b_phone_code' => $phoneCode,
                    'b_phone' => $this->input->post('phone'),
                );

                //chech if there is already main address, if no make this one main
                $whereConditions = array(
                    'ba.user_id' => $userId,
                    'ba.is_main' => 1
                );

                $mainAddress = $this->users_model->getBillingAddresses($whereConditions);
                if ($mainAddress->num_rows == 0) {
                    $userData = array_merge($userData, array('is_main' => 1));
                }
                
                $this->users_model->setBillingAddress($userData);
                $this->session->set_flashdata('success', 'Your billing address has been saved successfully');
                redirect('profile/billing-address');
            }
        }
        
        $data['title'] = 'Add Billing Address';
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $this->load->view('users/addBillingAddress', $data);
    }
    
    public function editBillingAddress($addressId)
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Profile' => 'profile',
            'Edit Address' => ''
        ));

        $county = $this->users_model->getCountry()->result();
        if (empty($county)) {
            $this->session->set_flashdata('success', 'Please insert the country list');
            redirect();
        }
        
        $data['countryData'] = $county;  
        
        $conditions = array(
            'ba.id' => $addressId
        );
        $fields = array(
            'ba.user_id', 'ba.b_address', 'ba.b_city', 'ba.id', 'ba.b_zip_code', 'ba.b_phone', 'ba.b_country_id', 'ba.b_phone_code', 'c.display_name');
        
        $currentAddress = $this->users_model->getBillingAddresses($conditions, $fields)->row();
        
        $data['postData'] = array(
            'searchBy' => '',
            'address' => $currentAddress->b_address,
            'city' => $currentAddress->b_city,
            'zip_code' => $currentAddress->b_zip_code,
            'country' => $currentAddress->display_name,
            'countryIdHidden' => $currentAddress->b_country_id,
            'phone' => $currentAddress->b_phone,
            'phoneCode' => $currentAddress->b_phone_code,
        );

        $this->form_validation->set_rules('address', 'Address', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('city', 'City', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('zip_code', 'Zip code', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('country_id', 'Country', 'trim|callback__check_country');
        $this->form_validation->set_rules('phone', 'Phone', 'required|trim|min_length[3]|numeric');
        
        if ($this->input->post()) {
            $data['postData'] = array_merge($this->input->post(), array('searchBy' => ''));
            
            if ($this->form_validation->run()) {
                $conditions = array(
                    'id' => $this->input->post('country_id')
                );
                $countryData = $this->users_model->getCountry($conditions)->row();
                if (!empty($countryData)) {
                    $phoneCode = $countryData->phonecode;
                }
                
                $data['userData'] = array(
                    'b_address' => $this->input->post('address'),
                    'b_city' => $this->input->post('city'),
                    'b_zip_code' => $this->input->post('zip_code'),
                    'b_country_id' => $this->input->post('country_id'),
                    'b_phone' => $this->input->post('phone'),
                    'b_phone_code' => $phoneCode
                );

                $this->users_model->updateBillingAddress($addressId, $data['userData']);
                $this->session->set_flashdata('success', 'You have successfully updated your billing address.');
                redirect('profile/billing-address');
            }
        }
        
        $data['title'] = 'Edit Billing Address';
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $this->load->view('users/addBillingAddress', $data);
    }

    public function mainDeliveryAddress($addressId)
    {
        //get the main delivery address
        $userId = $this->session->userdata('userId'); 
        $whereConditions = array(
            'ud.user_id' => $userId,
            'ud.is_main' => 1,
            'ud.is_deleted' => 0
        );
        $fields = array(
            'ud.detail_id', 'u.email', 'u.title', 'u.fname', 'u.lname', 'ud.address', 'ud.city', 'ud.phone', 'ud.phonecode', 'ud.zip_code', 'c.display_name'
        );
        $mainAddress = $this->users_model->getAllUserData($whereConditions, $fields)->row();

        if (!empty($mainAddress)) {
            //set the current main address to 0 (not main) if there is any
            $updateData = array(
                'is_main' => 0
            );
            $this->users_model->updateAddressDetails($mainAddress->detail_id, $updateData);
        }
        
        //set this address to main yo
        $updateData = array(
                'is_main' => 1
        );
        $this->users_model->updateAddressDetails($addressId, $updateData);
        
        $this->session->set_flashdata('success', 'You have successfully changed your current billing address');
        redirect('profile/personal-details');
    }
    
    public function mainBillingAddress($addressId)
    {
        //get the main billing address
        $userId = $this->session->userdata('userId'); 
        $whereConditions = array(
            'ba.user_id' => $userId,
            'ba.is_main' => 1,
            'ba.is_deleted' => 0
        );
        $fieldsToSelect = array(
            'ba.user_id', 'ba.b_address', 'ba.b_city', 'ba.id', 'ba.b_zip_code', 'ba.b_phone', 'ba.b_country_id', 'ba.b_phone_code', 'c.display_name');
        $mainAddress = $this->users_model->getBillingAddresses($whereConditions, $fieldsToSelect)->row();

        if (!empty($mainAddress)) {
             //set the current main address to 0 (not main) if there is any
            $updateData = array(
                'is_main' => 0
            );
            $this->users_model->updateBillingAddress($mainAddress->id, $updateData);
        }
        
        //set this address to main yo
        $updateData = array(
                'is_main' => 1
        );
        $this->users_model->updateBillingAddress($addressId, $updateData);
        
        $this->session->set_flashdata('success', 'You have successfully changed your current billing address');
        redirect('profile/billing-address');
    }
    
    public function softDeleteDeliveryAddress($addressId)
    {
        //check if the address you are trying to delete actually exists
        $conditions = array(
            'ud.detail_id' => $addressId
        );
        $fields = array(
            'ud.detail_id', 'u.email', 'u.title', 'u.fname', 'u.lname', 'ud.address', 'ud.city', 'ud.phone', 'ud.phonecode', 'ud.zip_code', 'c.display_name'
        );
        $addressData = $this->users_model->getAllUserData($conditions, $fields)->row();

        //if the address does not exists - redirect
        if (empty($addressData)) {
            redirect('profile/personal-details');
        }
        
        $updateData = array(
            'is_deleted' => 1
        );
        $this->users_model->updateAddressDetails($addressId, $updateData);
        
        $this->session->set_flashdata('success', 'You have successfully deleted this address');
        redirect('profile/personal-details');    
    }

    public function softDeleteBillingAddress($addressId)
    {
        //check if the address you are trying to delete actually exists
        $conditions = array(
            'ba.id' => $addressId
        );
        
        $addressData = $this->users_model->getBillingAddresses($conditions)->row();
        
        //if the address does not exists - redirect
        if (empty($addressData)) {
            redirect('profile/billing-address');
        }
        
        $updateData = array(
            'is_deleted' => 1
        );
        $this->users_model->updateBillingAddress($addressId, $updateData);
        
        $this->session->set_flashdata('success', 'You have successfully deleted this address');
        redirect('profile/billing-address');
    }

    public function getPhoneCode()
    {
        $countryId = $this->input->post('cId');
        
        $conditions = array(
            'id' => $countryId
        );
        $countryData = $this->users_model->getCountry($conditions)->row();
        
        if (!empty($countryData)) {
            $phoneCode = array(
                'phonecode' => $countryData->phonecode
            );            
        } else {
            $phoneCode = array(
                'phonecode' => 0
            );
        }
        
        echo json_encode($phoneCode);
        die;
        
    }
    
    public function orderHistory()
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Profile' => 'profile',
            'Order history' => ''
        ));
        $data = array();
        $this->load->library('pagination');
        
        $userId = $this->session->userdata('userId');

        $conditions = array(
            'user_id' => $userId
        );
        
        $total = $this->order_model->getOrders($conditions)->num_rows();

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1; //start page for pagination
        
        $pageLimit = 5;
        $max = array($pageLimit, ($page - 1) * $pageLimit);
        
        $data['orders'] = $this->order_model->getOrders($conditions, $max)->result();
        
        $config = array();
        $config['base_url'] = base_url() . 'profile/orders';
        $config['total_rows'] = $total;
        $config['per_page'] = $pageLimit;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = true;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li>';
        $config['num_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $data['postData'] = array(
            'searchBy' => ''
        );
        
        $data['title'] = 'Order History';
        $this->load->view('users/userOrderHistory', $data);
    }

    public function getOrderDetails()
    {
        $id = $this->input->post('id');
        $conditions = array(
            'po.order_id' => $id
        );
        $fields = array(
                    'o.order_id', 'o.fname', 'o.lname', 'o.email', 'o.address', 'o.city', 'o.country_name', 'o.zip_code', 'o.phone', 'o.status', 'o.order_price',
                    'po.quantity', 'po.price', 'po.total_price', 'po.product_id', 
                    'p.title'
                );
        
        $data['orderData'] = $this->order_model->getOrderData($conditions, $fields)->result();
       
        echo json_encode($data);
        die;
    }
    
    public function _check_title()
    {
        if ($this->input->post('title') == 'Title') {
            $this->form_validation->set_message('_check_title', 'Please choose your title.');
            return false;
        } else {
            return true;
        }
    }
    
    public function _check_country()
    {
        if ($this->input->post('country_id') == 0) {
            $this->form_validation->set_message('_check_country', 'Please select your country.');
            return false;
        } else {
            return true;
        }
    }
    
    public function _check_current_password($currPassword)
    {
        $conditions = array(
            'id' => $this->input->post('id'),
            'password' => $this->hash($this->input->post('currPassword'))
        );
        $userData = $this->users_model->getCurrentPassword($conditions)->num_rows();
        
        if ($userData == 0) {
            $this->form_validation->set_message('_check_current_password', "This password doesn't match your current password.");
            return false;
        } else {
            return true;
        }
    }

    public function _check_confirmation($passconf)
    {
        $passconf = $this->input->post('passconf');
        if ($this->input->post('password') == $passconf) {
            return true;
        } else {
            $this->form_validation->set_message('_check_confirmation', 'The confirmation password and the password does not match.');
            return false;
        }
    }

    public function _check_email($email)
    {
        $conditions = array(
            'email' => $email
        );
        $emailData = $this->users_model->getUserData($conditions)->num_rows();

        if ($emailData > 0) {
            $this->form_validation->set_message('_check_email', 'This email is already taken.');
            return false;
        } else {
            return true;
        }
    }
    
    public function ajaxCheckPassword()
    {       
        $conditions = array(
            'id' => $this->input->post('id'),
            'password' => $this->hash($_REQUEST['currPassword']),
        );
        
        $query = $this->users_model->getCurrentPassword($conditions);
        
        if ($query->num_rows() > 0) {
            echo 'true';
        } else {
            echo 'false';
        }
        die;
    }
    
    public function unlinkSocialAccounts($api){
        $this->load->model('users_model');
        
        $email = $this->session->userdata('email');
        
        $conditions = array(
            'email' => $email,
        );
        
        $checkApps = $this->users_model->getUserData($conditions)->row();
        
        if ($api == 'google') {
            if ($checkApps->google_id != null) {
                $data = array('google_id' => null, 'google_acc_token' => null);
                $this->users_model->updateUserData($email,$data);
                
                $this->session->set_flashdata('success', 'You have successfully unlinked your Google account.');
                redirect('profile');
            }else{
                $this->session->set_flashdata('error', 'You don\'t have linked Google account');
                redirect('profile');
            }
        }elseif($api == 'facebook'){
            if ($checkApps->facebook_id != null) {
                $data = array('facebook_id' => null, 'facebook_acc_token' => null);
                $this->users_model->updateUserData($email,$data);
                
                $this->session->set_flashdata('success', 'You have successfully unlinked your Facebook account.');
                redirect('profile');
            }else{
                $this->session->set_flashdata('error', 'You don\'t have linked Facebook account');
                redirect('profile');
            }
        }elseif($api == 'twitter'){
            if ($checkApps->twitter_id != null) {
                $data = array('twitter_id' => null, 'twitter_acc_token' => null);
                $this->users_model->updateUserData($email,$data);
                
                $this->session->set_flashdata('success', 'You have successfully unlinked your Twitter account.');
                redirect('profile');
            }else{
                $this->session->set_flashdata('error', 'You don\'t have linked Twitter account');
                redirect('profile');
            }
        }else{
            $this->session->set_flashdata('error', 'There\'s no such API');
            redirect('profile');
        }
    }
    
    public function hash($string) 
    {
        return hash("sha1", $string . config_item('encryption_key'));
    } 
}
