<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Finishing extends MY_Controller_Front {

    public function __construct() {
        parent::__construct();
        $this->load->model('finishing_front_m');
        $this->load->model('search_m');
        $this->load->model('products_model');
        $this->load->model('printers_m');
    }

    public function index($offset = 0) {

        $this->breadcrumb->populate(array(
            'Home' => '',
            'Finishing' => ''
        ));

        $data = array();
        $data['title'] = 'Finishing';
        $data['finishings'] = $this->finishing_front_m->getFinishings($offset, 12);
        $bestsells = $this->finishing_front_m->getFinishingCount();
        $current_time = time();
        foreach ($data['finishings']['data'] as $key => $val) {
            $data['finishings']['data'][$key]['flag'] = 0;
            if((($val['fl_date_begin'] == 0) && ($val['fl_date_end'] == 0))||($current_time < strtotime($val['fl_date_begin']))  )  {
            
                foreach ($bestsells as $k2 => $v2) {
                    if(($val['id']== $v2['id'])){ 
                        $data['finishings']['data'][$key]['flag'] = 1;
                    }   
                }
            }
        }
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() . 'finishing/index/';
        
         if ($this->input->get() != false) {
            $config['suffix'] = '?' . http_build_query($this->input->get(), '', '&');
            $config['first_url'] = base_url() . 'finishing/index/1' . $config['suffix'];
        }
        
        $config['total_rows'] = $data['finishings']['count'];
        $config['per_page'] = 12;
        $config['num_links'] = 9;
        $config['uri_segment'] = 3;

        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';


        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();
        
        $terms = $this->input->get('search');
        $data['postData'] = array(
            'searchBy' => ''
        );
        $data['search'] = $terms;
        checkFlashDate();

        $this->load->view('finishing/finishing_list', array(
            'title' => $data['title'],
            'finishings' => $data['finishings']['data'],
            'search' => $terms,
            'postData' => $data['postData'],
            'current_time' => $current_time,
            'links' => $links
        ));
    }

    public function finishingsByCategory($id = 0, $offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Finishing' => ''
        ));
        

        $data = array();
        $data['title'] = 'Finishing';

        $this->load->library('pagination');

        $search_term = $this->input->get('search');
        
        
        
        $id = $this->uri->segment(3);
        $data['finishings'] = $this->finishing_front_m->getFinishingsByCategory($id, $search_term, $offset, 12);

        $config = array();
        $config['base_url'] = base_url() . 'index.php/finishing/category/' . $this->uri->segment(3);
        $config['total_rows'] = $data['finishings']['count'];
        $config['per_page'] = 12;
        $config['num_links'] = 12;
        $config['uri_segment'] = 4;

        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';
        $config['next_tag_close'] = '</div></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();

        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        
        
        $this->load->view('finishing/finishing_cat_list', array(
            'title' => $data['title'],
            'postData' => $data['postData'],
            'finishings' => $data['finishings']['data'],
            'search' => $terms,
            'links' => $links,
        ));
        
    }

    public function finishingDetail($id = 0, $offset = 0) {
        $this->breadcrumb->populate(array(
            'Home' => 'home',
            'Finishing' => 'finishing',
            'Product Page' => 'finishing',
        ));

        $data = array();
        $data['title'] = 'Finishing';

        $this->load->library('pagination');

        $search_term = $this->input->get('search');

        $id = $this->uri->segment(3);
        $data['finishings'] = $this->finishing_front_m->getFinishingsByDetail($id, $search_term, $offset, 12);
        $data['images'] = $this->finishing_front_m->getImages($id);


        $config = array();
        $config['base_url'] = base_url() . 'index.php/finishing/finishingsByCategory/' . $this->uri->segment(3);
        $config['total_rows'] = $data['finishings']['count'];
        $config['per_page'] = 12;
        $config['num_links'] = 12;
        $config['uri_segment'] = 4;
        $config['first_link'] = false;
        $config['last_link'] = false;

        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();

        $terms = $this->input->get('search');
        $data['search'] = $terms;
        $data['postData'] = array(
            'searchBy' => ''
        );
        //For last seen - 2 products
        $idl = $this->session->userdata('idl');
        $idl[] = $id;
        if (is_array($idl)) {
            $twoLastSeen = array_unique(array_slice($idl, -2, 2));
            $last = array(
                'idl' => $twoLastSeen
            );
            $this->session->set_userdata($last);
        }
        //Get Related Finishings
        $data['current_time'] = date("Y-m-d H:i:s");
        $data['relatedFinishings']['relatedProducts'] = $this->products_model->getRelatedProducts(3,$id);

        $this->load->view('finishing/finishing_detail_list', array(
            'title' => $data['title'],
            'finishings' => $data['finishings']['data'],
            'images' => $data['images'],
            'search' => $data['search'],
            'postData' => $data['postData'],
            'links' => $links,
            'relatedProducts' => $data['relatedFinishings']['relatedProducts'],
            'current_time' => $data['current_time']
        ));
    }

    public function search() {
        $this->load->library('pagination');
        
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Finishing' => ''
        ));
        $cat = 0;
        
         
        if($this->uri->segment(1) == 'finishing'){
            $cat = 3;
        }elseif($this->uri->segment(1) == 'filamentportfolio'){
            $cat = 2;
        }elseif($this->uri->segment(1) == 'printersportfolio'){
            $cat = 1;
        }

        $data = array();

         $filterable = array(
                'min',
                'max',
                'per',
                'searchBy',
                'catName',
                'orderBy',
                'search',
                'man',
                'tech',
                'size',
                'acc',
                
            );
        
        $allqueryparams = $this->input->get();
        
        $actualfilters = array();
        
        foreach($filterable as $key){
            if (array_key_exists($key, $allqueryparams)){
                $actualfilters[$key] = $allqueryparams[$key];
            }
        }
        
        
        if(!array_key_exists('min', $this->input->get())){
            $actualfilters['min'] = null;
        }
        if(!array_key_exists('max', $this->input->get())){
            $actualfilters['max'] = null;
        }
        if(!array_key_exists('catName', $this->input->get())){
            $actualfilters['catName'] = null;
        }
        if(!array_key_exists('man', $this->input->get())){
            $actualfilters['man'] = null;
        }
        if(!array_key_exists('tech', $this->input->get())){
            $actualfilters['tech'] = null;
        }
        if(!array_key_exists('size', $this->input->get())){
            $actualfilters['size'] = null;
        }
        if(!array_key_exists('acc', $this->input->get())){
            $actualfilters['acc'] = null;
        }
        
        $like = array();

        if ($this->input->get()) {
            
            $prices = $this->search_m->getPrices();
            $a = array();
            $b = array();
            foreach($prices as $key=>$value){
                  array_push($a,$prices[$key]->price);  
                  array_push($b,$prices[$key]->new_price);  
            }
            
    
            switch ($this->input->get('orderBy')) {
                
                case 1: $option = 'price';
                    $option2 = 'ASC';
                    break;
                case 2: $option = 'price';
                    $option2 = 'DESC';
                    break;
                case 3: $option = 'title';
                    $option2 = 'ASC';
                    break;
                case 4: $option = 'title';
                    $option2 = 'DESC';
                    break;
                default : $option = 'date_added';
                    $option2 = 'DESC';
            }
        }

         $total = $this->search_m->getSearchResult(
                    $cat,$actualfilters['catName'],
                    null,
                    $like,
                    $option,
                    $option2,
                    $actualfilters['min'],
                    $actualfilters['max'],
                    $actualfilters['man'],
                    $actualfilters['tech'],
                    $actualfilters['size'],
                    $actualfilters['acc']
                )->num_rows();  
         
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1; 
        
        $pageLimit = 12;
        if(array_key_exists('per', $this->input->get())){
            $pageLimit = $this->input->get('per');
        }
        
        $max = array($pageLimit, ($page - 1) * $pageLimit);
        
        
        $messages = $this->search_m->getSearchResult(
                    $cat,
                    $actualfilters['catName'],
                    $max,
                    $like,
                    $option,
                    $option2,
                    $actualfilters['min'],
                    $actualfilters['max'],
                    $actualfilters['man'],
                    $actualfilters['tech'],
                    $actualfilters['size'],
                    $actualfilters['acc']
                )->result_array();

        //for best sellers
        $data['messageData'] = $messages;  
        $bestsells = $this->finishing_front_m->getFinishingCount();
        $current_time = time();
        foreach ($data['messageData'] as $key => $val) {
            $data['messageData'][$key]['flag'] = 0;
            if((($val['fl_date_begin'] == 0) && ($val['fl_date_end'] == 0))||($current_time < strtotime($val['fl_date_begin']))  )  {
            
                foreach ($bestsells as $k2 => $v2) {
                    if(($val['id']== $v2['id'])){ 
                        $data['messageData'][$key]['flag'] = 1;
                }   
            }
        }
        }
        //setting the $config array for the pagination
        $config = array();
        $config['base_url'] = base_url() . 'finishing/search';

        if ($this->input->get() != false) {
            $config['suffix'] = '?' . http_build_query($this->input->get(), '', '&');
            $config['first_url'] = base_url() . 'finishing/search/1' . $config['suffix'];
        }

        $config['total_rows'] = $total;
        $config['per_page'] = $pageLimit;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = true;

        $config['first_link'] = false;
        $config['last_link'] = false;

        $config['full_tag_open'] = '<ul class="examp">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="icon-triangle-left"></span> Prev';
        $config['prev_tag_open'] = '<li><div class="prev">';
        $config['prev_tag_close'] = '</div></li>';
        $config['next_link'] = 'Next <span class="icon-triangle-right"></span>';
        $config['next_tag_open'] = '<li><div class="next">';

        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = ' <li class="page">';
        $config['num_tag_close'] = '</li>';

        //initializing pagination
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['title'] = 'Finishings';

        $terms = $this->input->get('search');

        $data['search'] = $terms;   
        $data['postData'] = array(
            'searchBy' => ''
        );
        
        $data['category'] = $this->input->get('catName');
        $data['min'] = $this->input->get('min');
        $data['max'] = $this->input->get('max');
        $data['man'] = $this->input->get('man');
        $data['tech'] = $this->input->get('tech');
        $data['size'] = $this->input->get('size');
        $data['acc'] = $this->input->get('acc');
        $current_time = time();
        $data['current_time'] = $current_time;
        $this->load->view('sort/product_list', $data);
    }
    
    public function autoSearch(){
            $keyword = $this->input->post('keyword');
            $selectID = $this->input->post('selectId');

            $result = $this->finishing_front_m->autoSearch($keyword,$selectID);

            if($keyword == ''){
                
            }else{
            if(!empty($result)) {
            ?>
            <ul id="country-list">
            <?php
            foreach($result as $value) {
            ?>
            <li onClick="select('<?php echo $value->title; ?>');"><?php echo $value->title; ?></li>
            
            <?php } ?>
            </ul>
        <?php } } 

        }

}
