<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
class register extends MY_Controller_Front{
    
    public function __construct() {
        parent::__construct();
        if (isLogged()) {
            $this->session->set_flashdata('success', 'Please logout before registering a new accout');
            redirect('');
        }
        $this->load->model('users_model');
        $this->load->model('footer_m');
    }
    
    
    public function index()
    {
        $data['title'] = 'Welcome';
        $terms = $this->input->get('search');
        $data['search'] = $terms;  
        $data['postData'] = array(
            'searchBy' => ''
        );
        $this->load->view('register/registerSuccess_view', $data);
    }

    public function createAccount()
    {
        $this->breadcrumb->populate(array(
            'Home' => '',
            'Login/Register' => 'login',
            'Register'
        ));
        
        $data = array();
        $data['postData'] = array(
            'searchBy' => '',
            'password' => '',
            'email' => '',
            'fname' => '',
            'lname' => '',
            'title' => ''
        );
        $this->form_validation->set_rules('title', 'Title', 'required|callback__check_title');
        $this->form_validation->set_rules('fname', 'First name', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('lname', 'Last name', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|callback__check_confirmation');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback__check_email');
        $this->form_validation->set_rules('terms', 'Terms', 'callback__check_terms');
        
        if ($this->input->post()) {
            $data['postData'] = array_merge($this->input->post(), array('searchBy' => ''));
            
            if ($this->form_validation->run()) {
                            
                if ($this->input->post('title') == 0) {
                    $title = 'Ms';
                } elseif ($this->input->post('title') == 1) {
                    $title = 'Mr';
                }
            
                if ($this->input->post('news')) {                                      
                    $conditions = array(
                        'email' => $this->input->post('email'),
                        'date' => date("Y-m-d H:i:s")
                    );
                    $query = $this->footer_m->getEmails($conditions);

                    if ($query->num_rows() == 0) {
                        $this->footer_m->news($conditions);
                    }
                    
                }
                $data['userData'] = array(
                    'password' => $this->hash($this->input->post('password')),
                    'email' => $this->input->post('email'),
                    'fname' => $this->input->post('fname'),
                    'lname' => $this->input->post('lname'),
                    'title' => $title,
                );
                
                $this->users_model->setUserData($data['userData']);
                $this->session->set_flashdata('success', 'Thank you for registering to Imark');
                redirect('register/index');
            }
        }
        $terms = $this->input->get('search');       
        $data['search'] = $terms;  
        $data['title'] = 'Register';
        $this->load->view('register/register_view', $data);   
    }
    
    public function _check_title()
    {
        if ($this->input->post('title') == 'Title') {
            $this->form_validation->set_message('_check_title', 'Please choose your title');
            return FALSE;
        } else {
            return true;
        }
    }
    
    public function _check_terms()
    {   
        if ($this->input->post('terms') == false) {
            $this->form_validation->set_message('_check_terms', 'Please agree with our terms of use');
            return FALSE;
        } else {
            return true;
        }
    }

    public function _check_confirmation($passconf)
    {
        $passconf = $this->input->post('passconf');
        if ($this->input->post('password') == $passconf) {
            return true;
        } else {
            $this->form_validation->set_message('_check_confirmation', 'The confirmation password and the password does not match.');
            return false;
        }
    }

    public function _check_email($email)
    {
        $conditions = array(
            'email' => $email
        );
        $emailData = $this->users_model->getUserData($conditions)->num_rows();

        if ($emailData > 0) {
            $this->form_validation->set_message('_check_email', 'This email is already taken.');
            return false;
        } else {
            return true;
        }
    }
    
    public function ajaxCheckEmail()
    {
        $conditions = array(
            'email' => $_REQUEST['email'],
        );
        
        $query = $this->users_model->getUserData($conditions);
        if ($query->num_rows() > 0) {
            echo 'false';
        } else {
            echo 'true';
        }
        die;
    }

    public function hash($string) 
    {
        return hash("sha1", $string . config_item('encryption_key'));
    } 
}
