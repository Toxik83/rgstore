<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['default_controller'] = 'home';
$route['404_override'] = 'error';

$route['register'] = 'register/createAccount';
$route['login'] = 'login/validate';
$route['login/forgotten-password'] = 'login/forgottenPassword';
$route['login/reset'] = 'login/reset';
$route['login/google'] = 'oAuthController/google'; 
$route['login/facebook'] = 'oAuthController/facebook';
$route['login/twitter'] = 'oAuthController/twitter';
$route['login/set-password'] = 'oAuthController/setPassword';

$route['profile'] = 'userProfile/index';
$route['profile/edit-userinfo/(:num)'] = 'userProfile/editUserInfo/$1';
$route['profile/personal-details'] = 'userProfile/viewPersonalDetails';
$route['profile/change-main/(:num)'] = 'userProfile/mainDeliveryAddress/$1';
$route['profile/remove-details/(:num)'] = 'userProfile/softDeleteDeliveryAddress/$1';

$route['profile/billing-address'] = 'userProfile/viewBillingAddresses';
$route['profile/add-billing-address'] = 'userProfile/addBillingAddress';
$route['profile/edit-billing-address/(:num)'] = 'userProfile/editBillingAddress/$1';
$route['profile/main-billing-address/(:num)'] = 'userProfile/mainBillingAddress/$1';
$route['profile/remove/(:num)'] = 'userProfile/softDeleteBillingAddress/$1';

$route['profile/add-details'] = 'userProfile/addPersonalDetails';
$route['profile/edit-details/(:num)'] = 'userProfile/editPersonalDetails/$1';

$route['profile/orders'] = 'userProfile/orderHistory';
$route['profile/orders/(:num)'] = 'userProfile/orderHistory/$1';
$route['unlink/(:any)'] = 'userProfile/unlinkSocialAccounts/$1';

$route['contact-us'] = 'contactUs/index';
$route['contact-us/send-message'] = 'contactUs/sendMessage';

$route['admin/orders'] = 'admin/adminOrders/index';
$route['admin/orders/(:num)'] = 'admin/adminOrders/index/$1';
$route['admin/orders/details/(:num)'] = 'admin/adminOrders/orderDetails/$1';
$route['admin/orders/confirm/(:num)'] = 'admin/adminOrders/confirmOrder/$1';
$route['admin/orders/cancel/(:num)'] = 'admin/adminOrders/cancelOrder/$1';
$route['admin/orders/paid/(:num)'] = 'admin/adminOrders/setToPaid/$1';

$route['admin/bundle'] = 'admin/adminBundle/index';
$route['admin/bundle/create'] = 'admin/adminBundle/createBundle';
$route['admin/bundle/edit/(:num)'] = 'admin/adminBundle/editBundle/$1';
$route['admin/bundle/delete/(:num)'] =  'admin/adminBundle/softDelete/$1';

$route['admin/menus'] = 'admin/adminMenus/index';
$route['admin/menus/view/(:num)'] = 'admin/adminMenus/viewMenu/$1';
$route['admin/menus/add'] = 'admin/adminMenus/addLink';
$route['admin/menus/add/(:num)'] = 'admin/adminMenus/addLink/$1';
$route['admin/menus/edit-item/(:num)'] = 'admin/adminMenus/editLink/$1';
$route['admin/menus/delete-item/(:num)'] = 'admin/adminMenus/deleteItem/$1';
$route['admin/menus/create'] = 'admin/adminMenus/createMenu';
$route['admin/menus/edit-menu/(:num)'] = 'admin/adminMenus/editMenu/$1';
$route['admin/menus/delete-menu/(:num)'] = 'admin/adminMenus/deleteMenu/$1';
$route['admin/menus/ajax-delete'] = 'admin/adminMenus/ajaxDelete';

$route['page/(:any)'] = 'staticPages/view/$1';
$route['admin/static-pages'] = 'admin/adminStaticPages/index';
$route['admin/static-pages/create'] = 'admin/adminStaticPages/createPage';
$route['admin/static-pages/edit/(:num)'] = 'admin/adminStaticPages/editPage/$1';
$route['admin/static-pages/delete/(:num)'] = 'admin/adminStaticPages/deletePage/$1';
$route['admin/static-pages/bulk-delete'] = 'admin/adminStaticPages/bulkDelete';

$route['admin/admin-inbox'] ='admin/adminInbox/index';
$route['admin/admin-inbox/(:num)'] ='admin/adminInbox/index/$1';
$route['admin/admin-inbox/bulk-select'] = 'admin/adminInbox/bulkSelect';
$route['admin/admin-inbox/reply/(:num)'] ='admin/adminInbox/reply/$1';
$route['admin/admin-inbox/message/(:num)'] ='admin/adminInbox/readMessage/$1';
$route['admin/admin-inbox/delete/(:num)'] ='admin/adminInbox/deleteMessage/$1';

$route['admin/admin-slider'] = 'admin/adminSlider';
$route['admin/admin-slider/reorder'] = 'admin/adminSlider/reorder';
$route['admin/admin-slider/(:num)'] = 'admin/adminSlider/index/$1';
$route['admin/admin-slider/bulk-delete'] = 'admin/adminSlider/bulkDelete';
$route['admin/admin-slider/ajax-delete'] = 'admin/adminSlider/ajaxDelete';
$route['admin/admin-slider/create-slide'] = 'admin/adminSlider/createSlide';
$route['admin/admin-slider/edit-slide/(:num)'] = 'admin/adminSlider/editSlide';
$route['admin/admin-slider/delete/(:num)'] = 'admin/adminSlider/deleteSlide/$1';

$route['paypal-payment'] = 'PayPalPayment/index';

/*/
 * Velko`s routing xD
 */

$route['admin/chat/listing/(:num)'] = 'admin/chat/chatListing/$1';
$route['admin/finishing/flashdate-edit/(:num)'] = 'admin/finishing/flashdate_edit/$1';
$route['admin/finishing/edit/(:num)'] = 'admin/finishing/editFinishing/$1';
$route['admin/manage-admin/edit/(:num)'] = 'admin/manage_admin/edit/$1';
$route['admin/finishing-gallery/images/(:num)'] = 'admin/finishingGallery/getImages/$1';
$route['admin/price-range/edit/(:num)'] = 'admin/priceRange/edit/$1';
$route['admin/category/edit/(:num)'] = 'admin/finishing/editCat/$1';



$route['admin/manage-admin'] = 'admin/manage_admin';
$route['admin/manage-admin/create'] = 'admin/manage_admin/createAdmin';
$route['admin/finishing/create'] = 'admin/finishing/createFinishing';
$route['admin/price-range'] = 'admin/priceRange';
$route['admin/category'] = 'admin/finishing/createCategory';
$route['admin/finishing/edit'] = 'admin/finishing/editFinishing';

$route['finishing/category/(:num)'] = 'finishing/finishingsByCategory/$1';
$route['finishing/details/(:num)'] = 'finishing/finishingDetail/$1';

/*
 * End of it :) 
 */

/* End of file routes.php */
/* Location: ./application/config/routes.php */

