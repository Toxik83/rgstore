<?php

function getBundleDetails($bundleId)
{
    $CI 	=& get_instance();

    $CI->load->model('products_model');

    $conditions = array(
        'b.product_id' => $bundleId,
        'pi.main_pic' => 1,       
    );

    $fields = array('b.bundle_name', 'b.discount', 'b.discount_price', 'b.combined_price', 'p.title', 'p.id', 'p.price', 'p.cat_id', 'pi.name');
    
    $bundleData = $CI->products_model->getBundleData($conditions, $fields, null, null)->result();

    return $bundleData;
}

function getRelatedBundle($productId)
{
    $CI 	=& get_instance();

    $CI->load->model('products_model');

    $conditions = array(
        'bi.product_id' => $productId,
        'p.active' => 0,
        'p.cat_id' => 4
    );

    $fields = array('b.product_id', 'b.bundle_name', 'b.discount', 'b.discount_price', 'b.combined_price');
    
    $bundleData = $CI->products_model->getRelatedBundleData($conditions, $fields, $start='', $limit = 1)->row();
    
    return $bundleData;
}
