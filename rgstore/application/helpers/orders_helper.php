<?php

function getOrderDetails($orderId)
{
    $CI 	=& get_instance();

    $CI->load->model('order_model');

    $conditions = array(
        'po.order_id' => $orderId
    );

    $fields = array(
                    'o.order_id', 'o.fname', 'o.lname', 'o.email', 'o.address', 'o.city', 'o.country_name', 'o.date_added','o.zip_code', 'o.phone', 'o.phone_code', 'o.status', 'o.order_price', 
                    'po.quantity', 'po.price', 'po.total_price', 'po.product_id', 
                    'p.title'
                );
    
    $orderData = $CI->order_model->getOrderData($conditions, $fields)->result();
    
    return $orderData;
}