<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function getCategories() {
    $CI = & get_instance();

    $CI->load->model('finishing_front_m');

    $categories = $CI->finishing_front_m->getCatogories();

    return $categories;
}

function getManufacturer() {
    $CI = & get_instance();

    $CI->load->model('manufacturer_model');

    $manufacturers = $CI->manufacturer_model->show_Company();

    return $manufacturers;
}

function getPrintTech() {
    $CI = & get_instance();

    $CI->load->model('print_tech_model');

    $print_tech = $CI->print_tech_model->show_Print_tech();

    return $print_tech;
}

function getSize() {
    $CI = & get_instance();

    $CI->load->model('building_size_model');

    $sizes = $CI->building_size_model->show_Size();

    return $sizes;
}

function getAccuracy() {
    $CI = & get_instance();

    $CI->load->model('accuracy_model');

    $accuracies = $CI->accuracy_model->show_Accuracy();

    return $accuracies;
}

function getPrices() {
    $CI = & get_instance();

    $CI->load->model('price_m');

    $prices = $CI->price_m->getSidebarPrices();

    return $prices;
}

function Filament_Categories() {
    $CI = & get_instance();

    $CI->load->model('filament_model');

    $categories = $CI->filament_model->getCategories();

    return $categories;
}

function Chat() {
    $CI = & get_instance();
    $CI->load->model('chat_m');

    $CI->load->library('session');

    $chatList = $CI->chat_m->getAdminInfo();

    if ($CI->session->userdata('username') == 'admin') {
        $CI->chat_m->setOnline();
    }

    return $chatList;
}

function getLast() {

    $CI = & get_instance();
    $CI->load->model('printers_m');
    $CI->load->library('session');
    $idl = $CI->session->userdata('idl');
    $lastseen = array();
    if (!empty($idl)) {
        foreach ($idl as $idls => $id) {
            $lastseenbyIds = $CI->printers_m->getLastSeen($id);
            array_push($lastseen, $lastseenbyIds);
        }
    }
    return $lastseen;
}
