<?php

function getMenus($keywords = array())
{
    $CI 	=& get_instance();

    $CI->load->model('menus_model');

    $conditions = array(
        'keywords' => $keywords
    );

    $menuData = $CI->menus_model->getMenuHelper($conditions)->result();

    return $menuData;
}