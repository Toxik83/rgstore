<?php

function getCartContent()
{
    $CI 	=& get_instance();

    $CI->load->library('cart');

    $cartContent = array(
        'price' => $CI->cart->total(),
        'total' => $CI->cart->total_items()
    );
    
    return $cartContent;
}

function getCompareList()
{
    $CI 	=& get_instance();

    $items = count($CI->session->userdata('ids'));
    if ($CI->session->userdata('ids') == false) {
        $items = 0;
    }

    return $items;
}