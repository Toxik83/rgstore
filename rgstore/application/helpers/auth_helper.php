<?php 

    function isLogged()
    {
        $CI 	=& get_instance(); //Include-va framework-a (moga da izpolzvam instrumentite mu)
        $CI->load->library('session');

        return  $CI->session->userdata('logged_in') == '1' ? true : false;   
    }
    
    function isAdmin()
    {
        $CI 	=& get_instance(); //Include-va framework-a (moga da izpolzvam instrumentite mu)
        $CI->load->library('session');
        
        return  $CI->session->userdata('is_admin') == '1' ? true : false;   
    }

