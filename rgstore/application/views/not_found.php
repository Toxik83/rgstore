<?php $this->load->view('includes/header'); ?>
<section id="section">
    <div class="content_container">
        <div class="column_wrap">
            <div class="breadcrumbs">
                <?= $this->breadcrumb->output(); ?>
            </div>
            <div class="static_content">
                <div class="page_not_found">
                    <div class="error404">
                        404
                    </div>
                    <h2>This page is not available</h2>
                    <p>The page you are looking for does not exist.</p>
                </div>
            </div>
            <div class="static_sidebar_container">

                <?php $this->load->view('includes/static_sidebar'); ?>

            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>

