<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="main_content_full">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>
            </div>

            <?php if ($this->session->flashdata('success') != null) : ?>

                <span class="icon-success"></span>
                <p class="message">
                    <?= $this->session->flashdata('success'); ?>
                </p>

            <?php endif; ?>

            <div class="map">
                <div>
                    <iframe width="60%" height="550px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                            src="https://maps.google.com/maps?hl=en&amp;q=Hristo Botev 92 Plovdiv, Bulgaria&amp;ie=UTF8&amp;t=roadmap&amp;z=13&amp;iwloc=B&amp;output=embed">
                    </iframe>
                </div>
            </div>
            <h2 class="contact_title">Send us a message:</h2>
            <div class="envelope">
                <ul class="imakr_address">
                    <li>
                        Address: Plovdiv, Bulgaria
                    </li>
                    <li>
                        Phone: (123) 456-789
                    </li>
                    <li>
                        Fax: + 30 210 520 3315
                    </li>
                    <li>
                        Email: rgatewaytest@gmail.com
                    </li>
                </ul>
                </br></br></br>
                <form action="<?= site_url('contact-us/send-message'); ?>" method="post" class="contact_form">
                    <div class="contact_us">
                        <input type="email" id="from" name="from" placeholder="From (email):"
                               value="<?= $postData['from']; ?>"/>

                        <?php if (form_error('from')) : ?>

                            <div class="form_error">
                                <?= form_error('from'); ?>
                            </div>

                        <?php endif; ?>

                        <input type="text" id="subject" name="subject" placeholder="Subject:"
                               value="<?= $postData['subject']; ?>"/>

                        <?php if (form_error('subject')) : ?>

                            <div class="form_error">
                                <?= form_error('subject'); ?>
                            </div>

                        <?php endif; ?>

                    </div>
                    <div>
                        <textarea name="message" id="message" rows="10" cols="80"><?= $postData['message']; ?></textarea>
                        <?php if (form_error('message')) : ?>

                            <div class="form_error">
                                <?= form_error('message'); ?>
                            </div>

                        <?php endif; ?>

                    </div>
                    <button type="submit" name="send" class="contact_button">Send</button>
                    <div class="clear"></div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>
