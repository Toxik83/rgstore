<?php $this->load->view('includes/header'); ?>
<section>
    <form id="printer" method="get" action="printersportfolio/search">
        <div class="content_container">
            <div class="column_wrap">

                <?php if (isset($printerlist) && $printerlist) : ?>

                    <?php $this->load->view('includes/sidebar'); ?>

                    <div class="main_content">
                        <div class="breadcrumbs">
                            <?= $this->breadcrumb->output(); ?>
                        </div>
                    </div>
                    <div class="main_content select_roll">
                        <div class="sort sort_left">
                            <h2>Sort Results by:</h2>
                            <div class="custom_select">
                                <div class="select">
                                    <div class="select_text icon-triangle-down">
                                        <p>
                                            --
                                        </p>
                                    </div>
                                    <select class="sort_result" name="orderBy" onchange="printer.submit()">
                                        <option value="0" > -- </option>
                                        <option value="1" <?= isset($_GET['orderBy']) && $_GET['orderBy'] == 1 ? ' selected' : '' ?>>
                                            Price - Low to High
                                        </option>
                                        <option value="2" <?= isset($_GET['orderBy']) && $_GET['orderBy'] == 2 ? ' selected' : '' ?>>
                                            Price - High to Low
                                        </option>
                                        <option value="3" <?= isset($_GET['orderBy']) && $_GET['orderBy'] == 3 ? ' selected' : '' ?>>
                                            Product Name - A to Z
                                        </option>
                                        <option value="4" <?= isset($_GET['orderBy']) && $_GET['orderBy'] == 4 ? ' selected' : '' ?>>
                                            Product Name - Z to A
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="sort sort_right">
                            <h2>Results per page:</h2>
                            <div class="custom_select">
                                <div class="select">
                                    <div class="select_text icon-triangle-down">
                                        <p>
                                            12
                                        </p>
                                    </div>
                                    <select class="per" onchange="printer.submit()">
                                        <option value="12" <?= isset($_GET['per']) && $_GET['per'] == 12 ? ' selected' : '' ?>>
                                            12
                                        </option>
                                        <option value="24" <?= isset($_GET['per']) && $_GET['per'] == 24 ? ' selected' : '' ?>>
                                            24
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main_content">
                        <ul class="column_container listing" data-columns="4">

                            <?php foreach ($printerlist as $printer => $pr): ?>

                                <li>
                                    <div class="product_details">
                                        <a href="<?= site_url('printersportfolio/printersdetail/'.$pr['id']); ?>"  >
                                            <img src="<?= base_url('upload/images155x155/' . $pr['name']); ?>" />

                                            <?php if ($pr['flag'] == 1) : ?>

                                                <div class="bestseller arrow_box">
                                                    <span class="block">Best</span>
                                                    <span class="block">Seller</span>
                                                </div>

                                            <?php endif; ?>

                                            <?php if ((($pr['fl_date_begin']) === '0000-00-00 00:00:00') &&
                                                ($pr['fl_date_end']) === '0000-00-00 00:00:00') : ?>

                                                <?php if ($pr['new_price'] == '0.00') : ?>

                                                    <p>
                                                        <?= $pr['title']; ?>
                                                    </p>
                                                    <div class="price_compare">
                                                        <span class="new_price equal">
                                                            &pound;<?= $pr['price']; ?>
                                                        </span>
                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
<!--  -->                                                           <a href="#" class="compare compare_link" data-id="<?php echo  $pr['id']; ?>"> 

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $pr['id']; ?>"
                                                                          class="<?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2' ;?> cell">

                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                    </span>

                                                                <?php endif; ?>

<!--  -->                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell share_cell">Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$pr['id']);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                       <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$pr['title']?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$pr['id'])?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$pr['id'])?>" target="_blank" onclick="googleShare()">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                <?php else : ?>

                                                    <p>
                                                        <?= $pr['title']; ?>
                                                    </p>
                                                    <div class="price_compare">
                                                        <span class="old_price">
                                                            was
                                                            <span class="op_value">
                                                                &pound;<?= $pr['price']; ?>
                                                            </span>
                                                        </span>
                                                        <span class="new_price" >
                                                            &pound;<?= $pr['new_price']; ?>
                                                        </span>
                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
                                                            <a href="#" class="compare compare_link" data-id="<?= $pr['id']; ?>">

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $pr['id']; ?>"
                                                                          class="<?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                    </span>

                                                                <?php endif; ?>

                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell share_cell"> Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$pr['id']);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                        <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$pr['title']?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$pr['id'])?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$pr['id'])?>" target="_blank" onclick="googleShare()">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                <?php endif; ?>

                                            <?php endif; ?>

                                            <?php if ((($pr['fl_date_begin']) !== '0000-00-00 00:00:00') &&
                                                ($pr['fl_date_end']) !== '0000-00-00 00:00:00') : ?>

                                                <?php if (($current_time >= strtotime($pr['fl_date_begin'])) &&
                                                    ($current_time <= strtotime($pr['fl_date_end']))) : ?>

                                                    <?php if (((strtotime($pr['fl_date_end']) - $current_time) / (60 * 60 * 24)) <= 1 &&
                                                        ((strtotime($pr['fl_date_end']) - $current_time) / (60 * 60 * 24)) > 0)  : ?>

                                                        <div class="promo_tag">
                                                            <div class="flash_sale expiring arrow_box">
                                                                Flash Sale
                                                                <span class="duration">1 day left</span>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            <?= $pr['title']; ?>
                                                        </p>
                                                        <div class="price_compare">
                                                            <span class="old_price">
                                                                was
                                                                <span class="op_value">
                                                                    &pound;<?= $pr['price']; ?>
                                                                </span>
                                                            </span>
                                                            <span class="new_price">
                                                                &pound;<?= $pr['new_price']; ?>
                                                            </span>
                                                        </div>
                                                        <ul class="cl_share">
                                                            <li class="cl">
                                                                <a href="#" class="compare compare_link" data-id="<?= $pr['id']; ?>">

                                                                    <?php if ($this->session->userdata('ids') == null): ?>

                                                                        <span class="icon-justice2 cell"></span>
                                                                        <span class="cell">Add to Compare list</span>

                                                                    <?php else : ?>

                                                                        <span id="product_<?= $pr['id']; ?>"
                                                                              class="<?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                                  'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                        </span>
                                                                        <span class="cell">
                                                                            <?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                                'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                        </span>

                                                                    <?php endif; ?>

                                                                </a>
                                                            </li>
                                                            <li class="share dropdownShare">
                                                                <a href="javascript:void(0)" class="compare">
                                                                    <span class="icon-share cell"></span>
                                                                    <span class="cell share_cell"> Share</span>
                                                                </a>
                                                                <ul class="dropdown-share">
                                                                    <li class="social_share">
                                                                        <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$pr['id']);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                            <span class="icon-facebook2"></span>
                                                                        </a>
                                                                        <span class="icon-cancel-circle"></span>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://twitter.com/intent/tweet?text=I just bought <?=$pr['title']?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$pr['id'])?>&hashtags=RGStore" target="_blank">
                                                                            <span class="icon-twitter2"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$pr['id'])?>" target="_blank" onclick="googleShare()">
                                                                            <span class="icon-google-plus-square"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>

                                                    <?php  else : ?>

                                                        <div class="flash_sale arrow_box">
                                                            Flash Sale
                                                            <span class="duration">
                                                                <?= date_format(date_create($pr['fl_date_begin']), "d/m"); ?> -
                                                                <?= date_format(date_create($pr['fl_date_end']), "d/m"); ?>
                                                            </span>
                                                        </div>
                                                        <p>
                                                            <?= $pr['title']; ?>
                                                        </p>
                                                        <div class="price_compare">
                                                            <span class="old_price">
                                                                was
                                                                <span class="op_value">
                                                                    &pound;<?= $pr['price']; ?>
                                                                </span>
                                                            </span>
                                                            <span class="new_price">
                                                                &pound;<?= $pr['new_price']; ?>
                                                            </span>
                                                        </div>
                                                        <ul class="cl_share">
                                                            <li class="cl">
                                                                <a href="#" class="compare compare_link" data-id="<?= $pr['id']; ?>">

                                                                    <?php if ($this->session->userdata('ids') == null): ?>

                                                                        <span class="icon-justice2 cell"></span>
                                                                        <span class="cell">Add to Compare list</span>

                                                                    <?php else : ?>

                                                                        <span id="product_<?= $pr['id']; ?>"
                                                                              class="<?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                                  'icon-justice3' : 'icon-justice2' ;?> cell"></span>
                                                                        <span class="cell">
                                                                            <?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                                'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                        </span>

                                                                    <?php endif; ?>

                                                                </a>
                                                            </li>
                                                            <li class="share dropdownShare">
                                                                <a href="javascript:void(0)" class="compare">
                                                                    <span class="icon-share cell"></span>
                                                                    <span class="cell share_cell"> Share</span>
                                                                </a>
                                                                <ul class="dropdown-share">
                                                                    <li class="social_share">
                                                                        <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$pr['id']);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                            <span class="icon-facebook2"></span>
                                                                        </a>
                                                                        <span class="icon-cancel-circle"></span>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://twitter.com/intent/tweet?text=I just bought <?=$pr['title']?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$pr['id'])?>&hashtags=RGStore" target="_blank">
                                                                            <span class="icon-twitter2"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$pr['id'])?>" target="_blank" onclick="googleShare()">
                                                                            <span class="icon-google-plus-square"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>

                                                    <?php endif; ?>

                                                <?php  else : ?>

                                                    <p>
                                                        <?= $pr['title']; ?>
                                                    </p>
                                                    <div  class="price_compare equal" >
                                                        <span  id="demo" class="new_price">
                                                            &pound;<?= $pr['price']; ?>
                                                        </span>
                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
                                                            <a href="#" class="compare compare_link" data-id="<?= $pr['id']; ?>">

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $pr['id']; ?>"
                                                                          class="<?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($pr['id'], $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                    </span>

                                                                <?php endif; ?>

                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell share_cell"> Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$pr['id']);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                        <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$pr['title']?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$pr['id'])?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$pr['id'])?>" target="_blank" onclick="googleShare()">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                <?php endif; ?>

                                            <?php endif; ?>

                                        </a>
                                    </div>
                                </li>

                            <?php endforeach; ?>
                            
                            <li class="placeholder"></li>
                            <li class="placeholder"></li>
                        </ul>
                    </div>
                    <div class="main_content">
                        <div class="pagination">
                            <?= $links; ?>
                        </div>
                        <div class="sort sort_down">
                            <h2>Results per page:</h2>
                            <div class="custom_select">
                                <div class="select">
                                    <div class="select_text icon-triangle-down">
                                        <p>
                                            12
                                        </p>
                                    </div>
                                    <select class="per" name="per" onchange="printer.submit()">
                                        <option value="12" <?= isset($_GET['per']) && $_GET['per'] == 12 ? ' selected' : '' ?>>
                                            12
                                        </option>
                                        <option value="24" <?= isset($_GET['per']) && $_GET['per'] == 24 ? ' selected' : '' ?>>
                                            24
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                
                <?php else : ?>

                    <div class="main_content">
                        <div class="notice">
                            <p>
                                <span class="icon-light-bulb"></span>
                                Seems like there are currently no products in this category. Please, come by later.
                            </p>
                        </div>
                    </div>

                <?php endif; ?>

                <div class="clear"></div>
            </div>
        </div>
    </form>
</section>
<?php $this->load->view('includes/footer'); ?>


