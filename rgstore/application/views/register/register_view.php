<?php $this->load->view('includes/header'); ?>
    <section>
        <div class="content_container">
            <div class="column_wrap">
                <div class="main_content_full">
                    <div class="breadcrumbs">
                        <?= $this->breadcrumb->output(); ?>
                    </div>
                </div>
                <h1 class="blue_title">Create an account</h1>
                <div class="form">
                    <form id="registerForm" action="<?= site_url('register'); ?>" method="post" class="login_form">
                        <div class="login_left">
                            <h2 class="login_title">Personal details</h2>
                            <div class="custom_select">
                                <div class="select dropdown">
                                    <div class="select_text icon-triangle-down">
                                        <p>
                                            Title
                                        </p>
                                    </div>
                                    <select id="title" name="title">
                                        <option selected disabled>Title</option>
                                        <option value="0">Ms</option>
                                        <option value="1">Mr</option>
                                    </select>
                                </div>
                            </div>

                            <?php if (form_error('title')) : ?>

                                <div class="form_error">
                                    <?= form_error('title'); ?>
                                </div>

                            <?php endif; ?>

                            <input type="text" id="fname" name="fname" placeholder="First name"
                                   value="<?= $postData['fname']; ?>"/>

                            <?php if (form_error('fname')) : ?>

                                <div class="form_error">
                                    <?= form_error('fname'); ?>
                                </div>

                            <?php endif; ?>

                            <input type="text" id="lname" name="lname" placeholder="Last name"
                                   value="<?= $postData['lname']; ?>"/>

                            <?php if (form_error('lname')) : ?>

                                <div class="form_error">
                                    <?= form_error('lname'); ?>
                                </div>

                            <?php endif; ?>

                            <label for="terms">
                                <input id="terms" type="checkbox" name="terms"/>
                                I agree with the Terms & conditions of iMark
                            </label>

                            <?php if (form_error('terms')) : ?>

                                <div class="form_error">
                                    <?= form_error('terms'); ?>
                                </div>

                            <?php endif; ?>

                            <label for="updates">
                                <input id="updates" type="checkbox" checked="" name="news">
                                I would like to be kept updated with the latest promotions from iMark by email
                            </label>
                        </div>
                        <div class="login_right">
                            <h2 class="login_title">Set up you login details</h2>
                            <input type="email" id="email" name="email" placeholder="Email"
                                   value="<?= $postData['email']; ?>"/>

                            <?php if (form_error('email')) : ?>

                                <div class="form_error">
                                    <?= form_error('email'); ?>
                                </div>

                            <?php endif; ?>

                            <input type="password" id="password" name="password" placeholder="Password"/>

                            <?php if (form_error('password')) : ?>

                                <div class="form_error">
                                    <?= form_error('password'); ?>
                                </div>

                            <?php endif; ?>

                            <input type="password" id="passconf" name="passconf" placeholder="Confirm password"/>

                            <?php if (form_error('passconf')) : ?>

                                <div class="form_error">
                                    <?= form_error('passconf'); ?>
                                </div>

                            <?php endif; ?>

                            <button id="submit" type="submit" name="register" class="create_account clear_float">Create
                                account
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('includes/footer'); ?>