<?php $this->load->view('includes/header'); ?>
    <section>
        <div class="content_container">
            <div class="column_wrap">
                <div class="form like">
                    <h2 class="login_title">Thank you for registering to iMakr!</h2>
                    <a class="link_button return" href="<?= site_url(); ?>">
                        Return to shop
                    </a>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('includes/footer'); ?>