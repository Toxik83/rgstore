<?php $this->load->view('includes/header'); ?>
    <section>
        <div class="content_container">
            <div class="column_wrap">

                <?php $this->load->view('includes/sidebar'); ?>

                <div class="main_content">
                    <div class="breadcrumbs">
                        <?= $this->breadcrumb->output(); ?>
                    </div>

                    <?php if($this->session->flashdata('success') != null ) :?>

                        <div class="notification success">
                            <span class="icon-success"></span>
                            <p class="message">
                                <?= $this->session->flashdata('success'); ?>
                            </p>
                        </div>

                    <?php endif; ?>

                    <?php if($this->session->flashdata('error_q') != null ) :?>

                        <div class="notification warning">
                            <span class="icon-times-circle"></span>
                            <p class="message">
                                <?= $this->session->flashdata('error_q'); ?>
                            </p>
                        </div>

                    <?php endif;?>

                </div>
                <div class="main_content">
                    <div class="content_wrap">
                        <div id="flexslider">
                            <ul class="slides loading">
                                <?php if (isset($filament) && $filament) : ?>

                                <?php else : ?>

                                    <?php redirect('error'); ?>

                                <?php endif; ?>
                                <?php foreach ($pics as $pic): ?>

                                    <li data-thumb="<?= base_url('upload/thumbs/' . $pic->name); ?>">
                                        <img class="zoom"
                                             src="<?= base_url('upload/images280x280/' . $pic->name); ?>"
                                             data-zoom-image="<?= base_url('upload/images500x500/' . $pic->name); ?>"/>
                                    </li>

                                <?php endforeach; ?>
                                
                            </ul>
                        </div>
                        <div class="product_details2">
                            <div class="title_wrap">
                                <h2 class="product_title">
                                    <?= $filament->title; ?>
                                </h2>
                                <div class="availability_info">
                                    <span class="availability">In Stock</span>
                                    <span>delivered within 48 hr or collect in store</span>
                                </div>
                            </div>
                            <div class="product_description">
                                <?= $filament->text; ?>
                            </div>
                            <div class="product_price">
                                <div class="price_small">

                                    <?= form_open('cart/buybyId/'); ?>

                                    <input type="hidden" name="filament_id" value="<?= $this->uri->segment(3); ?>">

                                    <?php if ($filament->new_price == '0.00') { ?>

                                        <input type="hidden" name="price" value="<?= $filament->price ?>">
                                        <span class="new_price">
                                            &pound;<?= number_format($filament->price); ?>
                                        </span>
                                        <span class="vat">(inc VAT.)</span>

                                    <?php } else { ?>

                                        <input type="hidden" name="price" value="<?= $filament->price ?>">
                                        <span class="old_price">
                                            was
                                            &pound;<?= number_format($filament->price); ?>
                                        </span>
                                        <span class="new_price">
                                            &pound;<?= number_format($filament->new_price); ?>
                                        </span>
                                        <span class="vat">(inc VAT.)</span>

                                    <?php } ?>

                                </div>
                                <div class="add_to_basket buy_filament">
                                    <div class="qty_field small_field">
                                        <input type="number" name="quantity" class="qty_num" placeholder="Qty" min="0">
                                        <div class="controls">
                                            <span class="qty_add">
                                                <span class="icon-triangle-up"></span>
                                            </span>
                                            <span class="qty_remove">
                                                <span class="icon-triangle-down2"></span>
                                            </span>
                                        </div>
                                       
                                    </div>
                                    <div class="custom_select filament_select">
                                        <div class="select">
                                            <div class="select_text icon-triangle-down">
                                                <p>
                                                    Colour
                                                </p>
                                            </div>
                                            <select name='colour' class='input color_select'>
                                                <option disabled selected="selected" value="default">Colour</option>
                                                <option value="red">red</option>
                                                <option value="green">green</option>
                                                <option value="yellow">yellow</option>
                                            </select>
                                        </div>
                                    </div>
                                
                            
                                    <?php
                                    $dataButton = array(
                                        'name' => 'addFilament',
                                        'type' => 'submit',
                                        'content' => 'Add To Basket',
                                        'class' => 'add_basket',
                                    );
                                    ?>

                                    <?= form_button($dataButton); ?>
                                    <?= form_close(); ?>

                                </div>
                            </div>
                            <div class="share_buttons">
                                <span class='st_facebook_hcount' displayText='Facebook'></span>
                                <span class='st_twitter_hcount' displayText='Tweet'></span>
                                <span class='st_googleplus_hcount' displayText='Google +'></span>
                                <span class='st__hcount' displayText=''></span>
                            </div>
                        </div>
                    </div>
     

                <?php $relatedBundle = getRelatedBundle($filament->product_id); ?>

                <?php if (!empty($relatedBundle)): ?>

                   
                    <div class="buy_product ">
                        <h3 class="productAdd_title">
                            Bundle containing <?= $filament->title; ?>
                        </h3>
                        <form method="post" action="<?= site_url('cart/buyBundle/' . $relatedBundle->product_id); ?>" id="bundle_form">

                            <?php
                            $relatedBundleData = getBundleDetails($relatedBundle->product_id);
                            $numItems = count($relatedBundleData);
                            $i=0;
                            ?>

                            <div class="promo">
                                <div class="bundle">
                                    <p>
                                        <?= $relatedBundle->bundle_name; ?>
                                    </p>
                                    <ul class="promo_products">

                                        <?php foreach ($relatedBundleData as $key => $item): ?>

                                            <li>

                                                <?php if ($item->cat_id == 1): ?>

                                                    <a href="<?= site_url('printersportfolio/printersdetail/' . $item->id); ?>">
                                                        <img src="<?= site_url('upload/images60x70/' . $item->name); ?>"/>
                                                    </a>

                                                <?php endif; ?>

                                                <?php if ($item->cat_id == 2): ?>

                                                    <a href="<?= site_url('filamentportfolio/filamentsDetail/' . $item->id); ?>">
                                                        <img src="<?= site_url('upload/images60x70/' . $item->name); ?>"/>
                                                    </a>

                                                <?php endif; ?>

                                                <?php if ($item->cat_id == 3): ?>

                                                    <a href="<?= site_url('finishing/details/' . $item->id); ?>">
                                                        <img src="<?= site_url('upload/images60x70/' . $item->name); ?>"/>
                                                    </a>

                                                <?php endif; ?>

                                                <?php if (++$i < $numItems): ?>

                                                    <span class="icon-plus"></span>

                                                <?php endif; ?>

                                            </li>

                                        <?php endforeach; ?>

                                    </ul>
                                </div>
                                <div class="discount">
                                    <div class="discount_price">
                                        <span class="old_price">
                                            You save
                                            &pound;<?= ($relatedBundle->combined_price - $relatedBundle->discount_price); ?>
                                        </span>
                                        <span class="new_price">
                                            &pound;<?= $relatedBundle->discount_price; ?>
                                        </span>
                                    </div>
                                    <ul class="bundle_buttons">
                                        <li class="cl_discount">
                                            <a onclick="$('#bundle_form').submit()" href="#" class="compare" id="buy_bundle">
                                                <span class="icon-basket2 cell"></span>
                                                <span class="cell bundle_button">Add to Basket</span>
                                            </a>
                                        </li>
                                        <li class="share_discount dropdownShare">
                                            <a href="javascript:void(0)" class="compare">
                                                <span class="icon-share cell"></span>
                                                <span class="cell bundle_button"> Share</span>
                                            </a>
                                            <ul class="dropdown-share">
                                                <li class="social_share">
                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=  site_url()?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                       <span class="icon-facebook2"></span>
                                                    </a>
                                                    <span class="icon-cancel-circle"></span>
                                                </li>
                                                <li class="social_share">
                                                    <a href="https://twitter.com/intent/tweet?text=I just saved <?=(number_format($relatedBundle->combined_price - $relatedBundle->discount_price))?> on RGStore! I'm so happy&hashtags=RGStore" target="_blank">
                                                        <span class="icon-twitter2"></span>
                                                    </a>
                                                </li>
                                                <li class="social_share">
                                                    <a href="https://plus.google.com/share?url=<?= site_url();?>" target="_blank">
                                                        <span class="icon-google-plus-square"></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>

                <?php endif; ?>
               
                <?php if (isset($relatedProducts)) { ?>

                    <ul class="column_container related_products" data-columns="4">

                        <?php foreach ($relatedProducts as $key => $relatedProd): ?>

                            <li>
                                <div class="product_details">
                                    <a href="<?= site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id); ?>">
                                        <img src="<?= base_url('upload/images155x155/' . $relatedProd->name); ?>" />

                                        <?php if ((($relatedProd->fl_date_begin) === '0000-00-00 00:00:00') &&
                                            ($relatedProd->fl_date_end) === '0000-00-00 00:00:00') { ?>

                                            <?php if ($relatedProd->new_price == '0.00') { ?>

                                                <p>
                                                    <?= $relatedProd->title; ?>
                                                </p>
                                                <div class="price_compare">
                                                    <span class="new_price equal">
                                                        &pound;<?= $relatedProd->price; ?>
                                                    </span>
                                                </div>
                                                <ul class="cl_share">
                                                    <li class="cl">
                                                        <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                            <?php if ($this->session->userdata('ids') == null): ?>

                                                                <span class="icon-justice2 cell"></span>
                                                                <span class="cell">Add to Compare list</span>

                                                            <?php else : ?>

                                                                <span id="product_<?= $relatedProd->id; ?>"
                                                                      class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                          'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                </span>
                                                                <span class="cell">
                                                                    <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                        'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                </span>

                                                            <?php endif; ?>

                                                        </a>
                                                    </li>
                                                    <li class="share dropdownShare">
                                                        <a href="javascript:void(0)" class="compare">
                                                            <span class="icon-share cell"></span>
                                                            <span class="cell"> Share</span>
                                                        </a>
                                                        <ul class="dropdown-share">
                                                            <li class="social_share">
                                                                <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                    <span class="icon-facebook2"></span>
                                                                </a>
                                                                <span class="icon-cancel-circle"></span>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                    <span class="icon-twitter2"></span>
                                                                </a>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://plus.google.com/share?url=<?= site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>" target="_blank">
                                                                    <span class="icon-google-plus-square"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>

                                            <?php } else { ?>

                                                <p>
                                                    <?= $relatedProd->title; ?>
                                                </p>
                                                <div class="price_compare">
                                                    <span class="old_price">
                                                        was
                                                        <span class="op_value">
                                                            &pound;<?= $relatedProd->price; ?>
                                                        </span>
                                                    </span>
                                                    <span class="new_price" >
                                                        &pound;<?= $relatedProd->new_price; ?>
                                                    </span>
                                                </div>
                                                <ul class="cl_share">
                                                    <li class="cl">
                                                        <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                            <?php if ($this->session->userdata('ids') == null): ?>

                                                                <span class="icon-justice2 cell"></span>
                                                                <span class="cell">Add to Compare list</span>

                                                            <?php else : ?>

                                                                <span id="product_<?= $relatedProd->id; ?>"
                                                                      class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                          'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                </span>
                                                                <span class="cell">
                                                                    <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                        'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                </span>

                                                            <?php endif; ?>

                                                        </a>
                                                    </li>
                                                    <li class="share dropdownShare">
                                                        <a href="javascript:void(0)" class="compare">
                                                            <span class="icon-share cell"></span>
                                                            <span class="cell"> Share</span>
                                                        </a>
                                                        <ul class="dropdown-share">
                                                            <li class="social_share">
                                                                <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                    <span class="icon-facebook2"></span>
                                                                </a>
                                                                <span class="icon-cancel-circle"></span>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                    <span class="icon-twitter2"></span>
                                                                </a>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://plus.google.com/share?url=<?= site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>" target="_blank">
                                                                    <span class="icon-google-plus-square"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>

                                            <?php } ?>

                                        <?php } ?>

                                        <?php if ((($relatedProd->fl_date_begin) !== '0000-00-00 00:00:00') &&
                                            ($relatedProd->fl_date_end) !== '0000-00-00 00:00:00') { ?>

                                            <?php if (($current_time >= strtotime($relatedProd->fl_date_begin)) &&
                                                ($current_time <= strtotime($relatedProd->fl_date_end))) { ?>

                                                <?php if (((strtotime($relatedProd->fl_date_end) - $current_time) / (60 * 60 * 24)) <= 1 &&
                                                    ((strtotime($relatedProd->fl_date_end) - $current_time) / (60 * 60 * 24)) > 0) {?>

                                                    <div class="promo_tag">
                                                        <div class="flash_sale expiring arrow_box">
                                                            Flash Sale
                                                            <span class="duration">1 day left</span>
                                                        </div>
                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
                                                            <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $relatedProd->id; ?>"
                                                                          class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                    </span>

                                                                <?php endif; ?>

                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell"> Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                        <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>" target="_blank">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                <?php } else { ?>

                                                    <p>
                                                        <?= $relatedProd->title; ?>
                                                    </p>
                                                    <div class="price_compare">
                                                        <span class="old_price">
                                                            was
                                                            <span class="op_value">
                                                                &pound;<?= $relatedProd->price; ?>
                                                            </span>
                                                        </span>
                                                        <span class="new_price">
                                                            &pound;<?= $relatedProd->new_price; ?>
                                                        </span>
                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
                                                            <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $relatedProd->id; ?>"
                                                                          class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                    </span>

                                                                <?php endif; ?>

                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell"> Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                        <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>" target="_blank">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                <?php } ?>

                                            <?php } ?>

                                            <?php if ((($relatedProd->fl_date_begin) !== '0000-00-00 00:00:00') && ($relatedProd->fl_date_end) !== '0000-00-00 00:00:00') { ?>

                                                <?php if (($current_time >= strtotime($relatedProd->fl_date_begin)) && ($current_time <= strtotime($relatedProd->fl_date_end))) { ?>

                                                    <?php if (((strtotime($relatedProd->fl_date_end) - $current_time) / (60 * 60 * 24)) <= 1 &&
                                                            ((strtotime($relatedProd->fl_date_end) - $current_time) / (60 * 60 * 24)) > 0) {
                                                        ?>
                                                        <div class="promo_tag">
                                                            <div class="flash_sale expiring arrow_box">
                                                                Flash Sale
                                                                <span class="duration">1 day left</span>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            <?= $relatedProd->title; ?>
                                                        </p>
                                                        <div class="price_compare">
                                                            <span class="old_price">
                                                                was
                                                                <span class="op_value">
                                                                    &pound;<?= $relatedProd->price; ?>
                                                                </span>
                                                            </span>
                                                            <span class="new_price">
                                                                &pound;<?= $relatedProd->new_price; ?>
                                                            </span>
                                                        </div>
                                                        <ul class="cl_share">
                                                            <li class="cl">
                                                                <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                                    <?php if ($this->session->userdata('ids') == null): ?>

                                                                        <span class="icon-justice2 cell"></span>
                                                                        <span class="cell">Add to Compare list</span>

                                                                    <?php else : ?>

                                                                        <span id="product_<?= $relatedProd->id; ?>"
                                                                              class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                                  'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                        </span>
                                                                        <span class="cell">
                                                                            <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                                'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                        </span>

                                                                    <?php endif; ?>

                                                                </a>
                                                            </li>
                                                            <li class="share dropdownShare">
                                                                <a href="javascript:void(0)" class="compare">
                                                                    <span class="icon-share cell"></span>
                                                                    <span class="cell"> Share</span>
                                                                </a>
                                                                <ul class="dropdown-share">
                                                                    <li class="social_share">
                                                                        <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                            <span class="icon-facebook2"></span>
                                                                        </a>
                                                                        <span class="icon-cancel-circle"></span>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                            <span class="icon-twitter2"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://plus.google.com/share?url=<?= site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>" target="_blank">
                                                                            <span class="icon-google-plus-square"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    <?php } else { ?>
                                                        <div class="flash_sale arrow_box">
                                                            Flash Sale
                                                            <span class="duration">
                                                                <?= date_format(date_create($relatedProd->fl_date_begin), "d/m"); ?> -
                                                                <?= date_format(date_create($relatedProd->fl_date_end), "d/m"); ?>
                                                            </span>
                                                        </div>
                                                        <p>
                                                            <?= $relatedProd->title; ?>
                                                        </p>
                                                        <div class="price_compare">
                                                            <span class="old_price">
                                                                was
                                                                <span class="op_value">
                                                                    &pound;<?= $relatedProd->price; ?>
                                                                </span>
                                                            </span>
                                                            <span class="new_price">
                                                                &pound;<?= $relatedProd->new_price; ?>
                                                            </span>
                                                        </div>
                                                        <ul class="cl_share">
                                                            <li class="cl">
                                                                <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">
                                                                    <?php if ($this->session->userdata('ids') == null): ?>
                                                                        <span class="icon-justice2 cell"></span>
                                                                        <span class="cell">Add to Compare list</span>
                                                                    <?php else : ?>
                                                                        <span id="product_<?= $relatedProd->id; ?>" class="<?php echo (in_array($relatedProd->id, $this->session->userdata('ids'))) ? 'icon-justice3' : 'icon-justice2'; ?> cell"></span>
                                                                        <span class="cell"><?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ? 'Remove from Compare List' : 'Add to Compare List'; ?></span>
                                                                    <?php endif; ?>
                                                                </a>
                                                            </li>
                                                            <li class="share dropdownShare">
                                                                <a href="javascript:void(0)" class="compare">
                                                                    <span class="icon-share cell"></span>
                                                                    <span class="cell"> Share</span>
                                                                </a>
                                                                <ul class="dropdown-share">
                                                                    <li class="social_share">
                                                                        <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                            <span class="icon-facebook2"></span>
                                                                        </a>
                                                                        <span class="icon-cancel-circle"></span>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                            <span class="icon-twitter2"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://plus.google.com/share?url=<?= site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>" target="_blank">
                                                                            <span class="icon-google-plus-square"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    <?php } ?>

                                                <?php } else { ?>

                                                    <div class="flash_sale arrow_box">
                                                        Flash Sale
                                                        <span class="duration">
                                                            <?= date_format(date_create($relatedProd->fl_date_begin), "d/m"); ?> -
                                                            <?= date_format(date_create($relatedProd->fl_date_end), "d/m"); ?>
                                                        </span>
                                                    </div>
                                                    <p>
                                                        <?= $relatedProd->title; ?>
                                                    </p>
                                                    <div class="price_compare">
                                                        <span class="old_price">
                                                            was
                                                            <span class="op_value">
                                                                &pound;<?= $relatedProd->price; ?>
                                                            </span>
                                                        </span>
                                                        <span class="new_price">
                                                            &pound;<?= $relatedProd->new_price; ?>
                                                        </span>
                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
                                                            <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $relatedProd->id; ?>"
                                                                          class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                    </span>

                                                                <?php endif; ?>

                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell"> Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                        <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>" target="_blank">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                <?php } ?>

                                            <?php } else { ?>

                                                <p>
                                                    <?= $relatedProd->title; ?>
                                                </p>
                                                <div  class="price_compare equal" >
                                                    <span  id="demo" class="new_price">
                                                        &pound;<?= $relatedProd->price; ?>
                                                    </span>
                                                </div>
                                                <ul class="cl_share">
                                                    <li class="cl">
                                                        <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                            <?php if ($this->session->userdata('ids') == null): ?>

                                                                <span class="icon-justice2 cell"></span>
                                                                <span class="cell">Add to Compare list</span>

                                                            <?php else : ?>

                                                                <span id="product_<?= $relatedProd->id; ?>"
                                                                      class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                          'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                </span>
                                                                <span class="cell">
                                                                    <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                        'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                </span>

                                                            <?php endif; ?>

                                                        </a>
                                                    </li>
                                                    <li class="share dropdownShare">
                                                        <a href="javascript:void(0)" class="compare">
                                                            <span class="icon-share cell"></span>
                                                            <span class="cell"> Share</span>
                                                        </a>
                                                        <ul class="dropdown-share">
                                                            <li class="social_share">
                                                                <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                    <span class="icon-facebook2"></span>
                                                                </a>
                                                                <span class="icon-cancel-circle"></span>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                    <span class="icon-twitter2"></span>
                                                                </a>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://plus.google.com/share?url=<?= site_url('filamentportfolio/filamentsDetail/'.$relatedProd->id);?>" target="_blank">
                                                                    <span class="icon-google-plus-square"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>

                                            <?php } ?>

                                        <?php } ?>

                                    </a>
                                </div>
                            </li>

                        <?php endforeach; ?>

                        <li class="placeholder"></li>
                        <li class="placeholder"></li>
                        </ul>
                     </div>

                    <?php } // END IF of Related Products ?>

                <div class="clear"></div>
             </div>
        </div>
    </section>
<?php $this->load->view('includes/footer'); ?>