<?php $this->load->view('includes/header'); ?>
<section>
    <form id="finishing" method="get" action="finishing/search">
        <div class="content_container">
            <div class="column_wrap ">

                <?php if (isset($finishings) && $finishings) : ?>

                    <?php $this->load->view('includes/sidebar'); ?>

                    <div class="main_content">
                        <div class="breadcrumbs">
                            <?= $this->breadcrumb->output(); ?>
                        </div>
                    </div>
                    <div class="main_content select_roll">
                        <div class="sort sort_left">
                            <h2>Sort Results by:</h2>
                            <div class="custom_select">
                                <div class="select">
                                    <div class="select_text icon-triangle-down">
                                        <p>
                                            --
                                        </p>
                                    </div>
                                    <select  class="sort_result" name="orderBy" onchange="finishing.submit()">
                                        <option value="0" > -- </option>
                                        <option value="1" <?= isset($_GET['orderBy']) && $_GET['orderBy'] == 1 ? ' selected' : '' ?>>
                                            Price - Low to High
                                        </option>
                                        <option value="2" <?= isset($_GET['orderBy']) && $_GET['orderBy'] == 2 ? ' selected' : '' ?>>
                                            Price - High to Low
                                        </option>
                                        <option value="3" <?= isset($_GET['orderBy']) && $_GET['orderBy'] == 3 ? ' selected' : '' ?>>
                                            Product Name - A to Z
                                        </option>
                                        <option value="4" <?= isset($_GET['orderBy']) && $_GET['orderBy'] == 4 ? ' selected' : '' ?>>
                                            Product Name - Z to A
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="sort sort_right">
                            <h2>Results per page:</h2>
                            <div class="custom_select">
                                <div class="select">
                                    <div class="select_text icon-triangle-down">
                                        <p>
                                            12
                                        </p>
                                    </div>
                                    <select class="per" onchange="finishing.submit()">
                                        <option value="12" <?= isset($_GET['per']) && $_GET['per'] == 12 ? ' selected' : '' ?>>
                                            12
                                        </option>
                                        <option value="24" <?= isset($_GET['per']) && $_GET['per'] == 24 ? ' selected' : '' ?>>
                                            24
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main_content">
                        <ul class="column_container listing" data-columns="4">

                            <?php foreach ($finishings as $key => $value): ?>

                                <li>
                                    <div class="product_details">
                                        <a href="<?= site_url('finishing/details/'.$value['id']); ?>">
                                            <img src="<?= base_url('upload/images155x155/' . $value['name']); ?>" />

                                            <?php if ($value['flag'] == 1) : ?>

                                                <div class="bestseller arrow_box">
                                                    <span class="block">Best</span>
                                                    <span class="block">Seller</span>
                                                </div>

                                            <?php endif; ?>

                                            <?php if ((($value['fl_date_begin']) === '0000-00-00 00:00:00') &&
                                                ($value['fl_date_end']) === '0000-00-00 00:00:00') { ?>

                                                <p>
                                                    <?= $value['title']; ?>
                                                </p>
                                                <div class="price_compare">

                                                    <?php if ($value['new_price'] == '0.00') { ?>

                                                        <span class="new_price equal">
                                                            &pound; <?= number_format($value['price']); ?>
                                                        </span>

                                                    <?php } else { ?>

                                                        <span class="old_price">
                                                            was
                                                            <span class="op_value">
                                                                <?= number_format($value['price']); ?>
                                                            </span>
                                                        </span>
                                                        <span class="new_price">
                                                            &pound; <?= number_format($value['new_price']); ?>
                                                        </span>

                                                    <?php } ?>

                                                </div>
                                                <ul class="cl_share">
                                                    <li class="cl">

                                                        <a href="#" class="compare compare_link" data-id="<?= $value['id']; ?>">

                                                            <?php if ($this->session->userdata('ids') == null): ?>

                                                                <span class="icon-justice2 cell"></span>
                                                                <span class="cell">Add to Compare list</span>

                                                            <?php else : ?>

                                                                <span id="product_<?= $value['id']; ?>"
                                                                      class="<?= (in_array($value['id'], $this->session->userdata('ids'))) ?
                                                                          'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                </span>
                                                                <span class="cell">
                                                                    <?= (in_array($value['id'], $this->session->userdata('ids'))) ?
                                                                        'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                </span>

                                                            <?php endif; ?>

                                                        </a>
                                                    </li>
                                                    <li class="share dropdownShare">
                                                        <a href="javascript:void(0)" class="compare">
                                                            <span class="icon-share cell"></span>
                                                            <span class="cell share_cell"> Share</span>
                                                        </a>
                                                        <ul class="dropdown-share">
                                                            <li class="social_share">
                                                                <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('finishing/details/'.$value['id']);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                   <span class="icon-facebook2"></span>
                                                                </a>
                                                                <span class="icon-cancel-circle"></span>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://twitter.com/intent/tweet?text=I just bought <?=$value['title']?> from RGStore and it's awesome&url=<?=site_url('finishing/details/'.$value['id'])?>&hashtags=RGStore" target="_blank">
                                                                    <span class="icon-twitter2"></span>
                                                                </a>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://plus.google.com/share?url=<?= site_url('finishing/details/'.$value['id']);?>" target="_blank">
                                                                    <span class="icon-google-plus-square"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>

                                            <?php } ?>

                                            <?php if ((($value['fl_date_begin']) !== '0000-00-00 00:00:00') &&
                                                ($value['fl_date_end']) !== '0000-00-00 00:00:00') { ?>

                                                <?php if (($current_time >= strtotime($value['fl_date_begin'])) &&
                                                    ($current_time <= strtotime($value['fl_date_end']))) { ?>

                                                    <?php if (((strtotime($value['fl_date_end']) - $current_time) / (60 * 60 * 24)) <= 1 &&
                                                    ((strtotime($value['fl_date_end']) - $current_time) / (60 * 60 * 24)) > 0)  { ?>

                                                        <div class="promo_tag">
                                                            <div class="flash_sale expiring arrow_box">
                                                                Flash Sale
                                                                <span class="duration">1 day left</span>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            <?= $value['title']; ?>
                                                        </p>
                                                        <div class="price_compare">
                                                            <span class="old_price">
                                                                was
                                                                <span class="op_value">
                                                                    &pound;<?= $value['price']; ?>
                                                                </span>
                                                            </span>
                                                            <span class="new_price">
                                                                &pound;<?= $value['new_price']; ?>
                                                            </span>
                                                        </div>
                                                        <ul class="cl_share">
                                                            <li class="cl">
                                                                <a href="#" class="compare compare_link" data-id="<?= $value['id']; ?>">

                                                                    <?php if ($this->session->userdata('ids') == null): ?>

                                                                        <span class="icon-justice2 cell"></span>
                                                                        <span class="cell">Add to Compare list</span>

                                                                    <?php else : ?>

                                                                        <span id="product_<?= $value['id']; ?>"
                                                                              class="<?= (in_array($value['id'], $this->session->userdata('ids'))) ?
                                                                                  'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                        </span>
                                                                        <span class="cell">
                                                                            <?= (in_array($value['id'], $this->session->userdata('ids'))) ?
                                                                                'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                        </span>

                                                                    <?php endif; ?>

                                                                </a>
                                                            </li>
                                                            <li class="share dropdownShare">
                                                                <a href="javascript:void(0)" class="compare">
                                                                    <span class="icon-share cell"></span>
                                                                    <span class="cell share_cell"> Share</span>
                                                                </a>
                                                                <ul class="dropdown-share">
                                                                    <li class="social_share">
                                                                        <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('finishing/details/'.$value['id']);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                            <span class="icon-facebook2"></span>
                                                                        </a>
                                                                        <span class="icon-cancel-circle"></span>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://twitter.com/intent/tweet?text=I just bought <?=$value['title']?> from RGStore and it's awesome&url=<?=site_url('finishing/details/'.$value['id'])?>&hashtags=RGStore" target="_blank">
                                                                            <span class="icon-twitter2"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://plus.google.com/share?url=<?= site_url('finishing/details/'.$value['id']);?>" target="_blank">
                                                                            <span class="icon-google-plus-square"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>

                                                    <?php } else { ?>

                                                        <div class="flash_sale arrow_box">
                                                            Flash Sale
                                                            <span class="duration">
                                                                <?= date_format(date_create($value['fl_date_begin']), "m/d"); ?> -
                                                                <?= date_format(date_create($value['fl_date_end']), "m/d"); ?>
                                                            </span>
                                                        </div>
                                                        <p>
                                                            <?= $value['title']; ?>
                                                        </p>
                                                        <div class="price_compare">
                                                            <span class="old_price">
                                                                was
                                                                <span class="op_value">
                                                                    &pound;<?= $value['price']; ?>
                                                                </span>
                                                            </span>
                                                            <span class="new_price">
                                                                &pound;<?= $value['new_price']; ?>
                                                            </span>
                                                        </div>
                                                        <ul class="cl_share">
                                                            <li class="cl">
                                                                <a href="#" class="compare compare_link" data-id="<?= $value['id']; ?>">

                                                                    <?php if ($this->session->userdata('ids') == null): ?>

                                                                        <span class="icon-justice2 cell"></span>
                                                                        <span class="cell">Add to Compare list</span>

                                                                    <?php else : ?>

                                                                        <span id="product_<?= $value['id']; ?>"
                                                                              class="<?= (in_array($value['id'], $this->session->userdata('ids'))) ?
                                                                                  'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                        </span>
                                                                        <span class="cell">
                                                                            <?= (in_array($value['id'], $this->session->userdata('ids'))) ?
                                                                                'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                        </span>

                                                                    <?php endif; ?>

                                                                </a>
                                                            </li>
                                                            <li class="share dropdownShare">
                                                                <a href="javascript:void(0)" class="compare">
                                                                    <span class="icon-share cell"></span>
                                                                    <span class="cell share_cell"> Share</span>
                                                                </a>
                                                                <ul class="dropdown-share">
                                                                    <li class="social_share">
                                                                        <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('finishing/details/'.$value['id']);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                            <span class="icon-facebook2"></span>
                                                                        </a>
                                                                        <span class="icon-cancel-circle"></span>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://twitter.com/intent/tweet?text=I just bought <?=$value['title']?> from RGStore and it's awesome&url=<?=site_url('finishing/details/'.$value['id'])?>&hashtags=RGStore" target="_blank">
                                                                            <span class="icon-twitter2"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://plus.google.com/share?url=<?= site_url('finishing/details/'.$value['id']);?>" target="_blank">
                                                                            <span class="icon-google-plus-square"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>

                                                    <?php } ?>

                                                <?php } else { ?>

                                                    <p>
                                                        <?= $value['title']; ?>
                                                    </p>
                                                    <div  class="price_compare equal" >
                                                        <span  id="demo" class="new_price">
                                                            &pound;<?= $value['price']; ?>
                                                        </span>
                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
                                                            <a href="#" class="compare compare_link" data-id="<?= $value['id']; ?>">

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $value['id']; ?>"
                                                                          class="<?= (in_array($value['id'], $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($value['id'], $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                    </span>

                                                                <?php endif; ?>

                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell share_cell"> Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('finishing/details/'.$value['id']);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                        <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$value['title']?> from RGStore and it's awesome&url=<?=site_url('finishing/details/'.$value['id'])?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= site_url('finishing/details/'.$value['id']);?>" target="_blank">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                <?php } ?>

                                            <?php } ?>

                                        </a>
                                    </div>
                                </li>

                            <?php endforeach; ?>

                            <li class="placeholder"></li>
                            <li class="placeholder"></li>
                        </ul>
                    </div>
                    <div class="main_content">
                        <div class="pagination">
                            <?= $links; ?>
                        </div>
                        <div class="sort sort_down">
                            <h2>Results per page:</h2>
                            <div class="custom_select">
                                <div class="select">
                                    <div class="select_text icon-triangle-down">
                                        <p>
                                            12
                                        </p>
                                    </div>
                                    <select class="per" onchange="finishing.submit()">
                                        <option value="12" <?= isset($_GET['per']) && $_GET['per'] == 12 ? ' selected' : '' ?>>
                                            12
                                        </option>
                                        <option value="24" <?= isset($_GET['per']) && $_GET['per'] == 24 ? ' selected' : '' ?>>
                                            24
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php else : ?>

                    <div class="main_content">
                        <div class="notice">
                            <p>
                                <span class="icon-light-bulb"></span>
                                Seems like there are currently no products in this category. Please, come by later.
                            </p>
                        </div>
                    </div>

                <?php endif; ?>

                <div class="clear"></div>
            </div>
        </div>
    </form>
</section>
<?php $this->load->view('includes/footer'); ?>


