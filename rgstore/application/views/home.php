<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container meh">
        <div class="column_wrap">

            <?php $this->load->view('includes/sidebar'); ?>

            <div class="main_content home">

                <?php if ($this->session->flashdata('success') != null) : ?>

                    <div class="notification success">
                        <span class="icon-success"></span>
                        <p class="message">
                            <?= $this->session->flashdata('success'); ?>
                        </p>
                    </div>

                <?php endif; ?>

                <div class="flexslider">
                    <ul class="slides loading">

                        <?php foreach ($sliderData as $key => $slide) : ?>

                            <li class="slider_background"
                                style="background-image: url( <?= base_url('upload/home-slider/' . $slide->image); ?>);">
                                <div class="slider_details">
                                    <h1 class="slider_title">
                                        <?= $slide->title; ?>
                                    </h1>
                                    <p>
                                        <?= substr($slide->description, 0, 40) . '...'; ?>
                                    </p>
                                    <a class="link_button slider_button"
                                       href="<?= $slide->url; ?>" <?= ($slide->internal == 0) ? 'target="_blank"' : ''; ?>>
                                        View specifications / Buy
                                    </a>
                                </div>
                            </li>

                        <?php endforeach; ?>

                    </ul>
                    <div class="flexslider-controls">
                        <ol class="flex-control-nav">
                            <li>1</li>
                            <li>2</li>
                            <li>3</li>
                            <li>4</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="main_content">
                <div class="tab_box">
                    <ul class="tabs">
                        <li data-tabs="1" class="active_tab tab tab_deals">
                            <span>Best Sellers</span>
                        </li>
                        <li data-tabs="2" class="tab tab_deals">
                            <span>Hot Deals</span>
                        </li>
                    </ul>
                    <div data-tabs="1" class="panel active_panel">
                         <a href="<?= site_url('/sellers/products'); ?>" class="see_all">
                            See All Bestsellers &raquo;
                        </a>
                        <div class="clear"></div>

                        <?php if (empty($bestsellers)) : ?>

                            <div class="notice">
                                <p>
                                    <span class="icon-light-bulb"></span>
                                    There are currently no Best Selling products. Please, come by later.
                                </p>
                            </div>

                        <?php endif; ?>

                        <ul class="column_container" data-columns="4">

                            <?php foreach ($bestsellers as $bestsellers => $best): ?>

                                <li>
                                    <div class="product_details">

                                        <?php if ($best->cat_id == 1): ?>

                                            <a href="<?= site_url('printersportfolio/printersdetail/'.$best->product_id); ?>">
                                            <img src="<?= base_url('upload/images155x155/' . $best->name); ?>"/>
                                            <?php $bestUrlShare = site_url('printersportfolio/printersdetail/'.$best->product_id);?>
                                        <?php endif; ?>

                                            <?php if ($best->cat_id == 2): ?>

                                                <a href="<?= site_url('filamentportfolio/filamentsDetail/'.$best->product_id); ?>">
                                                <img src="<?= base_url('upload/images155x155/' . $best->name); ?>"/>
                                                <?php $bestUrlShare = site_url('filamentportfolio/filamentsDetail/'.$best->product_id);?>
                                            <?php endif; ?>

                                                <?php if ($best->cat_id == 3): ?>

                                                    <a href="<?= site_url('finishing/details/'.$best->product_id); ?>">
                                                    <img src="<?= base_url('upload/images155x155/' . $best->name); ?>"/>
                                                    <?php $bestUrlShare = site_url('finishing/details/'.$best->product_id);?>
                                                <?php endif; ?>

                                                    <div class="bestseller arrow_box">
                                                        <span class="block">Best</span>
                                                        <span class="block">Seller</span>
                                                    </div>
                                                    <p>
                                                        <?= $best->title; ?>
                                                    </p>
                                                    <div class="price_compare">

                                                        <?php if ($best->new_price == '0.00') : ?>

                                                            <span class="new_price equal">
                                                                &pound;
                                                                <?= $best->price; ?>
                                                            </span>

                                                        <?php  else : ?>

                                                            <span class="old_price">
                                                                was
                                                                <span class="op_value">
                                                                    <?= $best->price; ?>
                                                                </span>
                                                            </span>
                                                            <span class="new_price">
                                                                &pound;
                                                                <?= $best->new_price; ?>
                                                            </span>

                                                        <?php endif; ?>

                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
                                                            <a href="#" class="compare compare_link" data-id="<?= $best->product_id; ?>">

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $best->product_id; ?>"
                                                                          class="<?= (in_array($best->product_id, $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($best->product_id, $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                    </span>

                                                                <?php endif; ?>

                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell share_cell"> Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=$bestUrlShare;?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                        <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$best->title?> from RGStore and it's awesome&url=<?=$bestUrlShare?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= $bestUrlShare;?>" target="_blank">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </a>
                                        </a>
                                    </div>
                                </li>

                            <?php endforeach; ?>

                            <li class="placeholder"></li>
                            <li class="placeholder"></li>
                        </ul>
                    </div>
                    <div data-tabs="2" class="panel">

                        <a href="<?= site_url('/deals/products'); ?>" class="see_all">

                            See All Hot Deals &raquo;
                        </a>
                        <div class="clear"></div>

                        <?php if (empty($hotdeals)) : ?>

                            <div class="notice">
                                <p>
                                    <span class="icon-lightbulb"></span>
                                    There are currently no Hot Deals. Please, come by later.
                                </p>
                            </div>

                        <?php endif; ?>

                        <ul class="column_container hot_deals" data-columns="4">

                            <?php foreach ($hotdeals as $hotdeal => $pr): ?>

                                <li>
                                    <div class="product_details">

                                        <?php if ($pr->cat_id == 1): ?>

                                            <a href="<?= site_url('printersportfolio/printersdetail/'.$pr->product_id); ?>">
                                            <img src="<?= base_url('upload/images155x155/' . $pr->name); ?>"/>
                                            <?php $bestUrlShare = site_url('printersportfolio/printersdetail/'.$pr->product_id);?>
                                        <?php endif; ?>

                                            <?php if ($pr->cat_id == 2): ?>

                                                <a href="<?= site_url('filamentportfolio/filamentsDetail/'.$pr->product_id); ?>">
                                                <img src="<?= base_url('upload/images155x155/' . $pr->name); ?>"/>
                                                <?php $bestUrlShare = site_url('filamentportfolio/filamentsDetail/'.$pr->product_id);?>
                                            <?php endif; ?>

                                                <?php if ($pr->cat_id == 3): ?>

                                                    <a href="<?= site_url('finishing/details/'.$pr->product_id); ?>">
                                                    <img src="<?= base_url('upload/images155x155/' . $pr->name); ?>"/>
                                                    <?php $bestUrlShare = site_url('finishing/details/'.$pr->product_id);?>
                                                <?php endif; ?>

                                                    <?php if (((strtotime($pr->fl_date_end) - $current_time) / (60 * 60 * 24)) <= 1 &&
                                                        abs((strtotime($pr->fl_date_end) - $current_time) / (60 * 60 * 24)) >= 0 ) : ?>

                                                        <div class="promo_tag">
                                                            <div class="flash_sale expiring arrow_box">
                                                                Flash Sale
                                                                <span class="duration">1 day left</span>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            <?= $pr->title; ?>
                                                        </p>
                                                        <div class="price_compare">
                                                            <span class="old_price">
                                                                was
                                                                <span class="op_value">
                                                                    <?= number_format($pr->price); ?>
                                                                </span>
                                                            </span>
                                                            <span class="new_price">
                                                                &pound;
                                                                <?= number_format($pr->new_price); ?>
                                                            </span>
                                                        </div>
                                                        <ul class="cl_share">
                                                            <li class="cl">
                                                                <a href="#" class="compare compare_link" data-id="<?= $pr->product_id; ?>">

                                                                    <?php if ($this->session->userdata('ids') == null): ?>

                                                                        <span class="icon-justice2 cell"></span>
                                                                        <span class="cell">Add to Compare list</span>

                                                                    <?php else : ?>

                                                                        <span id="product_<?= $pr->product_id; ?>"
                                                                              class="<?= (in_array($pr->product_id, $this->session->userdata('ids'))) ?
                                                                                  'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                        </span>
                                                                        <span class="cell">
                                                                            <?= (in_array($pr->product_id, $this->session->userdata('ids'))) ?
                                                                                'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                        </span>

                                                                    <?php endif; ?>

                                                                </a>
                                                            </li>
                                                            <li class="share dropdownShare">
                                                                <a href="javascript:void(0)" class="compare">
                                                                    <span class="icon-share cell"></span>
                                                                    <span class="cell share_cell"> Share</span>
                                                                </a>
                                                                <ul class="dropdown-share">
                                                                    <li class="social_share">
                                                                        <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=$bestUrlShare;?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                            <span class="icon-facebook2"></span>
                                                                        </a>
                                                                        <span class="icon-cancel-circle"></span>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://twitter.com/intent/tweet?text=I just bought <?=$pr->title?> from RGStore and it's awesome&url=<?=$bestUrlShare;?>&hashtags=RGStore" target="_blank">
                                                                            <span class="icon-twitter2"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://plus.google.com/share?url=<?= $bestUrlShare;?>" target="_blank">
                                                                            <span class="icon-google-plus-square"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>

                                                    <?php  else : ?>

                                                        <div class="flash_sale arrow_box">
                                                            Flash Sale
                                                            <span class="duration">
                                                                <?= date_format(date_create($pr->fl_date_begin), "m/d"); ?> -
                                                                <?= date_format(date_create($pr->fl_date_end), "m/d"); ?>
                                                            </span>
                                                        </div>
                                                        <p>
                                                            <?= $pr->title; ?>
                                                        </p>
                                                        <div class="price_compare">
                                                            <span class="old_price">
                                                                was
                                                                <span class="op_value">
                                                                    <?= number_format($pr->price); ?>
                                                                </span>
                                                            </span>
                                                            <span class="new_price">
                                                                &pound;
                                                                <?= number_format($pr->new_price); ?>
                                                            </span>
                                                        </div>
                                                        <ul class="cl_share">
                                                            <li class="cl">
                                                                <a href="#" class="compare compare_link" data-id="<?= $pr->product_id; ?>">

                                                                    <?php if ($this->session->userdata('ids') == null): ?>

                                                                        <span class="icon-justice2 cell"></span>
                                                                        <span class="cell">Add to Compare list</span>

                                                                    <?php else : ?>

                                                                        <span id="product_<?= $pr->product_id; ?>"
                                                                              class="<?= (in_array($pr->product_id, $this->session->userdata('ids'))) ?
                                                                                  'icon-justice3' : 'icon-justice2' ;?> cell">
                                                                        </span>
                                                                        <span class="cell">
                                                                            <?= (in_array($pr->product_id, $this->session->userdata('ids'))) ?
                                                                                'Remove from Compare List' : 'Add to Compare List' ;?>
                                                                        </span>

                                                                    <?php endif; ?>

                                                                </a>
                                                            </li>
                                                            <li class="share dropdownShare">
                                                                <a href="javascript:void(0)" class="compare">
                                                                    <span class="icon-share cell"></span>
                                                                    <span class="cell share_cell"> Share</span>
                                                                </a>
                                                                <ul class="dropdown-share">
                                                                    <li class="social_share">
                                                                        <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=$bestUrlShare;?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                            <span class="icon-facebook2"></span>
                                                                        </a>
                                                                        <span class="icon-cancel-circle"></span>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://twitter.com/intent/tweet?text=I just bought <?=$pr->title?> from RGStore and it's awesome&url=<?=$bestUrlShare;?>&hashtags=RGStore" target="_blank">
                                                                            <span class="icon-twitter2"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="social_share">
                                                                        <a href="https://plus.google.com/share?url=<?= $bestUrlShare;?>" target="_blank">
                                                                            <span class="icon-google-plus-square"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>

                                                    <?php endif; ?>

                                                </a>
                                            </a>
                                        </a>
                                    </div>
                                </li>

                            <?php endforeach; ?>

                            <li class="placeholder"></li>
                            <li class="placeholder"></li>
                        </ul>

                        <?php if (isset($bundle)): ?>

                            <form method="post" action="<?= site_url('cart/buyBundle/' . $bundle->product_id); ?>"
                                  id="bundle_form">

                                <?php
                                $bundleData = getBundleDetails($bundle->product_id);                                
                                $numItems = count($bundleData);
                                $i = 0; ?>

                                <div class="promo">
                                    <div class="bundle">
                                        <p>
                                            <?= $bundle->bundle_name; ?>
                                        </p>
                                        <ul class="promo_products">

                                            <?php foreach ($bundleData as $key => $item): ?>

                                                <li>

                                                    <?php if ($item->cat_id == 1): ?>

                                                        <a href="<?= site_url('printersportfolio/printersdetail/' . $item->id); ?>">
                                                            <img src="<?= site_url('upload/images60x70/' . $item->name); ?>"/>
                                                        </a>

                                                    <?php endif; ?>

                                                    <?php if ($item->cat_id == 2): ?>

                                                        <a href="<?= site_url('filamentportfolio/filamentsdetail/' . $item->id); ?>">
                                                            <img src="<?= site_url('upload/images60x70/' . $item->name); ?>"/>
                                                        </a>

                                                    <?php endif; ?>

                                                    <?php if ($item->cat_id == 3): ?>

                                                        <a href="<?= site_url('finishing/details/' . $item->id); ?>">
                                                            <img  src="<?= site_url('upload/images60x70/' . $item->name); ?>"/>
                                                        </a>

                                                    <?php endif; ?>

                                                    <?php if (++$i < $numItems): ?>

                                                        <span class="icon-plus"></span>

                                                    <?php endif; ?>

                                                </li>

                                            <?php endforeach; ?>

                                        </ul>
                                    </div>
                                    <div class="discount">
                                        <div class="discount_price">
                                            <span class="old_price">
                                                You save
                                                &pound;<?= (number_format($bundle->combined_price - $bundle->discount_price)); ?>
                                            </span>
                                            <span class="new_price">
                                                &pound;<?= number_format($bundle->discount_price); ?>
                                            </span>
                                        </div>
                                        <ul class="bundle_buttons">
                                            <li class="cl_discount">
                                                <a onclick="$('#bundle_form').submit()" href="#" class="compare" id="buy_bundle">
                                                    <span class="icon-basket2 cell"></span>
                                                    <span class="cell bundle_button">Add to Basket</span>
                                                </a>
                                            </li>
                                            <li class="share_discount dropdownShare">
                                                <a href="javascript:void(0)" class="compare">
                                                    <span class="icon-share cell"></span>
                                                    <span class="cell bundle_button"> Share</span>
                                                </a>
                                                <ul class="dropdown-share">
                                                    <li class="social_share">
                                                        <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=  site_url()?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                           <span class="icon-facebook2"></span>
                                                        </a>
                                                        <span class="icon-cancel-circle"></span>
                                                    </li>
                                                    <li class="social_share">
                                                        <a href="https://twitter.com/intent/tweet?text=I just saved <?=(number_format($bundle->combined_price - $bundle->discount_price))?> on RGStore! I'm so happy&hashtags=RGStore" target="_blank">
                                                            <span class="icon-twitter2"></span>
                                                        </a>
                                                    </li>
                                                    <li class="social_share">
                                                        <a href="https://plus.google.com/share?url=<?= site_url();?>" target="_blank">
                                                            <span class="icon-google-plus-square"></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>

                        <?php endif; ?>

                    </div>
                </div>
            </div>
            <div class="main_content">
                <div class="video">
                    <iframe width="560" height="280" src="https://www.youtube.com/embed/1oFX9J99xCE"
                            frameborder="0" allowfullscreen></iframe>
                    <div class="video_title">
                        <h2> Welcome to iMakr.com </h2>
                    </div>
                </div>
                <div class="promises">
                    <?php $this->load->view('includes/static_sidebar'); ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>
