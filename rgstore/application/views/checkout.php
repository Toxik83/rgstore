<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="main_content_full">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>
                
                <?php if ($msg = $this->session->flashdata('error')) : ?>

                    <div class="notification warning">
                        <span class="icon-times-circle"></span>
                        <p class="message">
                           <?= $msg; ?>
                        </p>
                    </div>

                <?php endif; ?>
                
                <?php if (form_error('shipping')) : ?>
                    <div class="notification warning">
                        <span class="icon-times-circle"></span>
                        <p class="message">
                           <?= form_error('shipping'); ?>
                        </p>
                    </div>
                <?php endif; ?>
                
                <?php if (form_error('terms')) : ?>
                    <div class="notification warning">
                        <span class="icon-times-circle"></span>
                        <p class="message">
                           <?= form_error('terms'); ?>
                        </p>
                    </div>
                <?php endif; ?>
                            
            </div>
            <h1 class="blue_title">Checkout</h1>

            <?php $i = 1; ?>

            <div class="form shopping_cart">
                <h2 class="cart_title">Items in basket</h2>
                <table id="cartTable" class="sc_table">
                    <thead>
                    <tr>
                        <th class="sc_product">Product</th>
                        <th class="sc_product">Description</th>
                        <th>Colour</th>
                        <th>Unit price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($this->cart->contents() as $row): ?>

                        <tr>
                            <td>

                                <?php foreach ($this->cart->product_options($row['rowid']) as $option_name => $option_value): ?>

                                    <?php if (!($option_value) == NULL && $option_name == 'Image') { ?>

                                        <img src="<?= base_url('upload/images155x155/' . $option_value); ?>"/>

                                    <?php } ?>

                                <?php endforeach; ?>

                            </td>
                            <td>
                                <?= $row['name']; ?>
                            </td>
                            <td>

                                <?php foreach ($this->cart->product_options($row['rowid']) as $option_name => $option_value): ?>

                                    <?php if ($option_name === 'Colour'): ?>

                                        <?php if ($option_value === 'red'): ?>

                                            <span class="icon-square-red"></span>

                                        <?php endif; ?>

                                        <?php if ($option_value === 'green'): ?>

                                            <span class="icon-square-green"></span>

                                        <?php endif; ?>

                                        <?php if ($option_value === 'yellow'): ?>

                                            <span class="icon-square-yellow"></span>

                                        <?php endif; ?>

                                    <?php endif; ?>

                                <?php endforeach; ?>

                            </td>
                            <td>
                                &pound;<?= $this->cart->format_number($row['price']); ?>
                            </td>
                            <td>
                                <?= $row['qty']; ?>
                            </td>
                            <td>
                                &pound;<?= $this->cart->format_number($row['subtotal']); ?>
                            </td>
                        </tr>

                        <?php $i++; ?>

                    <?php endforeach; ?>

                    <tr>
                        <td colspan="7" class="colspan_text">
                            Total: &pound;<?= $this->cart->format_number($this->cart->total()); ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="form">
                <h2 class="cart_title">Personal details</h2>
                <ul class="user_cards" data-columns="2">
                    <li>
                        <div class="user_cards_content checkout_card">
                            <a href="<?= site_url('profile/personal-details') ?>" class="change_current">
                                <span>Change</span>
                            </a>
                            <h2 class="checkout_tittle">
                                Current delivery address
                            </h2>
                            <div class="location">
                                <span class="icon-location"></span>
                                <ul class="address_card">
                                    <li class="address_info">
                                        <?= $address->city; ?>,  <?= $address->display_name; ?>
                                    </li>
                                    <li class="address_info">
                                        <?= $address->address; ?>, <?= $address->zip_code; ?>
                                    </li>
                                    <li class="address_info">
                                        <?= '+'.$address->phonecode; ?><?= $address->phone; ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="user_cards_content checkout_card">
                            <a href="<?= site_url('profile/billing-address') ?>" class="change_current">
                                <span>Change</span>
                            </a>
                            <h2 class="checkout_tittle">
                                Current Billing address
                            </h2>
                            <div class="location">
                                <span class="icon-billing"></span>
                                <ul class="address_card">
                                    <li class="address_info">
                                        <?= $billingAddress->b_city; ?>, <?= $billingAddress->display_name; ?>
                                    </li>
                                    <li class="address_info">
                                        <?= $billingAddress->b_address; ?>, <?= $billingAddress->b_zip_code; ?> <?= $billingAddress->b_city; ?>
                                    </li>
                                    <li class="address_info">
                                        <?= '+'.$billingAddress->b_phone_code; ?><?= $billingAddress->b_phone; ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
                <form action="buyWithPayPal" method="post">
                <h2 class="cart_title">Shipping</h2>
                <div class="order_details">
                    <div class="shipping_form">

                        <?php foreach ($shipping as $shipping): ?>

                            <label class="radio">
                                <input class="radio_button" type="radio" name="shipping" <?= set_radio('shipping', $shipping->price); ?>value="<?= $shipping->price; ?>"/>
                                <?= $shipping->range; ?>
                                <span class="shipping_price">
                                     &pound;<?= $shipping->price; ?>
                                </span>
                            </label>

                        <?php endforeach; ?>

                    </div>
                </div>
                <table class="checkout_table">
                    <tbody>
                    <tr>
                        <td class="table_text">
                            Sub Total:
                        </td>
                        <td>
                            &pound;<?= $this->cart->format_number($this->cart->total()); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="table_text">
                            Shipping Cost:
                        </td>
                        <td>
                            &pound;<span id="shipping_cost">
                                <?php if (array_key_exists('shipping', $postData)): ?>
                                    <?= $postData['shipping']; ?>
                                <?php endif; ?>
                            </span>
                        </td>
                    </tr>
                    <tr class="table_total">
                        <td>
                            Total Payment:
                        </td>
                        <td>
                            &pound;<span id="total"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="clear"></div>
                <div class="terms_of_use">
                    <label>
                        <input type="checkbox" name="terms" value="1" id="terms" <?= set_checkbox('terms', 1); ?>>
                        I agree with the
                        <a href="<?= site_url('page/terms'); ?>" class="terms_link">
                            Terms of use
                        </a>
                    </label>
                </div>
                <div class="clear"></div>
                <a href="<?= site_url('cart/basket'); ?>" class="shop_button">
                    <span class="icon-triangle-left"></span>
                    Go back to basket
                </a>
                    <input type="submit" value="Complete payment" class="checkout_button">
                    
                 </form>
                <div style="text-align: right;">
                    Test buyer account for sandbox.paypal.com: <br>
                    Account: rgbuyer@email.com
                    Password: 12345678
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>

