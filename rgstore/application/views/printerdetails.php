<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">

            <?php $this->load->view('includes/sidebar'); ?>

            <div class="main_content">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>

                <?php if ($this->session->flashdata('success') != null) : ?>

                    <div class="notification success">
                        <span class="icon-success"></span>
                        <p class="message">
                            <?= $this->session->flashdata('success'); ?>
                        </p>
                    </div>

                <?php endif; ?>

                <?php if ($this->session->flashdata('error_q') != null) : ?>

                    <div class="notification warning">
                        <span class="icon-times-circle"></span>
                        <p class="message">
                            <?= $this->session->flashdata('error_q'); ?>
                        </p>
                    </div>

                <?php endif; ?>

                <?php if ($this->session->flashdata('success_filament') != null) : ?>

                    <div class="notification success">
                        <span class="icon-success"></span>
                        <p class="message">
                            <?= $this->session->flashdata('success_filament'); ?>
                        </p>
                    </div>

                <?php endif; ?>

            </div>
            <div class="main_content">
                <div class="content_wrap">                   
                        <div id="flexslider">
                        <ul class="slides loading">

                          <?php foreach ($printers as $printer): ?>

                                <li data-thumb="<?= base_url('upload/thumbs/' . $printer->name); ?>">
                                    <img class="zoom"
                                         src="<?= base_url('upload/images280x280/' . $printer->name); ?>"
                                         data-zoom-image="<?= base_url('upload/images500x500/' . $printer->name); ?>"/>
                                </li>

                            <?php endforeach; ?>


                        </ul>
                    </div>
                
                    <div class="product_details2">
                        <div class="title_wrap">
                            <h2 class="product_title">
                                <?= $printers[0]->title; ?>
                            </h2>
                            <div class="availability_info">
                                <span class="availability">In Stock</span>
                                <span>delivered within 48 hr or collect in store</span>
                            </div>
                        </div>
                        <div class="product_description">
                            <?= $printers[0]->text; ?>
                        </div>
                        <div class="product_price">
                            <div class="price">
                                <?= form_open('cart/buy/'); ?>
                                <input type="hidden" name="printer_id" id="product_id" value="<?= $this->uri->segment(3); ?>">

                                <?php if ($printer->new_price == '0.00') : ?>

                                    <input type="hidden" name="price" value="<?= $printer->price ?>">
                                    <span class="new_price">
                                        &pound;<?= number_format($printer->price); ?>
                                    </span>
                                    <span class="vat">(inc VAT.)</span>

                                <?php  else : ?>

                                    <input type="hidden" name="price" value="<?= $printer->price ?>">
                                    <span class="old_price">
                                        was
                                        &pound;<?= number_format($printer->price); ?>
                                    </span>
                                    <span class="new_price">
                                        &pound;<?= number_format($printer->new_price); ?>
                                    </span>
                                    <span class=vat>(inc VAT.)</span>

                                <?php endif; ?>

                            </div>
                            <div class="add_to_basket">
                                <div class="qty_field small_field">
                                    <input type="number" name="quantity" class="qty_num" placeholder="Qty" min="0">
                                    <div class="controls">
                                        <span class="qty_add">
                                            <span class="icon-triangle-up"></span>
                                        </span>
                                        <span class="qty_remove">
                                            <span class="icon-triangle-down2"></span>
                                        </span>
                                    </div>
                                </div>

                                <?php
                                $dataButton = array(
                                    'name' => 'addPrinter',
                                    'type' => 'submit',
                                    'content' => 'Add To Basket',
                                    'class' => 'add_basket',
                                );
                                ?>

                                <?= form_button($dataButton); ?>
                                <?= form_close(); ?>

                            </div>
                        </div>
                        <div class="share_buttons">
                            <span class='st_facebook_hcount' displayText='Facebook'></span>
                            <span class='st_twitter_hcount' displayText='Tweet'></span>
                            <span class='st_googleplus_hcount' displayText='Google +'></span>
                            <span class='st__hcount' displayText=''></span>
                        </div>
                    </div>
                </div>
                <div class="buy_product ">
                    <h3 class="productAdd_title">Add filament</h3>

                    <?= form_open('cart/buyFilament/'); ?>

                    <div class="color_form">
                        <div class="choose_product">
                            <div class="custom_select restricted_product">
                                <div class="select product">
                                    <div class="select_text restricted_text icon-triangle-down">
                                        <p>
                                            Choose filament
                                        </p>
                                    </div>
                                    <select id="category_input" name="category" class="input restricted_select id_input">
                                        <option disabled selected="selected" value="default">Choose filament</option>

                                        <?php foreach ($filaments as $val => $row): ?>

                                            <option value="<?= $row->id; ?>"><?= $row->title; ?></option>

                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
                            <div class="custom_select choose_color">
                                <div class="select product">
                                    <div class="select_text color_text color_arrow icon-triangle-down">
                                        <p>
                                            Colour
                                        </p>
                                    </div>
                                    <select name='colour' class='input color_select'>
                                        <option disabled selected="selected" value="default">Colour</option>
                                        <option value="red">red</option>
                                        <option value="green">green</option>
                                        <option value="yellow">yellow</option>
                                    </select>
                                </div>
                            </div>
                            <div class="qty_field color_field">
                                <input type="number" name="quantity" class="qty_num input" placeholder="Qty" min="0">
                                <div class="controls">
                                    <span class="qty_add">
                                        <span class="icon-triangle-up"></span>
                                    </span>
                                    <span class="qty_remove">
                                        <span class="icon-triangle-down2"></span>
                                    </span>
                                </div>
                            </div>
                            <a href="#" class="add_color">
                                <span class="icon-plus2"></span>
                                <span class="plus_color">Add</span>
                            </a>
                        </div>

                        <?php
                        $submitButton = array(
                            'name' => 'addfilament',
                            'type' => 'button',
                            'class' => 'add_basket2',
                            'content' => 'Add to basket',
                        );
                        ?>

                        <?= form_button($submitButton); ?>

                        <?= form_close(); ?>

                    </div>
                    <div class="quantity"></div>
                    <div class="product_added">
                        <h3>Filament added:</h3>
                        <span class="chosen_product"></span>
                        <div data-color="green" class="hidden color">
                            <span class="qty qty_green"></span>
                            x
                            <span class="icon-square-green"></span>
                            <a href="#" title="Remove colour">
                                <span data-color="green" class="icon-cancel-circle remove_color"></span>
                            </a>
                        </div>
                        <div data-color="yellow" class="hidden color">
                            <span class="qty qty_yellow"></span>
                            x
                            <span class="icon-square-yellow"></span>
                            <a href="#" title="Remove colour">
                                <span data-color="yellow" class="icon-cancel-circle remove_color"></span>
                            </a>
                        </div>
                        <div data-color="red" class="hidden color">
                            <span class="qty qty_red"></span>
                            x
                            <span class="icon-square-red"></span>
                            <a href="#" title="Remove colour">
                                <span data-color="red" class="icon-cancel-circle remove_color"></span>
                            </a>
                        </div>
                        <a href="#" class="remove_product" title="Remove filament">
                            <span class="icon-minus"></span>
                            <span class="minus_color">Remove</span>
                        </a>
                    </div>
                </div>
                <div class="buy_product">
                    <h3 class="productAdd_title">Add finishing</h3>

                    <?= form_open('cart/buyFinishingFromPrinters/'); ?>

                    <div class="color_form">
                        <div class="choose_product">
                            <div class="custom_select restricted_product">
                                <div class="select product">
                                    <div class="select_text restricted_text icon-triangle-down">
                                        <p>
                                            Choose finishing
                                        </p>
                                    </div>
                                    <select name="finishing_category" class="restricted_select">
                                        <option disabled selected="selected" value="default">Choose finishing</option>

                                        <?php foreach ($finishings as $val => $row): ?>

                                            <option value="<?= $row->id; ?>">
                                                <?= $row->title; ?>
                                            </option>

                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
                            <div class="custom_select choose_color">
                                <div class="select product">
                                    <div class="select_text color_text color_arrow icon-triangle-down">
                                        <p>
                                            Colour
                                        </p>
                                    </div>
                                    <select name='colour' class="color_select">
                                        <option disabled selected="selected" value="default">Colour</option>
                                        <option value="red">red</option>
                                        <option value="green">green</option>
                                        <option value="yellow">yellow</option>
                                    </select>
                                </div>
                            </div>
                            <div class="qty_field color_field">
                                <input type="number" name="quantity" class="qty_num" placeholder="Qty" min="0">
                                <div class="controls">
                                    <span class="qty_add">
                                        <span class="icon-triangle-up"></span>
                                    </span>
                                    <span class="qty_remove">
                                        <span class="icon-triangle-down2"></span>
                                    </span>
                                </div>
                            </div>
                            <a href="#" class="add_color">
                                <span class="icon-plus2"></span>
                                <span class="plus_color">Add</span>
                            </a>
                        </div>

                        <?php
                        $submitButton = array(
                            'name' => 'addfinishing',
                            'type' => 'submit',
                            'class' => 'add_basket2',
                            'content' => 'Add to basket',
                        );
                        ?>

                        <?= form_button($submitButton); ?>

                        <?= form_close(); ?>

                    </div>
                    <div class="product_added">
                        <h3>Finishing added:</h3>
                        <span class="chosen_product"></span>
                        <div data-color="green" class="hidden color">
                            <span class="qty qty_green"></span>
                            x
                            <span class="icon-square-green"></span>
                            <a href="#" title="Remove colour">
                                <span data-color="green" class="icon-cancel-circle remove_color"></span>
                            </a>
                        </div>
                        <div data-color="yellow" class="hidden color">
                            <span class="qty qty_yellow"></span>
                            x
                            <span class="icon-square-yellow"></span>
                            <a href="#" title="Remove colour">
                                <span data-color="yellow" class="icon-cancel-circle remove_color"></span>
                            </a>
                        </div>
                        <div data-color="red" class="hidden color">
                            <span class="qty qty_red"></span>
                            x
                            <span class="icon-square-red"></span>
                            <a href="#" title="Remove colour">
                                <span data-color="red" class="icon-cancel-circle remove_color"></span>
                            </a>
                        </div>
                        <a href="#" class="remove_product" title="Remove filament">
                            <span class="icon-minus"></span>
                            <span class="minus_color">Remove</span>
                        </a>
                    </div>
                </div>

                <?php $relatedBundle = getRelatedBundle($printers[0]->product_id); ?>

                <?php if (!empty($relatedBundle)): ?>

                    <div class="buy_product ">
                        <h3 class="productAdd_title">
                            Bundle containing <?= $printers[0]->title; ?>
                        </h3>
                        <form method="post" action="<?= site_url('cart/buyBundle/' . $relatedBundle->product_id); ?>"
                              id="bundle_form">

                            <?php
                            $relatedBundleData = getBundleDetails($relatedBundle->product_id);
                            $numItems = count($relatedBundleData);
                            $i = 0;
                            ?>

                            <div class="promo">
                                <div class="bundle">
                                    <p>
                                        <?= $relatedBundle->bundle_name; ?>
                                    </p>
                                    <ul class="promo_products">

                                        <?php foreach ($relatedBundleData as $key => $item): ?>

                                            <li>

                                                <?php if ($item->cat_id == 1): ?>

                                                    <a href="<?= site_url('printersportfolio/printersdetail/' . $item->id); ?>">
                                                        <img src="<?= site_url('upload/images60x70/' . $item->name); ?>"/>
                                                    </a>

                                                <?php endif; ?>

                                                <?php if ($item->cat_id == 2): ?>

                                                    <a href="<?= site_url('filamentportfolio/filamentsdetail/' . $item->id); ?>">
                                                        <img src="<?= site_url('upload/images60x70/' . $item->name); ?>"/>
                                                    </a>

                                                <?php endif; ?>

                                                <?php if ($item->cat_id == 3): ?>

                                                    <a href="<?= site_url('finishing/details/' . $item->id); ?>">
                                                        <img src="<?= site_url('upload/images60x70/' . $item->name); ?>"/>
                                                    </a>

                                                <?php endif; ?>

                                                <?php if (++$i < $numItems): ?>

                                                    <span class="icon-plus"></span>

                                                <?php endif; ?>

                                            </li>

                                        <?php endforeach; ?>

                                    </ul>
                                </div>
                                <div class="discount">
                                    <div class="discount_price">
                                        <span class="old_price">
                                            You save
                                            &pound;<?= ($relatedBundle->combined_price - $relatedBundle->discount_price); ?>
                                        </span>
                                        <span class="new_price">
                                            &pound;<?= $relatedBundle->discount_price; ?>
                                        </span>
                                    </div>
                                    <ul class="bundle_buttons">
                                        <li class="cl_discount">
                                            <a onclick="$('#bundle_form').submit()" href="#" class="compare" id="buy_bundle">
                                                <span class="icon-basket2 cell"></span>
                                                <span class="cell bundle_button">Add to Basket</span>
                                            </a>
                                        </li>
                                        <li class="share_discount dropdownShare">
                                            <a href="javascript:void(0)" class="compare">
                                                <span class="icon-share cell"></span>
                                                <span class="cell bundle_button"> Share</span>
                                            </a>
                                            <ul class="dropdown-share">
                                                <li class="social_share">
                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=  site_url()?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                       <span class="icon-facebook2"></span>
                                                    </a>
                                                    <span class="icon-cancel-circle"></span>
                                                </li>
                                                <li class="social_share">
                                                    <a href="https://twitter.com/intent/tweet?text=I just saved <?=(number_format($relatedBundle->combined_price - $relatedBundle->discount_price))?> on RGStore! I'm so happy&hashtags=RGStore" target="_blank">
                                                        <span class="icon-twitter2"></span>
                                                    </a>
                                                </li>
                                                <li class="social_share">
                                                    <a href="https://plus.google.com/share?url=<?= site_url();?>" target="_blank">
                                                        <span class="icon-google-plus-square"></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>

                <?php endif; ?>

                <?php // RELATED PRODUCTS
                if (isset($relatedProducts)) {
                ?>

                    <ul class="column_container related_products" data-columns="4">

                        <?php foreach ($relatedProducts as $key => $relatedProd): ?>

                            <li>
                                <div class="product_details">
                                    <a href="<?= site_url('printersportfolio/printersdetail/' . $relatedProd->id); ?>">
                                        <img src="<?= base_url('upload/images155x155/' . $relatedProd->name); ?>"/>

                                        <?php if ((($relatedProd->fl_date_begin) === '0000-00-00 00:00:00') &&
                                            ($relatedProd->fl_date_end) === '0000-00-00 00:00:00') : ?>

                                            <?php if ($relatedProd->new_price == '0.00') : ?>

                                                <p>
                                                    <?= $relatedProd->title; ?>
                                                </p>
                                                <div class="price_compare">
                                                    <span class="new_price equal">
                                                        &pound;<?= $relatedProd->price; ?>
                                                    </span>
                                                </div>
                                                <ul class="cl_share">
                                                    <li class="cl">
                                                        <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                            <?php if ($this->session->userdata('ids') == null): ?>

                                                                <span class="icon-justice2 cell"></span>
                                                                <span class="cell">Add to Compare list</span>

                                                            <?php else : ?>

                                                                <span id="product_<?= $relatedProd->id; ?>"
                                                                      class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                          'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                </span>
                                                                <span class="cell">
                                                                    <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                        'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                </span>

                                                            <?php endif; ?>

                                                        </a>
                                                    </li>
                                                    <li class="share dropdownShare">
                                                        <a href="javascript:void(0)" class="compare">
                                                            <span class="icon-share cell"></span>
                                                            <span class="cell"> Share</span>
                                                        </a>
                                                        <ul class="dropdown-share">
                                                            <li class="social_share">
                                                                <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                    <span class="icon-facebook2"></span>
                                                                </a>
                                                                <span class="icon-cancel-circle"></span>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                    <span class="icon-twitter2"></span>
                                                                </a>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>" target="_blank">
                                                                    <span class="icon-google-plus-square"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>

                                            <?php  else : ?>

                                                <p>
                                                    <?= $relatedProd->title; ?>
                                                </p>
                                                <div class="price_compare">
                                                    <span class="old_price">
                                                        was
                                                        <span class="op_value">
                                                            &pound;<?= $relatedProd->price; ?>
                                                        </span>
                                                    </span>
                                                    <span class="new_price">
                                                        &pound;<?= $relatedProd->new_price; ?>
                                                    </span>
                                                </div>
                                                <ul class="cl_share">
                                                    <li class="cl">
                                                        <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                            <?php if ($this->session->userdata('ids') == null): ?>

                                                                <span class="icon-justice2 cell"></span>
                                                                <span class="cell">Add to Compare list</span>

                                                            <?php else : ?>

                                                                <span id="product_<?= $relatedProd->id; ?>"
                                                                      class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                          'icon-justice3' : 'icon-justice2'; ?> cell"></span>
                                                                <span class="cell">
                                                                    <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                        'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                </span>

                                                            <?php endif; ?>

                                                        </a>
                                                    </li>
                                                    <li class="share dropdownShare">
                                                        <a href="javascript:void(0)" class="compare">
                                                            <span class="icon-share cell"></span>
                                                            <span class="cell"> Share</span>
                                                        </a>
                                                        <ul class="dropdown-share">
                                                            <li class="social_share">
                                                                <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                    <span class="icon-facebook2"></span>
                                                                </a>
                                                                <span class="icon-cancel-circle"></span>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                    <span class="icon-twitter2"></span>
                                                                </a>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>" target="_blank">
                                                                    <span class="icon-google-plus-square"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>

                                            <?php endif; ?>

                                        <?php endif ; ?>

                                        <?php if ((($relatedProd->fl_date_begin) !== '0000-00-00 00:00:00') &&
                                            ($relatedProd->fl_date_end) !== '0000-00-00 00:00:00') : ?>

                                            <?php if (($current_time >= strtotime($relatedProd->fl_date_begin)) &&
                                                ($current_time <= strtotime($relatedProd->fl_date_end))) : ?>

                                                <?php if (((strtotime($relatedProd->fl_date_end) - $current_time) / (60 * 60 * 24)) <= 1 &&
                                                    ((strtotime($relatedProd->fl_date_end) - $current_time) / (60 * 60 * 24)) > 0) :?>

                                                    <div class="promo_tag">
                                                        <div class="flash_sale expiring arrow_box">
                                                            Flash Sale
                                                            <span class="duration">1 day left</span>
                                                        </div>
                                                    </div>
                                                    <p>
                                                        <?= $relatedProd->title; ?>
                                                    </p>
                                                    <div class="price_compare">
                                                        <span class="old_price">
                                                            was
                                                            <span class="op_value">
                                                                &pound;<?= $relatedProd->price; ?>
                                                            </span>
                                                        </span>
                                                        <span class="new_price">
                                                            &pound;<?= $relatedProd->new_price; ?>
                                                        </span>
                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
                                                            <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $relatedProd->id; ?>"
                                                                          class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                    </span>

                                                                <?php endif; ?>

                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell"> Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                        <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>" target="_blank">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                <?php  else : ?>

                                                    <div class="flash_sale arrow_box">
                                                        Flash Sale
                                                        <span class="duration">
                                                            <?= date_format(date_create($relatedProd->fl_date_begin), "d/m"); ?>
                                                            -
                                                            <?= date_format(date_create($relatedProd->fl_date_end), "d/m"); ?>
                                                        </span>
                                                    </div>
                                                    <p>
                                                        <?= $relatedProd->title; ?>
                                                    </p>
                                                    <div class="price_compare">
                                                        <span class="old_price">
                                                            was
                                                            <span class="op_value">
                                                                &pound;<?= $relatedProd->price; ?>
                                                            </span>
                                                        </span>
                                                        <span class="new_price">
                                                            &pound;<?= $relatedProd->new_price; ?>
                                                        </span>
                                                    </div>
                                                    <ul class="cl_share">
                                                        <li class="cl">
                                                            <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                                <?php if ($this->session->userdata('ids') == null): ?>

                                                                    <span class="icon-justice2 cell"></span>
                                                                    <span class="cell">Add to Compare list</span>

                                                                <?php else : ?>

                                                                    <span id="product_<?= $relatedProd->id; ?>"
                                                                          class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                              'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                    </span>
                                                                    <span class="cell">
                                                                        <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                            'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                    </ span>

                                                                <?php endif; ?>

                                                            </a>
                                                        </li>
                                                        <li class="share dropdownShare">
                                                            <a href="javascript:void(0)" class="compare">
                                                                <span class="icon-share cell"></span>
                                                                <span class="cell"> Share</span>
                                                            </a>
                                                            <ul class="dropdown-share">
                                                                <li class="social_share">
                                                                    <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                        <span class="icon-facebook2"></span>
                                                                    </a>
                                                                    <span class="icon-cancel-circle"></span>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                        <span class="icon-twitter2"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="social_share">
                                                                    <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>" target="_blank">
                                                                        <span class="icon-google-plus-square"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                <?php endif ; ?>

                                            <?php  else : ?>

                                                <p>
                                                    <?= $relatedProd->title; ?>
                                                </p>
                                                <div class="price_compare equal">
                                                    <span id="demo" class="new_price">
                                                        &pound;<?= $relatedProd->price; ?>
                                                    </span>
                                                </div>
                                                <ul class="cl_share">
                                                    <li class="cl">
                                                        <a href="#" class="compare compare_link" data-id="<?= $relatedProd->id; ?>">

                                                            <?php if ($this->session->userdata('ids') == null): ?>

                                                                <span class="icon-justice2 cell"></span>
                                                                <span class="cell">Add to Compare list</span>

                                                            <?php else : ?>

                                                                <span id="product_<?= $relatedProd->id; ?>"
                                                                      class="<?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                          'icon-justice3' : 'icon-justice2'; ?> cell">
                                                                </span>
                                                                <span class="cell">
                                                                    <?= (in_array($relatedProd->id, $this->session->userdata('ids'))) ?
                                                                        'Remove from Compare List' : 'Add to Compare List'; ?>
                                                                </span>

                                                            <?php endif; ?>

                                                        </a>
                                                    </li>
                                                    <li class="share dropdownShare">
                                                        <a href="javascript:void(0)" class="compare">
                                                            <span class="icon-share cell"></span>
                                                            <span class="cell"> Share</span>
                                                        </a>
                                                        <ul class="dropdown-share">
                                                            <li class="social_share">
                                                                <a href="https://www.facebook.com/dialog/share?app_id=1485658125073129&display=popup&href=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>&redirect_uri=http://rgintern.www.demo.rewardgateway.com/" target="_blank">
                                                                    <span class="icon-facebook2"></span>
                                                                </a>
                                                                <span class="icon-cancel-circle"></span>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://twitter.com/intent/tweet?text=I just bought <?=$relatedProd->title?> from RGStore and it's awesome&url=<?=site_url('printersportfolio/printersdetail/'.$relatedProd->id)?>&hashtags=RGStore" target="_blank">
                                                                    <span class="icon-twitter2"></span>
                                                                </a>
                                                            </li>
                                                            <li class="social_share">
                                                                <a href="https://plus.google.com/share?url=<?= site_url('printersportfolio/printersdetail/'.$relatedProd->id);?>" target="_blank">
                                                                    <span class="icon-google-plus-square"></span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>

                                            <?php endif; ?>

                                        <?php endif ; ?>

                                    </a>
                                </div>
                            </li>

                        <?php endforeach; ?>

                        <li class="placeholder"></li>
                        <li class="placeholder"></li>
                    </ul>

                <?php } // END IF of Related Products ?>

            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>