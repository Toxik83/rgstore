<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="main_content_full">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>

                <?php if ($this->session->flashdata('success') != null) : ?>

                    <div class="notification success">
                        <span class="icon-success"></span>
                        <p class="message">
                            <?= $this->session->flashdata('success'); ?>
                        </p>
                    </div>

                <?php endif; ?>

                <?php if ($this->session->flashdata('error_b') != null) : ?>

                    <div class="notification warning">
                        <?= $this->session->flashdata('error_b'); ?>
                    </div>

                <?php endif; ?>

            </div>
            <h1 class="blue_title">Basket</h1>

            <?= form_open('cart/check'); ?>

            <?php $i = 1; ?>

            <div class="form">
                <h2 class="cart_title">My basket</h2>
                <table id="cartTable" class="sc_table">
                    <thead>
                    <tr>
                        <th class="sc_product">Product</th>
                        <th class="sc_product">Description</th>
                        <th>Colour</th>
                        <th>Unit price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Remove</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php if ($this->cart->total() == 0) { ?>

                        <tr>
                            <td colspan="7" class="no_data">
                                Your basket is empty.
                            </td>
                        </tr>

                    <?php } else { ?>

                        <?php foreach ($this->cart->contents() as $row): ?>

                            <tr>
                                <td>
                                    <?= form_hidden('rowid[]', $row['rowid']); ?>

                                    <?php foreach ($this->cart->product_options($row['rowid']) as $option_name => $option_value): ?>

                                        <?php if (!($option_value) == NULL && $option_name == 'Image') { ?>

                                            <img src="<?= base_url('upload/images155x155/' . $option_value); ?>"/>

                                        <?php } ?>

                                    <?php endforeach; ?>

                                </td>
                                <td>
                                    <?= $row['name']; ?>
                                </td>
                                <td>

                                    <?php foreach ($this->cart->product_options($row['rowid']) as $option_name => $option_value): ?>

                                        <?php if ($option_name === 'Colour'): ?>

                                            <?php if ($option_value === 'red'): ?>

                                                <span class="icon-square-red"></span>

                                            <?php endif; ?>

                                            <?php if ($option_value === 'green'): ?>

                                                <span class="icon-square-green"></span>

                                            <?php endif; ?>

                                            <?php if ($option_value === 'yellow'): ?>

                                                <span class="icon-square-yellow"></span>

                                            <?php endif; ?>

                                        <?php endif; ?>

                                    <?php endforeach; ?>

                                </td>
                                <td>
                                    &pound;<?= $this->cart->format_number($row['price']); ?>
                                </td>
                                <td>
                                    <div class="qty_field">
                                        <input type="number" name="qty[]" class="qty_num" placeholder="Qty" min="0"
                                               data-id="<?= $row['rowid']; ?>"
                                               value="<?= $row['qty']; ?>"/>
                                        <div class="controls basket_controls">
                                            <span class="qty_add">
                                                <span class="icon-triangle-up"></span>
                                            </span>
                                            <span class="qty_remove">
                                                <span class="icon-triangle-down2"></span>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    &pound;<?= $this->cart->format_number($row['subtotal']); ?>
                                </td>
                                <td>
                                    <a href="<?= site_url('cart/delete/' . $row['rowid']); ?>">
                                        <span class="icon-trashcan"></span>
                                    </a>
                                </td>
                            </tr>

                            <?php $i++; ?>

                        <?php endforeach; ?>

                    <?php } ?>

                    <tr>
                        <td colspan="7" class="colspan_text">

                            <?php if ($this->cart->total() == 0) { ?>

                                Total: &pound;0

                            <?php } else { ?>

                                Total: &pound;<?= $this->cart->format_number($this->cart->total()); ?>

                            <?php } ?>

                        </td>
                    </tr>
                    </tbody>
                </table>

                <?php if ($this->cart->total() == 0) { ?>

                    <a class="shop_button" href="<?= site_url(); ?>">
                        <span class="icon-triangle-left"></span>
                        Go back Shopping
                    </a>

                <?php } else { ?>

                    <a class="shop_button" href="<?= site_url(); ?>">
                        <span class="icon-triangle-left"></span>
                        Continue Shopping
                    </a>
                    <a class="checkout_button" href="<?= site_url('cart/checkout'); ?>">
                        Proceed to Checkout
                        <span class="icon-triangle-right"></span>
                    </a>

                <?php } ?>

            </div>

            <?= form_close(); ?>

        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>
