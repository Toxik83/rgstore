<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="static_content">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>
            </div>
            <div class="static_sidebar_container">

                <?php $this->load->view('includes/static_sidebar'); ?>

            </div>
            <div class="static_content">
                <h2>
                    <?= $pageData->title; ?>
                </h2>
                <?= $pageData->content; ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>
