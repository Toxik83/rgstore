<?php
    session_start();
    if($this->session->userdata('username') == false){
        $_SESSION['username'] = "Guest";
    }else{
        $_SESSION['username'] = $this->session->userdata('username');
    }// Must be already set
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    
    <meta property="og:locale" content="bg_BG" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="1485658125073129"/>
    <meta property="og:url" content="http://rgintern.www.demo.rewardgateway.com" />
    <meta property="og:title" content="RGStore - We can provide best products" />
    <meta property="og:site_name" content="RGStore"/>
    <meta property="og:description" content="The best web store ever" />
    <meta property="og:image" content="http://rgintern.www.demo.rewardgateway.com/media/public/images/logo.png" />
   
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300italic,300,400italic,700italic,700'
          rel='stylesheet' type='text/css'>
    
    <link href="<?= site_url('media/public/css/chat.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?= site_url('media/public/css/style.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?= site_url('media/public/css/icons_style.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?= site_url('media/public/css/icons_style2.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?= site_url('media/admin/css/style_admin.css'); ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?= site_url('media/public/css/flexslider.css'); ?>" type="text/css">

    <script src="<?= site_url('media/public/js/jquery-1.11.3.min.js'); ?>"></script>
    <script src="<?= site_url('media/public/js/shareButton.js'); ?>"></script>
    <script src="<?= site_url('media/public/js/jquery-ui.min.js'); ?>"></script>
    <script src="<?= site_url('media/public/js/enscroll-0.6.1.min.js'); ?>"></script>

    <script>var siteurl = "<?= site_url()?>";</script>
    <script src="<?= site_url('media/public/js/jqueryValidation/jquery.validate.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/jqueryValidation/reset-form.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/jqueryValidation/signup-form.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/jqueryValidation/update-user.js'); ?>" type="text/javascript"></script>
    
    <script src="<?= site_url('media/public/js/ajax.js'); ?>"></script>

    <script src="<?= site_url('media/public/js/cookie_bar.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/chat.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/filter.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/fb_login.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/auto_complete.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/history.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/phone_codes.js'); ?>" type="text/javascript"></script>
<!--    <script type="text/javascript" src="--><?//= site_url('media/public/js/shareButtons.js'); ?><!--"></script>-->
<!--    <script type="text/javascript" src="--><?//= site_url('media/public/js/jquery.elevatezoom.js'); ?><!--"></script>-->

    <script src="<?= site_url('media/public/js/img_error.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/shipping.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/scripts.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/cookie.js'); ?>" type="text/javascript"></script>
 
    <script src="<?= site_url('media/public/js/jquery.flexslider.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/home_slider.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url('media/public/js/slider_with_thumb.js'); ?>" type="text/javascript"></script>
    <script src="<?= site_url("/ckeditor/ckeditor.js"); ?>"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>

</head>
<div id="wrap">
    <header>
        <div class="ui-widget">
            <div class="cookie-message ui-widget-header blue">
                <p>By using this website you allow us to place cookies on your computer. They are harmless and never personally identify you.</p>
            </div>
        </div>
        <div class="content_container">
            <a href="<?= site_url(); ?>" class="logo_img">
                <img src="<?= site_url('media/public/images/logo.png'); ?>" alt="iMakr">
            </a>
            <div class="inner_header">
                <div class="menu_top_row">
                    <ul class="information column_container" data-columns="3">
                        <li>
                            <?php $listOfUsers = Chat(); ?>
                            <?php if (isset($listOfUsers) && !empty($listOfUsers)) :
                                foreach ($listOfUsers as $res) :  
                                ?>
                                <?php if($res->online == true) :?>
                                    <a href="javascript:void(0)" onClick="javascript:chatWith('<?= $res->username; ?>');">
                                        <span class="icon-comments"></span>
                                        <span class="live_chat" style="color: green;">Online </span>
                                    </a>
                                <?php else : ?>
                                    <a href="javascript:void(0)" onClick="javascript:chatWith('<?= $res->username; ?>');">
                                        <span class="icon-comments"></span>
                                        <span class="live_chat" style="color: red">Offline</span>
                                    </a>
                                <?php endif;
                                    endforeach;
                            else : ?>
                                <a href="" >
                                    <span class="icon-comments"></span>
                                    <span class="live_chat">Live chat</span>
                                </a>
                            <?php endif; ?>
                        </li>
                        <li>
                            <a href="tel:+35902071234567">
                                <span class="icon-phone"></span>
                                <span>0207 123 4567</span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:rgatewaytest@gmail.com">
                                <span class="icon-envelope"></span>
                                <span>info@imakr.com</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="user_buttons column_container" data-columns="2">

                        <?php if (isLogged()): ?>

                            <li>
                                <?= anchor('profile', 'Profile &raquo'); ?>
                            </li>
                            <li>
                                <?= anchor('login/logout', 'Logout &raquo'); ?>
                            </li>

                        <?php else: ?>

                            <li>
                                <?= anchor('register', 'Register &raquo'); ?>
                            </li>
                            <li>
                                <?= anchor('login', 'Login &raquo'); ?>
                            </li>

                        <?php endif; ?>

                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="menu_middle_row">
                    <div class="search">
                        <form method="get" action="<?= site_url('home/search'); ?>" class="search_form">
                            <div class="custom_select">
                                <div class="select">
                                    <div class="select_text icon-triangle-down">
                                        <p>
                                            <?php if ($postData['searchBy'] == 1) { ?>

                                                Printers

                                            <?php } else if ($postData['searchBy'] == 2) { ?>

                                                Filaments

                                            <?php } else if ($postData['searchBy'] == 3) { ?>

                                                Finishings

                                            <?php } else { ?>

                                                Printers

                                            <?php } ?>

                                        </p>
                                    </div>
                                    <select id="selectId" name="searchBy">
                                        <option value="1" <?= ($postData['searchBy'] == 1) ? "selected" : "" ?>>
                                            Printers
                                        </option>
                                        <option value="2" <?= ($postData['searchBy'] == 2) ? "selected" : "" ?>>
                                            Filaments
                                        </option>
                                        <option value="3" <?= ($postData['searchBy'] == 3) ? "selected" : "" ?>>
                                            Finishings
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <label for="search" class="icon-magnifier"></label>
                            <input id="autocomplete" autocomplete="off" type="text" name="search" value="<?= $search; ?>"/>
                            <button class="search_button" type="submit">
                                <span class="search_text">Search</span>
                                <span class="search_icon icon-magnifier"></span>
                            </button>
                        </form>
                        <div id="suggesstion-box"></div>
                    </div>
                    <ul class="cl_basket column_container" data-columns="2">
                        <li>
                            <a href="<?= site_url('/compare/compareItems') ?>">
                                <span class="icon-justice"></span>
                                <div class="cl_basket_content">
                                    <h3>Compare list</h3>
                                    <span class="cl_basket_info" id="compare_items"><?php echo getCompareList();?> items</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?= site_url('/cart/basket/') ?>">
                                <span class="icon-basket"></span>
                                <div class="cl_basket_content">
                                    <h3>Basket</h3>

                                    <?php $cartContent = getCartContent(); ?>

                                    <span class="cl_basket_info total">
                                        &pound; <?= number_format($cartContent['price']); ?>
                                        <span class="quantity">

                                            <?php if ($cartContent['total'] > 1) { ?>

                                                (<?= $cartContent['total']; ?> items)

                                            <?php } else if ($cartContent['total'] == 1) { ?>

                                                (1 item)

                                            <?php } else { ?>

                                                (0 items)

                                            <?php } ?>

                                        </span>
                                    </span>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="menu">
                    <ul class="menu_bottom_row active_nav">

                        <?php $menuData = getMenus(array('nav')); ?>

                        <?php foreach ($menuData as $key => $menu): ?>

                            <?php if ($menu->keyword == 'nav'): ?>

                                <li>
                                    <a href="<?= ($menu->internal == 0) ? $menu->page_link : site_url($menu->page_link) ;?>"
                                        <?= ($menu->internal == 0) ? 'target="_blank"' : ''; ?>>
                                        <?= $menu->link_name; ?>
                                    </a>
                                </li>

                            <?php endif; ?>

                        <?php endforeach; ?>

                    </ul>
                    <a class="toggle-nav" href="#">Menu &#9776;</a>
                </div>
            </div>
        </div>        
    </header>
    <div id="compare_error">
        <span id="compare_error_span"></span>
        <p class="message" id="compare_error_p"></p>
    </div>
