<div class="sidebar">
    <div class="filter">
        <?php $id = $this->input->get('searchBy'); ?>

        <?php if (
            ($this->uri->segment(1) == 'printersportfolio') ||
            ($this->uri->segment(1) == 'sellers') ||
            ($this->uri->segment(1) == 'deals') ||
            ($this->uri->segment(1) == 'home' && $id == '1') ||
            ($this->uri->segment(1) == '')
        )
            : ?>

            <h2>Printers</h2>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Prices
                </h2>
                <ul class="dropdown_menu">

                    <?php $prices = getPrices(); ?>

                    <?php foreach ($prices as $key => $value) : ?>

                        <li>
                            <a href="" class="min_max_pr" name="min_max" data-price-pr="<?= $value->min; ?>"
                               data-id2="<?= $value->max; ?>">
                                <?= $value->min ?>-<?= $value->max ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Manufacturer
                </h2>
                <ul id="manufacturer" class="dropdown_menu">

                    <?php $manufacturers = getManufacturer(); ?>

                    <?php foreach ($manufacturers as $company) : ?>

                        <li>
                            <a href="" class="man-pr" name="man" data-man-pr="<?= $company->id ?>">
                                <?= $company->man_name; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Printing Technology
                </h2>
                <ul class="dropdown_menu">

                    <?php $print_tech = getPrintTech(); ?>

                    <?php foreach ($print_tech as $print) : ?>

                        <li>
                            <a href="" class="tech" name="tech" data-tech="<?= $print->id ?>">
                                <?= $print->print_name; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Building Size
                </h2>
                <ul class="dropdown_menu">

                    <?php $sizes = getSize(); ?>

                    <?php foreach ($sizes as $size) : ?>

                        <li>
                            <a href="" class="size" name="size" data-size="<?= $size->id ?>">
                                <?= $size->build_name; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Accuracy
                </h2>
                <ul id="accuracy" class="dropdown_menu">

                    <?php $idccuracies = getAccuracy(); ?>

                    <?php foreach ($idccuracies as $idccuracy) : ?>

                        <li>
                            <a href="" class="acc" name="acc" data-acc="<?= $idccuracy->id ?>">
                                <?= $idccuracy->accuracy_name; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>

        <?php endif; ?>

        <?php if (
            ($this->uri->segment(1) == 'filamentportfolio') ||
            ($this->uri->segment(1) == 'sellers') ||
            ($this->uri->segment(1) == 'deals') ||
            ($this->uri->segment(1) == 'home' && $id == '2') ||
            ($this->uri->segment(1) == 'productslisting') ||
            ($this->uri->segment(1) == '')
        )
            : ?>

            <?php $category = Filament_Categories(); ?>
            <?php $prices = getPrices(); ?>

            <h2>Filament</h2>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Prices
                </h2>
                <ul class="dropdown_menu">

                    <?php foreach ($prices as $key => $value) : ?>
                        <li>
                            <a href="" class="min_max_fil" name="min_max" data-price-fil="<?= $value->min; ?>"
                               data-id2="<?= $value->max; ?>">
                                <?= $value->min ?>-<?= $value->max ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Manufacturer
                </h2>
                <ul class="dropdown_menu">

                    <?php $manufacturers = getManufacturer(); ?>
                    <?php foreach ($manufacturers as $company) : ?>

                        <li>
                            <a href="" class="man-fil" name="man" data-man-fil="<?= $company->id ?>">
                                <?= $company->man_name; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Categories
                </h2>
                <ul class="dropdown_menu">

                    <?php foreach ($category as $key => $value) : ?>

                        <li>
                            <a href="" class="cat-fil" name="catName" data-cat-fil="<?= $value->id ?>">
                                <?= $value->cat_name; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>

        <?php endif; ?>

        <?php if (
            ($this->uri->segment(1) == 'finishing') ||
            ($this->uri->segment(1) == 'sellers') ||
            ($this->uri->segment(1) == 'deals') ||
            ($this->uri->segment(1) == 'home' && $id == '3') ||
            ($this->uri->segment(1) == '')
        ): ?>

            <?php $category = getCategories(); ?>
            <?php $prices = getPrices(); ?>
            <?php $manufacturers = getManufacturer(); ?>

            <h2>Finishing</h2>

            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Prices
                </h2>
                <ul class="dropdown_menu">

                    <?php foreach ($prices as $key => $value) : ?>

                        <li>
                            <a href="" class="min_max" name="min_max" data-price="<?= $value->min; ?>"
                               data-id2="<?= $value->max; ?>">
                                <?= $value->min ?>-<?= $value->max ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Manufacturer
                </h2>
                <ul class="dropdown_menu">

                    <?php foreach ($manufacturers as $company) : ?>

                        <li>
                            <a href="" class="man" name="man" data-man="<?= $company->id ?>">
                                <?= $company->man_name; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
            <div class="dropdown_container">
                <h2 class="dropdown_title">
                    Categories
                </h2>
                <ul class="dropdown_menu">

                    <?php foreach ($category as $key => $value) : ?>

                        <li>
                            <a href="" class="cat" name="catName" data-cat="<?= $value->id ?>">
                                <?= $value->cat_name; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>

        <?php endif; ?>

    </div>

    <?php $lastseen = getLast(); ?>
    <?php if (isset($lastseen) && $lastseen): ?>

        <div class="last_seen">
            <h2>Last seen products</h2>
            <ul>

                <?php foreach ($lastseen as $key => $lasts) : ?>

                    <?php foreach ($lasts as $key1 => $last) : ?>

                        <li>

                            <?php if ($last['cat_id'] == 1): ?>

                            <a href="<?= site_url('printersportfolio/printersdetail/' . $last['id']); ?>">
                                <img style="width: 155px;" src="<?= base_url('upload/images155x155/' . $last['name']); ?>"/>

                                <?php endif; ?>

                                <?php if ($last['cat_id'] == 2): ?>

                                <a href="<?= site_url('filamentportfolio/filamentsDetail/' . $last['id']); ?>">
                                    <img style="width: 155px;" src="<?= base_url('upload/images155x155/' . $last['name']); ?>"/>

                                    <?php endif; ?>

                                    <?php if ($last['cat_id'] == 3): ?>

                                    <a href="<?= site_url('finishing/finishingDetail/' . $last['id']); ?>">
                                        <img style="width: 155px;" src="<?= base_url('upload/images155x155/' . $last['name']); ?>"/>

                                        <?php endif; ?>

                                <h3>
                                    <?= $last['title']; ?>
                                </h3>
                                <span>
                                    &pound;<?= $last['price'] ?>
                                </span>
                            </a>
                        </li>

                    <?php endforeach; ?>

                <?php endforeach; ?>

            </ul>
        </div>

    <?php endif; ?>

</div>



