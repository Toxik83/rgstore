<div class="static_sidebar">

    <?php $menuData = getMenus(array('static_sidebar')); ?>

    <?php if (array_key_exists(0, $menuData)): ?>

        <h2 class="promises_title">
            <?= $menuData[0]->menu_name; ?>
        </h2>

    <?php endif; ?>

    <ul>

        <?php foreach ($menuData as $key => $menu): ?>

            <?php if ($menu->keyword == 'static_sidebar'): ?>

                <li>
                    <a href="<?= ($menu->internal == 0) ? $menu->page_link : site_url($menu->page_link) ;?>"
                        <?= ($menu->internal == 0) ? 'target="_blank"' : ''; ?>>

                        <span class="<?= $menu->icon; ?>"></span>

                        <?= $menu->link_name; ?>

                    </a>
                </li>

            <?php endif; ?>

        <?php endforeach; ?>

    </ul>
</div>


                                        