<footer id="footer">
    <div class="footer_menu_container">
        <div class="content_container">
            <ul class="footer_menu">

                <?php $menuData = getMenus(array('footer_outer')); ?>

                <?php foreach ($menuData as $key => $menu): ?>

                    <?php if ($menu->keyword == 'footer_outer'): ?>

                        <li>
                            <a href="<?= ($menu->internal == 0) ? $menu->page_link : site_url($menu->page_link) ;?>"
                                <?= ($menu->internal == 0) ? 'target="_blank"' : ''; ?>>

                                <span class="<?= $menu->icon ;?>"></span>

                                <?= $menu->link_name; ?>
                            </a>
                        </li>

                    <?php endif; ?>

                <?php endforeach; ?>

            </ul>
        </div>
    </div>
    <div class="footer_inner">
        <div class="content_container">
            <div class="footer_inner_columns">
                <div class="column">
                    <div class="newsletter">
                        <h2 class="newsletter_title">
                            Newsletter Subscription
                        </h2>
                        <form action="<?= site_url('home/news'); ?>" method="post" class="newsletter_form">

                            <?php if($this->session->flashdata('error') != null ) :?>

                                <div class="notification warning">
                                    <?= $this->session->flashdata('error'); ?>
                                 </div>

                            <?php endif; ?>

                            <?php if ( $this->session->flashdata('success_news') !=null) : ?>

                                <div class="notification success">
                                    <span class="icon-success"></span>
                                    <p class="message">
                                        <?= $this->session->flashdata('success_news'); ?>
                                    </p>
                                </div>

                            <?php endif; ?>

                            <input type="email" name="email" placeholder="email">
                            <button type="submit" name="submit_button" class="submit_button">
                                Submit
                            </button>
                        </form>
                    </div>
                    <h2 class="newsletter_title">Follow us</h2>
                    <a href="#">
                        <span class="icon-facebook"></span>
                    </a>
                    <a href="#">
                        <span class="icon-twitter"></span>
                    </a>
                </div>
                <div class="column">

                    <?php $menuData = getMenus(array('footer_inner1')); ?>

                    <?php if (array_key_exists(0, $menuData)): ?>

                        <h2 class="footer_title">
                            <?= $menuData[0]->menu_name; ?>
                        </h2>

                    <?php endif; ?>

                    <?php foreach ($menuData as $key => $menu): ?>

                        <ul class="footer_list">

                            <?php if ($menu->keyword == 'footer_inner1'): ?>

                                <li>
                                    <a href="<?= ($menu->internal == 0) ? $menu->page_link : site_url($menu->page_link) ;?>"
                                        <?= ($menu->internal == 0) ? 'target="_blank"' : ''; ?>>
                                        <?= $menu->link_name; ?>
                                    </a>
                                </li>

                            <?php endif; ?>

                        </ul>

                    <?php endforeach; ?>

                </div>
                <div class="column">

                    <?php $menuData = getMenus(array('footer_inner2')); ?>

                    <?php if (array_key_exists(0, $menuData)): ?>

                        <h2 class="footer_title">
                            <?= $menuData[0]->menu_name; ?>
                        </h2>

                    <?php endif; ?>

                    <?php foreach ($menuData as $key => $menu): ?>

                        <ul class="footer_list">

                            <?php if ($menu->keyword == 'footer_inner2'): ?>

                                <li>
                                    <a href="<?= ($menu->internal == 0) ? $menu->page_link : site_url($menu->page_link) ;?>"
                                        <?= ($menu->internal == 0) ? 'target="_blank"' : ''; ?>>
                                        <?= $menu->link_name; ?>
                                    </a>
                                </li>

                            <?php endif; ?>

                        </ul>

                    <?php endforeach; ?>

                </div>
                <div class="column">

                    <?php $menuData = getMenus(array('footer_inner3')); ?>

                    <?php if (array_key_exists(0, $menuData)): ?>

                        <h2 class="footer_title">
                            <?= $menuData[0]->menu_name; ?>
                        </h2>

                    <?php endif; ?>

                    <?php foreach ($menuData as $key => $menu): ?>

                        <ul class="footer_list">

                            <?php if ($menu->keyword == 'footer_inner3'): ?>

                                <li>
                                    <a href="<?= $menu->page_link; ?>"<?= ($menu->internal == 0) ? 'target="_blank"' : ''; ?> >
                                        <?= $menu->link_name; ?>
                                    </a>
                                </li>

                            <?php endif; ?>

                        </ul>

                    <?php endforeach; ?>

                </div>
            </div>
            <div class="footer_credits">
                <div class="copyleft">
                    &copy; 2012 All Rights Reserved - iMark - Terms & Condition - Privacy Policy
                </div>
                <div class="copyright">
                    Website by: Dazines
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
</body>
</html>

                              





