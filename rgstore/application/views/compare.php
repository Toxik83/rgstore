<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="main_content_full">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>

                <?php if ($this->session->flashdata('success') != null) : ?>

                    <div class="notification success">
                        <span class="icon-success"></span>

                        <p class="message">
                            <?= $this->session->flashdata('success'); ?>
                        </p>
                    </div>

                <?php endif; ?>

            </div>
            <h1 class="blue_title">Compare List</h1>
            <div class="form">
                <h2 class="cart_title">Items to compare</h2>
                <table class="compare_table">

                    <?php if (isset($compareItems) && $compareItems) : ?>

                        <tr>
                            <td class="ct_default features">
                                <h2 class="ct_features">
                                    Features:
                                </h2>
                            </td>

                            <?php foreach ($compareItems as $item) : ?>

                                <td class="ct_image product_<?= $item->id; ?>">

                                    <?php if ($item->cat_id == 1): ?>

                                        <a href="<?= site_url('printersportfolio/printersdetail/' . $item->id); ?>">
                                            <img class="product_<?= $item->id; ?>"
                                                 src="<?= base_url('upload/images155x155/' . $item->name); ?>"/>
                                        </a>
                                        <a class="compare_link_remove" href="#" data-id="<?= $item->id; ?>">
                                            <span class="icon-cancel-circle"></span>
                                        </a>
                                        <a class="ct_title"
                                           href="<?= site_url('printersportfolio/printersdetail/' . $item->id); ?>">
                                            <?= $item->title; ?>
                                        </a>

                                    <?php endif; ?>

                                    <?php if ($item->cat_id == 2): ?>

                                        <a href="<?= site_url('filamentportfolio/filamentsdetail/' . $item->id); ?>">
                                            <img class="product_<?= $item->id; ?>"
                                                 src="<?= base_url('upload/images155x155/' . $item->name); ?>"/>
                                        </a>
                                        <a class="compare_link_remove" href="#" data-id="<?= $item->id; ?>">
                                            <span class="icon-cancel-circle"></span>
                                        </a>
                                        <a class="ct_title"
                                           href="<?= site_url('filamentportfolio/filamentsdetail/' . $item->id); ?>">
                                            <?= $item->title; ?>
                                        </a>

                                    <?php endif; ?>

                                    <?php if ($item->cat_id == 3): ?>
                                        <a href="<?= site_url('finishing/details/' . $item->id); ?>">
                                            <img class="product_<?= $item->id; ?>"
                                                 src="<?= base_url('upload/images155x155/' . $item->name); ?>"/>
                                        </a>
                                        <a class="compare_link_remove" href="#" data-id="<?= $item->id; ?>">
                                            <span class="icon-cancel-circle"></span>
                                        </a>
                                        <a class="ct_title"
                                           href="<?= site_url('finishing/details/' . $item->id); ?>">
                                            <?= $item->title; ?>
                                        </a>

                                    <?php endif; ?>

                                    <button name="addPrinter" data-id="<?= $item->id; ?>" class="add_basket ct_basket">
                                        Add To Basket
                                    </button>
                                </td>

                            <?php endforeach; ?>

                        </tr>
                        <tr>
                            <td class="ct_default">Price</td>

                            <?php foreach ($compareItems as $item) : ?>

                                <td class="product_<?= $item->id; ?>">
                                    &pound;<?= number_format($item->price); ?>
                                </td>

                            <?php endforeach; ?>

                        </tr>
                        <tr>
                            <td class="ct_default">Promo Price</td>

                            <?php foreach ($compareItems as $item) : ?>

                                <td class="product_<?= $item->id; ?>">
                                    <?= ($item->new_price != 0) ? "£" . number_format($item->new_price) : '-'; ?>
                                </td>

                            <?php endforeach; ?>

                        </tr>
                        <tr>
                            <td class="ct_default">Manufacturer</td>

                            <?php foreach ($compareItems as $item) : ?>

                                <td class="product_<?= $item->id; ?>">
                                    <?= ($item->man_name != null) ? $item->man_name : '-'; ?>
                                </td>

                            <?php endforeach; ?>

                        </tr>
                        <tr>
                            <td class="ct_default">Building size</td>

                            <?php foreach ($compareItems as $item) : ?>

                                <td class="product_<?= $item->id; ?>">
                                    <?= ($item->build_name != null) ? $item->build_name : '-'; ?>
                                </td>

                            <?php endforeach; ?>

                        </tr>
                        <tr>
                            <td class="ct_default">Accuracy</td>

                            <?php foreach ($compareItems as $item) : ?>

                                <td class="product_<?= $item->id; ?>">
                                    <?= ($item->accuracy_name != null) ? $item->accuracy_name : '-'; ?>
                                </td>

                            <?php endforeach; ?>

                        </tr>

                    <?php else : ?>

                        <tr>
                            <td class="ct_empty">
                                <h2>
                                    Features:
                                </h2>
                            </td>
                            <td rowspan="4" colspan="2">
                                <p>
                                    No items to compare.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="ct_empty">
                                Manufacturer
                            </td>
                        </tr>
                        <tr>
                            <td class="ct_empty">
                                Building size
                            </td>
                        </tr>
                        <tr>
                            <td class="ct_empty">
                                Accuracy
                            </td>
                        </tr>

                    <?php endif; ?>

                </table>
                <a class="shop_button" href="<?= site_url(); ?>">
                    <span class="icon-triangle-left"></span>
                    Go back Shopping
                </a>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('includes/footer'); ?>
