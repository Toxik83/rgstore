<?php $this->load->view('admin/includes/header'); ?>
<?php
session_start();
if ($this->session->userdata('username') == false) {
    $_SESSION['username'] = "Guest";
} else {
    $_SESSION['username'] = $this->session->userdata('username');
}// Must be already set
?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>

        <h1>Chat Room</h1>

        <h3>List of all users</h3>

        <?php
        echo form_open('admin/chat', array('method' => 'get', 'class' => 'admin_search'));
        echo form_input(array('name' => 'search', 'value' => $search_term, 'placeholder' => 'Seach user by User Name', 'class' => 'admin_chat_text'));
        echo form_button(array('name' => 'search_submit', 'type' => 'submit', 'class' => 'admin_search_button', 'content' => 'Search'));
        echo form_close();
        ?>

        <?php

        if (isset($listOfUsers) && $listOfUsers) : ?>

            <table class="admin_table">
                <tbody>
                <tr>
                    <th>Status</th>
                    <th>User Type</th>
                    <th>User Id</th>
                    <th>User Name</th>
                    <th>User History</th>
                </tr>

                <?php foreach ($listOfUsers as $res) : ?>

                    <tr>
                        <td><?php if ($res->online == 1) echo 'Active'; else echo 'Inactive'; ?></td>
                        <td><?php if ($res->is_admin == '0') : ?>
                                User
                            <?php else : ?>
                                Admin
                            <?php endif; ?>

                        </td>
                        <td><?= $res->id; ?></td>
                        <td>
                            <?php if ($_SESSION['username'] == $res->fname || $_SESSION['username'] == $res->username) : ?>

                            <?php else : ?>
                            <a href="javascript:void(0)"
                               onClick="javascript:chatWith('<?= $res->fname; ?>');">
                                <i class="fa fa-wechat"></i>
                                <?php endif; ?>
                                <?= $res->fname; ?>

                            </a>
                        </td>
                        <td>
                            <?php if ($_SESSION['username'] == $res->fname || $_SESSION['username'] == $res->username) : ?>

                            <?php else : ?>
                            <a href="<?= site_url('admin/chat/listing/' . $res->id); ?>">
                                <i class="fa fa-history"></i>
                                <?php endif; ?>
                                <?= $res->fname; ?>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; // end foreach loop ?>

                </tbody>
            </table>

            <?= $links; ?>

        <?php else : ?>
            <p class="error">
                No results found.
            </p>
            <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
        <?php endif; ?>

    </div>
</section>
<script>
function goBack() {
    window.history.back();
}
</script>

<?php $this->load->view('admin/includes/footer'); ?>