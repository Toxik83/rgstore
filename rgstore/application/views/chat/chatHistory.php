<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
        <h1>Chat History with <?= $chatName->fname; ?></h1>

        <h3>List of all messages from or to <?= $chatName->fname; ?></h3>

        <form method="get" class="admin_search" action="<?= site_url('admin/chat/chatListing/' . $chatName->id); ?>">
            <div class="custom_select">
                <div class="select">
                    <div class="select_text fa fa-caret-down">
                        <p>
                            <?php if(isset($_GET['searchBy']) && $_GET['searchBy'] == 1) :  ?>
                                From
                            <?php elseif(isset($_GET['searchBy']) && $_GET['searchBy'] == 2) :  ?>
                                To
                            <?php elseif(isset($_GET['searchBy']) && $_GET['searchBy'] == 3) :  ?>
                                Message
                            <?php else: ?>
                                From
                            <?php endif;?>
                        </p>
                    </div>
                    <select name="searchBy">
                        <option value="1" <?= isset($_GET['searchBy']) && $_GET['searchBy'] == 1 ? ' selected' : '' ?>>
                            From
                        </option>
                        <option value="2" <?= isset($_GET['searchBy']) && $_GET['searchBy'] == 2 ? ' selected' : '' ?>>
                            To
                        </option>
                        <option value="3" <?= isset($_GET['searchBy']) && $_GET['searchBy'] == 3 ? ' selected' : '' ?>>
                            Message
                        </option>
                    </select>
                </div>
            </div>
            <input type="text" class="admin_chat_text" placeholder="Search for message" name="search" value="<?= $search_term; ?>"/>
            <button type="submit" class="admin_search_button" value="Search">Search</button>
        </form>

        <?php
        if (isset($listOfUsers) && $listOfUsers) : ?>

            <table class="admin_table">
                <tbody>
                <tr>
                    <th>User Id</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Message</th>
                    <th>Sent</th>
                </tr>

                <?php foreach ($listOfUsers as $value) : ?>

                    <tr>
                        <td><?= $value->id; ?></td>
                        <td><?= $value->from; ?></td>
                        <td><?= $value->to; ?></td>
                        <td><?= $value->message; ?></td>
                        <td><?= $value->sent; ?></td>
                    </tr>

                <?php endforeach; ?>

                </tbody>
            </table>

            <?= $links; ?>

        <?php  else : ?>
            <p class="error">
                No results found.
            </p>
            <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
        <?php endif; ?>
    </div>
</section>
<script>
function goBack() {
    window.history.back();
}
</script>
<?php $this->load->view('admin/includes/footer'); ?>

