<?php $this->load->view('includes/header'); ?>
    <section>
        <div class="content_container">
            <div class="column_wrap">
                <div class="main_content_full">
                    <div class="breadcrumbs">
                        <?= $this->breadcrumb->output(); ?>
                    </div>
                </div>
                <div class="form">
                    <h2 class="login_title">Enter Password</h2>
                    <p>
                        In order to use your linked account as normal account, you'll need to create a password. After you set a password you'll
                        be able to login with your email too.
                    </p>
                    
                    <form action="" method="post" class="login_form">
                        <input type="password" name="password" placeholder="Enter Password" class="reset_pass"/>
                        <input type="password" name="passconf" placeholder="Confirm Password" class="reset_pass"/>
                        <?php if (form_error('password')) : ?>

                            <div class="form_error">
                                <?= form_error('password'); ?>
                            </div>

                        <?php endif; ?>

                        <button type="submit" name="sendLink" class="reset_button">Submit</button>
                    </form>                   
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('includes/footer'); ?>

