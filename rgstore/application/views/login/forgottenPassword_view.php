<?php $this->load->view('includes/header'); ?>
    <section>
        <div class="content_container">
            <div class="column_wrap">
                <div class="main_content_full">
                    <div class="breadcrumbs">
                        <?= $this->breadcrumb->output(); ?>
                    </div>
                </div>
                <div class="form">
                    <h2 class="login_title">Forgot Password</h2>
                    <p>
                        To reset your password, please enter the email that you use to login to you account and you will
                        receive a link where you can set up a new password.
                    </p>
                    
                    <form action="<?= site_url('login/forgotten-password'); ?>" method="post" class="login_form">
                        <input type="email" name="email" placeholder="Email Address" value="<?= $postData['email']; ?>"
                               class="reset_pass"/>

                        <?php if (form_error('email')) : ?>

                            <div class="form_error">
                                <?= form_error('email'); ?>
                            </div>

                        <?php endif; ?>

                        <button type="submit" name="sendLink" class="reset_button">Submit</button>
                    </form>                   
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('includes/footer'); ?>