<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="main_content_full">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>
            </div>
            <div class="form">
                <h2 class="login_title">Reset Password</h2>
                <p>
                    Enter new password.
                </p>
                <form id="resetForm" action="<?= site_url('login/reset/' . $token); ?>" method="post"
                      class="login_form">

                    <?php if (form_error('password')) : ?>

                        <?= form_error('password'); ?>

                    <?php endif; ?>

                    <input type="password" id="password" name="password" placeholder="New password" value=""
                           class="new_pass"/>

                    <?php if (form_error('passconf')) : ?>

                        <?= form_error('passconf'); ?>

                    <?php endif; ?>

                    <input type="password" id="passconf" name="passconf" placeholder="Confirm New Password" value=""
                           class="new_pass"/>
                    <button type="submit" name="sendLink" class="reset_button">Save</button>
                </form>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>
