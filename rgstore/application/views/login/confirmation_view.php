<?php $this->load->view('includes/header'); ?>
    <section>
        <div class="content_container">
            <div class="column_wrap">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>
                <div class="form like">
                    <h2 class="login_title">Confirmation</h2>

                    <?php if ($msg = $this->session->flashdata('validation_failed')) : ?>

                        <p class="alert alert-danger alert-dismissable">
                            <?= $msg; ?>
                        </p>

                    <?php endif; ?>

                    <?php if ($msg = $this->session->flashdata('success')) : ?>

                        <div class="notification success">
                            <span class="icon-success"></span>
                            <p class="message">
                                <?= $msg; ?>
                            </p>
                        </div>

                    <?php endif; ?>

                    <form action="<?= site_url('login'); ?>" method="post" class="login_form">

                        <?php if (form_error('email')) : ?>

                            <?= form_error('email'); ?>

                        <?php endif; ?>

                        <input type="email" name="email" placeholder="Email" class="new_pass"/>

                        <?php if (form_error('password')) : ?>

                            <?= form_error('password'); ?>

                        <?php endif; ?>

                        <input type="password" name="password" placeholder="Password" class="new_pass"/>
                        <button type="submit" name="register" class="reset_button clear_float">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('includes/footer'); ?>