<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="main_content_full">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>

                <?php if ($this->session->flashdata('authentication') != null) : ?>

                    <div class="alert alert-danger alert-dismissable notification warning">
                        <span class="icon-times-circle"></span>

                        <p class="message">
                            <?= $this->session->flashdata('authentication'); ?>
                        </p>
                    </div>

                <?php endif; ?>

                <?php if ($this->session->flashdata('token_request') != null) : ?>

                    <div class="notification warning">
                        <span class="icon-times-circle"></span>

                        <p class="message">
                            <?= $this->session->flashdata('token_request'); ?>
                        </p>
                    </div>

                <?php endif; ?>

                <?php if ($this->session->flashdata('success') != null) : ?>

                    <div class="notification success">
                        <span class="icon-success"></span>

                        <p class="message">
                            <?= $this->session->flashdata('success'); ?>
                        </p>
                    </div>

                <?php endif; ?>

            </div>
            <h1 class="blue_title">Login</h1>
            <div class="form">
                <div class="login_left">
                    <h2 class="login_title">Log in to your account</h2>

                    <?php if ($this->session->flashdata('validation_failed') != null) : ?>

                        <div class="form_error">
                            <p>
                                <?= $this->session->flashdata('validation_failed'); ?>
                            </p>
                        </div>

                    <?php endif; ?>

                    <form action="<?= site_url('login'); ?>" method="post" class="login_form">
                        <label for="login_email">Email</label>
                        <input id="login_email" type="text" name="email"/>

                        <?php if (form_error('email')) : ?>

                            <div class="form_error">
                                <?= form_error('email'); ?>
                            </div>

                        <?php endif; ?>

                        <label for="login_password">Password</label>
                        <input id="login_password" type="password" name="password"/>

                        <?php if (form_error('password')) : ?>

                            <div class="form_error">
                                <?= form_error('password'); ?>
                            </div>

                        <?php endif; ?>

                        <label for="remember">
                            <input id="remember" type="checkbox" name="remember"
                                   value="1" <?= (isset($_COOKIE['testP'])) ? 'checked' : ''; ?>/>
                            Remember me
                        </label>
                        <a class="forgotten_password" href="<?= site_url('login/forgotten-password'); ?>">
                            Forgotten password
                        </a>
                        <button type="submit" name="register" class="search_button clear_float">Log in</button>
                    </form>
                    <h2 class="login_title">Or login using</h2>
                    <div class="social_login">
                        <a href="<?= site_url('login/facebook?go=go')?>">
                            <span class="icon-facebook2"></span>
                        </a>
                        <a href="<?= site_url('login/twitter?go=go')?>" class="twitter">
                            <span class="icon-twitter2"></span>
                        </a>
                        <a href="<?= site_url('login/google?go=go')?>">
                            <span class="icon-google-plus-square"></span>
                        </a>
                    </div>
                </div>
                <div class="login_right">
                    <h2 class="login_title">New to imakr</h2>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                    <a class="link_button create_acc_redirect" href="<?= site_url('register'); ?>">Create new
                        account</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>



