<h1 class="blue_title">Profile</h1>
<div class="form profile_page">
    <h2 class="id_title cart_title">Your iMakr ID:</h2>
    <h2 class="current_delivery cart_title">Your current delivery address:</h2>
    <div class="user_card">
        <div class="card_header">

            <?php if (!empty($userData) && $userData->title === 'Mr'): ?>

                <span class="icon-user2"></span>

            <?php else: ?>

                <span class="icon-user"></span>

            <?php endif; ?>

            <h2>
                <?= ucfirst($userData->fname) .' '. ucfirst($userData->lname); ?>
            </h2>
        </div>

        <ul class="user_details">
            <li>
                <span class="icon-home3"></span>
                <div class="user_info">
                    <span>

                        <?php if (empty($userData)): ?>

                            <a href="<?= site_url('profile/add-details'); ?>" class="">
                                Setup your address details
                            </a>

                        <?php else: ?>

                            <?= $userData->address; ?>, <?= $userData->display_name; ?>

                        <?php endif; ?>

                    </span>
                </div>
            </li>
            <li>
                <span class="icon-phone"></span>
                <div class="user_info">
                    <span>

                        <?php if (empty($userData)): ?>

                            <a href="<?= site_url('profile/add-details'); ?>" class="">
                                Setup your phone number
                            </a>

                        <?php elseif (!empty($userData)): ?>

                            <?=  '+' . $userData->phonecode . '/' . $userData->phone; ?>

                        <?php endif; ?>

                    </span>
                </div>
            </li>
            <li>
                <span class="icon-envelope"></span>
                <div class="user_info">
                    <?php if (empty($userData)): ?>
                        <span>
                            <a href="<?= site_url('profile/add-details'); ?>" class="">
                                Setup your address details
                            </a>
                        </span>
                    <?php else : ?>
                        <span>
                             <?=  $userData->email; ?>
                        </span>
                    <?php endif; ?>
                </div>
            </li>
        </ul>
    </div>
    <div class="current_delivery_address">
        <div class="card_header">

            <?php if (empty($userData)): ?>

                <a href="<?= site_url('profile/add-details'); ?>" class="edit_pencil" title="Edit">
                    <span class="icon-pencil"></span>
                </a>

            <?php else : ?>

                <a href="<?= site_url('profile/edit-details/' . $userData->detail_id); ?>" class="edit_pencil" title="Edit">
                    <span class="icon-pencil"></span>
                </a>

            <?php endif; ?>

            <span class="icon-location"></span>
            <h2>
                <?php if (empty($userData)): ?>

                    <a href="<?= site_url('profile/add-details'); ?>" class="">
                         Setup your address details
                    </a>

                <?php else: ?>
                
                    <?= $userData->address; ?>, <?= $userData->display_name; ?>

                <?php endif; ?>
            </h2>
        </div>
        <ul class="current_address_details">
            
            <li>

                <?php if (empty($userData)): ?>

                    <a href="<?= site_url('profile/add-details'); ?>" class="">
                        Setup your address details
                    </a>

                <?php else: ?>

                    <?= $userData->city; ?>, <?= $userData->display_name; ?>

                <?php endif; ?>

            </li>
            
            <li>

                <?php if (empty($userData)): ?>

                    <a href="<?= site_url('profile/add-details'); ?>" class="">
                        Setup your address details
                    </a>

                <?php else: ?>

                    <?= $userData->address; ?>,  <?= $userData->zip_code; ?>

                <?php endif; ?>

            </li>
            
            <li>
                <?php if (empty($userData)): ?>
                    <a href="<?= site_url('profile/add-details'); ?>" class="">
                         Setup your address details
                    </a>
                <?php else: ?>
                    <?= ucfirst($userData->fname) .' '. ucfirst($userData->lname); ?>
                <?php endif; ?>
            </li>
            
        </ul>
    </div>
</div>
<div class="profile_menu">
    <ul class="tabs profile_tabs">
        <li class="tab tab_profile">
            <a href="<?= site_url('profile/orders'); ?>">Order History</a>
        </li>
        <li class="tab">
            <a href="<?= site_url('profile'); ?>">Profile Details</a>
        </li>
        <li class="tab">
            <a href="<?= site_url('profile/personal-details'); ?>">Delivery Address</a>
        </li>
        <li class="tab">
            <a href="<?= site_url('profile/billing-address'); ?>">Billing Address</a>
        </li>
    </ul>
</div>



