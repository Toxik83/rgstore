<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="main_content_full">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>

                <?php if($this->session->flashdata('success') != null ) :?>

                    <div class="notification success">
                        <span class="icon-success"></span>
                        <p class="message">
                            <?= $this->session->flashdata('success'); ?>
                        </p>
                    </div>

                <?php endif; ?>
            </div>

            <div class="panel">
                <h2 class="login_title">
                    Your order history:
                </h2>

                <?php if (empty($orders)): ?>

                    <h2 class="false_at current_accordion">
                        Order № 0
                    </h2>
                    <table class="order_table">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="9" class="no_data">
                                You have no orders yet.
                            </td>
                        </tr>
                        </tbody>
                    </table>

                <?php else: ?>

                <div class="accordion">

                    <?php foreach ($orders as $key => $order): ?>

                        <div class="accordion_section">
                            <h2 class="accordion_title" data-id='<?= $order->order_id; ?>'>
                                Order № <?= $order->order_id; ?> / <?= $order->date_added; ?>
                            </h2>
                            <div class="accordion_content">
                                <div class="order_list">
                                    <table class="order_table">
                                        <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php $orderDetails = getOrderDetails($order->order_id); ?>

                                        <?php 
                                        $subtotal = '';
                                        foreach ($orderDetails as $key => $detail): ?>
                                        <?php $subtotal+= $detail->total_price; ?>
     
                                            <tr>
                                                <td>
                                                    <?= $detail->title; ?>
                                                </td>
                                                <td>
                                                    <?= $detail->quantity; ?>
                                                </td>
                                                <td>
                                                    &pound;
                                                    <?= $detail->price; ?>
                                                </td>
                                                <td>
                                                    &pound;
                                                    <?= $detail->total_price; ?>
                                                </td>
                                            </tr>

                                        <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="order_address">
                                    <table class="order_table">
                                        <thead>
                                        <tr>
                                            <th>Delivery Address</th>
                                            <th>Order Subtotal</th>
                                            <th>Shipping Cost</th>
                                            <th>Order Total</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <ul>
                                                    <li>
                                                        <?= $detail->address ?>
                                                    </li>
                                                    <li>
                                                        <?= $detail->city ?>
                                                    </li>
                                                    <li>
                                                        <?= $detail->country_name ?>
                                                    </li>
                                                    <li>
                                                        + <?= $detail->phone_code; ?><?= $detail->phone ?>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                &pound; <?= $subtotal; ?>
                                            </td>
                                            <td>
                                                &pound; <?= $detail->order_price - $subtotal; ?>
                                            </td>
                                            <td class="order_total">
                                                &pound; <?= $detail->order_price; ?>
                                            </td>
                                            <td>

                                                <?php if ($detail->status == 0): ?>

                                                    Pending

                                                <?php elseif ($detail->status == 1): ?>

                                                    Completed

                                                <?php elseif ($detail->status == 3): ?>

                                                    Paid

                                                <?php else: ?>

                                                    Canceled

                                                <?php endif; ?>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                    <?php endforeach; ?>

                </div>
                <div class="pagination">
                    <?= $pagination; ?>
                </div>

                <?php endif; ?>

            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>
