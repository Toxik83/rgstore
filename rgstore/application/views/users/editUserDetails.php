<?php $this->load->view('includes/header'); ?>
    <section>
        <div class="content_container">
            <div class="column_wrap">
                <div class="profile_form_breadcrumb">
                    <div class="breadcrumbs">
                        <?= $this->breadcrumb->output(); ?>
                    </div>
                </div>       

                <div class="profile_form">
                    <h2>Profile details</h2>

                    <?php if ($userData->title === 'Mr'): ?>

                        <span class="icon-user2"></span>

                    <?php else: ?>

                        <span class="icon-user"></span>

                    <?php endif; ?>

                    <p class="compulsory">
                        All fields indicated with * are compulsory.
                    </p>
                    <form id="editUserForm" method="post" class="login_form details_form">
                        <div class="custom_select">
                            <div class="select product">
                                <div class="select_text icon-triangle-down">
                                    <p>
                                        <?= $postData['title']; ?>
                                    </p>
                                </div>
                                <select name="title">
                                    <option value="0" <?= ($postData['title'] == 'Ms') ? 'selected' : ''; ?>>Ms</option>
                                    <option value="1" <?= ($postData['title'] == 'Mr') ? 'selected' : ''; ?>>Mr</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="userId" value="<?= $postData['id']; ?>"/>
                        <label for="fname">
                            <span class="required">*</span>
                            First name:
                        </label>
                        <input type="text" id="fname" class="user" name="fname" value="<?= $postData['fname']; ?>"/>

                        <?php if (form_error('fname')) : ?>

                            <div class="form_error">
                                <?= form_error('fname'); ?>
                            </div>

                        <?php endif; ?>

                        <label for="lname">
                            <span class="required">*</span>
                            Last name:
                        </label>
                        <input type="text" id="lname" class="user" name="lname" value="<?= $postData['lname']; ?>"/>

                        <?php if (form_error('lname')) : ?>

                            <div class="form_error">
                                <?= form_error('lname'); ?>
                            </div>

                        <?php endif; ?>

                        <label for="email">
                            <span class="required">*</span>
                            Email:
                        </label>
                        <input type="email" id="email" class="user" name="email" value="<?= $postData['email']; ?>"/>
                        <input type="hidden" id="emailHidden" class="user" name="emailHidden" value="<?= $email['email']; ?>"/>

                        <?php if (form_error('email')) : ?>

                            <div class="form_error">
                                <?= form_error('email'); ?>
                            </div>

                        <?php endif; ?>

                        <label for="currPassword">
                            <span class="required">*</span>
                            Current password:
                        </label>
                        <input type="password" id="currPassword" class="user" name="currPassword"/>

                        <?php if (form_error('currPassword')) : ?>

                            <div class="form_error">
                                <?= form_error('currPassword'); ?>
                            </div>

                        <?php endif; ?>

                        <label for="password">
                            New password:
                        </label>
                        <input type="password" id="password" class="user" name="password"/>

                        <?php if (form_error('password')) : ?>

                            <div class="form_error">
                                <?= form_error('password'); ?>
                            </div>

                        <?php endif; ?>

                        <label for="passconf">
                            Confirm new password:
                        </label>
                        <input type="password" id="passconf" class="user" name="passconf"/>

                        <?php if (form_error('passconf')) : ?>

                            <div class="form_error">
                                <?= form_error('passconf'); ?>
                            </div>

                        <?php endif; ?>

                        <button type="submit" name="update" id="update" class="profile_button">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('includes/footer'); ?>