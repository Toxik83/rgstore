<?php $this->load->view('includes/header'); ?>
    <section>
        <div class="content_container">
            <div class="column_wrap">
                <div class="main_content_full">
                    <div class="breadcrumbs">
                        <?= $this->breadcrumb->output(); ?>
                    </div>

                    <?php if($this->session->flashdata('success') != null ) :?>

                        <div class="notification success">
                            <span class="icon-success"></span>
                            <p class="message">
                                <?= $this->session->flashdata('success'); ?>
                            </p>
                        </div>

                    <?php endif; ?>

                </div>

                <?php $this->load->view('users/usersMenu', $userData); ?>

                <div class="panel">
                    <h2 class="login_title">
                        Your account information:
                    </h2>
                    <ul class="user_cards" data-columns="2">
                        <li>
                            <div class="user_cards_content">
                                <a href="<?= site_url('profile/edit-userinfo/' . $userDetails->id); ?>" class="edit_pencil" title="Edit">
                                    <span class="icon-pencil"></span>
                                </a>
                                <h2 class="checkout_tittle">
                                    Profile details
                                </h2>
                                <ul class="profile_info">
                                    <li class="user_data">

                                        <?php if ($userDetails->title === 'Mr'): ?>

                                            <span class="icon-user2-small"></span>

                                        <?php else: ?>

                                            <span class="icon-user-small"></span>

                                        <?php endif; ?>

                                        <?= $userDetails->title . ' ' . ucfirst($userDetails->fname) . ' ' . ucfirst($userDetails->lname); ?>
                                    </li>
                                    <li class="user_data">
                                        <span class="icon-envelope"></span>
                                        <?= $userDetails->email; ?>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="user_cards_content">
                                <h2 class="checkout_tittle">
                                    Linked accounts
                                </h2>
                                <ul class="linked_accounts">
                                    <li class="social_account">
                                        <span class="icon-facebook2"></span>
                                        <span>Facebook account</span>
                                        <?php if($userDetails->facebook_id != null): ?>
                                            <span>Linked</span>
                                            <a href="<?=site_url('unlink/facebook')?>">Unlink</a>
                                        <?php else: ?>
                                            <span>Not linked</span>
                                            <a href="<?=site_url('login/facebook?go=go')?>">Link</a>
                                        <?php endif;?>
                                    </li>
                                    <li class="social_account">
                                        <span class="icon-twitter2"></span>
                                        <span>Twitter account</span>
                                        <?php if($userDetails->twitter_id != null): ?>
                                            <span>Linked</span>
                                            <a href="<?=site_url('unlink/twitter')?>">Unlink</a>
                                        <?php else: ?>
                                            <span>Not linked</span>
                                            <a href="<?=site_url('login/twitter?go=go')?>">Link</a>
                                        <?php endif;?>
                                    </li>
                                    <li class="social_account">
                                        <span class="icon-google-plus-square"></span>
                                        <span>Google+ account</span>
                                        <?php if($userDetails->google_id != null): ?>
                                            <span>Linked</span>
                                            <a href="<?=site_url('unlink/google')?>">Unlink</a>
                                        <?php else: ?>
                                            <span>Not linked</span>
                                            <a href="<?=site_url('login/google?go=go')?>">Link</a>
                                        <?php endif;?>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('includes/footer'); ?>