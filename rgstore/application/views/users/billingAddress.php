<?php $this->load->view('includes/header'); ?>
    <section>
        <div class="content_container">
            <div class="column_wrap">
                <div class="main_content_full">
                    <div class="breadcrumbs">
                        <?= $this->breadcrumb->output(); ?>
                    </div>
                </div>
            
                <?php if ($msg = $this->session->flashdata('success')) : ?>

                    <div class="notification success">
                        <span class="icon-success"></span>
                        <p class="message">
                            <?php echo $msg; ?>
                        </p>
                    </div>

                <?php endif; ?>
                
                <?php if ($msg = $this->session->flashdata('accDetails')) : ?>

                    <div class="notification warning">
                        <span class="icon-times-circle"></span>
                        <p class="message">
                            <?php echo $msg; ?>
                        </p>
                    </div>

                <?php endif; ?>
                
                <?php $this->load->view('users/usersMenu', $userData); ?>          
                
                <div class="panel">
                    <h2 class="login_title">
                        Your billing addresses:
                    </h2>
                    
                    <?php if (!empty($mainAddress)): ?>                      
                        <ul class="user_cards" data-columns="2">
                            <li>
                                <div class="user_cards_content">
                                    <a href="<?= site_url('profile/edit-billing-address/' . $mainAddress->id); ?>" class="edit_pencil" title="Edit">
                                        <span class="icon-pencil"></span>
                                    </a>                          
                                    <h2 class="checkout_tittle">
                                        Current Billing address
                                    </h2>
                                    <div class="location">
                                        <span class="icon-billing"></span>
                                        <ul class="address_card">
                                            <li class="address_info">
                                                <?= $mainAddress->b_city; ?>, <?= $mainAddress->display_name; ?>
                                            </li>
                                            <li class="address_info">
                                                <?= $mainAddress->b_address; ?>, <?= $mainAddress->b_zip_code; ?>
                                            </li>
                                            <li class="address_info">
                                                <?= '+' . $mainAddress->b_phone_code . '/' . $mainAddress->b_phone; ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div> 
                            </li>
                        
                            <?php foreach ($billingAddresses as $key => $address): ?>

                                <li>
                                    <div class="user_cards_content">
                                        <a href="<?= site_url('profile/main-billing-address/' . $address->id); ?>" class="make_current">
                                            <span>Make current</span>
                                        </a>
                                        <a onclick="return confirm('Are you sure?')" href="<?= site_url('profile/remove/' . $address->id); ?>" class="remove_address" title="Remove">
                                            <span class="icon-cancel"></span>
                                        </a>
                                        <a href="<?= site_url('profile/edit-billing-address/' . $address->id); ?>" class="edit_pencil" title="Edit">
                                            <span class="icon-pencil"></span>
                                        </a>
                                        <h2 class="checkout_tittle">
                                            Billing address
                                        </h2>
                                        
                                        <div class="location">
                                             <span class="icon-billing"></span>
                                            <ul class="address_card">
                                                <li class="address_info">
                                                    <?= $address->b_city; ?>, <?= $address->display_name; ?>
                                                </li>
                                                <li class="address_info">
                                                    <?= $address->b_address; ?>, <?= $address->b_zip_code; ?>
                                                </li>
                                                <li class="address_info">
                                                    <?= '+' . $address->b_phone_code . '/' . $address->b_phone; ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>

                            <?php endforeach; ?>
                            
                            <li>
                                <div class="empty_delivery_address">
                                    <div class="fill">
                                        <a href="<?= site_url('profile/add-billing-address'); ?>" class="fill">
                                            <span class="icon-plus"></span>
                                            Add billing address
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <?php else : ?>
                            <ul class="user_cards" data-columns="2">
                                <li>
                                    <div class="empty_delivery_address">
                                        <div class="fill">
                                            <a href="<?= site_url('profile/add-billing-address'); ?>">
                                                <span class="icon-plus"></span>
                                                Add billing address
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
    </section>
<?php $this->load->view('includes/footer'); ?>