<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="main_content_full">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>

                <?php if($this->session->flashdata('success') != null ) :?>

                    <div class="notification success">
                        <?= $this->session->flashdata('success'); ?>
                    </div>

                <?php endif; ?>
                
                <?php if ($this->session->flashdata('accDetails') != null) : ?>

                    <div class="notification warning">
                        <span class="icon-times-circle"></span>
                        <p class="message">
                            <?= $this->session->flashdata('accDetails'); ?>
                        </p>
                    </div>

                <?php endif; ?>

            </div>

            <?php $this->load->view('users/usersMenu', $userData); ?>

            <div class="panel">
                <h2 class="login_title">
                    Your delivery addresses:
                </h2>

                <?php if (empty($userData)): ?>
                    <ul class="user_cards" data-columns="2">
                        <li>
                            <div class="empty_delivery_address">
                                <div class="fill">
                                    <a href="<?= site_url('profile/add-details'); ?>">
                                        <span class="icon-plus"></span>
                                        Add delivery address
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                <?php else : ?>

                <ul class="user_cards" data-columns="2">
                    <li>
                        <div class="user_cards_content">
                            <a href="<?= site_url('profile/edit-details/' . $userData->detail_id); ?>" class="edit_pencil" title="Edit">
                                <span class="icon-pencil"></span>
                            </a>
                            <h2 class="checkout_tittle">
                                Current delivery address
                            </h2>
                            <div class="location">
                                <span class="icon-location"></span>
                                <ul class="address_card">
                                    <li class="address_info">
                                        <?= $userData->city; ?>, <?= $userData->display_name; ?>
                                    </li>
                                    <li class="address_info">
                                        <?= $userData->address; ?>, <?= $userData->zip_code; ?>
                                    </li>
                                    <li class="address_info">
                                        <?= '+' . $userData->phonecode . '/' . $userData->phone; ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    
                    <?php foreach ($deliveryAddresses as $key => $address): ?>
                        <li>
                            <div class="user_cards_content">
                                <a href="<?= site_url('profile/edit-details/' . $address->detail_id); ?>" class="edit_pencil" title="Edit">
                                    <span class="icon-pencil"></span>
                                </a>
                                <a href="<?= site_url('profile/change-main/' . $address->detail_id); ?>" class="make_current">
                                    <span>Make current</span>
                                </a>
                                <a onclick="return confirm('Are you sure?')" href="<?= site_url('profile/remove-details/' . $address->detail_id); ?>" class="remove_address" title="Remove">
                                    <span class="icon-cancel"></span>
                                </a>
                                <h2 class="checkout_tittle">
                                    Delivery address
                                </h2>
                                <div class="location">
                                    <span class="icon-location"></span>
                                    <ul class="address_card">
                                        <li class="address_info">
                                            <?= $address->city; ?>, <?= $address->display_name; ?>
                                        </li class="address_info">
                                        <li class="address_info">
                                            <?= $address->address; ?>, <?= $address->zip_code; ?>
                                        </li>
                                        <li class="address_info">
                                            <?= '+' . $address->phonecode . '/' . $address->phone; ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    
                    <li>
                        <div class="empty_delivery_address">
                            <div class="fill">
                                <a href="<?= site_url('profile/add-details'); ?>">
                                    <span class="icon-plus"></span>
                                    Add delivery address
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>


                <?php endif; ?>

            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>
