<?php $this->load->view('includes/header'); ?>
<section>
    <div class="content_container">
        <div class="column_wrap">
            <div class="profile_form_breadcrumb">
                <div class="breadcrumbs">
                    <?= $this->breadcrumb->output(); ?>
                </div>
            </div>

            <div class="profile_form">
                <h2>Address details</h2>
                <span class="icon-location"></span>
                <p class="compulsory">
                    All fields indicated with * are compulsory.
                </p>
                <form method="post" class="login_form">
                    <label for="address">
                        <span class="required">*</span>
                        Address
                    </label>
                    <input type="text" id="address" name="address" value="<?= $postData['address']; ?>"/>

                    <?php if (form_error('address')) : ?>

                        <div class="form_error">
                            <?= form_error('address'); ?>
                        </div>

                    <?php endif; ?>

                    <label for="city">
                        <span class="required">*</span>
                        City
                    </label>
                    <input type="text" id="city" name="city" value="<?= $postData['city']; ?>"/>

                    <?php if (form_error('city')) : ?>

                        <div class="form_error">
                            <?= form_error('city'); ?>
                        </div>

                    <?php endif; ?>

                    <label for="zip_code">
                        <span class="required">*</span>
                        Zip code
                    </label>
                    <input type="text" id="zip_code" name="zip_code" value="<?= $postData['zip_code']; ?>"/>

                    <?php if (form_error('zip_code')) : ?>

                        <div class="form_error">
                            <?= form_error('zip_code'); ?>
                        </div>

                    <?php endif; ?>

                    <label for="country">
                        <span class="required">*</span>
                        Country
                    </label>
                    <div class="custom_select">
                        <div class="select product">
                            <div class="select_text icon-triangle-down">
                                <p>
                                    Choose country
                                </p>
                            </div>
                            <input type="hidden" id="country_id" name="countryIdHidden"
                                   value="<?= $postData['countryIdHidden']; ?>"/>
                            <select name="country_id" id="countries">
                                <option disabled value="0" selected="selected">Select a country</option>

                                <?php foreach ($countryData as $key => $country): ?>

                                    <option
                                        value="<?= $country->id; ?>" <?= ($postData['countryIdHidden'] == $country->id) ? 'selected' : ''; ?>>
                                        <?= $country->display_name; ?>
                                    </option>

                                <?php endforeach; ?>

                            </select>
                        </div>
                    </div>

                    <?php if (form_error('country_id')) : ?>

                        <div class="form_error">
                            <?= form_error('country_id'); ?>
                        </div>

                    <?php endif; ?>

                    <label for="phone">
                        <span class="required">*</span>
                        Phone
                    </label>
                    <span id="code"><?= '+' . $postData['phoneCode']; ?></span>
                    <input type="text" id="phone" name="phone" value="<?= $postData['phone']; ?>"/>
                    <input type="hidden" id="phoneCode" name="phoneCode" value="<?= $postData['phoneCode']; ?>"/>

                    <?php if (form_error('phone')) : ?>

                        <div class="form_error">
                            <?= form_error('phone'); ?>
                        </div>

                    <?php endif; ?>

                    <button type="submit" name="add" class="profile_button">Save</button>
                </form>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/footer'); ?>

