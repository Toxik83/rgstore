<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
    <div class="admin_breadcrumbs">
        <?php echo $this->breadcrumb->output(); ?>
    </div>
        <h1>Message</h1>
            <div>
                <a href="<?php echo site_url('admin/admin-inbox/reply/'.$messageData->id); ?>">Reply</a>
                <table class="admin_table">
                    <tr>
                        <td><p>From: <?= $messageData->from ;?></p></td>
                    </tr>
                    <tr>
                        <td><p>Subject: <?= $messageData->subject ;?></p></td>
                    </tr>
                    <tr>
                        <td><p>Message: <?= $messageData->message ;?></p></td>
                    </tr> 
                </table>
            </div>
            <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
        </div>
</section>

<script>
    function goBack() {
        window.history.back();
    }
</script>
<?php $this->load->view('admin/includes/footer'); ?>
