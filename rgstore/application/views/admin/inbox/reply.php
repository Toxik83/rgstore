<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">

        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <h1>Reply</h1>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="alert alert-danger alert-dismissable">
                <?php echo $msg; ?>
            </p>
        <?php endif; ?>

        <?= validation_errors('<div class="error">', '</div>'); ?>

        <form action="<?= site_url('admin/admin-inbox/reply/' . $postData['id']); ?>" method="post">   
            <table>
                <tr>               
                    <td><label for="from">Title</label></td>
                    <td>
                        <input type="text" class="text_field" id="from" name="from" placeholder="From" value="<?= $postData['from']; ?>" />   
                    </td>
                </tr>

                <tr>               
                    <td><label for="subject">Subject</label></td>
                    <td>
                        <input type="text" class="text_field" id="subject" name="subject" placeholder="Subject" value="<?= $postData['subject']; ?>" />   
                    </td>
                </tr>

                <tr>               
                    <td><label for="message">Message</label></td>
                    <td>
                        <textarea name="message" id="message" rows="10" cols="80"><?= $postData['message']; ?></textarea>  
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" name="send" value="Send" class="dark_button"/>
                    </td>
                </tr>

            </table>

        </form>
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    </div>
</section>

<script>
    function goBack() {
        window.history.back();
    }
</script>
<?php $this->load->view('admin/includes/footer'); ?>
