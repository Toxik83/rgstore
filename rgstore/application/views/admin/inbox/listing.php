<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <h1>Inbox</h1>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="success">
                <?= $msg; ?>
            </p>
        <?php endif; ?>
  
        <div>
            <p id="success"></p>
        </div>
        <form method="get" action="<?= site_url('admin/admin-inbox'); ?>" class="admin_search">
            <div class="custom_select">
                <div class="select">
                    <div class="select_text fa fa-caret-down">
                        <p>
                            From
                        </p>
                    </div>
                    <select name="searchBy">
                        <option value="1" <?= ($filter == 'Form') ? 'selected' : '';?>>From</option>
                        <option value="3" <?= ($filter == 'Subject') ? 'selected' : '';?>>Subject</option>
                        <option value="4" <?= ($filter == 'Message') ? 'selected' : '';?>>Message</option>
                    </select>
                </div>
            </div>
            <input type="text" name="search" value="<?= $search; ?>" class="admin_chat_text" />
            <button type="submit" name="submit" value="Search" class="admin_search_button">Search</button>
        </form>
        
        <?php if (!empty($messageData)): ?>
        <form method="post" action="<?= site_url('admin/admin-inbox/bulk-select'); ?>"
              class="admin_slider_form">
            <div class="inbox_dropdown">
                <div class="custom_select">
                    <div class="select">
                        <div class="select_text fa fa-caret-down">
                            <p>
                                From
                            </p>
                        </div>
                        <select name="option">
                            <option selected>Delete</option>
                            <option>Mark as read</option>
                            <option>Mark as unread</option>
                        </select>
                    </div>
                </div>
                <button type="submit" name="doAction" value="Submit" class="dark_button">Submit</button>
            </div>

            <br>
            <br>
            <table class="admin_table">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="main-checkbox"><label for="main-checkbox">Select item</label></td>
                        <th>From</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Sent at</th>
                        <th>Status</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>

                <?php foreach ($messageData as $key => $message): ?>
                    <tr>
                        <td>
                            <input class="bulk-checkbox" type="checkbox" value="<?php echo $message->id; ?>"
                                   name="bulkSelect[]">
                        </td>
                        <td><a href="<?php echo site_url('admin/admin-inbox/message/'.$message->id); ?>"><?php echo $message->from; ?></a></td>
                        <td><?php echo $message->subject; ?></td>
                        <td><?php echo substr($message->message, 0, 20); ?></td>
                        <td><?php echo $message->sent_at; ?></td>
                        <td><?php echo ($message->status == 1) ? 'Read' : 'Unread'; ?></td>
                        <td>
                            <a onclick="event.preventDefault();" href="<?php echo site_url('admin/admin-inbox/delete/'.$message->id); ?>" data-id="<?php echo $message->id; ?>" id="delete-<?php echo $message->id; ?>"
                               class="delete-row">Delete
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>

            <?php else : ?>
                <p class="error">
                    No results found.
                </p>

            <?php endif; ?>

        </form>

        <?php echo $pagination; ?>
        
    </div>
</section>
<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure?');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/index.php/admin/adminInbox/ajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                            $('#success').addClass('success');
                            $('#success').text('You have successfuly deleted the item');
                        }                   
                    }
                });
            }
        });
    });
</script>

<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });
</script>


<?php $this->load->view('admin/includes/footer'); ?>

