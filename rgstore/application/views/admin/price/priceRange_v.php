<?php $this->load->view('admin/includes/header'); ?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>

        <h1>Create price range</h1>

        <?php
        echo form_open('admin/priceRange');
        echo validation_errors('<div class="error">', '</div>');
        ?>

        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>
                
        <div id="success"></div>
        <br>

        <table cellpadding="2" cellspacing="2" class="create_table small">
            <tr>
                <td class="label_small_table"><label for ='min'>MIN:</label></td>
                <td><input id='min' type="text" name="min" required placeholder="Enter min price" value="<?= $postData['min']; ?>"></td>
            </tr>    
            <tr>
               <td class="label_small_table"><label for ='max'>MAX:</label></td>
                <td><input id='max' type="text" name="max" required placeholder="Enter max price" value="<?= $postData['max']; ?>"></td>
            </tr>
            <tr>
                <td colspan="2"><?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Create', 'class' => 'dark_button')); ?></td>
            </tr>
            <?= form_close(); ?>
        </table>

        <br>
        <br>
        <hr>
        <br>
        <br>
        <h3>List of All Price Ranges</h3>

        <form method="post" action="<?= site_url('admin/priceRange/bulkDelete1'); ?>"
              class="admin_slider_form">


            <?php if (isset($prices) && $prices) : ?>
            <?php else : ?>
                <p style="color:red; ">
                    No data 
                </p>
            <?php endif; ?>
            <br>


            <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                    href="<?= site_url('admin/priceRange/bulkDelete1'); ?>"
                    class="dark_button">Delete selected
            </button>
            <p></p>
            <table border="1" class="admin_table" >

                <tr>
                    <th></th>
                    <th><input type="checkbox" id="main-checkbox"><label for="main-checkbox">Select items</label></th>
                    <th>ID</th>  
                    <th>Min</th> 
                    <th>Max</th> 
                    <th>Date created</th> 
                    <th>Edit</th> 
                    <th>Delete</th> 
                </tr>

                <tbody id="sortable">
                    <?php foreach ($prices as $key => $value): ?>
                        <tr>
                            <td><i class="fa fa-arrows-v"></td>
                            <td>
                                <input class="bulk-checkbox" type="checkbox" value="<?= $value->id; ?>"
                                       name="bulkDelete[]"> 
                            </td>
                            <td>
                                <?= $value->id; ?>
                                <input type="hidden" class="order" name="order[]" value="<?= $value->id; ?>"/>
                            </td>
                            <td><?= $value->min;?></td>
                            <td><?= $value->max; ?></td>
                            <td><?= $value->date; ?></td>
                            <td>    
                                <?= anchor('admin/price-range/edit/' . $value->id, 'Edit'); ?>     
                            </td> 
                            <td>
                                <button type="button" data-id="<?= $value->id; ?>" id="delete-<?= $value->id; ?>"
                                        class="delete-row admin_link_button" class="admin_button">Delete
                                </button>
                            </td> 
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
            <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
        </form>
        <?= $links; ?>
<script>
function goBack() {
    window.history.back();
}
</script>
<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure ? ');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: 'priceRange/ajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                            $('#success').addClass('success').text('You have successfuly deleted the price range .' );
                        }
                        if (data.success === 0) {
                            $('#success').addClass('error').text('You have products in your category!' );
                        }
                        
                    }

                });
            }
        });
    });
</script>

<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });
</script>
<script>
    $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            update: function () {
                var $vals = $('.order').map(function () {
                    return $(this).val();
                }).get();
                $.ajax({
                    url: 'priceRange/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                     success:function () {
                            $('#success').addClass('success').text('You have successfully reorder the price ranges :) ');
                     }
                })
            }
        });
        $("#sortable").disableSelection();
    });
</script>
</section>



