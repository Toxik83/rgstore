<?php $this->load->view('admin/includes/header'); ?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
        <h2>Edit Price Range</h2> 
            <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>
                
                
        <?= form_open('admin/priceRange/editRange'); ?>

        <input type="hidden" name="id" value="<?= $this->uri->segment(4); ?> "/>

        <table cellpadding="2" cellspacing="2" class="create_table small">
            <tr>
                <td><label>ID: </label></td>
                <td><input readonly type="text" value="<?= $source->id;?>"></td>
            </tr>          
            <tr>
                <td class="label_small_table"><label for='min'>MIN:</label></td>
                <td><input id='min' type="text" name="min" required value="<?= $source->min; ?>"  ></td>
            </tr>
            <tr>
                <td class="label_small_table"><label for ='max'>MAX:</label></td>
                <td><input id='max' type="text" name="max" required value="<?= $source->max; ?>" ></td>
            </tr>
            <tr>
                <td class="label_small_table"><label for="date">Date: </label></td>
                <td><input id="date" type="text" name="date" required value="<?= $source->date; ?>" ></td>
            </tr>
            <tr>
                <td colspan="2"><?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Edit', 'class' => 'dark_button')); ?></td>
            </tr>
        </table>
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
        <?= form_close(); ?>

</section>
<script>
function goBack() {
    window.history.back();
}
$('input[readonly]').focus(function(){
    this.blur();
});
</script>



