<?php $this->load->view('admin/includes/header'); ?>
<section>
        <div class="admin_content_container"
             <div class="admin_form">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
        <?= form_error(); ?>             
        <?php
        $image = array(
            'name' => 'userfile',
        );
        $submit = array(
            'name' => 'submit',           
            'value' => 'Upload'
        );
        ?>
            <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif ($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
            <?php endif; ?>
            <div id="success"></div>        
            <br>
            <?= form_open_multipart('admin/printersgallery/save'); ?>
            <?= validation_errors('<div class="error">', '</div>'); ?>
            <table border="1" class="admin_table">
            <tr>
                <th>Add image</th>
                <th>Send</th>
            </tr>
            <tr>
            <td>
            <?= form_upload($image); ?>
            <input type="hidden" name="printer_id" value="<?php echo $this->uri->segment(4); ?> "/>
            </td>
            <td>
            <?= form_submit($submit); ?>
            </td>
            </tr>
            </table>
            <?= form_close(); ?>
            <br>
            <br>
            <h3>All images for printer with ID: <?= $this->uri->segment(4); ?></h3>
            <br>
            <div class="admin_margin">
                <table border="1" cellpadding="3px;" class="admin_table">                    
                    <tr><th>Reorder</th>
                        <th>Image ID</th>
                        <th>Printer Title</th>
                        <th>Image</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Main Picture</th>
                    </tr>
                    <tbody id="sortable">
                    <?= form_open('admin/printersgallery/make_main_pic','id="makeItMain"'); ?>
                    <?php foreach ($images as $image): ?>
                    
                    <tr><td><i class="fa fa-arrows-v"></td>
                        <td><?= $image->id; ?></td>
                        <td><?= $image->title; ?></td>
                        <td><img src="<?= base_url('upload/images155x155/' . $image->name); ?>"/></td>
                        <input type="hidden" class="order" name="order[]" value="<?= $image->id; ?>"/>
                        <td>
                            <a href="<?= site_url(); ?>admin/printersgallery/update/<?= $image->id; ?>/<?= $image->product_id; ?>">Edit</a>
                        </td>
                        <td>
                            <a href="<?= site_url(); ?>admin/printersgallery/delete/<?= $image->id; ?>/<?= $image->product_id; ?>" onClick="return confirmDialog();">Delete</a>
                        </td>
                        <td>
                            <input type="radio" name="image_check" value="<?= $image->id; ?>" <?php if($image->main_pic == 1) : ;?> checked = "checked" <?php  endif; ?>/> main picture
                            <input type="hidden" name="printer_id" value="<?= $image->product_id ;?>"/>
                        </td>
                        </tr>
                        <?php endforeach; ?>
                        <?= form_close(); ?>
                    </tbody>
                </table>
            </div>
            <br>
            <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
        </div>

    </section>
<script>
    $(document).ready(function () {
    $('input[name=image_check]').on('change', function () {
            $('#makeItMain').submit();
        });
    });
</script>
<script>
function goBack() {
    window.history.back();
}
</script>
<script>
    $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            update: function(){
                    var $vals=$('.order').map(function(){
                        return $(this).val();
                    }).get();
                    $.ajax({
                    url: '/index.php/admin/printersgallery/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                    error: function(){
                        alert('Error');
                    }
                    })}
                });
                $("#sortable").disableSelection();
            });
</script>

<?php $this->load->view('admin/includes/footer'); ?>