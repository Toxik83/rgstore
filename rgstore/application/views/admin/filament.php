<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>

        <h1>Filaments</h1>
        <h4><?php echo anchor('admin/filament/addFilament', 'Create Filament'); ?>&nbsp;&nbsp;&nbsp;</h4>
        <br>
        <a href="<?= site_url(); ?>admin/finishing/createCategory" style="color:green;">Create Category </a> | |
        <a href="<?= site_url(); ?>admin/priceRange" style="color:green;">Create Price Range</a> | |
        <a href="<?= site_url(); ?>admin/manufacturer" style="color:green;">Create Manufacturer</a> | |
        <a href="<?= site_url(); ?>admin/filament/showInactive" style="color:green;">Show Inactive</a> 
        <br>

        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>

        <div id="success" style="color:red;"></div>
        <br>

        <h3>List of All Filaments</h3>


        <?php
        echo form_open('admin/filament', array('method' => 'get', 'class' => 'admin_search'));
        echo form_input(array('name' => 'search', 'value' => $search_term, 'class' => 'admin_chat_text','placeholder'=>'Search by Filament\'s Title'));
        echo form_button(array('name' => 'search_submit', 'type' => 'submit', 'class' => 'admin_search_button', 'content' => 'Search'));
        echo form_close();
        ?>


        <form method="post" action="<?= site_url('admin/filament/bulkDelete'); ?>"
              class="admin_slider_form">

            <?php if (isset($filaments) && $filaments) : ?>

            <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                    <?= site_url('admin/filament/bulkDelete'); ?> class="dark_button">
                Delete selected
            </button>
            <p></p>

            <table border="1" class="admin_table">

                <tr>
                    <th><input type="checkbox" id="main-checkbox" ><label for="main-checkbox">Select items</label></th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Image Gallery</th>
                    <th>Price</th>
                    <th>New Price</th>
                    <th>Manufacturer</th>
                    <th>Category</th>
                    <th>Flash Date</th>
                    <th>Date Added</th>
                    <th>Edit</th>
                    <th>Inactive</th>
                    <th>Delete</th>
                </tr>
                <?php foreach ($filaments as $key => $value): ?>
                <!--It`s because we want to use 'Edit button' functionality ... that`s why we made this sh*t .. sorry :(  -->
        </form>
                <tr>
                    <td>
                        <input class="bulk-checkbox" type="checkbox" value="<?= $value->id; ?>"
                               name="bulkDelete[]">
                    </td>
                    <td>
                        <?= $value->id; ?>
                        <input type="hidden" name='new_price_edit' value="<?= $value->id;?>">
                    </td>
                    <td><?= $value->title; ?></td>
                    <td align="center">
                        <?php if (($value->name) == NULL) { ?>
                            <a href="<?php echo site_url("/admin/filamentsgallery/loadGalleryPage/" . $value->id); ?>">

                                <img style="width: 100px;"
                                     src="<?php echo base_url('upload/images155x155/def.jpeg'); ?>"/><br>Gallery</a>

                        <?php } else { ?>
                            <a href="<?php echo site_url(); ?>/admin/filamentsgallery/loadGalleryPage/<?php echo $value->id; ?>">
                                <img style="width: 100px;"
                                     src="<?php echo base_url('upload/images155x155/' . $value->name); ?>"/>
                                <br>Gallery</a>
                        <?php } ?>

                    </td>
                    <td><?= $value->price; ?></td>
                    <td class="table_new_price">
                        <?= form_open('admin/filament/new_Price');?>
                            <?php $data = array(
                                'name' => 'new_price',
                                'value'=> $value->new_price
                            );?>
                            <?= form_input($data)?>
                        
                        <input type="hidden" name='new_price_id' value="<?= $value->id;?>">
                            
                        <?= form_button(array('type' => 'submit','content' => 'Edit','class' => 'admin_link_button'))?>
                        <?= form_close();?>
                    </td>
                    <td><?= $value->man_name; ?></td>
                    <td><?= $value->cat_name; ?></td>
                    <td align="center">
                        <?php if ($value->fl_date_begin == '0000-00-00 00:00:00' && $value->fl_date_end == '0000-00-00 00:00:00') { ?>
                            <a href="<?php echo site_url(); ?>/admin/filament/flashdate/<?php echo $value->id; ?>">

                                Add Flash Date</a>
                        <?php } else { ?> <a
                            href="<?php echo site_url(); ?>/admin/filament/flashdate_edit/<?php echo $value->id; ?>">
                            Edit Flash Date
                            <br><font color="red">begin: <?php echo $value->fl_date_begin; ?>
                                <br>end: <?php echo $value->fl_date_end; ?></font>

                            <?php }; ?>
                    </td>
                    <td><?= $value->date_added; ?></td>
                    <td>
                        <?php echo anchor('admin/filament/updateFilament/' . $value->id, 'Edit'); ?>
                    </td>
                     <td align="center" ><a href="<?= site_url(); ?>admin/filament/delete/<?= $value->id; ?>" >Make Inactive</a>
                    </td>
                    <td align="center" ><a href='<?= site_url(); ?>admin/filament/softdelete/<?= $value->id; ?>' onClick="return confirmDialog();">Delete</a>
                    </td> 
                    <?php endforeach; ?>
                </tr>

            </table>
        <?php echo $links; ?>

    <?php else : ?>
        <p class="error">
            No results found.
        </p>
        <a href="#" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    <?php endif; ?>
<script>
            $(function () {
                $('.delete-row').on('click', function () {
                    var conf = confirm('Are you sure?');
                    var id = $(this).data('id');
                    var $ele = $(this).parent().parent();
                    if (conf === true) {
                        $.ajax({
                            url: '/index.php/admin/filament/ajaxDelete',
                            data: {
                                id: id
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (data) {
                                if (data.success === 1) {
                                    $ele.remove();
                                    $('#success').text('You have successfully deleted the filament product');
                                }

                                if (data.success === 2) {
                                    $('#success').text('This product is part of a Bundle. Edit the bundle before continuing.');
                                }
                            }

                        });
                    }
                });
            });
</script>
<script>
            $(function () {
                $('#main-checkbox').change(function () {
                    var checkboxes = $(this).closest('form').find(':checkbox');
                    if ($(this).is(':checked')) {
                        checkboxes.prop('checked', true);
                    } else {
                        checkboxes.prop('checked', false);
                    }
                });
            });
</script>
    </div>
</section>




