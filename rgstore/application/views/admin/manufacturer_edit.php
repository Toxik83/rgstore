<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>

        <h1>Edit Manufacturer </h1>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>
        <?= form_error(); ?>
        <?= form_open('admin/manufacturer/edit_manufacturer'); ?>
        <input type="hidden" name="man_id" value="<?= $this->uri->segment(4); ?> "/>

        <table cellpadding="2" cellspacing="2" class="create_table small">
            <tr>
                <td><label>ID :</label></td>
                <td><?= $companies->id; ?></td>
            </tr>
            <tr>
                <td  class="label_small_table"><label for='name'>Name :</label></td>
                <td><input id='name' type="text" name="man_name" value="<?= $companies->man_name; ?>"></td>
            </tr>
            <tr>
                <td colspan="2"><?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Edit', 'class' => 'dark_button')); ?></td>
            </tr>
        </table>
        <?= form_close(); ?>
        <a href="<?php site_url(); ?>/admin/manufacturer/" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
        </a>
    </div>
</section>

<?php $this->load->view('admin/includes/footer'); ?>


