<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
                <h1>Edit Flash Date</h1>
                    <?php echo form_open('admin/printers/updateFlDate/' . $this->uri->segment(4)); ?>
                        <?php if ($msg = $this->session->flashdata('success')) : ?>
                            <p class="success"><?= $msg; ?></p>
                        <?php elseif ($msg = $this->session->flashdata('error')) : ?>
                            <p class="error"><?= $msg; ?></p>
                        <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
                        <?= $msg; ?>
                        <?php endif; ?>
                            <?= validation_errors(); ?>
                                <?php foreach ($newprice as $newp => $p) : ?>
                                    <p style="color: red; ">
                                    </p>
                                    <table cellpadding="2" cellspacing="2" class="create_table">
                                        <tr>
                                            <td>
                                                Printer name:
                                            </td>
                                            <td>
                                                <?= $p['title']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Flash Date (begin): 
                                            </td>
                                            
                                            <td>
                                                <input type="text" placeholder="Pickup Date"   name="fl_date_begin" data-field="datetime" data-format="yyyy-MM-dd hh:mm:ss"  value = "<?= $flashdates->fl_date_begin; ?>"  readonly>
                                            </td>
                                            <td>
                                                <div id="dtBox"></div>
                                                
                                            </td>                                           
                                        <tr>
                                            <td>    
                                                Flash Date (end) : 
                                            </td>
                                            <td>
                                                <input type="text" data-field="datetime" name="fl_date_end" data-format="yyyy-MM-dd hh:mm:ss"  value = "<?= $flashdates->fl_date_end; ?>" readonly placeholder="Pickup Date">
                                            </td>
                                            <td>
                                                <div id="dtBox2"></div>
                                                <input type="hidden" name="printer_id" value = "<?= $flashdates->id;?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h3>
                                                Regular Price: <?= $p['price']; ?>
                                                </h3>
                                            <td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for = "label_promoprice"> Edit promo price:</label>
                                            </td>
                                            <td>
                                                <input id = "label_promoprice" type="text" style="width: 70px;" value="<?= $p['new_price']; ?>" name="new_price"> 
                                            </td>
                                        </tr>
                                                
                                        <tr>
                                                <td colspan="2">
                                                    <?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Edit', 'class' => 'dark_button flash')); ?>
                                                    <?= form_close(); ?>
                                                </td>
                                        
                                        
                                            <?= form_open('admin/printers/flDateDelete/' . $this->uri->segment(4)); ?>
                                                <td colspan="2">
                                                    <input type ="submit" name="submit" value ="Delete" class="dark_button flash" onClick="return confirmDialog();">    
                                                    <?= form_close(); ?>
                                                </td>
                                        </tr>
        </table>
        <?php endforeach; ?>
    </div>
</section>
<script type="text/javascript">
$js = 'onClick ="return confirmDialog()"';
    $(document).ready(function ()
    {
        $("#dtBox").DateTimePicker({
            dateTimeFormat: "yyyy-MM-dd HH:mm:ss"
        });
    });


    $(document).ready(function ()
    {

        $("#dtBox2").DateTimePicker();

    });

</script>
<?php $this->load->view('admin/includes/footer'); ?>