<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <h3>Edit Filament</h3>

        <h2><?php echo $this->session->flashdata('my key'); ?></h2>

        <?php echo form_open('admin/filament/updateFilament/'.$filament->id); ?>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="success"><?= $msg; ?></p>
        <?php elseif ($msg = $this->session->flashdata('error')) : ?>
            <p class="error"><?= $msg; ?></p>
        <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
            <?= $msg; ?>
        <?php endif; ?>

        <table cellpadding="2" cellspacing="4">
            <tr>
                <td>ID:</td>
                <td><?= form_label($filament->id); ?></td>
            </tr>
            <tr>
                <td><label for="title">Title</label></td>
                <td>
                    <input type="text" id='title' name="title" value="<?php echo $filament->title; ?>"/>
                <td><input type="hidden" name="id" value="<?= $this->uri->segment(4); ?>"></td>
                </td>
            </tr>
            <tr>
                <td>
                    Text:
                </td>
                <td>
                    <?php echo form_textarea(array('name' => 'text', 'id' => 'desc', 'class' => "ckeditor", 'value' => "$filament->text")); ?>
                </td>
            </tr>
            <tr>
                <td><label for="price">Price</label></td>
                <td>
                    <input type="text" id='price' name="price" value="<?php echo $filament->price; ?>"/>
                </td>
            </tr>
            <tr>
                <td><label for="new_price"> New Price</label></td>
                <td>
                    <input type="text" id='new_price' name="new_price" value="<?php echo $filament->new_price; ?>"/>
                </td>
            </tr>
            <tr>
                <td>
                    Manufacturer :
                </td>
                <td>
                    <select name="manufacturer">
                        <?php foreach ($companies as $company): ?>
                            <option value="<?php echo $company->id; ?>" <?php if ($filament->man_id === $company->id) {
                                ; ?> selected="selected" <?php } ?>><?php echo $company->man_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php echo form_fieldset_close(); ?>
                </td>
            </tr>
            <tr>
                <td>
                    Category :
                </td>
                <td>
                    <select name="category">
                        <?php foreach ($categories as $cat): ?>

                            <option value="<?php echo $cat->id; ?>" <?php if ($filament->f_cat_id === $cat->id) {
                                ; ?> selected="selected" <?php } ?>><?php echo $cat->cat_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php echo form_fieldset_close(); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php echo form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Submit', 'class' => 'dark_button product'));
                    echo form_close(); ?>
                </td>
            </tr>
        </table>

        <a href="<?= site_url('admin/filament/'); ?>" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    </div>
</section>

<?php $this->load->view('admin/includes/footer'); ?>

