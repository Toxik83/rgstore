<?php $this->load->view('admin/includes/header'); ?>
<section>
        <div class="admin_content_container">
            <div class="admin_breadcrumbs">
                <?= $this->breadcrumb->output(); ?>
            </div>
                <h1>Manufacturer</h1>
                    
                <?= form_open('admin/manufacturer/add'); ?>
                <?= validation_errors('<div class="error">', '</div>'); ?>
                <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif ($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?> 
                    <?php if(isset($result)) : ?> 
                    <?php foreach($result as $res =>$product) :
                    echo 'id: '.$product->id.'---'.$product->title; ?>
                    
                    <?php endforeach ; ?>
                    <?php endif ;?>
                </p>
            <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
            <?php endif; ?>

            <div id="success"></div>
        
            <br>
                <label>Company Name: </label><br>
                <input type="text" name="man_name" value="" placeholder='Enter Company name'/>
                <button type="submit" class="dark_button">Add
                </button>
                <?= form_close(); ?>
                <br>
                <br>
                <hr>
                <br>
                <br>
                    <h3>List of All manufacturers</h3>
                        <?php if (isset($companies)) : ?>
                        <form action="<?= site_url('admin/manufacturer');?>" method = "get" class="admin_search">
                            <input type="text" name = "keyword" class="admin_chat_text" value="" />
                            <button type="submit" class="admin_search_button">Search
                            </button>
                        </form>
                        <form method="post" action="<?= site_url('admin/manufacturer/bulkDelete'); ?>"
                            accept-charset=""class="admin_slider_form">
                            <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                                class="dark_button">Delete selected
                            </button>
                            <p></p>
                        <table border="1" class="admin_table">
                                <tr>
                                    <th>Reorder</th>
                                    <th><input type="checkbox" id="main-checkbox"><label for="main-checkbox"> Select items</label></th>
                                    <th>ID</th>
                                    <th>Printer's technology name</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                <tbody id="sortable">
                                <?php foreach ($companies as $comp): ?>
                                    <tr><td><i class="fa fa-arrows-v"></td>
                                        <td>
                                            <input class="bulk-checkbox" type="checkbox" value="<?= $comp->id; ?>"
                                            name="bulkDelete[]"> 
                                        </td>
                                        <td><?= $comp->id; ?></td>
                                        <td><?= $comp->man_name; ?></td>
                                        <input type="hidden" class="order" name="order[]" value="<?= $comp->id; ?>"/>
                                        <td>
                                            <a href="<?= site_url(); ?>/admin/manufacturer/update/<?= $comp->id; ?>">Edit</a>
                                        </td>
                                        <td>
                                            <a href='<?= site_url(); ?>/admin/manufacturer/delete/<?= $comp->id; ?>' onclick ="return confirmDialog();" >Delete</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                         </form>
                    <?php echo $links; ?>
                    <br>
                    <?php //if($this->input->post('keyword')) : ?>
                <a href="#" class="error_go_back"  onclick="goBack()">
                    <i class="fa  fa-long-arrow-left"></i>
                    Go back to previous page
                </a>  
                    <?php //else : ?>
 <!--               <a href="<?php site_url(); ?>/admin/printers/" class="error_go_back">
                        <i class="fa  fa-long-arrow-left"></i>
                        Go back to previous pagePrinters
                </a>
                    <?php //endif ;?>
 -->
            <?php  else : ?>
            <p class="error">
                No results found.
            </p>
        <a href="#" class="error_go_back" onclick="goBack()">
            <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
        <?php endif; ?>
    </div>
</section>
<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });
</script>
<script>
    $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            update: function(){
                    var $vals=$('.order').map(function(){
                        return $(this).val();
                    }).get();
                    $.ajax({
                    url: '/index.php/admin/manufacturer/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                    error: function(){
                        alert('Error');
                    }
                    })}
                });
                $("#sortable").disableSelection();
            });
</script>
<script>
function goBack() {
    window.history.back();
}
</script>
<?php $this->load->view('admin/includes/footer'); ?>
