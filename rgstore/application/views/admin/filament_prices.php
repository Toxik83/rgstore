<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <h2>Price Range </h2>
        <form action="<?php echo site_url('admin/filament_price'); ?>" method = "post">
            <input type="text" name = "keyword" />
            <input type="submit" value = "Search" />
        </form>
        <?php if ($this->input->post()) { ?>

            <table border="1" class="admin_table">

                <tr>
                    <th>ID</th>
                    <th>Range</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>

                <?php foreach ($results as $res) : ?>
                    <tr>
                        <td><?php echo $res->id; ?></td>
                        <td><?php echo $res->price_range; ?></td>
                        <td>
                            <a href="<?php echo site_url(); ?>/admin/filament_prices/update/<?php echo $res->id; ?>">Edit</a>
                        </td>
                        <td>
                            <a href='<?php echo site_url(); ?>/admin/filament_prices/delete/<?php echo $res->id; ?>' onClick="return confirmDialog();">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <?php echo anchor('admin/range', 'Back to all reccords'); ?>
        <?php } else { ?>
            <table border="1" class="admin_table">

                <tr>
                    <th>ID</th>
                    <th>Range</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>

                <?php foreach ($range as $row): ?>
                    <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->price_range; ?></td>
                        <td>
                            <a href="<?php echo site_url(); ?>/admin/filament_prices/update/<?php echo $row->id; ?>">Edit</a>
                        </td>
                        <td>
                            <a href='<?php echo site_url(); ?>/admin/filament_prices/delete/<?php echo $row->id; ?>' onClick="return confirmDialog();">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php } ?>
        <br>

        <h3><?php echo $this->session->flashdata('range_key'); ?></h3>
        <?php echo validation_errors(); ?>
        <?php echo form_open('admin/filament_price/add'); ?>
        <label>Add Price Range: </label><br>
        <input type="text" name="range" value="" placeholder='Enter price range. Formath: 1000 - 2000'/><br><br>
        <input type="submit" value="Add"/>
        <?php echo form_close(); ?>
        <?php echo anchor('admin/printers', 'Back to Filament list'); ?>
    </div>
</section>
<?php $this->load->view('admin/includes/footer'); ?>

