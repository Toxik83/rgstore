<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        
        <h1>Bundles</h1>
        
        <h4><a href="<?= site_url('admin/bundle/create'); ?>">Create bundle</a></h4>
        <br>
        <br>
        
         <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="success">
                <?= $msg; ?>
            </p>
        <?php endif; ?>
            
        <table class="admin_table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Combined price</th>
                    <th>Price with discount</th>
                    <th>Discount(%)</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($bundles as $key => $bundle): ?>
                <tr>
                    <td><?= $bundle->product_id; ?></td>
                    <td><?= $bundle->bundle_name; ?></td>
                    <td><?= $bundle->combined_price; ?></td>
                    <td><?= $bundle->discount_price; ?></td>
                    <td><?= $bundle->discount; ?></td>
                    <td><a href="<?= site_url('admin/bundle/edit/'.$bundle->product_id); ?>">Edit</a></td>
                    <td><a onclick="return confirm('Are you sure?');" href="<?= site_url('admin/bundle/delete/'.$bundle->product_id); ?>">Delete</a></td>
                </tr>               
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</section>
<?php $this->load->view('admin/includes/footer'); ?>