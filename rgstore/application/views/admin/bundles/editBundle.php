<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>

        <h1>Edit Bundle</h1>
        
        <?= validation_errors('<div class="error">', '</div>'); ?>

        <form method="post" id="form_bundle" style="margin-bottom: 20px">
            <table class="manage_product_table">

                <tr>
                    <td style="width: 10%">
                        <label for="name">Bundle name:</label>
                    </td>
                    <td>
                        <input type="text" id="name" name="name" placeholder="Name"
                               value="<?php echo (array_key_exists('bundle_name', $postData)) ? $postData['bundle_name'] : ''; ?>"/>
                    </td>
                </tr>

                <tr>
                    <td style="width: 10%">
                        <label for="bundle">Select products:</label>
                    </td>
                    <td>                          
                        <select multiple="" name="bundle[]" id="bundle">
                            <?php foreach ($products as $key => $product): ?>      
                            <option <?php if (!empty($postData['bundle'])): ?>
                                    <?php foreach ($postData['bundle'] as $key => $id): ?>
                                        <?= ($id == $product->id) ? 'selected' : ''; ?>
                                    <?php endforeach; ?>
                                    <?php endif; ?> value="<?= $product->id; ?>"><?= $product->title; ?>
                            </option>         
                            <?php endforeach; ?>
                        </select>   
                    </td>
                </tr>

                <tr>
                    <td style="width: 10%">
                        <label for="discount">Discount:</label>
                    </td>
                    <td  style="padding: 10px 0;">
                        <select name="discount" id="discount">
                            <option value="0" selected="" disabled="">Select</option>
                            <option value="10" <?= ($postData['discount'] == 10) ? 'selected' : ''; ?>>10%</option>
                            <option value="20" <?= ($postData['discount'] == 20) ? 'selected' : ''; ?>>20%</option>
                            <option value="30" <?= ($postData['discount'] == 30) ? 'selected' : ''; ?>>30%</option>
                            <option value="40" <?= ($postData['discount'] == 40) ? 'selected' : ''; ?>>40%</option>
                            <option value="50" <?= ($postData['discount'] == 50) ? 'selected' : ''; ?>>50%</option>
                            <option value="60" <?= ($postData['discount'] == 60) ? 'selected' : ''; ?>>60%</option>
                            <option value="70" <?= ($postData['discount'] == 70) ? 'selected' : ''; ?>>70%</option>
                            <option value="80" <?= ($postData['discount'] == 80) ? 'selected' : ''; ?>>80%</option>
                            <option value="90" <?= ($postData['discount'] == 90) ? 'selected' : ''; ?>>90%</option>
                            <option value="100" <?= ($postData['discount'] == 100) ? 'selected' : ''; ?>>100%</option>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2">
                        <input type="hidden" id="total_price_hidden" name="total_price_hidden"
                               value="<?php echo $postData['total_price_hidden']; ?>"/>
                        <input type="hidden" id="discount_hidden" name="discount_hidden" value=""/>
                        <input type="hidden" id="discount_price_hidden" name="discount_price_hidden"
                               value="<?php echo $postData['discount_price_hidden']; ?>"/>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" value="Edit" class="dark_button"/>
                    </td>
                </tr>

            </table>
        </form>

        <div>
            <label>Total price of the selected products:</label>

            <p id="total_price" style="display: inline-block;"><?= $postData['total_price_hidden']; ?></p>
        </div>
        <div>
            <label>Price after discount:</label>

            <p id="discount_price" style="display: inline-block;"><?= $postData['discount_price_hidden']; ?></p>
        </div>
        
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    </div>
</section>

<script>
    $('#bundle').chosen({width: "20%"});
    $('#discount').chosen({width: "10%"});
</script>

<script>
    function goBack() {
        window.history.back();
    }
</script>

<script>
    $(function () {
        $('#bundle').on('change', function () {
            var $id = $(this).val();
            var $dcnt = $('#discount').find(":selected").val();

            $.ajax({
                url: '/index.php/admin/adminBundle/calculatePrice',
                type: 'post',
                dataType: 'json',
                data: {
                    id: $id,
                    discount: $dcnt
                },
                success: function (details) {
                    $('#total_price').text(details.price);
                    $('#discount_price').text(details.discountPrice);
                    $('#total_price_hidden').attr('value', function () {
                        return details.price;
                    });
                    $('#discount_price_hidden').attr('value', function () {
                        return details.discountPrice;
                    });
                },
                error: function () {
                    console.log('error');
                }
            });
        });
    });
</script>

<script>
    $(function () {
        $('#discount').on('change', function () {
            var $discount = $(this).val();
            var $price = $('#total_price_hidden').val();
            $('#discount_hidden').attr('value', function () {
                return $discount;
            });
            $.ajax({
                url: '/index.php/admin/adminBundle/calculateDiscount',
                type: 'post',
                dataType: 'json',
                data: {
                    discount: $discount,
                    price: $price
                },
                success: function (price) {
                    $('#discount_price').text(price);
                    $('#discount_price_hidden').attr('value', function () {
                        return price;
                    });
                },
                error: function () {
                    console.log('error discount');
                }
            });
        });
    });
</script>
<?php $this->load->view('admin/includes/footer'); ?>
