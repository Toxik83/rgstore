<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
    <?php echo $this->breadcrumb->output(); ?>
</div>
        <h3>Filament Category </h3>
        <h4><?php echo $this->session->flashdata('my_key');?></h4>
        <h4><?php echo $this->session->flashdata('my_key2');?></h4>
         <?php 
            echo form_open('admin/filament_category', 'method=get');
            echo form_input(array('name' => 'search','value' => $search_term));
            echo form_submit('search_submit', 'Search');
            echo form_close();
        ?> 
        <?php if ($this->input->post()) { ?>
            <table border="1" class="admin_table">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                  
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>

                <?php foreach ($categories as $res) : ?>
                    <tr>
                        <td><?php echo $res->id; ?></td>
                       <td><?php echo $res->cat_name; ?></td>
                        <td>
                            <a href="<?php echo site_url(); ?>/admin/filament_category/update/<?php echo $res->id; ?>">Edit</a>
                        </td>
                        <td>
                            <a href='<?php echo site_url(); ?>/admin/filament_category/delete/<?php echo $res->id; ?>' onClick="return confirmDialog();">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

        <?php } else { ?>
 
            </br>
            <table border="1" class="admin_table">
                <?php echo $this->session->flashdata('deleted'); ?>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>

                <?php foreach ($categories as $row): ?>
                    <tr>
                        <td><?php echo $row->id; ?></td>
                        <td><?php echo $row->cat_name; ?></td>
                        
                        <td>
                            <a href="<?php echo site_url(); ?>/admin/filament_category/update/<?php echo $row->id; ?>">Edit</a>
                        </td>
                        <td>
                            <a href='<?php echo site_url(); ?>/admin/filament_category/delete/<?php echo $row->id; ?>' onClick="return confirmDialog();">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php } ?>
        <br>
        <?php echo form_open('admin/filament_category/add'); ?>
         
        
                     <label for="cat">Category Name</label>
                    <input id='cat' type="text" name="name" placeholder="Category name"
                           value="" class="admin_input"/>
                    
        <?php echo form_submit('submit', 'Add');
        echo '</p>';
                    
                echo form_close();
        ?>

    </div>
</section>
<script>
    $(function () {
        $("#sortable tbody").sortable({
            placeholder: "ui-state-highlight",
            update: function () {
                var $vals = $('.order').map(function () {
                    return $(this).val();
                }).get();

                $.ajax({
                    url: '/index.php/admin/filament_category/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                    error: function () {
                        alert('esa si e*a maikata....');
                    }
                })
            }
        });
        $("#sortable").disableSelection();
    });
</script>
<?php $this->load->view('admin/includes/footer'); ?>