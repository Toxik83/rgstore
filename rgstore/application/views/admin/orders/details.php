<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
<div class="admin_breadcrumbs">
    <?php echo $this->breadcrumb->output(); ?>
</div>
<h1>Orders</h1>

    <table class="admin_table">
        <tr>
            <td>From</td>
            <td>Personal details</td>
            <td>Quantity</td>
            <td>Price</td>
            <td>Price*quantity</td>
            <td>Product id</td>
            <td>Product title</td>
        </tr>
              
        <?php foreach ($orderData as $key => $details): ?>         
        <tr>
            <td>
                <?php echo $details->fname.' ';
                      echo $details->lname.'<br/>';
                      echo $details->email; ?>
            </td>     
            <td>
                <?php echo $details->address.'<br/>';
                      echo $details->city.'<br/>';
                      echo $details->country_name.'<br/>';
                      echo $details->zip_code.'<br/>';
                      echo $details->phone; ?>
            </td>
            <td><?php echo $details->quantity; ?></td>
            <td><?php echo $details->price; ?></td>
            <td><?php echo $details->total_price; ?></td>
            <td><?php echo $details->product_id; ?></td>
            <td><?php echo $details->title; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <p>Shipping: <?php echo $shipping; ?></p>
    <p>Total price: <?php echo $orderPrice; ?></p>
    <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
    </a>
    </div>
</section>
<script>
    function goBack() {
        window.history.back();
    }
</script>
<?php $this->load->view('admin/includes/footer'); ?>