<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
<div class="admin_breadcrumbs">
    <?php echo $this->breadcrumb->output(); ?>
</div>

    <h1>Orders</h1>
        
    <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="success"><?php echo $msg; ?></p>
    <?php endif; ?>
            
    <form method="get" action="<?= site_url('admin/orders'); ?>" class="admin_search">
        <input type="text" name="search" placeholder="Search for recipient" value="<?= $search; ?>" class="admin_chat_text" />
        <button type="submit" name="submit" class="admin_search_button">Search</button>
    </form>
            
    <?php if (!empty($orders)): ?>
            
        <table class="admin_table">
            <tr>
                <th>Id</th>
                <th>From</th>
                <th>City</th>
                <th>Country</th>
                <th>Order Date</th>
                <th>Details</th>
                <th>Status</th>
                <th>Order Action</th>
            </tr>

            <?php foreach ($orders as $key => $order): ?>
            <tr>
                <td><?php echo $order->order_id; ?></td>
                <td><?php echo $order->email; ?></td>
                <td><?php echo $order->city; ?></td>
                <td><?php echo $order->country_name; ?></td>
                <td><?php echo $order->date_added; ?></td>
                <td><a href="<?php echo site_url('admin/orders/details/'.$order->order_id); ?>">Details</a></td>
                <td>
                    <?php if ($order->status == 0): ?>
                        <?php echo 'Pending'; ?>
                    <?php elseif ($order->status == 1 ): ?>
                        <?php echo 'Completed'; ?>
                    <?php elseif ($order->status == 3 ): ?>
                        <?php echo 'Paid'; ?>
                    <?php else: ?>
                        <?php echo 'Canceled'; ?>
                    <?php endif; ?>
                </td>
                <td>
                    <?php if ($order->status == 3): ?>
                        <a href="<?php echo site_url('admin/orders/confirm/'.$order->order_id); ?>">Complete</a>
                    <?php endif; ?>
                
                    <?php if ($order->status != 1 && $order->status != 3): ?>
                        <a href="<?php echo site_url('admin/orders/paid/'.$order->order_id); ?>">Set to Paid</a>
                    <?php endif; ?>
                
                    <?php if ($order->status != 2 && $order->status != 0): ?>
                        <a href="<?php echo site_url('admin/orders/cancel/'.$order->order_id); ?>">Cancel</a>
                    <?php endif; ?>              
                </td>
            </tr>
            <?php endforeach; ?>
            
            <?php else: ?>
            <p class="error">
                No results found.
            </p>
            <?php endif; ?>   
        </table>

        <?= $pagination; ?>

    </div>
</section>