<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
    <div class="admin_breadcrumbs">
        <?= $this->breadcrumb->output(); ?>
    </div>
       <h1>Printers Page - Active</h1>   
            <h4><?= anchor('admin/printers/input', 'Create Printers'); ?>&nbsp;&nbsp;&nbsp;</h4>
            <br>
            <?php if(empty($prices)) : ?>
                       <p class="error">There are no price range records. Please fill them first!</p> 
            <?php endif; ?>
            <a href="<?= site_url(); ?>admin/priceRange" style="color:green;">Create Price Range</a> | |
            
            <?php if (empty($companies)) : ?>
                        <p class="error">There are no company records. Please fill them first!</p> 
            <?php endif; ?>
            <a href="<?= site_url(); ?>admin/manufacturer" style="color:green;">Create Manufacturer</a> | |
            
            <?php if(empty($print_tech)) : ?>
                   <p class="error">There are no printers' technology records. Please fill them first!</p> 
            <?php endif; ?>
                   <a href="<?= site_url(); ?>admin/print_tech/" style="color:green;">Create Printer Technology</a> | |
            
            <?php if(empty($sizes)) : ?>
                   <p class="error">There are no building sizes records. Please fill them first!</p> 
            <?php endif; ?>
                   <a href="<?= site_url(); ?>admin/building_size/" style="color:green;">Create Building Sizes</a> | |
            
            <?php if(empty($accuracies)) : ?>
                   <p class="error">There are no accuracy sizes. Please fill them first!</p> 
            <?php endif; ?>
                   <a href="<?= site_url(); ?>admin/accuracy" style="color:green;">Create Accuracy Size</a> | |
            
            <?= anchor('admin/printers/showInactive', 'Show Inactive Products'); ?></h4>
            <br>
            <br>

            <br>
            <br>
                   
                <?php if ($msg = $this->session->flashdata('success')) : ?>
                    <p class="success"><?= $msg; ?></p>
                <?php elseif ($msg = $this->session->flashdata('error')) : ?>
                    <p class="error"><?= $msg; ?></p>
                <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
                    <?= $msg; ?>
                <?php endif; ?>

                <div id="success"></div>
                <br>

            <h3>List of All Printers</h3>
        <?php 
            echo form_open('admin/printers', array('method' => 'get', 'class' => 'admin_search'));
            echo form_input(array('name' => 'search','value' => $search_term, 'class' => 'admin_chat_text', 'placeholder'=>'Search by Printer\'s Title'));
            echo form_button(array('name' => 'search_submit', 'type' => 'submit', 'class' => 'admin_search_button', 'content' => 'Search'));
            echo form_close();
        ?> 
        <br>
        <form method="post" action="<?= site_url('admin/printers/bulkDelete'); ?>"
              class="admin_slider_form">
        <?php if (isset($printers)) : ?>
            <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                         class="dark_button">
                    Delete selected
            </button>
            <p></p>
            <table border="1" class="admin_table">
                <tr>
                    <th><input type="checkbox" id="main-checkbox" ><label for="main-checkbox"> Select items</label></th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Image Gallery</th>
                    <th>Price</th>
                    <th>New Price</th>
                    <th>Manufacturer</th>
                    <th>Print Technologies</th>
                    <th>Building Size</th>
                    <th>Accuracy</th>
                    <th>Date_update</th>
                    <th>Flash Date </th>    
                    <th>Edit</th>
                    <th>Active/Inactive</th>
                    <th>Delete</th> 
                </tr>  
        
                    <?php foreach ($printers as $printer =>$pr): ?>
        </form>
                <tr>
                    <td>
                        <input class="bulk-checkbox" type="checkbox" value="<?= $pr->id; ?>" name="bulkDelete[]">
                    </td>
                    <td><?= $pr->id; ?></td>                   
                    <td><?= $pr->title; ?></td>                    
                    <td align="center">
                        
                        <?php if($pr->name == NULL ) : ?>
                        
                        <a href="<?= site_url(); ?>admin/printersgallery/loadGalleryPage/<?= $pr->id; ?>">
                                
                        <img style = "width: 100px;" src="<?= base_url('upload/images155x155/def.jpeg'); ?>" /><br>Gallery</a>
                         
                        <?php  else : ?>
                            <a href="<?= site_url(); ?>admin/printersgallery/loadGalleryPage/<?= $pr->id; ?>">
                            <img style = "width: 100px;" src="<?= base_url('upload/images155x155/'.$pr->name); ?>" />
                            <br>Gallery</a>
                            <?php endif; ?>                        
                    </td>
                    <td align="center" ><?= $pr->price; ?></td>
                    <td class="table_new_price"><?php echo form_open('admin/printers/editPromoPrice'); ?>                        
                        <b><input type="text" style="width: 70px;" value="<?= $pr->new_price; ?>" name="new_price"></b>
                        <input type="hidden" value="<?= $pr->id; ?>" name="printer_id">
                        <?= form_button(array('type' => 'submit','content' => 'Edit','class' => 'admin_link_button'))?>
                        <?= form_close(); ?>
                    </td>                    
                    <td align="center" ><?= $pr->man_name; ?></td>
                    <td align="center" ><?= $pr->print_name; ?></td>
                    <td align="center" ><?= $pr->build_name; ?></td>
                    <td align="center" ><?= $pr->accuracy_name; ?></td>
                    <td align="center" ><?= $pr->date_update; ?></td>
                    <td align="center" ><?php if ($pr->fl_date_begin === '0000-00-00 00:00:00' && $pr->fl_date_end === '0000-00-00 00:00:00'): ?>
                        <a href="<?= site_url(); ?>admin/printers/flashdate/<?= $pr->id; ?>">                             
                            Add Flash Date</a>
                            <?php  else : ?> <a href="<?= site_url(); ?>admin/printers/flashdate_edit/<?= $pr->id; ?>">Edit Flash Date
                                <br><font color="red">begin: <?= $pr->fl_date_begin ;?><br>end: <?= $pr->fl_date_end; ?></font>                                    
                       <?php endif; ?></td>                                                            
                    <td align="center" ><a href="<?= site_url(); ?>admin/printers/update/<?= $pr->id; ?>">Edit</a>
                    </td>
                    <td align="center" ><a href="<?= site_url(); ?>admin/printers/delete/<?= $pr->id; ?>" >Make Inactive</a>
                    </td>
                    <td align="center" ><a href='<?= site_url(); ?>admin/printers/softdelete/<?= $pr->id; ?>' onClick="return confirmDialog();">Delete</a>
                    </td>                    
                        <?php endforeach; ?>
                </tr>
            </table>
            <br>
             <?= $links; ?>
            <?php  else : ?>
            <p class="error">
                No results found.
            </p>
                <a href="<?= site_url(); ?>admin/printers/" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
                </a>
            <br>
            <?php endif;?>
    </div>
</section>
    <script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure?');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/index.php/admin/printers/bulkDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                            $('#success').text('You have successfully deleted the product');
                        }

                        if (data.success === 2) {
                            $('#success').text('This product is part of a Bundle. Edit the bundle before continuing.');
                        }
                    }

                });
            }
        });
    });
</script>
<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });

</script>
<?php $this->load->view('admin/includes/footer'); ?>