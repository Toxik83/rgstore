<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <br>
        <h1>Gallery</h1>

        <p style=" color: red;">
            <?php echo $this->session->flashdata('success'); ?>
        </p>
        <div id="success" style="color:red;"></div>
        <br>
        <?php echo form_open_multipart('admin/filamentsgallery/save'); ?>
        <table border="1" class="admin_table">
            <tr>
                <th>Add image</th>
                <th>Send</th>
            </tr>
            <tr>
                <td>
                    <?= form_upload('userfile', '', 'required'); ?>
                    <input type="hidden" name="filament_id" value="<?php echo $this->uri->segment(4); ?> "/>
                </td>
                <td>
                    <?php echo form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Create', 'class' => 'dark_button image')); ?>
                </td>
            </tr>
        </table>
            <input type="hidden" name="filament_id" value="<?php echo $this->uri->segment(4); ?> "/>
        

        <?= form_close(); ?>
        <br>
        <br>
        <hr>
        <br>
        <br>

        <h3>List of All Images for Filament with ID: <?= $this->uri->segment(4); ?></h3>

        <form id="makeItMain" action="<?= site_url();?>admin/filamentsgallery/make_main_pic" method="post">
            <table border="1" class="admin_table">
                <tr>
                    <td></td>
                    <th>Image ID</th>
                    <th>Filament Name</th>
                    <th>Image</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Main Picture</th>
                </tr>
                <tbody id="sortable">
                    <?php foreach ($images as $value): ?>
                        <tr class="admin_sidebar_images">
                            <td><i class="fa fa-arrows-v"></td>
                            <td><?= $value->id; ?></td>
                            <td>
                                <?= $value->title; ?>
                                <input type="hidden" class="order" name="order[]" value="<?php echo $value->id; ?>"/>
                            </td>
                            <td>
                                <img 
                                    src="<?php echo base_url() . 'upload/images155x155/' . $value->name ?>  "
                                    style="text-decoration: none; ">
                            </td>
                            <td>
                                <?php echo anchor('admin/filamentsgallery/update/' . $value->id, 'Edit'); ?>     
                            </td> 
                            
                             <td>
                                <button type="button" data-id="<?php echo $value->id; ?>" id="delete-<?php echo $value->id; ?>"
                                        class="delete-row admin_link_button" class="admin_button">Delete
                                </button>
                            </td> 
                            <td>
                                <input id="<?php echo $value->id; ?>" type="radio" name="pic_id"  value="<?php echo $value->id; ?>" <?php if ($value->main_pic == 1) {
                                    ?> checked = "checked" <?php } ?>/>
                               <label for="<?php echo $value->id; ?>">Make main</label>
                                <input type="hidden" name="product_id" value="<?php echo $value->product_id; ?>"/>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
        </form>
    </div>

</section>
<script>
function goBack() {
    window.history.back();
}
</script>
<script type='text/javascript'>
    $(document).ready(function () {
        $('input[name=pic_id]').on('change', function () {
            $('#makeItMain').submit();
        });
    });
</script>
<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure you want to delete this image ?');
            var id = $(this).data('id');
           
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/index.php/admin/filamentsgallery/ajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 0) {
                            $('#success').text('You cannot delete Main Picture !');
                        } else {
                            $ele.remove();
                            $('#success').text('You have successfully deleted the finishing image :) ');
                        }
                    }

                });
            }
        });
    });
</script>
<script>
   $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            update: function(){
                    var $vals=$('.order').map(function(){
                        return $(this).val();
                    }).get();
                    $.ajax({
                    url: '/index.php/admin/filamentsgallery/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                    error: function(){
                        alert('Error');
                    }
                    })}
                });
                $("#sortable").disableSelection();
            });
</script>
<?php $this->load->view('admin/includes/footer'); ?> 


