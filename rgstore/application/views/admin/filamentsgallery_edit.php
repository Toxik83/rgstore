<?php $this->load->view('admin/includes/header'); ?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
            <h2>Edit Gallery </h2>
            <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif ($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
            <?php endif; ?>
        <div id="success"></div
        <br>
            <?php
                $image = array(
                    'name' => 'userfile'
                );

                $submit = array(
                    'name' => 'submit',
                    'value' => 'Upload'
                );
            ?>
            <?= form_open_multipart('admin/filamentsgallery/edit'); ?>
        <b>
        <br>
            <?= validation_errors('<div class="error">', '</div>'); ?>
            <table border="1" class="admin_table">
                <tr>
                    <th>Edit image</th>
                    <th>Send</th>
                </tr>
                <tr>
                    <td>
                        <?= form_upload($image); ?>
                            <input type="hidden" name="pic_id" value="<?= $images->id; ?>"/>
                            <input type="hidden" name="filament_id" value="<?= $this->uri->segment(5); ?> " class="admin_input"/>
                            <img src="<?= base_url('/upload/images155x155/' . $images->name); ?>"/>
                    </td>
                    <td>
                        <?= form_submit($submit); ?>
                    </td>
                </tr>
                <?= form_close(); ?>
            </table>
            <br>
            <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
    </div>
</section>
<script>
function goBack() {
    window.history.back();
}
</script>
<?php $this->load->view('admin/includes/footer'); ?>
