<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>

        <h3>Edit Category </h3>
        <h2><?php echo $this->session->flashdata('my key2'); ?></h2>
        <?php echo form_error(); ?>
        <?php echo form_open('admin/filament_category/edit_category'); ?>

        <label>
            <input type="hidden" name="id" value="<?php echo $category->id; ?>"/>
        </label>
        <br>
        <label>Category Name:
            <input type="text" name="name" value="<?php echo $category->cat_name; ?>"/>
        </label>
        <input type="submit" value="Send"/>
        <?php echo form_close(); ?>
    </div>
</section>
<?php $this->load->view('admin/includes/footer'); ?>