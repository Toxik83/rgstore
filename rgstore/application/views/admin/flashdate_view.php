<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
            <br>
                <h1>Add Flash Date</h1>                    
                    <?= form_open('admin/printers/inputflashdate/'.$this->uri->segment(4));   ?>
                        <?php if ($msg = $this->session->flashdata('success')) : ?>
                            <p class="success"><?= $msg; ?></p>
                        <?php elseif ($msg = $this->session->flashdata('error')) : ?>
                            <p class="error"><?= $msg; ?></p>
                        <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
                        <?= $msg; ?>
                        <?php endif; ?>
                        <div id="success"></div>
                        <br>
                            <?php echo form_error(); ?>
                                <p style="color: red; ">
                                    <?php foreach ($newprice as $newp => $p) : ?> 
                                        <table cellpadding="2" cellspacing="2" class="create_table" >
                                            <tr>
                                                <td>
                                                    Printer name:
                                                </td>
                                                <td>
                                                    <?= $p['title']; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Flash Date (begin): 
                                                </td>
                                                <td>
                                                    <input type="text" placeholder="Pickup Date" id="pick" name="fl_date_begin" data-field="datetime" data-format="yyyy-MM-dd hh:mm:ss"  readonly value="<?= set_value('fl_date_begin'); ?>">
                                                </td>
                                                <td>
                                                    <div id="dtBox"></div>
                                                    <input type="hidden" name="printer_id" value = "<?= $this->uri->segment(4);?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Flash Date (end) : 
                                                </td>
                                                <td>
                                                    <input type="text" data-field="datetime" name="fl_date_end" data-format="yyyy-MM-dd hh:mm:ss" value="<?= set_value('fl_date_end'); ?>" readonly placeholder="Pickup Date">
                                                </td>
                                                <td>
                                                    <div id="dtBox2"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h3>
                                                        Regular Price: <?= $p['price']; ?>
                                                    </h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <?php if($p['new_price'] == 0) :?>
                                                    <td>
                                                        Please enter promo price! 
                                                    </td> 
                                                <?php endif;?>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for = "label_promoprice"> Promo price:</label>
                                                </td>
                                                <td>
                                                    <input id = "label_promoprice" type="text" style="width: 70px;" value="<?= $p['new_price']; ?>" name="new_price"> 
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td colspan="2">
                                                    <?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Add', 'class' => 'dark_button flash')); ?>
                                                </td>
                                            </tr>
                                            <?= form_close(); ?> 
                                        </table>
                                <br>
        <?= anchor('admin/printers', 'Back to Printers list'); ?>
    </div>   
</section>
<script>		
$(document).ready(function()
{
    $("#dtBox").DateTimePicker({

            dateTimeFormat: "yyyy-MM-dd HH:mm:ss"
    });
});

	
$(document).ready(function()
 {
    $("#dtBox2").DateTimePicker();
   
 });
 
</script>

<?php $this->load->view('admin/includes/footer'); ?>