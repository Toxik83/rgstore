<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>

        <h1>Create Flash Date</h1>
        <?php
        echo form_open('admin/filament/flashdate');
        ?>     
        <br>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>
        
        <table cellpadding="2" cellspacing="2" class="create_table" >
            <?php if (isset($sources) && $sources) : ?>
             <tr>
                    <td><label for="title">Title :</label></td>
                    <td><input  id='title' type="text" name='title'  value="<?= $sources->title; ?>"></td>
                </tr>
                <tr>
                    <td><label for="price">Price :</label></td>
                    <td><input  id='price' type="text" name='price'  value="<?= $sources->price; ?>"></td>
                </tr>
                 <tr>
                    <td><label for="new_price">New Price :</label></td>
                    <td><input  id='new_price' type="text" name="new_price"  value="<?= $sources->new_price; ?>"></td>
                </tr>
                <tr>
                    <td>Flash Date(begin) :</td>
                    <td><input type="text" placeholder="Pickup Date" id="pick" name="fl_date_begin" data-field="datetime" data-format="yyyy-MM-dd hh:mm:ss"   value="" ></td>
                    <td><input id ='fl_begin' type="hidden" name="id" value = "<?= $this->uri->segment(4); ?>"></td>
                </tr>
                <div class="datepicker"></div>

                <tr>
                    <td>Flash Date (end) : </td>
                    <td><input type="text" data-field="datetime" name="fl_date_end" data-format="yyyy-MM-dd hh:mm:ss"  placeholder="Pickup Date" value=""  ></td>
                <tr>

                <tr>
                    <td colspan="2"><?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Set', 'class' => 'dark_button flash')); ?></td>
                </tr>
                
            <?php endif; ?>
            <?php
            echo form_close();
            ?>
        </table>
 <a href="<?= site_url('admin/filament/'); ?>" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
</a>
</div>

</section>

<script>
    $(document).ready(function ()
    {
        $(".datepicker").DateTimePicker({
            dateTimeFormat: "yyyy-MM-dd HH:mm:ss"
        });
    });
</script>
<?php $this->load->view('admin/includes/footer'); ?>