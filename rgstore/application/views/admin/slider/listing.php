<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
   
        <h1>Slider</h1>
        <h4><a href=" <?php echo site_url('admin/admin-slider/create-slide/'); ?>">Create new slide</a></h4>
        <br>
        <br>
       
        <?php if ($msg = $this->session->flashdata('success')) : ?>
        <p class="success"><?php echo $msg; ?></p>
        <?php endif; ?>
            
        <div>
            <p id="success"></p>
        </div>
        
        <form method="post" action="<?php echo site_url('admin/admin-slider/bulk-delete'); ?>"
              class="admin_slider_form">
            <button type="submit" onclick="return confirm('Are you sure')" class="dark_button">Delete selected</button>
            <p></p>
            <table border="1" class="admin_table" >
                <thead>
                    <th></th>
                    <th><input type="checkbox" id="main-checkbox"><label for="main-checkbox">Select item</label></th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Created at</th>
                    <th>Last updated at</th>
                    <th>Edit</th>
                    <th>Delete</th>                  
                </thead>

                <tbody id="sortable">
                    <?php foreach ($sliderData as $row): ?>
                        <tr>
                            <td><i class="fa fa-arrows-v"></td>
                            <td>
                                <input class="bulk-checkbox" type="checkbox" value="<?php echo $row->id; ?>"
                                       name="bulkDelete[]">
                            </td>
                            <td>
                                <?php echo $row->title; ?>
                                <input type="hidden" class="order" name="order[]" value="<?php echo $row->id; ?>"/>
                            </td>                   
                            <td><?php echo $row->description; ?></td>
                            <td><img src="<?php echo base_url('upload/home-slider/' . $row->image300x100); ?>"></td>
                            <td><?php echo $row->created_at; ?></td>
                            <td><?php echo $row->updated_at; ?></td>
                            <td><a href=" <?php echo site_url('admin/admin-slider/edit-slide/' . $row->id); ?> ">Edit</a>
                            </td>
                            <td>
                                <a onclick="event.preventDefault();" href="<?php echo site_url('admin/admin-slider/delete/'.$row->id); ?>" data-id="<?php echo $row->id; ?>" id="delete-<?php echo $row->id; ?>"
                                   class="delete-row">Delete
                                </a>
                            </td>                        
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </form>

<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure?');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/index.php/admin/adminSlider/ajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                            $('#success').addClass('success');
                            $('#success').text('You have successfuly deleted the item');
                        }                      
                    }
                });
            }
        });
    });
</script>

<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });
</script>

<script>
    $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            update: function(){
                    var $vals=$('.order').map(function(){
                        return $(this).val();
                    }).get();
                    $.ajax({
                    url: '/index.php/admin/adminSlider/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                    error: function(){
                        alert('esa si e*a maikata....');
                    }
                    })}
                });
                $("#sortable").disableSelection();
            });
</script>
<?php $this->load->view('admin/includes/footer'); ?>

