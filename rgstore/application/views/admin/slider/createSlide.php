<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
        <?php echo $this->breadcrumb->output(); ?>
        </div>
        <h1>Slide</h1>
        <div>
            <p>The image must be with maximum size of 2MB and 1920x1080.                
            Only GIF, JPG, PNG, JPEG formats are allowed.</p>
        </div>

        <?= validation_errors('<div class="error">', '</div>'); ?>

        <table class="manage_product_table">    
            <?php echo form_open_multipart(); ?>

            <tr>
                <td style="width: 10%;">
                    Image:
                </td>
                <td style="padding: 10px 0;">
                    <?php if (isset($postData['image']) && $postData['image'] != '') : ?>
                        <label>Current image:</label>
                            <img src="<?=  base_url('upload/home-slider/' . $postData['image300x100']) ?>">
                    <?php endif; ?>

                    <?=  (!empty($error)) ? $error['error'] : ''; ?>
                    <input type="file" name="image" size="20">
                </td>
            </tr>

            <tr>               
                <td><label for="title">Title:</label></td>
                <td>
                    <input type="text" class="text_field" id="title" name="title" placeholder="Title" value="<?= $postData['title']; ?>" />   
            </tr>

            <tr>               
                <td><label for="description">Description:</label></td>
                <td>
                    <input type="text" class="text_field" id="description" name="description" placeholder="Description" value="<?= $postData['description']; ?>" />   
                </td>
            </tr> 

            <tr>               
                <td><label for="url">URL:</label></td>
                <td>
                    <input type="text" class="text_field" id="url" name="url" placeholder="Enter a valid url address" value="<?= $postData['url']; ?>" />   
                </td>
            </tr>

            <tr>
                <td>
                    <div>
                        <input type="submit" value="Submit" class="dark_button">
                    </div> 
                </td>
            </tr>


            <?php echo form_close(); ?>
        </table>  
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    </div>
</section>

<script>
    function goBack() {
        window.history.back();
    }
</script>
<?php $this->load->view('admin/includes/footer'); ?>