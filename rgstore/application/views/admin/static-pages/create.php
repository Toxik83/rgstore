<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
    <div class="admin_breadcrumbs">
        <?php echo $this->breadcrumb->output(); ?>
    </div>

    <h1>Static pages</h1>
    <table class="manage_product_table">
        
        <?= validation_errors('<div class="error">', '</div>'); ?>
        
        <form method="post">    
            <table class="manage_product_table">
                <tr>               
                    <td><label for="title">Title</label></td>
                    <td>
                        <input type="text" class="text_field" id="title" name="title" placeholder="Title" value="<?= $postData['title']; ?>" />   
                    </td>
                </tr>

                <tr>
                    <td><label for="link_name">Link Name</label></td>
                    <td>
                        <input type="text" class="text_field" id="link_name" name="link_name" placeholder="Link name" value="<?= $postData['link_name']; ?>" />
                    </td>
                </tr>

                <tr>
                    <td><label for="slug">Slug</label></td>
                    <td>
                        <input type="text" class="text_field" id="slug" name="slug" placeholder="Slug" value="<?= $postData['slug']; ?>" />
                    </td>
                </tr>

                <tr>
                    <td><label for="content">Content</label></td>
                    <td>     
                        <textarea class="ckeditor" name="content" id="content" rows="10" cols="80"><?= $postData['content']; ?></textarea>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <button type="submit" name="save" class="dark_button product">Save</button>
                    </td>
                </tr>
        
            </table>
        </form>
        <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
        </a>
    </div>
</section>
<script>
    function goBack() {
        window.history.back();
    }
</script>
<?php $this->load->view('admin/includes/footer'); ?>
