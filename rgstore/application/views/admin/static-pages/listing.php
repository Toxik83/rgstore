<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <h1>Static pages</h1>
        <h4><a href="<?php echo site_url('admin/static-pages/create'); ?>">Create page</a></h4>
        <br>
        <br>

    <?php if ($msg = $this->session->flashdata('success')) : ?>
    <p class="success"><?php echo $msg; ?></p>
    <?php endif; ?>
        
    <div>
        <p id="success"></p>
    </div>

        <form method="post" action="<?php echo site_url('admin/static-pages/bulk-delete'); ?>">
            <button type="submit" onclick="return confirm('Are you sure')" class="dark_button">Delete selected</button>
            <p></p>
            <table class="admin_table">
                <tr>
                    <th><input type="checkbox" id="main-checkbox"><label for="main-checkbox">Select item</label></th>
                    <th>Slug</th>
                    <th>Link name</th>
                    <th>Title</th>
                    <th>Link to page</th>
                    <th>Content</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>

                <?php foreach ($pageData as $key => $page): ?>
                    <tr>
                        <td>
                            <input class="bulk-checkbox" type="checkbox" value="<?php echo $page->id; ?>"
                                   name="bulkDelete[]">
                        </td>
                        <td><?php echo substr($page->slug, 0, 30); ?></td>
                        <td><?php echo $page->link_name; ?></td>
                        <td><?php echo substr($page->title, 0, 30); ?></td>
                        <td><a href="<?php echo $page->link_to_page; ?>"><?php echo $page->link_to_page; ?></a></td>
                        <td><?php echo substr($page->content, 0, 100); ?></td>
                        <td>
                            <a href=" <?php echo site_url('admin/static-pages/edit/' . $page->id); ?> ">Edit</a>
                        </td>
                        <td>
                            <a onclick="event.preventDefault();"
                               href="<?php echo site_url('admin/static-pages/delete/' . $page->id); ?>"
                               data-id="<?php echo $page->id; ?>" id="delete-<?php echo $page->id; ?>"
                               class="delete-row">Delete
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </form>
    </div>
</section>
function goBack() {
    window.history.back();
}
<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure?');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/index.php/admin/adminStaticPages/ajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                            $('#success').addClass('success');
                            $('#success').text('You have successfuly deleted the item');
                        } 
                    }

                });
            }
        });
    });
</script>
<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });
</script>
<?php $this->load->view('admin/includes/footer'); ?>
