<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
            <h3>Add Filament</h3>
                <?php echo $this->session->flashdata('my key'); ?>
                    <?php echo form_open_multipart('admin/filament/addFilament');
                    echo validation_errors();?>
            <table cellpadding="2" cellspacing="4"  >
                <tr>
                    <td><label for="title">Title</label></td>
                    <td>
                        <input type="text" id='title' name="title" value="">
                    </td>
                </tr>
                <tr>
                    <td>
                        Text:
                    </td>
                    <td>
                        <?php echo form_textarea(array('name' => 'text', 'id' => 'desc', 'class' => "ckeditor"));?>
                    </td>
                </tr>
                <tr>
                   <td><label for="price">Price</label></td>
                    <td>
                    <input type="text" id='price' name="price" value=""/>
                </td>
                </tr>
                 <tr>
                   <td><label for="new_price"> New Price</label></td>
                    <td>
                    <input type="text" id='new_price' name="new_price" value=""/>
                </td>
                <tr>
                <td>Filament Category : </td>
                <td>
                    <select name="cat_id">
                        <?php foreach ($categories as $cat): ?>
                        <option value="<?php echo $cat->id; ?>"><?php echo $cat->cat_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
                <tr>
                <td>Manufacturer  : </td>
                <td>
                    <select name="manufacturer">
                        <?php foreach ($companies as $comp): ?>
                        <option value="<?php echo $comp->id; ?>"><?php echo $comp->man_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
                <tr>
                   <td>Upload picture  : </td>
                       
                            <?php
                            $image = array(
                                'name' => 'userfile'
                            );

                            $submit = array(
                                'name' => 'submit',
                                'value' => 'Upload'
                            );
                            ?>
                            <?php echo validation_errors(); ?>
                            <h2><?php echo $this->session->flashdata('key'); ?></h2>
                    </td>
                    <td>
                            <?php echo form_upload($image, '', 'required'); ?>          
                        <?php echo form_fieldset_close(); ?>
                    </td>
                </tr>
                    <tr>
                <td colspan="2">
                    <?php echo form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Create', 'class' => 'dark_button product')); ?>
                </td>
                    </tr>    
            </table>
          <a href="<?= site_url('admin/filament/'); ?>" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    </div>
</section>
<?php $this->load->view('admin/includes/footer'); ?>
