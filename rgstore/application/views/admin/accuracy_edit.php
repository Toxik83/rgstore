<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
        <h2>Edit Accuracy Size </h2>.
        <br>
        <br>
                <?php if ($msg = $this->session->flashdata('success')) : ?>
                    <p class="success"><?= $msg; ?></p>
                <?php elseif ($msg = $this->session->flashdata('error')) : ?>
                    <p class="error"><?= $msg; ?></p>
                <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
                    <?= $msg; ?>
                <?php endif; ?>

                <div id="success"></div>
                <br>
       
        <?= form_open('admin/accuracy/edit_accuracy'); ?>
            <input type="hidden" name="id" value="<?= $this->uri->segment(4); ?> "/>
            <table cellpadding="2" cellspacing="2" class="create_table small">
                <tr>
                    <td>
                        <?= form_hidden('accuracy_id',$accuracies->id); ?>
                    </td>
                </tr>
               
                <tr>
                    <td class="label_small_table">
                        <label for="acc_name">Accuracy Size:</label>
                    </td>
                    <td>
                        <input type="text" id="acc_name" name="accuracy_name" value="<?= $accuracies->accuracy_name; ?>"/>
                    </td>
                    <td>
                        <input type="submit" name="edit" value="Edit" class="dark_button"/>
                    </td>
                </tr> 
            </table>
        <?= form_close(); ?>
            <a href="<?php site_url(); ?>/admin/accuracy/" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
    </div>
</section>
<?php $this->load->view('admin/includes/footer'); ?>
