<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
    <div class="admin_breadcrumbs">
        <?= $this->breadcrumb->output(); ?>
    </div>
        <h1>Edit Printers</h1>
        
        <h2><?= $this->session->flashdata('my key'); ?></h2>
        <?= form_open('admin/printers/update/'.$printer->id); ?>
        <?= validation_errors('<div class="error">', '</div>'); ?>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>
        <table class="manage_product_table">
            <tr>
                <td>ID:</td> 
                <td><?= form_label($printer->id); ?></td>
            </tr>     
            <tr>
            <tr>
                <td>
                    <label for="label_title">Title:</label>
                </td>
                <td>
                    <input id="label_title" class="text_field" type="text" name="title" value="<?= $printer->title; ?>"/>
                </td>
                <td><input type="hidden" name="printer_id" value="<?= $printer->id?>"/></td>
            </tr>
            <tr>
                <td>
                    Text:
                </td>
                <td>
                    <?= form_textarea(array('name' => 'text', 'id' => 'desc', 'class' => "ckeditor", 'value' => "$printer->text")); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="label_price" >Price:</label>
                </td>
                <td>
                    <input id="label_price" class="text_field" type="text" name="price" value="<?= $printer->price; ?>"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="label_newprice">New price:</label>
                </td>
                <td>
                    <input id="label_newprice" class="text_field" type="text" name="new_price" value="<?= $printer->new_price; ?>"/>
                </td>
            </tr>
            <tr>
                <td>
                    Select Manufacturer
                </td>
                <td>
                    <select class="special_field" name="manufacturer">
                        <?php foreach ($companies as $company): ?>
                            <option value="<?= $company->id; ?>" <?php if($printer->man_id === $company->id): ;?> selected = "selected" <?php  endif; ?>><?= $company->man_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Select Printing Technology
                </td>
                <td>
                    <?php foreach ($print_tech as $print): ?>
                    <input id="<?= $print->id; ?>"  type="radio" name="print_tech"  <?php if($printer->print_id === $print->id) : ;?>checked ="checked" <?php endif; ?> value="<?= $print->id; ?>"  style="margin:10px" /><label for="<?= $print->id; ?>"><?= $print->print_name; ?></label>               
                    <?php endforeach; ?>       
                </td>
            </tr>
            <tr>
                <td>
                    Select Building Size
                </td>
                <td>
                    <select class="special_field"  name="building_size">
                        <?php foreach ($sizes as $size): ?>
                            <option <?php if($printer->build_id === $size->id) : ;?> selected = "selected" <?php  endif; ?>value="<?= $size->id; ?>"><?= $size->build_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Select Accuracy Size
                </td>
                <td>
                    <?php foreach ($accuracies as $accuracy): ?>
                    <input id="b<?= $accuracy->id; ?>" type="radio" name="accuracy" <?php if($printer->acc_id ===$accuracy->id) : ;?>checked ="checked"<?php endif; ?> value="<?= $accuracy->id; ?>"  style="margin:10px" /><label for="b<?php echo $accuracy->id; ?>"><?php echo $accuracy->accuracy_name; ?></label>            
                    <?php endforeach; ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="td_buttons">
                    
                    <?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Edit', 'class' => 'dark_button product')); ?>
                    <?= form_close(); ?>
                </td>
            </tr>    
        </table>
                <a href="" onclick="goBack()" class="error_go_back">
                    <i class="fa  fa-long-arrow-left"></i>
                    Go back to previous page
                </a>
    </div>
</section>
<?php $this->load->view('admin/includes/footer'); ?>
<script>
function goBack() {
    window.history.back();
}    
</script>

