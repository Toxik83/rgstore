<?php $this->load->view('admin/includes/header'); ?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
        <br>
        <h2>Create admin</h2>

        <?php
        echo form_open('admin/manage_admin/createAdmin');
        echo validation_errors('<div class="error">', '</div>');
        ?>
        
        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>

        <table cellpadding="2" cellspacing="2"  >
            <tr>
                <td><label for="username">Username : </label></td>
                <td><input id="username" type="text" name="username" value="<?= $postData['username']; ?>"  style="width: 90%"></td>
            </tr>    
            <tr>
                <td><label for="name">First Name : </label></td>
                <td><input id="name" type="text" name="name" value="<?= $postData['name']; ?>"  style="width: 90%"></td>
            </tr>    
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr>
                <td><label for="password">Password : </label></td>
                <td><input id="password" type="password" name="password" value=""  style="width: 90%"></td>     
            </tr>  
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr>
                <td><label for="conf_pas">Confirm Password : </label></td>
                <td><input id="conf_pas" type="password" name="confirm_password" value=""  style="width: 90%"></td>
            </tr>    

        </table>
                
        <br>
        <?= form_button(array('type' => 'submit', 'class' => 'dark_button', 'content' => 'Submit')); ?>
        
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
        <?= form_close(); ?>
</section>
<script>
function goBack() {
    window.history.back();
}
</script>



