<?php $this->load->view('admin/includes/header'); ?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
        <br>
        <h1>Edit admin</h1>
        <h3 style="color:green;">If you don`t set new password , current will not be changed :)  </h3>
        
        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>
        
        <?= form_open('admin/manage_admin/editAdmin/'); ?>

        <input type="hidden" name="id" value="<?= $this->uri->segment(4); ?> "/>
        
        <table cellpadding="2" cellspacing="2"  >
            <tr>
                <td><label>ID: </label></td> 
                <td><input type="text" name="username" readonly value="<?= $source->id; ?>"  style="width: 90%"></td>
            </tr>          
            <tr>
                <td><label for="username">Username : </label></td>
                <td><input id="username" type="text" name="username"  value="<?= $source->username; ?>"  style="width: 90%"></td>           
            </tr>
            <tr>
                <td><label for="name">First Name :</label></td>
                <td><input id="name" type="text" name="name" value="<?= $source->fname; ?>"  style="width: 90%"></td>
            </tr> 
            <tr>
                <td><label for="password" >New password : </label></td>
                <td><input id="password" type="password" name="password" value=""  style="width: 90%"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <?= form_button(array('type' => 'submit', 'class' => 'dark_button', 'content' => 'Submit')); ?>
                </td>
            </tr>
            
        </table>
        
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
        <?= form_close(); ?>
        
</section>
<script>
function goBack() {
    window.history.back();
}
</script>



