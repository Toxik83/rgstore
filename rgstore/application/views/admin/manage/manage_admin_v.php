<?php $this->load->view('admin/includes/header'); ?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>

        <h1>Manage admins</h1>
        <h4><?= anchor('admin/manage-admin/create', 'Create admin'); ?></h4>
        <br>

        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>
                
        <div id="success" ></div>


        <h3>All admins</h3>
        <?php
        echo form_open('admin/manage_admin', array('method' => 'get', 'class' => 'admin_search'));
        echo form_input(array('name' => 'search', 'value' => $search_term, 'class' => 'admin_chat_text','placeholder' => 'Search admin by Username or Name'));
        echo form_button(array('name' => 'search_submit', 'type' => 'submit', 'class' => 'admin_search_button', 'content' => 'Search'));
        echo form_close();
        ?> 
        
        <form>

            <?php if (isset($admins) && $admins) : ?>
            
            <br>

            <table border="1" class="admin_table">

                <tr>
                    <th>ID</th>
                    <th>Username</th>  
                    <th>Name</th> 
                    <th>Date registered</th> 
                    <th>Edit</th> 
                    <th>Delete</th> 
                </tr>

                <?php foreach ($admins as $key => $value): ?>
                    <tr>
                        <td><?= $value->id; ?></td>
                        <td><?= $value->username; ?></td>
                        <td><?= $value->fname; ?></td>
                        <td><?= $value->registered_at; ?></td>
                        <td>    
                            <?= anchor('admin/manage-admin/edit/' . $value->id, 'Edit'); ?>     
                        </td> 
                        <td>
                            <button type="button" data-id="<?= $value->id; ?>" id="delete-<?= $value->id; ?>"
                                    class="delete-row admin_link_button" class="admin_button admin_link_button">Delete
                            </button>
                        </td> 

                    </tr>

                <?php endforeach; ?>
            </table>
            <?php else : ?>
                <p class="error">
                    No data 
                </p>
                <a href="" onclick="goBack()" class="error_go_back">
                    <i class="fa  fa-long-arrow-left"></i>
                    Go back to previous page
                </a>
            <?php endif; ?> 
                
        </form>
        <?= $links; ?>
</section>
<script>
function goBack() {
    window.history.back();
}
</script>
<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure?');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (id === 1) {
                $('#success').addClass('error').text('You cannot delete the Main admin');
                return;
            }
            if (conf === true) {
                $.ajax({
                    url: '/admin/manage_admin/ajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {

                        if (data.success === 1) {
                            $ele.remove();
                        }
                        $('#success').addClass('success').text('You have successfuly deleted the admin');
                    }

                });
            }
        });
    });
</script>



