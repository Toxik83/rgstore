<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>

        <h1>Inactive Finishing</h1>

        <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="success"><?= $msg; ?></p>
        <?php elseif ($msg = $this->session->flashdata('error')) : ?>
            <p class="error"><?= $msg; ?></p>
        <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
            <?= $msg; ?>
        <?php endif; ?>

        <div id="success"></div>
        
        <br>

        <h3>List of All Inactive Finishings</h3>


        <?php
        echo form_open('admin/finishing/inactive', array('method' => 'get', 'class' => 'admin_search'));
        echo form_input(array('name' => 'search', 'value' => $search_term, 'class' => 'admin_chat_text','placeholder' => 'Search finishing by Title'));
        echo form_button(array('name' => 'search_submit', 'type' => 'submit', 'class' => 'admin_search_button', 'content' => 'Search'));
        echo form_close();
        ?>


        <form method="post" action="<?= site_url('admin/finishing/bulkDelete'); ?>"
              class="admin_slider_form">

            <?php if (isset($finishings) && $finishings) : ?>

            <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                    href="<?= site_url('admin/newsletter/bulkDelete'); ?>" class="dark_button">
                Delete selected
            </button>
            <p></p>

            <table border="1" class="admin_table">

                <tr>
                    <th><input type="checkbox" id="main-checkbox" ><label for="main-checkbox">Select items</label></th>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Image Gallery</th>
                    <th>Price</th>
                    <th>New Price</th>
                    <th>Manufacturer</th>
                    <th>Category</th>
                    <th>Flash Date</th>
                    <th>Date Added</th>
                    <th>Edit</th>
                    <th>Make Inactive</th>
                    <th>Delete</th>
                </tr>
                <?php foreach ($finishings as $key => $value): ?>
                <!--It`s because we want to use 'Edit button' functionality ... that`s why we made this sh*t .. sorry :(  -->
        </form>
                <tr>
                    <td>
                        <input class="bulk-checkbox" type="checkbox" value="<?= $value->id; ?>"
                               name="bulkDelete[]">
                    </td>
                    <td>
                        <?= $value->id; ?>
                        <input type="hidden" name='new_price_edit' value="<?= $value->id;?>">
                    </td>
                    <td><?= $value->title; ?></td>
                    <td align="center">
                        <?php if (($value->name) == NULL) : ?>
                            <a href="<?= site_url("admin/finishing-gallery/images" . $value->id); ?>">

                                <img style="width: 100px;"
                                     src="<?= base_url('upload/images155x155/def.jpeg'); ?>"/><br>Gallery</a>

                        <?php else : ?>
                            <a href="<?= site_url(); ?>admin/finishing-gallery/images/<?= $value->id; ?>">
                                <img style="width: 100px;"
                                     src="<?= base_url('upload/images155x155/' . $value->name); ?>"/>
                                <br>Gallery</a>
                        <?php endif; ?>

                    </td>
                    <td><?= $value->price; ?></td>
                    <td class="table_new_price">
                        <?= form_open('admin/finishing/newPriceEdit');?>
                            <?php $data = array(
                                'name' => 'new_price',
                                'value'=> $value->new_price
                            );?>
                            <?= form_input($data)?>
                        
                        <input type="hidden" name='new_price_id' value="<?= $value->id;?>">
                            
                        <?= form_button(array('type' => 'submit','content' => 'Edit','class' => 'admin_link_button'))?>
                        <?= form_close();?>
                    </td>
                    <td><?= $value->man_name; ?></td>
                    <td><?= $value->cat_name; ?></td>
                    <td align="center">
                        <?php if ($value->fl_date_begin == '0000-00-00 00:00:00' && $value->fl_date_end == '0000-00-00 00:00:00') : ?>
                            <a href="<?= site_url(); ?>admin/finishing/flashdate/<?= $value->id; ?>">

                                Add Flash Date</a>
                        <?php else : ?> <a
                            href="<?= site_url(); ?>admin/finishing/flashdate-edit/<?= $value->id; ?>">
                            Edit Flash Date
                            <br><font color="red">begin: <?= $value->fl_date_begin; ?>
                                <br>end: <?= $value->fl_date_end; ?></font>

                            <?php endif; ?>
                    </td>
                    <td><?= $value->date_added; ?></td>
                    <td>
                        <?= anchor('admin/finishing/edit/' . $value->id, 'Edit'); ?>
                    </td>
                    <td>
                        <?= anchor('admin/finishing/makeActive/' . $value->id, 'Make Active'); ?>
                    </td>
                    <td>
                        <button type="button" data-id="<?= $value->id; ?>" id="delete-<?= $value->id; ?>"
                                class="delete-row admin_link_button">Delete
                        </button>
                    </td>
                    <?php endforeach; ?>
                </tr>

            </table>
        <?= $links; ?>

    <?php else : ?>
        <p class="error">
            No results found.
        </p>
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    <?php endif; ?>
        
    </div>
</section>
<script>
function goBack() {
    window.history.back();
}
</script>
<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure?');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/admin/finishing/ajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                            $('#success').addClass('success').text('You have successfully deleted the finishing product');
                        }

                        if (data.success === 2) {
                            $('#success').addClass('error').text('This product is part of a Bundle. Edit the bundle before continuing.');
                        }
                    }

                });
            }
        });
    });
</script>

<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });
</script>




