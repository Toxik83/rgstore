<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>

        <h1>Create Flash Date</h1>
        <?php
        echo form_open('admin/finishing/flashdate');
        ?>     
        <br>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>
        
        <table cellpadding="2" cellspacing="2" class="create_table" >
            <?php if (isset($sources) && $sources) : ?>
                <tr>
                    <td><label for="name">Finishing name : </label></td>
                    <td><input type="text"  readonly value="<?= $sources->title; ?>"></td>
                </tr>
                <tr>
                    <td><label >Price :  </td>
                    <td><input type="text"  readonly value="<?= $sources->price; ?>"></td>
                </tr>
                <tr>
                    <td><label for="n_price">New Price : </label></td>
                    <td><input id="n_price" type="text" name='new_price' value="<?= $sources->new_price; ?>"></td>
                </tr>
                <tr>
                    <td><label for="fl">Flash Date (begin) : </label></td>
                    <td><input id="fl" type="text" placeholder="Pickup Date" id="pick" name="fl_date_begin" data-field="datetime" data-format="yyyy-MM-dd hh:mm:ss"  readonly value="" ></td>
                    <td><input type="hidden" name="id" value = "<?= $this->uri->segment(4); ?>"></td>
                </tr>
                <div class="datepicker"></div>

                <tr>
                    <td><label for="fl2">Flash Date (end) : <label></td>
                    <td><input id="fl2" type="text" data-field="datetime" name="fl_date_end" data-format="yyyy-MM-dd hh:mm:ss" readonly placeholder="Pickup Date" value=""  ></td>
                <tr>

                <tr>
                    <td colspan="2"><?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Set', 'class' => 'dark_button flash')); ?></td>
                </tr>
                
            <?php endif; ?>
            <?php
            echo form_close();
            ?>
        </table>

        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    </div>

</section>
<script>
function goBack() {
    window.history.back();
}
$('input[readonly]').focus(function(){
    this.blur();
});
</script>
<script>
    $(document).ready(function ()
    {
        $(".datepicker").DateTimePicker({
            dateTimeFormat: "yyyy-MM-dd HH:mm:ss"
        });
    });
</script>
<?php $this->load->view('admin/includes/footer'); ?>