<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>

        <h1>Create Category</h1>

        <?php
        echo form_open('admin/finishing/createCategory');
        echo validation_errors('<div class="error">', '</div>');
        ?>

        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>

        <div id="success"></div>

        <table cellpadding="2" cellspacing="2" class="create_table">
            <tr>
                <td class="label_small_table"><label for='cat'>Category Name :</label></td>
                <td><input id='cat' type="text" name="name" placeholder="Enter category name"
                           value="<?= $postData['name']; ?>"></td>
            </tr>
            <tr>
                <td colspan="2"><?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Create', 'class' => 'dark_button')); ?></td>
            </tr>
            <?= form_close(); ?>
        </table>

        <br>
        <br>
        <hr>
        <br>
        <br>

        <h3>List of All Categories</h3>
        <?php
        echo form_open('admin/finishing/createCategory', array('method' => 'get', 'class' => 'admin_search'));
        echo form_input(array('name' => 'search', 'value' => $search_term, 'class' => 'admin_chat_text','placeholder' => 'Search category by Name'));
        echo form_button(array('name' => 'search_submit', 'type' => 'submit', 'class' => 'admin_search_button', 'content' => 'Search'));
        echo form_close();
        ?>

        <form method="post" action="<?= site_url('admin/finishing/Cat_bulkDelete'); ?>"
              class="admin_slider_form">

            <?php if (isset($categories) && $categories) : ?>

            <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                    href="<?= site_url('admin/finishing/Cat_bulkDelete'); ?>"
                    class="dark_button">Delete selected
            </button>
            <p></p>
            <table border="1" class="admin_table">

                <tr>
                    <th></th>
                    <th><input type="checkbox" id="main-checkbox"><label for="main-checkbox">Select items</label></th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Date created</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>

                <tbody id="sortable">
                <?php foreach ($categories as $key => $value): ?>
                    <tr>
                        <td><i class="fa fa-arrows-v"></td>
                        <td>
                            <input class="bulk-checkbox" type="checkbox" value="<?= $value->id; ?>"
                                   name="bulkDelete[]">
                        </td>
                        <td>
                            <?= $value->id; ?>
                            <input type="hidden" class="order" name="order[]" value="<?= $value->id; ?>"/>
                        </td>
                        <td><?= $value->cat_name; ?></td>
                        <td><?= $value->date; ?></td>
                        <td>
                            <?= anchor('admin/category/edit/' . $value->id, 'Edit'); ?>
                        </td>
                        <td>
                            <button type="button" data-id="<?= $value->id; ?>"
                                    id="delete-<?= $value->id; ?>"
                                    class="delete-row admin_link_button" class="admin_button">Delete
                            </button>
                        </td>
                    </tr>

                <?php endforeach; ?>

                </tbody>
            </table>
        </form>

    <?= $links; ?>
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>

    <?php else : ?>
        <p class="error">
            No results found.
        </p>
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    <?php endif; ?>
<script>
function goBack() {
    window.history.back();
}
</script>
<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure ? ');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/admin/finishing/CatajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                             $('#success').addClass("success").text('You have successfuly deleted the category');
                        }
                        if (data.error=== 1) { 
                             $('#success').addClass("error").text('You have products for  this category');
                        }  
                    }
                });
            }
        });
    });
</script>

<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });
</script>
<script>
    $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            update: function () {
                var $vals = $('.order').map(function () {
                    return $(this).val();
                }).get();
                $.ajax({
                    url: '/admin/finishing/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                    success: function () {
                        $('#success').addClass("success").text('You have successfully reorder the category :) ');
                    }
                })
            }
        });
        $("#sortable").disableSelection();
    });
</script>
</section>



