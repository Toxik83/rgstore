<?php $this->load->view('admin/includes/header'); ?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
        <h1>Create finishing</h1>

        <?php
        echo form_open_multipart('admin/finishing/createFinishing');
        echo validation_errors('<div class="error">', '</div>');
        ?>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>

        <table class="manage_product_table">
            <tr>
                <td><label for="title">Title : </label></td>
                <td><input id="title" class="text_field" type="text" name="title" value="<?= $postData['title']; ?>"></td>
            </tr>    
            <tr>
                <td><label>Text: </label></td>
                <td ><textarea class ="ckeditor" rows="5" cols="20" name="text" style="width: 97%; height: 98%"> <?= $postData['text']; ?></textarea></td>
            </tr>     
            <tr>
                <td><label for="price">Price : </label></td>
                <td><input id="price" class="text_field" type="text" name="price" value="<?= $postData['price']; ?> "  ></td>
            </tr>  
            <tr>
                <td><label for="n_price">New Price : </label></td>
                <td><input id="n_price" class="text_field" type="text" name="new_price" value="<?= $postData['new_price']; ?>"  ></td>
            </tr>  
            <tr>
                <td><label>Finishing Category: </label></td>
                <td>
                    <select name="cat_id" required class="special_field">
                        <?php foreach ($categories as $key => $value): ?>
                            <option value="<?= $value->id; ?>" <?= ($postData['cat_id'] == $value->id) ? "selected" : "" ?>><?= $value->cat_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label>Manufacturer : </label></td>
                <td>
                    <select name="man_id" required class="special_field">
                        <?php foreach ($companies as $key => $value): ?>
                            <option value="<?= $value->id; ?>" <?= ($postData['man_id'] == $value->id) ? "selected" : "" ?>><?= $value->man_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Upload picture : </td>
                <td>
                    <?= form_upload('image', '', 'required'); ?>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Create', 'class' => 'dark_button product')); ?>
                </td>
            </tr>
        </table>
        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
        <br>
        
        <?= form_close(); ?>
    </div>
</section>
<script>
function goBack() {
    window.history.back();
}
</script>
<?php $this->load->view('admin/includes/footer'); ?>
