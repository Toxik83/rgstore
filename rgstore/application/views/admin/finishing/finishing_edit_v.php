<?php $this->load->view('admin/includes/header'); ?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>

        <h1>Edit finishing</h1>

        <?php
        echo form_open('admin/finishing/edit');
        ?>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>

        <table class="manage_product_table" >
            <tr>
                <td><label>ID: </label></td> 
                <td><input class="text_field" type="text" name="price" readonly value="<?= $source->id; ?>"></td>
            </tr>     
            <tr>
                <td><label for="title">Title : </label></td>
                <td><input id="title" class="text_field" type="text" name="title" value="<?= $source->title; ?>" <?= ($postData['title'] == $source->id) ? "selected" : "" ?>></td>
                <td><input type="hidden" name="id" value = "<?= $this->uri->segment(4); ?>"></td>
            </tr>    
            <tr>
                <td><label for="text">Text: </label></td>
                <td>
                    <textarea id="text" class ="ckeditor" rows="5" cols="20" name="text" style="width: 97%; height: 98%"><?= $source->text; ?>
                        <?= ($postData['text'] == $source->id) ? "selected" : "" ?>
                    </textarea>
                </td>
            </tr>     
            <tr>
                <td><label for="price">Price : </label></td>
                <td>
                    <input id="price" class="text_field" type="text" name="price" value="<?= $source->price; ?>"
                           <?= ($postData['price'] == $source->id) ? "selected" : "" ?>  >
                </td>     
            </tr>  
            <tr>
                <td><label for="n_price">New Price : <label></td>
                <td>
                    <input id="n_price" class="text_field" type="text" name="new_price" value="<?= $source->new_price; ?>"
                           <?= ($postData['title'] == $source->id) ? "selected" : "" ?>>
                </td>     
            </tr>  
            <tr>
                <td><label>Finishing Category: </label></td>
                <td>
                    <select  name="cat_id" class="special_field">
                        <?php foreach ($sourceOne as $key => $value): ?>
                        <option  <?php if ($value->id == $sourceCat->id  ) :?>  selected = "selected" <?php endif; ?>value="<?= $value->id ; ?>"><?= $value->cat_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label>Manufacturer : </label></td>
                <td>
                    <select name="man_id" required class="special_field">
                        <?php foreach ($companies as $key => $value): ?>
                            <option <?php if ($value->id == $sourceMan->id  ) : ?>  selected = "selected" <?php endif; ?> value="<?= $value->id; ?>" <?= ($postData['man_id'] == $value->id) ? "selected" : "" ?>><?= $value->man_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="td_buttons">
                    <?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Submit', 'class' => 'dark_button product')); ?>
                </td>
            </tr>
        </table>   
                <a href="" onclick="goBack()" class="error_go_back">
                    <i class="fa  fa-long-arrow-left"></i>
                    Go back to previous page
                </a>
        <?= form_close(); ?>
    <br>
</section>
<script>
function goBack() {
    window.history.back();
}
</script>
<?php $this->load->view('admin/includes/footer'); ?>
