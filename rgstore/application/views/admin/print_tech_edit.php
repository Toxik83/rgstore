<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
            <h1>Edit Printers Technology </h1>
            <h2><?= $this->session->flashdata('print key'); ?></h2>
                <?= form_error(); ?>
                <?= form_open('admin/print_tech/edit_print_tech'); ?>
                <label>
                    <input type="hidden" name="print_id" value="<?= $print_tech->id; ?>"/>
                </label>
                <br>
                <label>Printer Technology:
                    <input type="text" name="print_name" value="<?= $print_tech->print_name; ?>"/>
                </label>
                <input type="submit" value="Edit" class="dark_button"/>
                <?= form_close(); ?>
                <a href="<?php site_url(); ?>/admin/print_tech" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    </div>
</section>
<?php $this->load->view('admin/includes/footer'); ?>