<?php $this->load->view('admin/includes/header'); ?>
    <section>
        <div class="admin_content_container">
            <a href="<?= site_url() ?>/admin/page/addEdit/" class="admin_goback">Go Back to Page</a>

            <div>


                <?php if (isset($colour) && is_object($colour)): ?>
                    <h1>Edit Page Box</h1>
                <?php else: ?>
                    <h1>Create Page Box</h1>
                <?php endif; ?>


                <div style="width:800px;">
                   
                    <form method="POST" action="">


                        <input type="hidden" name="id" value="<?= (is_object($colour) ? $colour->id : '') ?>"/>
                        <input type="hidden" name="filament_id" value="<?= $filament_id ?>"/>
                         <?php echo '<p>Name:';?>
                        </br>
                        <input type="text" name="name" placeholder="Box Name"
                               value="<?= (is_object($colour) ? $colour->name : '') ?>"/>
                         <?php echo '<p>Price:';?>
                        </br>
                        <input type="text" name="price" placeholder="Price"
                               value="<?= (is_object($colour) ? $colour->price : '') ?>"/>
                         </br>
                         <?php echo '<p>Quantity:';?>
                         </br>
                       <input type="number" name="quantity" 
                           min='1'    value="<?= (is_object($colour) ? $colour->quantity : '') ?>"/>
                       </br>
                        <input type="submit" value="Save" class="admin_button"/>
                    </form>
                   
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('admin/includes/footer'); ?>


