<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title><?= $title; ?></title>

        <!--jQuery -->

        <script src="<?= base_url(); ?>media/public/js/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!--jQuery end-->

        <!--DateTimePicker.js -->
        <script type="text/javascript" src="<?= base_url(); ?>media/admin/js/DateTimePicker.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>media/admin/css/DateTimePicker.css" >
        <!--DateTimePicker end-->


        <!-- Chosen plugin js-->
        <script src="<?= base_url(); ?>media/admin/js/chosen/chosen.jquery.min.js"></script>
        <!-- Chosen end-->        

        <script src="<?= site_url('media/admin/js/scripts.js'); ?>" type="text/javascript"></script>
        <script src="<?= site_url('media/public/js/chat.js'); ?>" type="text/javascript"></script>

        <!-- Font awesome included-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Font awesome end-->

        <!-- Chosen plugin css-->
        <link rel="stylesheet" href="<?= base_url(); ?>media/admin/js/chosen/chosen.min.css" media="screen" type="text/css">
        <!-- Chosen plugin end-->


        <link href="<?= site_url('media/public/css/chat.css'); ?>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?= base_url(); ?>media/admin/css/admin_panel.css" media="screen"type="text/css">
        <link rel="stylesheet" href="<?= base_url(); ?>css/style.css" media="screen" type="text/css">

        <script src="<?= base_url("/ckeditor/ckeditor.js"); ?>"></script>
        <script>
            $(document).ready(function () {
                $('select').change(function () {
                    var selected_option = $(this).find('option:selected');
                    var select_text = selected_option.text();
                    $(this).siblings('.select_text').find('p').text(select_text);

                });
            });
        </script>

    </head>
    <body>
        <div id="wrap">
            <header>
                <div class="admin_menu">
                    <ul>
                        <li>
                            <?= anchor('admin/administration', 'RGStore'); ?>
                        </li>
                        <li>
                            <?= anchor('admin/printers', 'Printers'); ?>
                        </li>                       
                        <li>
                            <?= anchor('admin/filament', 'Filament'); ?>
                        </li>
                        <li>
                            <?= anchor('admin/finishing', 'Finishing'); ?>
                        </li>
                        <li>
                            <?= anchor('admin/admin-slider', 'Slider'); ?>
                        </li>
                        <li>
                            <?= anchor('admin/static-pages', 'Static Pages'); ?>
                        </li>
                        <li>
                            <?= anchor('admin/menus', 'Menus'); ?>
                        </li>
                        <li>
                            <?= anchor('admin/orders', 'Orders'); ?>
                        </li>
                        <li>
                            <?= anchor('admin/bundle', 'Bundles'); ?>
                        </li>
                        <li>
                            <?= anchor('admin/newsletter', 'Newsletter'); ?>
                        </li>
                        <li>
                            <a href="<?= site_url('admin/admin-inbox'); ?>">Inbox <?= ($newMessages != 0) ? '(' . $newMessages . ')' : ''; ?></a> 
                        </li>
                        <li class="logout">
                            <a href="<?= site_url('admin/chat'); ?>">
                                <span class="live_chat">Live chat</span>
                            </a>
                            <?= anchor('admin/manage-admin', 'Manage admins'); ?>
                            <?= anchor('admin/home/logout', 'Logout'); ?>
                        </li>
                    </ul>

                </div>
            </header>









