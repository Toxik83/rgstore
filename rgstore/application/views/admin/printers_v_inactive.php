<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
            <br>
                <h1>Printers Page - Inactive</h1>        
                <div id="menu" class="admin_portfolio_view">
                    <?= anchor('admin/printers', 'Show Active Products'); ?>
                </div>
                <br>
                <?php 
                    echo form_open('admin/printers/showInactive', array('method' => 'get', 'class' => 'admin_search'));
                    echo form_input(array('name' => 'search','value' => $search_term, 'class' => 'admin_chat_text'));
                    echo form_button(array('name' => 'search_submit', 'type' => 'submit', 'class' => 'admin_search_button', 'content' => 'Search'));
                    echo form_close();
                ?> 
                <form method="post" action="<?= site_url('admin/printers/bulkDelete'); ?>"
                      class="admin_slider_form">
                <br>
                 <?php if (isset($printers)) :?>
                <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                        class="dark_button">
                        Delete selected
                    </button>
                <br>
                <br>
                    <table border="1" class="admin_table">
                        <tr><th><input type="checkbox" id="main-checkbox" ><label for="main-checkbox"> Select items</label></th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Image Gallery</th>
                            <th>Price</th>
                            <th>New Price</th>
                            <th>Manufacturer</th>
                            <th>Print Technologies</th>
                            <th>Building Size</th>
                            <th>Accuracy</th>
                            <th>Date_update</th>
                            <th>Flash Date </th>    
                            <th>Edit</th>
                            <th>Active/Inactive</th>
                        </tr>

                        <?php foreach ($printers as $allprinter => $printer ): ?>
                </form>
                        <tr>
                            <td>
                                <input class="bulk-checkbox" type="checkbox" value="<?= $printer->id; ?>" name="bulkDelete[]">
                            </td>
                            <td><?php echo $printer->id; ?></td>

                            <td><?php echo $printer->title; ?></td>

                            <td align="center">
                                    <a href="<?= site_url(); ?>/admin/printersgallery/loadGalleryPage/<?= $printer->id; ?>">
                                    <img style = "width: 100px;" src="<?= base_url('upload/images155x155/'.$printer->name); ?>" />
                                    <br>Gallery</a>
                            </td>
                            <td align="center" ><?= $printer->price; ?></td>
                            <td class="table_new_price"><?php echo form_open('admin/printers/editPromoPrice'); ?>                        
                                <input type="text" style="width: 70px;" value="<?= $printer->new_price; ?>" name="new_price">
                                <input type="hidden" value="<?= $printer->id; ?>" name="printer_id">
                                <?= form_button(array('type' => 'submit','content' => 'Edit','class' => 'admin_link_button'))?>
                                <?= form_close(); ?>
                            </td>                                      
                            <td align="center" ><?= $printer->man_name; ?></td>
                            <td align="center" ><?= $printer->print_name; ?></td>
                            <td align="center" ><?= $printer->build_name; ?></td>
                            <td align="center" ><?= $printer->accuracy_name; ?></td>
                            <td align="center" ><?= $printer->date_update; ?></td>
                            <td align="center" ><?php if ($printer->fl_date_begin =='0000-00-00 00:00:00' && $printer->fl_date_end =='0000-00-00 00:00:00'): ?>
                                <a href="<?= site_url(); ?>/admin/printers/flashdate/<?= $printer->id; ?>">
                                    Add Flash Date</a>
                                    <?php  else : ?> <a href="<?= site_url(); ?>/admin/printers/flashdate_edit/<?= $printer->id; ?>">Edit Flash Date
                                        <br><font color="red">begin: <?= $printer->fl_date_begin ;?><br>end: <?= $printer->fl_date_end ;?></font>
                               <?php endif; ?></td>
                            <td align="center" ><a href="<?= site_url(); ?>/admin/printers/update/<?= $printer->id; ?>">Edit</a>
                            </td>
                            <td align="center" ><a href="<?= site_url(); ?>/admin/printers/active/<?= $printer->id; ?>" >Make Active</a>
                            </td>
                                <?php endforeach; ?>
                        </tr>
                    </table>
                    <br>
                    <?= $links; ?>
                    <?php  else : ?>
                    <p class="error">
                        No results found.
                    </p>
            <a href="<?= site_url(); ?>admin/printers/" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to Printers page active
            </a>
            <br>
        <?php endif;?>
    </div>
</section>
<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure?');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/index.php/admin/printers/bulkDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                            $('#success').text('You have successfully deleted the product');
                        }

                        if (data.success === 2) {
                            $('#success').text('This product is part of a Bundle. Edit the bundle before continuing.');
                        }
                    }

                });
            }
        });
    });
</script>
<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });

</script>        
<?php $this->load->view('admin/includes/footer'); ?>