<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
            <h1>Create Printers</h1>
                    <?= form_open_multipart('admin/printers/input'); ?>
                    <?= validation_errors('<div class="error">', '</div>');?>
            <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif ($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
            <?php endif; ?>

            <div id="success"></div>
            <br>
            <table cellpadding="2" cellspacing="4"  >
                <tr>
                    <td>
                        <label for ="label_title">Title</label>
                    </td>
                    <td>
                        <input type="text" name="title" value="<?= set_value('title'); ?>" id="label_title" class="text_field">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Text:</label>
                    </td>
                    <td>
                        <?= form_textarea(array('name' => 'text', 'id' => 'desc', 'class' => "ckeditor"));?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for ="label_price">Price:</label>
                    </td>
                    <td>
                        <input type="text" name="price" value="<?= set_value('price'); ?>" id="label_title" class="text_field">
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for ="label_new_price">New Price: </label>
                    </td>
                    <td>
                        <?= form_input(array('name' =>'new_price', 'id'=>'label_new_price', 'class'=>'text_field'), set_value('new_price'));  ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Select Manufacturer:</label>
                    </td>
                    <td>
                            <select name="manufacturer" >
                                <?php foreach ($companies as $company): ?>
                                    
                                    <option value="<?= $company->id; ?>" <?= set_select('manufacturer',  $company->id); ?>  ><?= $company->man_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Select Printing Technology:
                    </td>
                    <td>
                        <?php foreach ($print_tech as $print): ?>
                        <input type="radio" name="print_tech"  id ="<?= $print->id; ?>" value="<?= $print->id; ?>" <?= set_radio('print_tech',  $print->id); ?>   style="margin:10px" /><label for="<?= $print->id; ?>"><?= $print->print_name; ?></label>               
                            <?php endforeach; ?>       
                    </td>
                </tr>
                <tr>
                    <td>
                        Select Building Size: 
                    </td>
                    <td>
                            <select name="building_size">
                                <?php foreach ($sizes as $size): ?>
                                    <option   value="<?= $size->id; ?>" <?= set_select('building_size',  $size->id); ?>><?= $size->build_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Select Accuracy Size:
                    </td>
                    <td>
                            <?php foreach ($accuracies as $accuracy): ?>
                        <input type="radio" name="accuracy"  value="<?= $accuracy->id; ?>" <?= set_radio('accuracy',  $accuracy->id); ?> id="l<?= $accuracy->id; ?>" style="margin:10px" /><label for="l<?= $accuracy->id; ?>"><?= $accuracy->accuracy_name; ?></label>             
                            <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Upload picture:
                            <?php
                            $image = array(
                                'name' => 'userfile'
                            );
                            ?>
                    </td>
                    <td> 
                            <?= form_upload($image, '','required'); ?>          
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Create', 'class' => 'dark_button product')); ?>
                    </td>
                </tr>
            </table>
            <br>
            <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
        <?= form_close();  ?>
    </div>
</section>
<script>
function goBack() {
    window.history.back();
}
</script>
<?php $this->load->view('admin/includes/footer'); ?>
