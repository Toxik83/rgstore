<?php $this->load->view('admin/includes/header'); ?>
    <section>
        <div class="admin_content_container">
            <a href="<?= site_url() ?>/admin/page/addEdit/" class="admin_goback">Go Back to Page</a>

            <div>


                <?php if (isset($box) && is_object($box)): ?>
                    <h1>Edit Page Box</h1>
                <?php else: ?>
                    <h1>Create Page Box</h1>
                <?php endif; ?>


                <div style="width:800px;">
                    <?php echo $this->session->flashdata('box_name'); ?>
                    <form method="POST" action="">


                        <input type="hidden" name="id" value="<?= (is_object($box) ? $box->id : '') ?>"/>
                        <input type="hidden" name="page_id" value="<?= $page_id ?>"/>
                         <?php echo '<p>Name:';?>
                        </br>
                        <input type="text" name="name" placeholder="Box Name"
                               value="<?= (is_object($box) ? $box->name : '') ?>"/>
                        <?php echo '<p>Box Content:';?>
                        <hr/>
                         
                        <textarea class="text-input textarea ckeditor"
                                  name="content"><?= (is_object($box) ? $box->content : '') ?></textarea>

                        <hr/>
                        <input type="submit" value="Save" class="admin_button"/>
                    </form>


                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('admin/includes/footer'); ?>