<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">

        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <h1>Preview Menu</h1>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="success"><?php echo $msg; ?></p>
        <?php endif; ?>

        <div>
            <p id="success"></p>
        </div>

        <?php if ($menuData[0]->item_id != null): ?>

            <form method="post">
                <table class="admin_table">
                    <thead>
                        <tr>
                            <td></td>
                            <td>Menu name</td>
                            <td>Link</td>
                            <td>Edit</td>
                            <td>Delete item</td>
                        </tr>
                    </thead>
                    <tbody id="sortable">
                        <?php foreach ($menuData as $key => $menu): ?>
                            <tr>
                                <td><i class="fa fa-arrows-v"></i></td>
                                <td>
                                    <?php echo $menu->menu_name; ?>
                                    <input type="hidden" class="order" name="order[]" value="<?php echo $menu->item_id; ?>"/>
                                </td>
                                <td><a href="<?= ($menu->internal == 0) ? $menu->page_link : site_url($menu->page_link); ?>"  <?= ($menu->internal == 0) ? 'target="_blank"' : ''; ?>>
                                        <?php echo $menu->link_name; ?>
                                    </a>
                                </td>
                                <td><a href=" <?php echo site_url('admin/menus/edit-item/' . $menu->item_id); ?> ">Edit</a></td>
                                <?php if ($menu->is_deletable == 1): ?>
                                    <td>
                                        <a onclick="event.preventDefault();" href="<?php echo site_url('admin/menus/delete-item/' . $menu->item_id); ?>" data-id="<?php echo $menu->item_id; ?>" id="delete-<?php echo $menu->item_id; ?>"
                                           class="delete-row">Delete
                                        </a>
                                    </td>
                                <?php else: ?>
                                    <td>Undeletable</td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </form>

        <?php else: ?>
            <p>This menu is empty</p>
        <?php endif; ?>

        <a href="" onclick="goBack()" class="error_go_back">
            <i class="fa  fa-long-arrow-left"></i>
            Go back to previous page
        </a>
    </div>

</section>
<script>
    function goBack() {
        window.history.back();
    }
</script>

<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure?');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/index.php/admin/adminMenus/ajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                            $('#success').addClass('success');
                            $('#success').text('You have successfuly deleted the item');
                        }
                        if (data.success === 0) {
                            $('#success').text('You cannot delete this item');
                        }

                    }

                });
            }
        });
    });
</script>

<script>
    $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            update: function () {
                var $vals = $('.order').map(function () {
                    return $(this).val();
                }).get();
                $.ajax({
                    url: '/index.php/admin/adminMenus/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                    error: function () {
                        alert('esa si e*a maikata....');
                    }
                })
            }
        });
        $("#sortable").disableSelection();
    });
</script>
<?php $this->load->view('admin/includes/footer'); ?>
