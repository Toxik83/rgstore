<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>

        <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="success"><?php echo $msg; ?></p>
        <?php endif; ?>

        <?= validation_errors('<div class="error">', '</div>'); ?>

        <h1>Add menu item</h1>

        <div id="success"></div>
        <form method="post">
            <table class="manage_product_table">


                <tr>
                    <td style="width: 10%;"><label for="menu">Select menu:</label></td>
                    <td>
                        <select name="menu_id" id="menu" class="special_field">
                            <?php foreach ($menuData as $key => $menu): ?>
                                <option value="<?=  $menu->menus_id ?>" <?= ($option == $menu->menus_id) ? 'selected' : ''; ?>><?= $menu->menu_name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td><label for="page_link">Page Link:</label></td>
                    <td>
                        <input type="text" class="text_field" id="page_link" name="page_link" placeholder="Page Link" value="<?= $postData['page_link']; ?>" />   
                    </td>
                </tr>

                <tr>
                    <td><label for="link_name">Link name:</label></td>
                    <td>
                        <input type="text" class="text_field" id="link_name" name="link_name" placeholder="Link name" value="<?= $postData['link_name']; ?>" />   
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" name="Add" value="Add" class="dark_button product">
                    </td>
                </tr>

            </table>
        </form>
        <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
        </a>
    </div>
</section>

<script>
    function goBack() {
        window.history.back();
    }
</script>
<?php $this->load->view('admin/includes/footer'); ?>
