<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">

    <div class="admin_breadcrumbs">
        <?php echo $this->breadcrumb->output(); ?>
    </div>
    <h1>Edit Menu</h1>
    
    <?= validation_errors('<div class="error">', '</div>'); ?>

        <form method="post">
            <table class="manage_product_table">
                <tr>
                    <td style="width: 10%;"><label for="name">Name:</label></td>
                    <td>
                        <input type="text" class="text_field" id="name" name="name" placeholder="Name"
                               value="<?php echo $postData['name']; ?>"/>
                    </td>
                </tr>

                <tr>
                    <td><label for="keyword">Keyword:</label></td>
                    <td>
                        <input type="text" class="text_field" id="keyword" name="keyword" placeholder="Keyword" value="<?= $postData['keyword']; ?>" />   
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" name="Add" value="Edit" class="dark_button product">
                    </td>
                </tr>
            </table>
        </form>
    
        <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
        </a>
    </div>
</section>

<script>
    function goBack() {
        window.history.back();
    }
</script>
<?php $this->load->view('admin/includes/footer'); ?>
