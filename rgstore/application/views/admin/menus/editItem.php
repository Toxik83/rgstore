<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>
        <h1>Edit Item</h1>

        <?= validation_errors('<div class="error">', '</div>'); ?>

        <form method="post">
            <table class="manage_product_table">

                <tr>
                    <td style="width: 10%;"><label for="page_link">Page Link:</label></td>
                    <td>
                        <input type="text" class="text_field" id="page_link" name="page_link" placeholder="Page Link"
                               value="<?php echo $postData['page_link']; ?>"/>
                    </td>
                </tr>

                <tr>
                    <td><label for="link_name">Link name:</label></td>
                    <td>
                        <input type="text" class="text_field" id="link_name" name="link_name" placeholder="Link name" value="<?= $postData['link_name']; ?>" />   
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" name="Add" value="Edit" class="dark_button product">
                    </td>
                </tr>
            </table>
        </form>
        <a href="" onclick="goBack()" class="error_go_back">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
        </a>
    </div>
</section>

<script>
    function goBack() {
        window.history.back();
    }
</script>
<?php $this->load->view('admin/includes/footer'); ?>
