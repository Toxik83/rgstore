<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
<div class="admin_breadcrumbs">
    <?php echo $this->breadcrumb->output(); ?>
</div>
        
<h1>Menus</h1>

<?php if ($msg = $this->session->flashdata('success')) : ?>
    <p class="success"><?php echo $msg; ?></p>
<?php endif; ?>
    <div>
        <p id="success"></p>
    </div>
    <h4>
        <a href="<?php echo site_url('admin/menus/create'); ?>">Create menu</a>
        <a href="<?php echo site_url('admin/menus/add'); ?>">Add items</a>
    </h4>
        <br>
        <br>
   

    <form method="post">
        <table class="admin_table">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Keyword</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

            <?php foreach ($menuData as $key => $menu): ?>
                <tr>
                    <td><?php echo $menu->menus_id; ?></td>
                    <td><a href=" <?php echo site_url('admin/menus/view/'.$menu->menus_id); ?> "><?php echo substr($menu->menu_name, 0, 30); ?></a></td>
                    <td><?php echo $menu->keyword; ?></td>
                    <td><a href="<?php echo site_url('admin/menus/edit-menu/'.$menu->menus_id); ?>">Edit</a></td>
                        <?php if ($menu->is_deletable == 1): ?>
                            <td>
                                <a onclick="event.preventDefault();" href="<?php echo site_url('admin/menus/delete-menu/'.$menu->menus_id); ?>" data-id="<?php echo $menu->menus_id; ?>" id="delete-<?php echo $menu->menus_id; ?>"
                                   class="delete-row">Delete
                                </a>
                            </td>
                        <?php else: ?>
                            <td>Undeletable</td>
                        <?php endif; ?>                 
                </tr>
            <?php endforeach; ?>
        </table>
    </form>
    </div>
</section>
<script>
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure?');
            var id = $(this).data('id');
            var $ele = $(this).parent().parent();
            if (conf === true) {
                $.ajax({
                    url: '/index.php/admin/adminMenus/ajaxDeleteMenu',
                    data: {
                        id: id
                    },
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === 1) {
                            $ele.remove();
                            $('#success').addClass('success');
                            $('#success').text('You have successfuly deleted the item');
                        }
                        if (data.success === 0) {                           
                            $('#success').text('You cannot delete the menu');
                        }
                        
                    }

                });
            }
        });
    });
</script>
<?php $this->load->view('admin/includes/footer'); ?>
