<?php $this->load->view('admin/includes/header'); ?>
    <section>
        <div class="admin_content_container">
            <div class="admin_breadcrumbs">
                <?php echo $this->breadcrumb->output(); ?>
            </div>
            <br>
            <h1>Create Building Size</h1>
            <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
            <?php elseif($msg = $this->session->flashdata('build_key')): ?>
                <p class='success'><?=$msg?></p>
            <?php endif; ?>
            <?= form_open('admin/building_size/add',array('class' => 'set_width')); ?>
                <?= validation_errors(); ?>
                <label>Building Size: </label>
                <input type="text" name="build_name" value="" placeholder='Enter Building Size'/>
                <input type="submit" class="dark_button" value="Add"/>
            <?= form_close(); ?>  
            <br>
            <br>
            <hr>
            <br>
            <br>
            <h3>Building Sizes </h3>
            <?php if(!empty($sizes)):?>
                <form action="<?= site_url('admin/building_size');?>" method = "get" class="admin_search">
                    <input type="text" name = "keyword" class="admin_chat_text"/>
                    <button type="submit" class="admin_search_button">Search</button>
                </form>
                <form method="post" action="<?= site_url('admin/building_size/bulkDelete'); ?>"
                      class="admin_slider_form">
                    <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                        class="dark_button">Delete selected
                    </button>
                    <p></p>
                    <table border="1" class="admin_table">
                        <tr>
                            <th>Reorder</th>
                            <th><input type="checkbox" id="main-checkbox"><label for="main-checkbox">Select items</label></th>
                            <th>ID</th>
                            <th>Building Size</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        <tbody id="sortable">
                        <?php foreach ($sizes as $size): ?>
                            <tr>
                                <td><i class="fa fa-arrows-v"></td>
                                <td>
                                    <input class="bulk-checkbox" type="checkbox" value="<?= $size->id; ?>"
                                           name="bulkDelete[]"> 
                                </td>
                                <td><?= $size->id; ?></td>
                                <td><?= $size->build_name; ?></td>
                                <input type="hidden" class="order" name="order[]" value="<?= $size->id; ?>"/>
                                <td>
                                    <a href="<?= site_url(); ?>/admin/building_size/update/<?= $size->id; ?>">Edit</a>
                                </td>
                                <td>
                                    <a href ='<?= site_url(); ?>/admin/building_size/delete/<?= $size->id; ?>' onClick="return confirmDialog();">Delete</a>                           
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?=$links?>
                    <br>
                    <a href="#" class="error_go_back"  onclick="goBack()">
                        <i class="fa  fa-long-arrow-left"></i>
                        Go back to previous page
                    </a> 
                </form>
            <?php else:?>
                <p class="error">
                    No results found.
                </p>
                <a href="#" class="error_go_back">
                    <i class="fa  fa-long-arrow-left"></i>
                    Go back to previous page
                </a>
            <?php endif;?>
        </div>
    </section>
<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });
</script>
<script>
    $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            update: function(){
                    var $vals=$('.order').map(function(){
                        return $(this).val();
                    }).get();
                    $.ajax({
                    url: '/index.php/admin/building_size/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                    error: function(){
                        alert('Error');
                    }
                    })}
                });
                $("#sortable").disableSelection();
            });
</script>
<script>
function goBack() {
    window.history.back();
}
</script>
<?php $this->load->view('admin/includes/footer'); ?>
