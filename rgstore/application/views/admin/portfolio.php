<?php $this->load->view('includes/header'); ?>
<body>
    <h2>Test Page For controller</h2>
    <?php if (isset($proba) && $proba): ?>

                   <?php foreach ($proba as $key => $value): ?>

                        <div class="column about_us_boxes">
                             <h2>
                                 <?= $value->name ?>
                             </h2>
                            <div class="details">
                                 <?= $value->content ?>
                            </div>
                         </div>

                   <?php endforeach; ?>

               <?php else: ?>

                   <h2> No content to display. </h2>

                <?php endif; ?>

</body>
</html>