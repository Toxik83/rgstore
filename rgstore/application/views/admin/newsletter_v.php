<?php $this->load->view('admin/includes/header'); ?>

<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
        
        <h1>Newsletter</h1>
        <h3>List of all subsribers</h3>

        <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="success"><?= $msg; ?></p>
        <?php elseif ($msg = $this->session->flashdata('error')) : ?>
            <p class="error"><?= $msg; ?></p>
        <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
            <?= $msg; ?>
        <?php endif; ?>
        
        <div id="success"></div>
        <p>
            <?php
            echo form_open('admin/newsletter', array('method' => 'get', 'class' => 'admin_search'));
            echo form_input(array('name' => 'search', 'value' => $search_term, 'class' => 'admin_chat_text','placeholder' => 'Search newsletter by Email'));
            echo form_button(array('name' => 'search_submit', 'type' => 'submit', 'class' => 'admin_search_button', 'content' => 'Search'));
            echo form_close();
            ?>
        </p>

        <form method="post" action="<?php echo site_url('admin/newsletter/bulkDelete'); ?>"
              class="admin_slider_form">

            <?php if (isset($newsletters) && $newsletters) : ?>
            
                <button type="submit" onclick="return confirm('Are you sure ?')" class="dark_button"
                    href="<?= site_url('admin/newsletter/bulkDelete'); ?>">Delete selected 
            </button>
            <p></p>


            <table border="1" class="admin_table">

                <tr>
                    <th><input type="checkbox" id="main-checkbox"><label for="main-checkbox">Select items</label></th>
                    <th>ID</th>
                    <th>Email</th>  
                    <th>Date Added</th> 
                    <th>Delete</th> 
                </tr>

                <?php foreach ($newsletters as $key => $value): ?>
                    <tr>
                        <td>
                            <input class="bulk-checkbox" type="checkbox" value="<?php echo $value->id; ?>"
                                   name="bulkDelete[]">
                        </td>
                        <td><?= $value->id; ?></td>
                        <td><?= $value->email; ?></td>
                        <td><?= $value->date; ?></td>
                        <td>
                            <button type="button" data-id="<?php echo $value->id; ?>" id="delete-<?php echo $value->id; ?>"
                                    class="delete-row admin_link_button" class="admin_button">Delete
                            </button>
                        </td> 

                    </tr>

                <?php endforeach; ?>

            </table>
            <?php else : ?>
                <p class="error">
                    No data 
                </p>
                <a href="" onclick="goBack()" class="error_go_back">
                    <i class="fa  fa-long-arrow-left"></i>
                    Go back to previous page
                </a>
            <?php endif; ?>
        </form>
        <?php echo $links; ?>
        <br>
        <hr>
        <br>
        <h3>Sending email to all subscribers</h3>
        <?php
        echo form_open_multipart('admin/newsletter/email');

        echo validation_errors();

         
        echo '<p>Content: ';
        $data = array(
            'name' => 'text',
            'rows' => '10',
            'cols' => '20',
            'class' => "ckeditor"   
        );

        echo form_textarea($data);
        echo '</p>';

        echo form_button(array('type' => 'submit', 'class' => 'dark_button', 'content' => 'Send'));

        echo '</p>';
        echo form_close();
        ?>
        <script>
        function goBack() {
            window.history.back();
        }
        </script>

        <script>
            $(function () {
                $('.delete-row').on('click', function () {
                    var conf = confirm('Are you sure ?');
                    var id = $(this).data('id');
                    var $ele = $(this).parent().parent();
                    if (conf === true) {
                        $.ajax({
                            url: 'newsletter/ajaxDelete',
                            data: {
                                id: id
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (data) {
                                if (data.success === 1) {
                                    $ele.remove();
                                }
                                $('#success').addClass('success').text('You have successfuly deleted the email !');
                            }

                        });
                    }
                });
            });
        </script>

        <script>
            $(function () {
                $('#main-checkbox').change(function () {
                    var checkboxes = $(this).closest('form').find(':checkbox');
                    if ($(this).is(':checked')) {
                        checkboxes.prop('checked', true);
                    } else {
                        checkboxes.prop('checked', false);
                    }
                });
            });
        </script>
    </div> 
</section>



