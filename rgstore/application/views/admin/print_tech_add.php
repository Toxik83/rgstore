<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
        <h1>Printers Technologies</h1>
        <?= form_open('admin/print_tech/add'); ?>
        <?= validation_errors('<div class="error">', '</div>'); ?>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
            <p class="success"><?= $msg; ?></p>
        <?php elseif ($msg = $this->session->flashdata('error')) : ?>
            <p class="error"><?= $msg; ?></p>
        <?php elseif ($msg = $this->session->flashdata('error_validation')) : ?>
            <?= $msg; ?>
        <?php endif; ?>
        <div id="success"></div>
        <br>
        <label>Technology name: </label><br>
        <input type="text" name="print_name" value="" placeholder='Enter Technology name'/>
        <button type="submit" class="dark_button">Add
        </button>
        <?= form_close(); ?>
        <br>
        <br>
        <hr>
        <br>
        <br>
        <h3>List of All Printers Technologies</h3>
        <?php if (isset($print_tech)) : ?>
            <form action="<?= site_url('admin/print_tech'); ?>" method = "get" class="admin_search">
                <input type="text" name = "keyword" class="admin_chat_text" />
                <button type="submit" class="admin_search_button">Search
                </button>
            </form>
            <form method="post" action="<?= site_url('admin/print_tech/bulkDelete'); ?>"
                  accept-charset=""class="admin_slider_form">
                <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                        class="dark_button">Delete selected
                </button>
                <p></p>
                <table border="1" class="admin_table">
                    <tr>
                        <th>Reorder</th>
                        <th><input type="checkbox" id="main-checkbox"><label for="main-checkbox"> Select items</label></th>
                        <th>ID</th>
                        <th>Printer's technology name</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    <tbody id="sortable">
                        <?php foreach ($print_tech as $print): ?>
                            <tr>
                                <td><i class="fa fa-arrows-v">
                                </td>
                                <td>
                                    <input class="bulk-checkbox" type="checkbox" value="<?= $print->id; ?>"
                                    name="bulkDelete[]"> 
                                </td>
                                <td><?= $print->id; ?></td>
                                <td><?= $print->print_name; ?></td>
                                <input type="hidden" class="order" name="order[]" value="<?= $print->id; ?>"/>
                                <td>
                                    <a href="<?= site_url(); ?>/admin/print_tech/update/<?= $print->id; ?>">Edit</a>
                                </td>
                                <td>
                                    <a href='<?= site_url(); ?>/admin/print_tech/delete/<?= $print->id; ?>' onClick="return confirmDialog();">Delete</a>
                                </td>
                            </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </form>
            <?php echo $links; ?>
            <br>
            <a href="#" class="error_go_back"  onclick="goBack()">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a> 
        <?php else : ?>
            <p class="error">
                No results found.
            </p>
            <a href="#" class="error_go_back" onclick="goBack()">
                <i class="fa  fa-long-arrow-left"></i>
                Go back to previous page
            </a>
        <?php endif; ?>
    </div>
</section>
<script>
    $(function () {
        $('#main-checkbox').change(function () {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.prop('checked', true);
            } else {
                checkboxes.prop('checked', false);
            }
        });
    });
</script>
<script>
    $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            update: function () {
                var $vals = $('.order').map(function () {
                    return $(this).val();
                }).get();
                $.ajax({
                    url: '/index.php/admin/print_tech/ajaxReorder',
                    type: 'post',
                    data: {
                        order: $vals
                    },
                    error: function () {
                        alert('Error');
                    }
                })
            }
        });
        $("#sortable").disableSelection();
    });
</script>
<script>
    function goBack() {
        window.history.back();
    }
</script>
<?php $this->load->view('admin/includes/footer'); ?>
