<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?php echo $this->breadcrumb->output(); ?>
        </div>

        <h1>Edit Flash Date</h1>
        <?php
        echo form_open('admin/filament/flashdate_edit');
        ?>     
        <br>
        <?php if ($msg = $this->session->flashdata('success')) : ?>
                <p class="success"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error')) : ?>
                <p class="error"><?= $msg; ?></p>
            <?php elseif($msg = $this->session->flashdata('error_validation')) : ?>
                <?= $msg; ?>
        <?php endif; ?>
        <table cellpadding="2" cellspacing="2"  class="create_table" >
            <tr>
                <td><label for="title">Filament name :</label></td>
                <td><input id="title" type="text"  name='title' value="<?= $sources->title; ?>"></td>
            </tr>
            <tr>
                <td><label for="price">Price :</label></td>
                <td><input id='price' type="text"  name='price' value="<?= $sources->price; ?>"></td>
            </tr>
            <tr>
                <td><label for="new_price">New Price :</label></td>
                <td><input id ='new_price' type="text" name='new_price' value="<?= $sources->new_price; ?>"></td>
            </tr>
            <tr>
            <tr>
                <td>Flash Date (begin): </td>
                <td><input type="text" placeholder="Pickup Date" id="pick" name="fl_date_begin" data-field="datetime" data-format="yyyy-MM-dd hh:mm:ss"  readonly value="<?= $sources->fl_date_begin; ?>"></td>
                <td><input type="hidden" name="id" value = "<?= $this->uri->segment(4); ?>"></td>
            </tr>
            <div class="datepicker"></div>
            <tr>
                <td>Flash Date (end) : </td>
                <td><input type="text" data-field="datetime" name="fl_date_end" data-format="yyyy-MM-dd hh:mm:ss" readonly value="<?= $sources->fl_date_end; ?>" placeholder="Pickup Date" > </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?= form_button(array('name' => 'submit', 'type' => 'submit', 'content' => 'Update', 'class' => 'dark_button flash')); ?>
                    <?= form_close(); ?>
                </td>
                <?= form_open('admin/filament/flashDelete/' . $this->uri->segment(4)); ?>
                <td colspan="2">
                    <input type ="submit" name="submit" value ="Delete" class="dark_button flash" onClick="return confirmDialog();">    
                    <?= form_close(); ?>
                </td>
            </tr>
                <?php
                echo form_close();
                ?>
        </table>


    </div>

</section>
<script type="text/javascript">
    $(function () {
        $('.delete-row').on('click', function () {
            var conf = confirm('Are you sure you want to delete flash date ?');
            var id = $(this).data('id');
            if (conf === true) {
                $.ajax({
                    url: '/admin/filament/FlashajaxDelete',
                    data: {
                        id: id
                    },
                    type: 'post',
                    success: function () {
                       window.location.replace(window.location.origin + "/admin/filament");
                    }
                    
                });
            }
        });
    });
</script>

<script>		
$(document).ready(function()
{
    $(".datepicker").DateTimePicker({

            dateTimeFormat: "yyyy-MM-dd HH:mm:ss"
    });
});
</script>
<?php $this->load->view('admin/includes/footer'); ?>