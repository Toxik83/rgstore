<?php $this->load->view('admin/includes/header'); ?>
<section>
    <div class="admin_content_container">
        <div class="admin_breadcrumbs">
            <?= $this->breadcrumb->output(); ?>
        </div>
            <br>
                <h1>Filaments Page - Inactive</h1>        
                <div id="menu" class="admin_portfolio_view">
                    <?= anchor('admin/filament', 'Show Active Products'); ?>
                </div>
                <br>
                <?php 
                    echo form_open('admin/filament/showInactive', array('method' => 'get', 'class' => 'admin_search'));
                    echo form_input(array('name' => 'search','value' => $search_term, 'class' => 'admin_chat_text'));
                    echo form_button(array('name' => 'search_submit', 'type' => 'submit', 'class' => 'admin_search_button', 'content' => 'Search'));
                    echo form_close();
                ?> 
                <br>
                
        <form method="post" action="<?= site_url('admin/filament/bulkDelete'); ?>"
              class="admin_slider_form">
                 <?php if (isset($filaments)) :?>
              <button type="submit" onclick="return confirm('Selected items will be deleted. Continue?')"
                    <?= site_url('admin/filament/bulkDelete'); ?> class="dark_button">
                Delete selected
            </button>
            <p></p>

                    <table border="1" class="admin_table">
                        <tr>
                            <th><input type="checkbox" id="main-checkbox" ><label for="main-checkbox">Select items</label></th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Image Gallery</th>
                            <th>Price</th>
                            <th>New Price</th>
                            <th>Manufacturer</th>
                            <th>Date_update</th>
                            <th>Flash Date </th>    
                            <th>Edit</th>
                            <th>Active/Inactive</th>
                            <th>Delete</th>
                        </tr>

                        <?php foreach ($filaments as $allfilaments => $filament ): ?>
        </form>
                                <tr>
                                    <td>
                                        <input class="bulk-checkbox" type="checkbox" value="<?= $filament->id; ?>"
                                               name="bulkDelete[]">
                                    </td>
                                    <td><?php echo $filament->id; ?></td>

                                    <td><?php echo $filament->title; ?></td>

                                    <td align="center">
                                        <a href="<?= site_url(); ?>/admin/filamentsgallery/loadGalleryPage/<?= $filament->id; ?>">
                                            <img style = "width: 100px;" src="<?= base_url('upload/images155x155/' . $filament->name); ?>" />
                                            <br>Gallery</a>
                                    </td>
                                    <td align="center" ><?= $filament->price; ?></td>
                                    <td class="table_new_price"><?php echo form_open('admin/filament/editPromoPrice'); ?>                        
                                        <input type="text" style="width: 70px;" value="<?= $filament->new_price; ?>" name="new_price">
                                        <input type="hidden" value="<?= $filament->id; ?>" name="filament_id">
                                        <?= form_button(array('type' => 'submit', 'content' => 'Edit', 'class' => 'admin_link_button')) ?>
                                        <?= form_close(); ?>
                                    </td>  
                                    <td align="center" ><?= $filament->man_name; ?></td>

                                    <td align="center" ><?= $filament->date_update; ?></td>
                                    <td align="center" ><?php if ($filament->fl_date_begin == '0000-00-00 00:00:00' && $filament->fl_date_end == '0000-00-00 00:00:00'): ?>
                                            <a href="<?= site_url(); ?>/admin/filament/flashdate/<?= $filament->id; ?>">
                                                Add Flash Date</a>
                                        <?php else : ?> <a href="<?= site_url(); ?>/admin/filament/flashdate_edit/<?= $filament->id; ?>">Edit Flash Date
                                                <br><font color="red">begin: <?= $filament->fl_date_begin; ?><br>end: <?= $filament->fl_date_end; ?></font>
                                            <?php endif; ?></td>
                                            <td align="center" ><a href="<?= site_url(); ?>/admin/filament/updateFilament/<?= $filament->id; ?>">Edit</a>
                                            </td>
                                            <td align="center" ><a href="<?= site_url(); ?>/admin/filament/active/<?= $filament->id; ?>" >Make Active</a>
                                            </td>
                                            <td align="center" ><a href='<?= site_url(); ?>admin/filament/softdelete/<?= $filament->id; ?>' onClick="return confirmDialog();">Delete</a>
                    </td>
                                <?php endforeach; ?>
                        </tr>
                    </table>
                    <br>
                    <?= $links; ?>
                    <?php  else : ?>
                    <p class="error">
                        No results found.
                    </p>
            
            <br>
        <?php endif;?>
    </div>
</section>
<?php $this->load->view('admin/includes/footer'); ?>