<?php $this->load->view('includes/header'); ?>
<body>
<div class="admin_form_container">
    <div align="center">

        <?php if ($this->session->flashdata('error') != null) : ?>

            <span class="notification warning">
                <?= $this->session->flashdata('error'); ?>
            </span>

        <?php endif; ?>

    </div>
    <div class="form_admin">
        <h1>Login</h1>

        <?= form_open('admin/home/login'); ?>

        <div class="user">
            Username:
            <?= form_input('username'); ?>
        </div>

        <div class="user">Password:
            <?= form_password('pass'); ?>
        </div>

        <?php if ($this->session->flashdata('empty_f') != null) : ?>

            <div class="form_error">
                <p>
                    <?= $this->session->flashdata('empty_f'); ?>
                </p>
            </div>

        <?php endif; ?>

        <?php
        $loginButton = array(
            'type' => 'submit',
            'content' => 'Login',
            'class' => ''
        );
        ?>
        <?= form_button($loginButton); ?>

        <?= form_close(); ?>

    </div>
</div>
</body>
</html>