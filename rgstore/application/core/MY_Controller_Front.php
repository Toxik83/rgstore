<?php

class MY_Controller_Front extends CI_Controller {

    function __construct() {
        parent::__construct();       
        $this->load->model('users_model');

        if (!$this->session->userdata('logged_in')) {                     
            if (isset($_COOKIE['test'])) {
                
                $rememberedUser = $this->users_model->getUserByHash($_COOKIE['test']);
                
                if ($rememberedUser->num_rows() == 1) { 
                    
                    $userData = $rememberedUser->row();                            
                    $sessionData = array(
                        'userId' => $userData->id,
                        'email' => $userData->email,
                        'logged_in' => true,
                    );

                    $this->session->set_userdata($sessionData);
                }
            }
        }
       ////        ЗАДЪЛЖИТЕЛНО ПРЕДАИТЕ НА VIEW-ТО СИ $this->news !!!
//        В ПРОТИВЕН СЛУЧАЙ НЯМА ДА ВИ СЕ ПРЕДАДАТ ДАННИТЕ КЪМ VIEW-ТО ... 
      
    }
    
     public function news() {
        $data = array();
    
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback__email_check');

        if ($this->input->post()) {
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect($_SERVER['HTTP_REFERER']);
                echo 'asdasd';
            } else {
                $data = array(
                    'email' => $this->input->post('email'),
                    'date' => date("Y-m-d H:i:s")
                );
                
                $this->session->set_flashdata('success_news', 'You have successfully sent your email and subscribed for our hot news. Congratulations!');

                $this->footer_m->news($data);
                redirect($_SERVER['HTTP_REFERER']);
                
            }
        }
    }
    
    public function _email_check($email) {
        $conditions = array(
            'email' => $email
        );
        $emailData = $this->footer_m->getEmails($conditions)->num_rows();

        if ($emailData > 0) {

            $this->form_validation->set_message('_email_check', 'This Email already exist and receives our latest news :)');
            return false;
        } else {
            return true;
        }

    }
    
}
