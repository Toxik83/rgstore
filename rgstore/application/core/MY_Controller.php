<?php

class MY_Controller extends CI_Controller {

    function __construct() {

        parent::__construct();       
        if (!isAdmin()) {
            redirect('admin');
        }
        
        $this->load->model('contact_model');
        $conditions = array(
            'status' => 0
        );

        $data['newMess'] = $this->data['newMessages'] = $this->contact_model->getMessage($conditions, null)->num_rows();

        $like = array();
        $this->data['newMessages'] = $this->contact_model->getMessage($conditions, null, $like)->num_rows();

//        ЗАДЪЛЖИТЕЛНО ПРЕДАИТЕ НА VIEW-ТО СИ 'RECENT' => $this->data['recent'] !!!
//        В ПРОТИВЕН СЛУЧАЙ НЯМА ДА ВИ СЕ ПРЕДАДАТ ДАННИТЕ КЪМ VIEW-ТО ... 
    }

    public function index() {
        
    }

}
