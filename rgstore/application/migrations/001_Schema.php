<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Schema extends CI_Migration {

    public function up() {

        // newsletter table starts here
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'date' => array(
                'type' => 'TIMESTAMP',
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('newsletter');

        // end of newsletter table
        //Slider table
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'image' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'image300x100' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'url' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'internal' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('slider');

        //Users table
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '16',
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'fname' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'lname' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'reset_token' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'registered_at' => array(
                'type' => 'DATETIME', 
            ),
            'reset_at' => array(
                'type' => 'DATETIME',
            ),
            'google_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
            ),
            'google_acc_token' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
            ),
            'facebook_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
            ),
            'facebook_acc_token' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
            ),
            'twitter_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
            ),
            'twitter_acc_token' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
            ),
            'is_admin' => array(
                'type' => 'int',
                'constraint' => '1'
            ),
            'online' => array(
                'type' => 'int',
                'constraint' => '1'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('users');

        //user_details table
        $this->dbforge->add_field(array(
            'detail_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'city' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'zip_code' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'country_id' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
            'phone' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'phonecode' => array(
                'type' => 'VARCHAR',
                'constraint' => '45',
            ),
            'is_main' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
            'is_deleted' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
        ));
        $this->dbforge->add_key('detail_id', TRUE);
        $this->dbforge->create_table('user_details');

        //inbox table
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'from' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'subject' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'message' => array(
                'type' => 'TEXT',
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'sent_at' => array(
                'type' => 'DATETIME',
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('inbox');

        //products table start
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'cat_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'text' => array(
                'type' => 'TEXT'
            ),
            'price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2'
            ),
            'new_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2'
            ),
            
            'f_cat_id' => array(
                'type' => 'INT',
                'constarint' => '11',
                'null' => TRUE
            ),
            'man_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE

            ),
            'print_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE

            ),
            'build_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE

            ),
            'acc_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE

            ), 
            'fl_date_begin' => array(
                'type' => 'DATETIME',
                'default' => '0000-00-00 00:00:00'
            ),
            'fl_date_end' => array(
                'type' => 'DATETIME',
                'default' => '0000-00-00 00:00:00'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            ),
            'date_update' => array(
                'type' => 'DATETIME'
            ), 
             'sold' => array(
                'type' => 'INT',
                 'constraint'=>'11'
            ),
            'active' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('man_id');
        $this->dbforge->add_key('print_id');
        $this->dbforge->add_key('build_id');
        $this->dbforge->add_key('acc_id');
        $this->dbforge->add_key('f_cat_id');
        $this->dbforge->create_table('products');
        //products table end
        //products_images table begin
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'product_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'main_pic' => array(
                'type' => 'INT',
                'constraint' => 1
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('product_id');
        $this->dbforge->create_table('products_images');
        //products_images table end
        //accuracy table begin
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'accuracy_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'active' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('accuracy');
        //accuracy table end
        //building_size table begin
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'build_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'active' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('building_size');
        //building_size table end
        //manufacturer table begin
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'man_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'active' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('manufacturer');
        //manufacturer table end
        //price_range table begin
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'min' => array(
                'type' => 'INT',
                'constraint' => 1
            ),
            'max' => array(
                'type' => 'INT',
                'constraint' => 5
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'active' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ),
            'date' => array(
                'type' => 'DATETIME',
                'default' => '0000.00.00 00:00:00'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('price_ranges');
        //price_ranges table end
        //print_tech table begin
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'print_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'active' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('print_tech');
        //print_tech table end
        //shipping table begin
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'range' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'price' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            )
        ));
        $this->dbforge->add_key('id', TRUE);

        $this->dbforge->create_table('shipping');
        //printers_images table end
        //menus table  
        $this->dbforge->add_field(array(
            'menus_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'menu_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'keyword' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'is_deletable' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
        ));
        $this->dbforge->add_key('menus_id', TRUE);
        $this->dbforge->create_table('menus');

        //static_pages table  
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
            ),
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'link_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'link_to_page' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
            ),
            'content' => array(
                'type' => 'TEXT',
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('static_pages');

        //country table
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'iso' => array(
                'type' => 'CHAR',
                'constraint' => '2',
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'display_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'iso_3' => array(
                'type' => 'CHAR',
                'constraint' => '3',
            ),
            'numcode' => array(
                'type' => 'SMALLINT',
                'constraint' => 6,
            ),
            'phonecode' => array(
                'type' => 'INT',
                'constraint' => 5,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('country');

        //menu_items table  
        $this->dbforge->add_field(array(
            'item_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'menu_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'page_link' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'link_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'internal' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
            'is_deletable' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
            'icon' => array(
                'type' => 'VARCHAR',
                'constraint' => '45',
            ),
        ));
        $this->dbforge->add_key('item_id', TRUE);
        $this->dbforge->create_table('menu_items');



        //Table category

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'cat_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '128'
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
                'active' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ),
            'date' => array(
                'type' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL',
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('category');

        //products_ordered
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'product_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'quantity' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
            'total_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
            'options' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('products_ordered');
        $this->dbforge->add_key('order_id');

        //Orders table
        $this->dbforge->add_field(array(
            'order_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'fname' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'lname' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'city' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'zip_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'country_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'phone' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'phone_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'order_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2'
            ),
            'date_added' => array(
                'type' => 'DATETIME'
            ),
            'status' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ),
            'b_address' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'b_city' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'b_zip_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'b_country_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'b_phone' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'b_phone_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
        ));
        $this->dbforge->add_key('order_id', TRUE);
        $this->dbforge->add_key('user_id');
        $this->dbforge->create_table('orders');

        //End of Orders
        //bundle table
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'product_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'bundle_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'combined_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2'
            ),
            'discount_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2'
            ),
            'discount' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('product_id');
        $this->dbforge->create_table('bundle');

        //bundle_items
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'product_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'bundle_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('bundle_id');
        $this->dbforge->create_table('bundle_items');

        // Transactions paypal talbe
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'payment_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'hash' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'complete' => array(
                'type' => 'INT',
                'constraint' => 1
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('transactions_paypal');
        
        //chat table
        
          $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'from' => array(
                'type' => 'varchar',
                'constraint' => 255,
            ),
            'to' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'message' => array(
                'type' => 'text'
            ),
            'sent' => array(
                'type' => 'DATETIME',
                'default' => '0000-00-00 00:00:00'
            ),
            'recd' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => 0
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('chat');
        
        //end of chat table
        
        // billing_addresses table
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'b_address' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'b_city' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'b_zip_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'b_country_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'b_phone_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'b_phone' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'is_main' => array(
                'type' => 'INT',
                'constraint' => 11,
            ), 
            'is_deleted' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ), 
            
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('user_id');
        $this->dbforge->create_table('billing_addresses');
        
        //Foreign Keys
        $this->db->query('ALTER TABLE billing_addresses
                          ADD FOREIGN KEY (user_id)
                          REFERENCES users(id) ON DELETE CASCADE ON 
                          UPDATE CASCADE'
        );
        
        $this->db->query('ALTER TABLE products_images
                          ADD FOREIGN KEY (product_id)
                          REFERENCES products(id) ON DELETE CASCADE ON 
                         UPDATE CASCADE'
        );
        
        $this->db->query('ALTER TABLE products
                          ADD FOREIGN KEY (man_id)
                          REFERENCES manufacturer(id) ON DELETE CASCADE ON 
                         UPDATE CASCADE'
        );
        
        $this->db->query('ALTER TABLE products
                          ADD FOREIGN KEY (print_id)
                          REFERENCES print_tech(id) ON DELETE CASCADE ON 
                         UPDATE CASCADE'
        );
        
        $this->db->query('ALTER TABLE products
                          ADD FOREIGN KEY (build_id)
                          REFERENCES building_size(id) ON DELETE CASCADE ON 
                         UPDATE CASCADE'
        );
        
        $this->db->query('ALTER TABLE products
                          ADD FOREIGN KEY (acc_id)
                          REFERENCES accuracy(id) ON DELETE CASCADE ON 
                         UPDATE CASCADE'
        );
        
        $this->db->query('ALTER TABLE products
                          ADD FOREIGN KEY (f_cat_id)
                          REFERENCES category(id) ON DELETE CASCADE ON 
                          UPDATE CASCADE'
        );
        
        $this->db->query('ALTER TABLE menu_items
                          ADD FOREIGN KEY (menu_id)
                          REFERENCES menus(menus_id) ON DELETE CASCADE ON 
                         UPDATE CASCADE'
        );

        $this->db->query('ALTER TABLE user_details
                          ADD FOREIGN KEY (country_id)
                          REFERENCES country(id) ON DELETE CASCADE ON 
                          UPDATE CASCADE'
        );

        $this->db->query('ALTER TABLE products_ordered
                          ADD FOREIGN KEY (order_id)
                          REFERENCES orders(order_id) ON DELETE CASCADE ON 
                          UPDATE CASCADE'
        );

        $this->db->query('ALTER TABLE orders
                          ADD FOREIGN KEY (user_id)
                          REFERENCES users(id) ON DELETE CASCADE ON 
                          UPDATE CASCADE'
        );

        $this->db->query('ALTER TABLE bundle
                          ADD FOREIGN KEY (product_id)
                          REFERENCES products(id) ON DELETE CASCADE ON 
                          UPDATE CASCADE'
        );
        
        $this->db->query('ALTER TABLE bundle_items
                          ADD FOREIGN KEY (bundle_id)
                          REFERENCES bundle(product_id) ON DELETE CASCADE ON 
                          UPDATE CASCADE'
        );
        
    }

    public function down() {
        $this->dbforge->drop_table('admins');
        $this->dbforge->drop_table('newsletter');
        $this->dbforge->drop_table('slider');
        $this->dbforge->drop_table('users');
        $this->dbforge->drop_table('user_details');
        $this->dbforge->drop_table('products');
        $this->dbforge->drop_table('products_images');
        $this->dbforge->drop_table('accuracy');
        $this->dbforge->drop_table('building_size');
        $this->dbforge->drop_table('manufacturer');
        $this->dbforge->drop_table('price_ranges');
        $this->dbforge->drop_table('print_tech');
        $this->dbforge->drop_table('finishings');
        $this->dbforge->drop_table('category');
        $this->dbforge->drop_table('inbox');
        $this->dbforge->drop_table('menus');
        $this->dbforge->drop_table('menu_items');
        $this->dbforge->drop_table('static_pages');
        $this->dbforge->drop_table('country');
        $this->dbforge->drop_table('category');
        $this->dbforge->drop_table('products_ordered');
        $this->dbforge->drop_table('orders');
        $this->dbforge->drop_table('bundle');
        $this->dbforge->drop_table('bundle_items');
        $this->dbforge->drop_table('transactions_paypal');
        $this->dbforge->drop_table('billing_addresses');
    }

}
