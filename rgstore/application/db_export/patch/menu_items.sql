-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 07, 2015 at 09:51 AM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rgstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE IF NOT EXISTS `menu_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `page_link` varchar(255) NOT NULL,
  `link_name` varchar(255) NOT NULL,
  `internal` int(1) NOT NULL,
  `order_id` int(1) NOT NULL,
  `is_deletable` int(1) NOT NULL,
  `icon` varchar(45) DEFAULT 'icon-cube',
  PRIMARY KEY (`item_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`item_id`, `menu_id`, `page_link`, `link_name`, `internal`, `order_id`, `is_deletable`, `icon`) VALUES
(44, 9, '/', 'Home', 1, 0, 0, 'icon-cube'),
(45, 9, '/printersportfolio', 'Printers', 1, 1, 0, 'icon-cube'),
(46, 9, 'filamentportfolio', 'Filament', 1, 2, 0, 'icon-cube'),
(47, 9, '/finishing', 'Finishing', 1, 3, 0, 'icon-cube'),
(48, 10, 'page/news', 'News', 1, 48, 1, 'icon-cube'),
(49, 10, 'page/about', 'About us', 1, 49, 1, 'icon-cube'),
(50, 10, 'page/career', 'Careers', 1, 50, 1, 'icon-cube'),
(51, 10, 'contact-us', 'Contact us', 1, 51, 1, 'icon-cube'),
(52, 11, 'page/support', 'Custumer support help', 1, 52, 1, 'icon-tools2'),
(53, 11, 'page/delivery-europe', 'Delivery by DHL in Europe', 1, 53, 1, 'icon-cube2'),
(54, 11, 'page/instalation', 'Installation', 1, 54, 1, 'icon-tools'),
(55, 11, 'page/guarantee', 'Guarantee', 1, 55, 1, 'icon-thumbsup'),
(56, 11, 'page/return', 'Returns', 1, 56, 1, 'icon-reply'),
(57, 11, 'https://www.google.bg', 'Google search', 0, 57, 1, 'icon-cube'),
(58, 12, 'page/payment', 'Payment', 1, 0, 1, 'icon-dollar'),
(59, 12, 'page/delivery', 'Delivery', 1, 1, 1, 'icon-cube'),
(60, 12, 'page/instalation', 'Installation', 1, 2, 1, 'icon-tools'),
(61, 12, 'page/return', 'Returns', 1, 3, 1, 'icon-reply'),
(62, 12, 'page/guarantee', 'Guarantee', 1, 4, 1, 'icon-thumbsup'),
(63, 12, 'contact-us', 'Contact us', 1, 5, 1, 'icon-phone2'),
(64, 13, 'page/money-back', 'Money-Back Guarantee', 1, 0, 1, 'icon-dollar2'),
(65, 13, 'page/best-choice', 'Best Price & Largest Choice', 1, 1, 1, 'icon-discout'),
(66, 13, 'page/delivery-europe', 'Delivery by DHL in Europe', 1, 2, 1, 'icon-cube2'),
(67, 13, 'page/stores', 'Collect in Store', 1, 3, 1, 'icon-hand'),
(68, 13, 'page/support', 'Custumer support & Installation', 1, 4, 1, 'icon-tools2'),
(69, 13, 'page/guarantee', 'Guarantee', 1, 5, 1, 'icon-thumbsup2'),
(70, 13, 'page/buyers-guide', 'Buyer''s Guide', 1, 6, 1, 'icon-clipboard'),
(71, 14, '/printersportfolio/printersdetail/10', 'Printer 2', 1, 71, 1, 'icon-cube');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`menus_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
