-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 17, 2015 at 02:52 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rgstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `accuracy`
--

CREATE TABLE IF NOT EXISTS `accuracy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accuracy_name` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `accuracy`
--

INSERT INTO `accuracy` (`id`, `accuracy_name`, `order_id`, `active`) VALUES
(1, '0.10mm', 0, 0),
(2, '0.15mm', 0, 0),
(3, '0.20mm', 0, 0),
(4, '0.25mm', 0, 0),
(5, '0.30mm', 0, 0),
(6, '0.35mm', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `billing_addresses`
--

CREATE TABLE IF NOT EXISTS `billing_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `b_address` varchar(255) NOT NULL,
  `b_city` varchar(255) NOT NULL,
  `b_zip_code` varchar(255) NOT NULL,
  `b_country_id` int(11) NOT NULL,
  `b_phone_code` varchar(255) NOT NULL,
  `b_phone` varchar(255) NOT NULL,
  `is_main` int(1) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `billing_addresses`
--

INSERT INTO `billing_addresses` (`id`, `user_id`, `b_address`, `b_city`, `b_zip_code`, `b_country_id`, `b_phone_code`, `b_phone`, `is_main`, `is_deleted`) VALUES
(9, 7, 'Street num 212', 'Plovdiv', '4000', 33, '359', '885791348', 0, 0),
(10, 7, 'Uarsd eist 143', 'Aalborg', '4124', 58, '45', '112233', 0, 0),
(11, 7, 'adresa mi', 'Aalborg', '6000', 58, '45', '123999332', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `building_size`
--

CREATE TABLE IF NOT EXISTS `building_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build_name` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `building_size`
--

INSERT INTO `building_size` (`id`, `build_name`, `order_id`, `active`) VALUES
(1, 'XL', 0, 1),
(2, 'Small', 0, 0),
(3, 'Medium', 0, 0),
(4, 'Large', 0, 0),
(5, 'XXL', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bundle`
--

CREATE TABLE IF NOT EXISTS `bundle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `bundle_name` varchar(255) NOT NULL,
  `combined_price` decimal(10,2) NOT NULL,
  `discount_price` decimal(10,2) NOT NULL,
  `discount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bundle`
--

INSERT INTO `bundle` (`id`, `product_id`, `bundle_name`, `combined_price`, `discount_price`, `discount`) VALUES
(1, 4, 'Printer 1 + Filament 1 + Finishing 1', 1600.00, 1440.00, 10),
(2, 41, 'Printer 7 + Finishing 3 + Filament 12 + Filament 3', 2365.00, 1892.00, 20),
(3, 42, 'Printer 10 + Finishing 7 + Filament 13 + Filament 11', 3375.00, 3037.50, 10),
(4, 43, 'Printer 13 + Finishing 3 + Filament 8 + Filament 9', 4455.00, 3118.50, 30);

-- --------------------------------------------------------

--
-- Table structure for table `bundle_items`
--

CREATE TABLE IF NOT EXISTS `bundle_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `bundle_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bundle_id` (`bundle_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `bundle_items`
--

INSERT INTO `bundle_items` (`id`, `product_id`, `bundle_id`) VALUES
(1, 1, 4),
(2, 2, 4),
(3, 3, 4),
(4, 6, 41),
(5, 23, 41),
(6, 30, 41),
(7, 39, 41),
(8, 10, 42),
(9, 26, 42),
(10, 38, 42),
(11, 40, 42),
(12, 6, 43),
(13, 29, 43),
(14, 35, 43),
(15, 36, 43);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(128) NOT NULL,
  `order_id` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_name`, `order_id`, `active`, `date`) VALUES
(1, 'ABS', 0, 0, '2015-10-27 14:37:28'),
(2, 'HIPS', 0, 0, '2015-10-27 14:37:45'),
(3, 'Nylon', 0, 0, '2015-10-27 14:37:59'),
(4, 'PC', 0, 0, '2015-10-27 14:38:05'),
(5, 'PET', 0, 0, '2015-10-27 14:38:09'),
(6, 'PETT', 0, 0, '2015-10-27 14:38:15'),
(7, 'Soft PLA', 0, 0, '2015-10-27 14:38:23'),
(8, 'TPE', 0, 0, '2015-10-27 14:38:26'),
(9, 'Wood', 0, 0, '2015-10-27 14:38:35'),
(10, 'Paint', 0, 0, '2015-10-27 14:39:02'),
(11, 'Aceton', 0, 0, '2015-10-27 14:39:32');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) NOT NULL DEFAULT '',
  `to` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `to` (`to`),
  KEY `from` (`from`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=123 ;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `from`, `to`, `message`, `sent`, `recd`) VALUES
(1, 'Guest', 'user', 'asd', '2015-11-10 17:58:22', 1),
(2, 'Guest', 'user', 'das', '2015-11-10 17:58:23', 1),
(3, 'Guest', 'user', 'das', '2015-11-10 17:58:23', 1),
(4, 'Guest', 'user', 'sad', '2015-11-10 17:58:24', 1),
(5, 'user', 'Velko', 'hi there', '2015-11-10 17:58:55', 1),
(6, 'user', 'Velko', 'peeras', '2015-11-10 17:58:58', 1),
(7, 'admin', 'Velko', 'a', '2015-11-10 17:59:25', 1),
(8, 'Velko', 'user', 'a', '2015-11-10 18:01:09', 1),
(9, 'Velko', 'user', '?', '2015-11-10 18:01:12', 1),
(10, 'Velko', 'user', 'wtf', '2015-11-10 18:01:18', 1),
(11, 'Velko', 'user', 'asd', '2015-11-10 18:02:22', 1),
(12, 'Guest', 'Velko', 'Hello there', '2015-11-10 18:03:51', 1),
(13, 'admin', 'Velko', 'a', '2015-11-10 18:04:46', 1),
(14, 'admin', 'Velko', 'WTF E', '2015-11-10 18:04:48', 1),
(15, 'Guest', 'user', 'hi there', '2015-11-10 18:05:29', 1),
(16, 'user', 'user', 'asd', '2015-11-10 18:05:45', 1),
(17, 'user', 'Guest', 'a', '2015-11-10 18:06:03', 1),
(18, 'user', 'Guest', '?', '2015-11-10 18:06:04', 1),
(19, 'user', 'Guest', 'aklshdas', '2015-11-10 18:06:05', 1),
(20, 'user', 'Guest', 'aksjd', '2015-11-10 18:06:06', 1),
(21, 'user', 'user', 'asd', '2015-11-10 18:06:07', 1),
(22, 'user', 'user', 'asd', '2015-11-10 18:06:19', 1),
(23, 'user', 'Guest', 'asd', '2015-11-10 18:06:23', 1),
(24, 'user', 'user', 'asd', '2015-11-10 18:06:30', 1),
(25, 'user', 'Guest', 'asd', '2015-11-10 18:06:34', 1),
(26, 'user', 'Guest', 'asd', '2015-11-10 18:06:37', 1),
(27, 'user', 'Guest', 'asd', '2015-11-10 18:06:39', 1),
(28, 'user', 'Guest', 'asd', '2015-11-10 18:06:39', 1),
(29, 'user', 'Velko', 'Hello there', '2015-11-10 18:07:34', 1),
(30, 'user', 'Velko', 'Helloooo', '2015-11-10 18:07:45', 1),
(31, 'Velko', 'user', 'hi', '2015-11-10 18:08:22', 1),
(32, 'Velko', 'user', 'well ...', '2015-11-10 18:08:31', 1),
(33, 'Velko', 'user', 'hi there', '2015-11-10 18:09:50', 1),
(34, 'Velko', 'VelkoVelkovich', 'a', '2015-11-10 18:10:18', 0),
(35, 'Velko', 'VelkoVelkovich', 'Hellooo', '2015-11-10 18:10:20', 0),
(36, 'Guest', 'Velko', 'hi Vadmin', '2015-11-10 18:18:51', 1),
(37, 'admin', 'Velko', 'asd', '2015-11-10 18:19:21', 1),
(38, 'admin', 'Velko', 'ads', '2015-11-10 18:19:22', 1),
(39, 'admin', 'Velko', 'asd', '2015-11-10 18:19:23', 1),
(40, 'admin', 'Velko', 'asd', '2015-11-10 18:19:23', 1),
(41, 'admin', 'Velko', 'asd', '2015-11-10 18:19:24', 1),
(42, 'admin', 'Velko', 'asd', '2015-11-10 18:19:25', 1),
(43, 'Didka', 'Velko', 'asd', '2015-11-10 18:19:42', 1),
(44, 'Didka', 'Velko', 'das', '2015-11-10 18:21:04', 1),
(45, 'Didka', 'Velko', 'das', '2015-11-10 18:21:15', 1),
(46, 'Didka', 'Velko', 'das', '2015-11-10 18:21:16', 1),
(47, 'Didka', 'Velko', 'dsa', '2015-11-10 18:21:16', 1),
(48, 'Didka', 'Velko', 'asd', '2015-11-10 18:21:17', 1),
(49, 'Didka', 'Velko', 'Velko ?', '2015-11-10 18:21:19', 1),
(50, 'Didka', 'Velko', 'asd', '2015-11-10 18:22:07', 1),
(51, 'admin', 'Didka', 'a', '2015-11-10 18:23:07', 1),
(52, 'admin', 'Didka', 'i`m the admin', '2015-11-10 18:23:11', 1),
(53, 'Didka', 'admin', 'Hi admin , what`s up ?', '2015-11-10 18:23:47', 1),
(54, 'admin', 'Didka', 'i`m fine tnx', '2015-11-10 18:24:22', 1),
(55, 'Didka', 'admin', 'hi there', '2015-11-10 18:26:45', 1),
(56, 'Didka', 'admin', 'Ñ‚Ð¸ ÑÐ¸ Ð¿ÐµÐµÑ€Ð°Ñ', '2015-11-10 18:28:13', 1),
(57, 'admin', 'Didka', 'Ð¼ÐµÑ€ÑÐ¸', '2015-11-10 18:28:42', 1),
(58, 'admin', 'Didka', 'Ð±Ð»Ñ Ð±Ð»Ñ ?', '2015-11-10 18:29:39', 1),
(59, 'Didka', 'admin', 'Ð¹ÐµÐ¹', '2015-11-10 18:30:01', 1),
(60, 'Didka', 'admin2', 'a', '2015-11-10 18:31:00', 1),
(61, 'Didka', 'admin2', 'hello', '2015-11-10 18:31:06', 1),
(62, 'Guest', 'admin', 'Hello admin :)', '2015-11-11 13:11:31', 1),
(63, 'admin', 'Guest', 'Hello there :)', '2015-11-11 13:11:54', 1),
(64, 'Didka', 'admin', 'Hi there :)', '2015-11-11 13:21:45', 1),
(65, 'Didka', 'admin', 'I have a Q', '2015-11-11 13:21:54', 1),
(66, 'Didka', 'admin', 'ÐºÐ²Ð¾ ÑÑ‚Ð°Ð²Ð° Ð±Ð°Ñ‚ÐºÐ° :D', '2015-11-11 13:23:44', 1),
(67, 'Didka', 'admin', 'hi', '2015-11-11 13:43:50', 1),
(68, 'Guest', 'admin', 'asd', '2015-11-11 13:58:59', 1),
(69, 'Didka', 'admin', 'asdasd', '2015-11-11 14:06:24', 1),
(70, 'admin', 'Didka', 'asd', '2015-11-11 14:07:14', 1),
(71, 'Didka', 'admin', 'haha', '2015-11-11 14:07:29', 1),
(72, 'admin', 'admin', 'asd', '2015-11-11 14:34:16', 1),
(73, 'admin', 'admin', 'dsa', '2015-11-11 14:34:17', 1),
(74, 'admin', 'admin', 'asd', '2015-11-11 14:34:18', 1),
(75, 'admin', 'admin', 'dsa', '2015-11-11 14:34:19', 1),
(76, 'admin', 'admin', 'dsa', '2015-11-11 14:34:20', 1),
(77, 'admin', 'admin', 'dsa', '2015-11-11 14:34:21', 1),
(78, 'admin', 'admin', 'dsa', '2015-11-11 14:34:22', 1),
(79, 'admin', 'admin', 'd', '2015-11-11 14:34:23', 1),
(80, 'admin', 'admin', 'd', '2015-11-11 14:34:24', 1),
(81, 'admin', 'admin', 'd', '2015-11-11 14:34:27', 1),
(82, 'admin', 'admin', 'asd', '2015-11-11 15:05:09', 1),
(83, 'Didka', 'admin', 'asd', '2015-11-11 15:06:05', 1),
(84, 'Didka', 'admin', 'avsdv', '2015-11-11 15:06:19', 1),
(85, 'Guest', 'admin', 'asd', '2015-11-11 15:06:51', 1),
(86, 'Guest', 'admin', 'bql  ?', '2015-11-11 15:06:55', 1),
(87, 'Guest', 'admin', 'asd', '2015-11-11 15:11:24', 1),
(88, 'admin', 'admin', 'asd', '2015-11-11 15:25:39', 1),
(89, 'Didka', 'admin', 'asd', '2015-11-11 15:28:38', 1),
(90, 'admin', 'Didka', 'asd', '2015-11-11 15:28:51', 1),
(91, 'admin', 'Didka', 'asd', '2015-11-11 15:29:16', 1),
(92, 'admin', 'Didka', 'bqllbq', '2015-11-11 15:29:36', 1),
(93, 'admin', 'admin', 'Hello there', '2015-11-11 15:30:06', 1),
(94, 'Didka', 'admin', 'hi there', '2015-11-11 15:30:41', 1),
(95, 'Didka', 'admin', 'asd', '2015-11-11 15:36:56', 1),
(96, 'admin', 'Didka', 'asd', '2015-11-11 15:37:16', 1),
(97, 'admin', 'Didka', 'bql', '2015-11-11 15:37:28', 1),
(98, 'admin', 'Didka', 'bql', '2015-11-11 15:37:31', 1),
(99, 'Guest', 'admin', 'hi admin', '2015-11-11 15:38:12', 1),
(100, 'Didka', 'admin', 'Hello', '2015-11-11 15:39:14', 1),
(101, 'Didka', 'admin', 'There', '2015-11-11 15:39:15', 1),
(102, 'admin', 'Didka', 'hi there :)', '2015-11-11 15:39:35', 1),
(103, 'Didka', 'admin', 'Hy admin', '2015-11-11 15:47:25', 1),
(104, 'Didka', 'admin', 'i have a question', '2015-11-11 15:47:28', 1),
(105, 'admin', 'Didka', 'what is your question ?', '2015-11-11 15:57:13', 1),
(106, 'Guest', 'admin', 'asdasd', '2015-11-12 10:34:23', 1),
(107, 'Didka', 'admin', 'hi there', '2015-11-12 10:39:07', 1),
(108, 'admin', 'Didka', 'Kvo ?', '2015-11-12 13:23:27', 1),
(109, 'Guest', 'admin', 'Ð°ÑÐ´', '2015-11-12 15:25:36', 1),
(110, 'Guest', 'admin', 'Ð°ÑÐ´', '2015-11-12 15:25:42', 1),
(111, 'Guest', 'admin', 'Ð°ÑÐ´', '2015-11-12 15:25:51', 1),
(112, 'Didka', 'admin', 'a ?', '2015-11-12 15:28:02', 1),
(113, 'Didka', 'admin', ':Ð”', '2015-11-12 15:28:04', 1),
(114, 'Didka', 'admin', ':D', '2015-11-12 15:28:07', 1),
(115, 'Guest', 'admin', 'asd', '2015-11-12 15:28:36', 1),
(116, 'Guest', 'admin', 'asdasd', '2015-11-12 15:38:37', 1),
(117, 'Guest', 'admin', 'asd', '2015-11-12 15:38:38', 1),
(118, 'admin', 'admin', 'asd', '2015-11-13 09:52:04', 1),
(119, 'admin', 'admin', 'asd', '2015-11-13 09:52:06', 1),
(120, 'Guest', 'admin', 'asd', '2015-11-13 10:23:36', 1),
(121, 'Guest', 'admin', 'hellooo', '2015-11-13 10:23:38', 1),
(122, 'admin', 'Guest', 'hii', '2015-11-13 10:23:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `display_name` varchar(80) NOT NULL,
  `iso_3` char(3) NOT NULL,
  `numcode` smallint(6) NOT NULL,
  `phonecode` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `display_name`, `iso_3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', '', 0, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', '', 0, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', '', 0, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', '', 0, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', '', 0, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D''IVOIRE', 'Cote D''Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', '', 0, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', '', 0, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF', 'Korea, Democratic People''s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE''S DEMOCRATIC REPUBLIC', 'Lao People''s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', '', 0, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', '', 0, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', '', 0, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', '', 0, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', '', 0, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', '', 0, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE IF NOT EXISTS `inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `sent_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `inbox`
--

INSERT INTO `inbox` (`id`, `from`, `to`, `subject`, `message`, `status`, `sent_at`) VALUES
(1, 'user@email.com', 'administrator', 'Hello', 'Message to the admin here...', '', '2015-11-13 16:34:52');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `man_name` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`id`, `man_name`, `order_id`, `active`) VALUES
(1, 'Solidoodle', 0, 0),
(2, 'UP!', 0, 0),
(3, 'LeapFrog', 0, 0),
(4, 'Weistek', 0, 0),
(5, 'Cubify', 0, 0),
(6, 'Makerbot', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `menus_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `is_deletable` int(1) NOT NULL,
  PRIMARY KEY (`menus_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`menus_id`, `menu_name`, `keyword`, `is_deletable`) VALUES
(9, 'Navigation', 'nav', 0),
(10, 'Get to know us', 'footer_inner1', 1),
(11, 'Let us help you', 'footer_inner2', 1),
(12, 'Footer outer', 'footer_outer', 1),
(13, 'Static sidebar', 'static_sidebar', 1),
(14, 'Best sellers products', 'footer_inner3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE IF NOT EXISTS `menu_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `page_link` varchar(255) NOT NULL,
  `link_name` varchar(255) NOT NULL,
  `internal` int(1) NOT NULL,
  `order_id` int(1) NOT NULL,
  `is_deletable` int(1) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`item_id`, `menu_id`, `page_link`, `link_name`, `internal`, `order_id`, `is_deletable`) VALUES
(44, 9, '', 'Home', 1, 1, 0),
(45, 9, 'printersportfolio', 'Printers', 1, 45, 0),
(46, 9, 'filamentportfolio', 'Filament', 1, 46, 0),
(47, 9, 'finishing', 'Finishing', 1, 47, 0),
(48, 10, 'page/news', 'News', 1, 48, 1),
(49, 10, 'page/about', 'About us', 1, 49, 1),
(50, 10, 'page/career', 'Careers', 1, 50, 1),
(51, 10, 'contact-us', 'Contact us', 1, 51, 1),
(52, 11, 'page/support', 'Custumer support help', 1, 52, 1),
(53, 11, 'page/delivery-europe', 'Delivery by DHL in Europe', 1, 53, 1),
(54, 11, 'page/instalation', 'Instalation', 1, 54, 1),
(55, 11, 'page/guarantee', 'Guarantee', 1, 55, 1),
(56, 11, 'page/return', 'Returns', 1, 56, 1),
(57, 11, 'https://www.google.bg', 'Google search', 0, 57, 1),
(58, 12, 'page/payment', 'Payment', 1, 58, 1),
(59, 12, 'page/delivery', 'Delivery', 1, 59, 1),
(60, 12, 'page/instalation', 'Instalation', 1, 60, 1),
(61, 12, 'page/return', 'Returns', 1, 61, 1),
(62, 12, 'page/guarantee', 'Guarantee', 1, 62, 1),
(63, 12, 'contact-us', 'Contact us', 1, 63, 1),
(64, 13, 'page/money-back', 'Money-Back Guarantee', 1, 64, 1),
(65, 13, 'page/best-choice', 'Best Price & Largest Choice', 1, 65, 1),
(66, 13, 'page/delivery-europe', 'Delivery by DHL in Europe', 1, 66, 1),
(67, 13, 'page/stores', 'Collect in Store', 1, 67, 1),
(68, 13, 'page/support', 'Custumer support & Installation', 1, 68, 1),
(69, 13, 'page/guarantee', 'Guarantee', 1, 69, 1),
(70, 13, 'page/buyers-guide', 'Buyer''s Guide', 1, 70, 1),
(71, 14, '//printersportfolio/printersdetail/10', 'Printer 2', 1, 71, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`, `date`) VALUES
(1, 'user@abv.bg', '2015-10-27 14:33:18'),
(2, 'forrums@abv.bg', '2015-10-29 09:28:20'),
(3, 'gfrdfgdf@abv.bg', '2015-11-02 08:18:25'),
(4, 'a@abv.bg', '2015-11-10 16:00:44'),
(5, 'user@abv.bg', '2015-11-10 16:18:33');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `phone_code` varchar(255) NOT NULL,
  `order_price` decimal(10,2) NOT NULL,
  `date_added` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `b_address` varchar(255) NOT NULL,
  `b_city` varchar(255) NOT NULL,
  `b_zip_code` varchar(255) NOT NULL,
  `b_country_name` varchar(255) NOT NULL,
  `b_phone` varchar(255) NOT NULL,
  `b_phone_code` varchar(255) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `email`, `title`, `fname`, `lname`, `address`, `city`, `zip_code`, `country_name`, `phone`, `phone_code`, `order_price`, `date_added`, `status`, `b_address`, `b_city`, `b_zip_code`, `b_country_name`, `b_phone`, `b_phone_code`) VALUES
(3, 7, 'user@abv.bg', 'Mr', 'User', 'Userov', 'Brezovska 55', 'Plovdiv', '4000', 'Bulgaria', '885791348', '359', 70.00, '2015-11-16 16:28:08', 0, 'Street num 212', 'Plovdiv', '4000', 'Bulgaria', '885791348', '359'),
(6, 7, 'user@abv.bg', 'Mr', 'User', 'Userov', 'Uarsd eist 143', 'Aalborg', '4124', 'Denmark', '123999332', '45', 249.00, '2015-11-17 14:39:50', 3, 'adresa mi', 'Aalborg', '6000', 'Denmark', '123999332', '45');

-- --------------------------------------------------------

--
-- Table structure for table `price_ranges`
--

CREATE TABLE IF NOT EXISTS `price_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min` int(1) NOT NULL,
  `max` int(5) NOT NULL,
  `order_id` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `price_ranges`
--

INSERT INTO `price_ranges` (`id`, `min`, `max`, `order_id`, `active`, `date`) VALUES
(1, 0, 500, 0, 0, '2015-10-27 16:34:24'),
(2, 500, 1000, 0, 0, '2015-10-27 16:34:28'),
(3, 1000, 1500, 0, 0, '2015-10-27 16:34:32'),
(4, 1500, 3000, 0, 0, '2015-10-27 16:34:37'),
(5, 3000, 5000, 0, 0, '2015-10-27 16:34:42');

-- --------------------------------------------------------

--
-- Table structure for table `print_tech`
--

CREATE TABLE IF NOT EXISTS `print_tech` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `print_name` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `print_tech`
--

INSERT INTO `print_tech` (`id`, `print_name`, `order_id`, `active`) VALUES
(1, 'FDM', 0, 0),
(2, 'Sintering', 0, 0),
(3, 'Stereolitography', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `new_price` decimal(10,2) NOT NULL,
  `f_cat_id` int(11) DEFAULT NULL,
  `man_id` int(11) DEFAULT NULL,
  `print_id` int(11) DEFAULT NULL,
  `build_id` int(11) DEFAULT NULL,
  `acc_id` int(11) DEFAULT NULL,
  `fl_date_begin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fl_date_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `sold` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `man_id` (`man_id`),
  KEY `print_id` (`print_id`),
  KEY `build_id` (`build_id`),
  KEY `acc_id` (`acc_id`),
  KEY `f_cat_id` (`f_cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `cat_id`, `title`, `text`, `price`, `new_price`, `f_cat_id`, `man_id`, `print_id`, `build_id`, `acc_id`, `fl_date_begin`, `fl_date_end`, `date_added`, `date_update`, `sold`, `active`) VALUES
(1, 1, 'Printer 1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1500.00, 0.00, NULL, 1, 3, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:36:00', '2015-11-02 09:18:00', 0, 0),
(2, 3, 'Finishing 1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 50.00, 0.00, 10, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 3, 0),
(3, 2, 'Filament 1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 50.00, 0.00, 1, 6, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:46:00', '0000-00-00 00:00:00', 4, 0),
(4, 4, 'Printer 1 + Filament 1 + Finishing 1', '', 1440.00, 0.00, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(5, 3, 'Finishing 2', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 30.00, 0.00, 11, 1, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(6, 3, 'Finishing 3', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 50.00, 0.00, 11, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(7, 3, 'Finishing 4', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 60.00, 0.00, 10, 4, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 1, 0),
(8, 3, 'Finishing 5', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 70.00, 0.00, 11, 6, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(9, 3, 'Finishing 6', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 80.00, 0.00, 10, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(10, 3, 'Finishing 7', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 90.00, 0.00, 11, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 11, 0),
(11, 3, 'Finishing 8', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 100.00, 0.00, 10, 3, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(12, 3, 'Finishing 9', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 110.00, 0.00, 10, 4, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(13, 3, 'Finishing 10', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 120.00, 0.00, 11, 1, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(14, 3, 'Finishing 11', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 130.00, 0.00, 11, 4, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(15, 3, 'Finishing 12', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 140.00, 0.00, 10, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(16, 3, 'Finishing 13', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 150.00, 0.00, 10, 6, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(17, 1, 'Printer 2', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1500.00, 0.00, NULL, 5, 2, 4, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-29 09:59:00', '2015-11-02 09:45:00', 4, 0),
(18, 1, 'Printer 3', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1000.00, 0.00, NULL, 5, 3, 3, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:01:00', '0000-00-00 00:00:00', 0, 0),
(19, 1, 'Printer 4', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1250.00, 0.00, NULL, 2, 2, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:01:00', '0000-00-00 00:00:00', 0, 0),
(20, 1, 'Printer 5', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1890.00, 0.00, NULL, 6, 1, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:01:00', '2015-11-02 10:06:00', 0, 0),
(21, 1, 'Printer 6', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 2100.00, 0.00, NULL, 1, 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:02:00', '2015-11-02 10:37:00', 0, 0),
(22, 2, 'Filament 2', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 20.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(23, 1, 'Printer 7', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 2200.00, 0.00, NULL, 5, 2, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:02:00', '2015-11-02 10:38:00', 0, 0),
(24, 1, 'Printer 8', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 2500.00, 0.00, NULL, 3, 3, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:02:00', '0000-00-00 00:00:00', 0, 0),
(25, 1, 'Printer 9', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 3000.00, 0.00, NULL, 4, 2, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:02:00', '2015-11-02 10:37:00', 0, 0),
(26, 1, 'Printer 10', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 3100.00, 0.00, NULL, 3, 2, 4, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:02:00', '2015-11-02 10:37:00', 0, 0),
(27, 1, 'Printer 11', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 3500.00, 0.00, NULL, 6, 3, 3, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:02:00', '2015-11-02 10:36:00', 0, 0),
(28, 1, 'Printer 12', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 4000.00, 0.00, NULL, 2, 1, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:02:00', '2015-11-04 13:43:00', 0, 0),
(29, 1, 'Printer 13', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 4300.00, 0.00, NULL, 5, 1, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:02:00', '2015-11-02 10:36:00', 0, 0),
(30, 2, 'Filament 3', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 25.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(31, 2, 'Filament 4', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 30.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(32, 2, 'Filament 5', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 35.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(33, 2, 'Filament 6', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 40.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(34, 2, 'Filament 7', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 45.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(35, 2, 'Filament 8', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 50.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(36, 2, 'Filament 9', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 55.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(37, 2, 'Filament 10', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 60.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(38, 2, 'Filament 11', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 75.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(39, 2, 'Filament 12', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 90.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 11, 0),
(40, 2, 'Filament 13', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 110.00, 0.00, 1, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-02 10:15:00', '0000-00-00 00:00:00', 0, 0),
(41, 4, 'Printer 7 + Finishing 3 + Filament 12 + Filament 3', '', 1892.00, 0.00, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(42, 4, 'Printer 10 + Finishing 7 + Filament 13 + Filament 11', '', 3037.50, 0.00, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(43, 4, 'Printer 13 + Finishing 3 + Filament 8 + Filament 9', '', 3118.50, 0.00, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(44, 3, 'Finishing 14', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 155.00, 0.00, 11, 6, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(45, 3, 'Finishing 15', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 165.00, 0.00, 11, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(46, 3, 'Finishing 16', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 175.00, 0.00, 11, 4, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(47, 3, 'Finishing 17', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 500.00, 0.00, 11, 3, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(48, 3, 'Finishing 18', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 501.00, 0.00, 11, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(49, 3, 'Finishing 19', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1001.00, 0.00, 11, 1, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(50, 3, 'Finishing 20', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 200.00, 0.00, 10, 6, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(51, 3, 'Finishing 21', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 250.00, 0.00, 10, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(52, 3, 'Finishing 22', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 25.00, 0.00, 10, 4, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(53, 3, 'Finishing 23', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 160.00, 0.00, 10, 3, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(54, 3, 'Finishing 24', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 240.00, 0.00, 10, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(55, 3, 'Finishing 25', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 180.00, 0.00, 10, 1, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0);
INSERT INTO `products` (`id`, `cat_id`, `title`, `text`, `price`, `new_price`, `f_cat_id`, `man_id`, `print_id`, `build_id`, `acc_id`, `fl_date_begin`, `fl_date_end`, `date_added`, `date_update`, `sold`, `active`) VALUES
(56, 3, 'Finishing 26', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 50.00, 0.00, 10, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(57, 3, 'Finishing 27', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 30.00, 0.00, 11, 1, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(58, 3, 'Finishing 28', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 50.00, 0.00, 11, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(59, 3, 'Finishing 29', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 60.00, 0.00, 10, 4, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 1, 0),
(60, 3, 'Finishing 30', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 70.00, 0.00, 11, 6, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(61, 3, 'Finishing 31', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 120.00, 0.00, 11, 1, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(62, 3, 'Finishing 32', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 50.00, 0.00, 10, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(63, 3, 'Finishing 33', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 180.00, 0.00, 10, 1, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(64, 3, 'Finishing 34', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 240.00, 0.00, 10, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(65, 3, 'Finishing 35', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 160.00, 0.00, 10, 3, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(66, 3, 'Finishing 36', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 25.00, 0.00, 10, 4, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(67, 3, 'Finishing 37', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 200.00, 0.00, 10, 6, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(68, 3, 'Finishing 38', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 155.00, 0.00, 11, 6, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(69, 3, 'Finishing 39', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 165.00, 0.00, 11, 5, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(70, 3, 'Finishing 40', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 500.00, 0.00, 11, 3, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(71, 3, 'Finishing 41', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 175.00, 0.00, 11, 4, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(72, 3, 'Finishing 42', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 500.00, 0.00, 11, 3, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(73, 3, 'Finishing 43', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 501.00, 0.00, 11, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(74, 3, 'Finishing 44', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 501.00, 0.00, 11, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(75, 3, 'Finishing 45', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\r\n', 1001.00, 0.00, 11, 1, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-27 16:40:08', '0000-00-00 00:00:00', 0, 0),
(76, 1, 'Printer 14', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 3500.00, 0.00, NULL, 2, 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:44:00', '0000-00-00 00:00:00', 0, 0),
(77, 1, 'Printer 15', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 2999.00, 0.00, NULL, 3, 1, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:44:00', '0000-00-00 00:00:00', 0, 0),
(78, 1, 'Printer 16', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 4800.00, 0.00, NULL, 4, 1, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:45:00', '0000-00-00 00:00:00', 0, 0),
(79, 1, 'Printer 17', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 3100.00, 0.00, NULL, 5, 3, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:45:00', '0000-00-00 00:00:00', 0, 0),
(80, 1, 'Printer 18', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1900.00, 0.00, NULL, 6, 3, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:46:00', '0000-00-00 00:00:00', 0, 0),
(81, 1, 'Printer 19', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 4750.00, 0.00, NULL, 1, 3, 4, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:47:00', '0000-00-00 00:00:00', 0, 0),
(82, 1, 'Printer 20', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 3550.00, 0.00, NULL, 3, 3, 3, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:48:00', '0000-00-00 00:00:00', 0, 0),
(83, 1, 'printer 22', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 490.00, 0.00, NULL, 2, 2, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:48:00', '0000-00-00 00:00:00', 0, 0),
(84, 1, 'Printer 23', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 470.00, 0.00, NULL, 4, 1, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:49:00', '0000-00-00 00:00:00', 0, 0),
(85, 1, 'Printer 24', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 550.00, 0.00, NULL, 5, 3, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:50:00', '0000-00-00 00:00:00', 0, 0),
(86, 1, 'Printer 25', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 980.00, 0.00, NULL, 3, 1, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:50:00', '0000-00-00 00:00:00', 0, 0),
(87, 1, 'Printer 26', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1340.00, 0.00, NULL, 2, 2, 4, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:51:00', '0000-00-00 00:00:00', 0, 0),
(88, 1, 'Printer 27', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 800.00, 0.00, NULL, 4, 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:52:00', '0000-00-00 00:00:00', 0, 0),
(89, 1, 'Printer 28', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 499.00, 0.00, NULL, 1, 3, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:52:00', '0000-00-00 00:00:00', 0, 0),
(90, 1, 'Printer 29', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 500.00, 0.00, NULL, 6, 2, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:53:00', '0000-00-00 00:00:00', 0, 0),
(91, 1, 'Printer 30', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 100.00, 0.00, NULL, 2, 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:54:00', '0000-00-00 00:00:00', 0, 0),
(92, 1, 'Printer 31', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1500.00, 0.00, NULL, 4, 2, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:54:00', '0000-00-00 00:00:00', 0, 0),
(93, 1, 'Printer 33', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 3000.00, 0.00, NULL, 5, 3, 3, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:55:00', '0000-00-00 00:00:00', 0, 0),
(94, 1, 'Printer 34', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 2780.00, 0.00, NULL, 2, 2, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:56:00', '0000-00-00 00:00:00', 0, 0),
(95, 1, 'Printer 35', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1200.00, 0.00, NULL, 1, 2, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:57:00', '0000-00-00 00:00:00', 0, 0),
(96, 1, 'Printer 36', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1430.00, 0.00, NULL, 2, 1, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:57:00', '0000-00-00 00:00:00', 0, 0),
(97, 1, 'Printer 38', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1499.00, 0.00, NULL, 3, 3, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:58:00', '0000-00-00 00:00:00', 0, 0),
(98, 1, 'Printer 37', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1190.00, 0.00, NULL, 4, 1, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:59:00', '0000-00-00 00:00:00', 0, 0),
(99, 1, 'Printer 39', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 1375.00, 0.00, NULL, 6, 2, 4, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 14:59:00', '0000-00-00 00:00:00', 0, 0),
(100, 1, 'Printer 40', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 670.00, 0.00, NULL, 2, 3, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 15:00:00', '0000-00-00 00:00:00', 0, 0),
(101, 1, 'Printer 41', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 840.00, 0.00, NULL, 1, 1, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 15:01:00', '0000-00-00 00:00:00', 0, 0),
(102, 1, 'Printer 42', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 440.00, 0.00, NULL, 1, 3, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 15:01:00', '0000-00-00 00:00:00', 0, 0),
(103, 1, 'Printer 43', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 399.00, 0.00, NULL, 3, 1, 5, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 15:02:00', '0000-00-00 00:00:00', 0, 0),
(104, 1, 'Printer 44', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 400.00, 29.00, NULL, 5, 2, 3, 2, '2015-11-16 15:50:06', '2015-12-16 15:50:06', '2015-11-04 15:03:00', '2015-11-16 15:50:00', 0, 0),
(105, 1, 'Printer 45', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 499.00, 153.00, NULL, 6, 3, 2, 1, '2015-11-16 15:49:55', '2015-12-16 15:49:55', '2015-11-04 15:04:00', '2015-11-16 15:50:00', 0, 0),
(106, 3, 'Finishing 46', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam ante, elementum ac nunc at, varius condimentum nisl. Praesent ornare lorem nulla, ut auctor ex tempus ut. Nunc aliquet viverra facilisis. Vivamus eget massa tortor. Pellentesque metus felis, iaculis sit amet pretium sit amet, posuere ut felis. Nulla in sollicitudin dui, et sodales neque. Curabitur mollis ut est sed hendrerit. Sed vehicula, lectus eget consequat elementum, ligula erat blandit sapien, at mollis nulla sem sed mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce odio urna, vulputate at enim in, ornare fringilla ex. Pellentesque in est laoreet, dignissim tortor et, malesuada odio. Nulla maximus dictum libero. Mauris sollicitudin iaculis lacinia.</p>\n', 111.00, 0.00, 11, 2, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-04 15:06:11', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products_images`
--

CREATE TABLE IF NOT EXISTS `products_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `main_pic` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=109 ;

--
-- Dumping data for table `products_images`
--

INSERT INTO `products_images` (`id`, `name`, `product_id`, `order_id`, `main_pic`) VALUES
(1, 'd653f1846c8c32799fb7ebc02a128425bbe50f71.jpg', 1, 0, 1),
(2, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 2, 0, 1),
(3, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 3, 0, 1),
(5, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 5, 0, 1),
(6, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 6, 0, 1),
(7, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 7, 0, 1),
(8, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 8, 0, 1),
(9, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 9, 0, 1),
(10, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 10, 0, 1),
(11, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 11, 0, 1),
(12, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 12, 0, 1),
(13, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 13, 0, 1),
(14, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 14, 0, 1),
(15, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 15, 0, 1),
(16, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 16, 0, 1),
(17, 'f2c5985f7a12f552c4ff0f400f6eb26683a96546.jpeg', 17, 0, 1),
(18, 'f5313b0b26c50098aafe10bcf2834464df556e3f.jpeg', 17, 0, 0),
(19, '2d794bfce7b7af65ecfea3edf7c640c38971d365.jpeg', 17, 0, 0),
(20, '3f770dd5228e7a05e0632e5e59546302e7958a11.jpeg', 17, 0, 0),
(21, '7984045daf3f26527928cd1fad8918e5b4e711a1.jpg', 18, 0, 1),
(22, '82fbecb4950cbae43682fe0d15c357644732675a.png', 19, 0, 1),
(23, '11683d1f806d243b71cf99dd5a31161f45466b9d.jpg', 20, 0, 1),
(24, '06d7696de24db0761dacae72de795dbbb9879046.jpg', 21, 0, 1),
(27, 'f2c5985f7a12f552c4ff0f400f6eb26683a96546.jpeg', 23, 0, 1),
(28, '7984045daf3f26527928cd1fad8918e5b4e711a1.jpg', 24, 0, 1),
(29, '82fbecb4950cbae43682fe0d15c357644732675a.png', 25, 0, 1),
(30, '11683d1f806d243b71cf99dd5a31161f45466b9d.jpg', 26, 0, 1),
(31, '06d7696de24db0761dacae72de795dbbb9879046.jpg', 27, 0, 1),
(32, 'd653f1846c8c32799fb7ebc02a128425bbe50f71.jpg', 28, 0, 1),
(33, 'f2c5985f7a12f552c4ff0f400f6eb26683a96546.jpeg', 29, 0, 1),
(34, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 30, 0, 1),
(35, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 32, 0, 1),
(36, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 37, 0, 1),
(37, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 35, 0, 1),
(38, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 36, 0, 1),
(39, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 34, 0, 1),
(40, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 39, 0, 1),
(41, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 31, 0, 1),
(42, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 33, 0, 1),
(43, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 38, 0, 1),
(44, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 40, 0, 1),
(45, 'f73896fe6d69c08f5d853f8fb175e664ca6a2119.jpg', 22, 0, 1),
(46, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 44, 0, 1),
(47, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 45, 0, 1),
(48, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 46, 0, 1),
(49, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 47, 0, 1),
(50, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 48, 0, 1),
(51, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 49, 0, 1),
(52, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 50, 0, 1),
(53, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 51, 0, 1),
(54, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 52, 0, 1),
(55, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 53, 0, 1),
(56, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 54, 0, 1),
(57, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 55, 0, 1),
(58, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 56, 0, 1),
(59, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 57, 0, 1),
(60, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 58, 0, 1),
(61, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 59, 0, 1),
(62, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 60, 0, 1),
(63, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 61, 0, 1),
(64, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 62, 0, 1),
(65, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 63, 0, 1),
(66, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 64, 0, 1),
(67, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 65, 0, 1),
(68, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 66, 0, 1),
(69, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 67, 0, 1),
(70, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 68, 0, 1),
(71, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 69, 0, 1),
(72, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 70, 0, 1),
(73, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 71, 0, 1),
(74, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 72, 0, 1),
(75, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 73, 0, 1),
(76, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 74, 0, 1),
(77, '0104aea1460eb1965f1ed2e4bf73104f1446de421.jpg', 75, 0, 1),
(78, '070cfc07e61d90f0f8f5c3cc7f996ab02878c7b5.jpg', 76, 0, 1),
(79, '7aa2a50382b458f4bd3700f9b2bff76702604806.png', 77, 0, 1),
(80, '1a1c3a4c5385847fe48ffbad6b1536cd497c27de.jpg', 78, 0, 1),
(81, '3d85fd78b24e412d87a60ff5684da2269345644a.jpg', 79, 0, 1),
(82, '32b2b4fdcdc8a7c93739e4f9dd30607bd5d38721.jpg', 80, 0, 1),
(83, '47da2d05e069268fe91a4b47ef573e3e8013d9a1.jpeg', 81, 0, 1),
(84, 'fdc80cb0423c3afa9e1c088fc7e61e1d2514b46b.jpg', 82, 0, 1),
(85, '345884ec28c274576eae5ce93f487697bcd77a27.png', 83, 0, 1),
(86, '19aaf5594ad839c8223ba96abdfd8cbfc5090a77.jpg', 84, 0, 1),
(87, '24e70feff2b059efab55f4800dce261b5bf6c18d.jpg', 85, 0, 1),
(88, 'e335f2e24f7e16e6395a16dfffeeedbd1b79c817.jpg', 86, 0, 1),
(89, '7fd0d19383a1ce45c1e3e835c2d169a5c953a730.jpeg', 87, 0, 1),
(90, 'ed5e9d4d18dca7eaf9404f5a900286b6e40748da.jpg', 88, 0, 1),
(91, '8340729353b8b4eb094f6968347a7e8e034017aa.png', 89, 0, 1),
(92, 'ac063970b06ddf0c221de6a3e3f1378e38b7d5ec.png', 90, 0, 1),
(93, '5f1bdbb491aadf3012db4f5a5a22537e0ff3fcd7.jpg', 91, 0, 1),
(94, '38b497a7003d84ab2af70032ba1fd73be91ce9e8.jpg', 92, 0, 1),
(95, 'c02a05e7673591385ea78f8a192d99fbef8c6a9c.jpg', 93, 0, 1),
(96, '6d7fe590258169bbe225aa993fa2ee1a66eff19f.jpg', 94, 0, 1),
(97, '39826d41489d77939a62e550d42cf693c9bcd0ad.png', 95, 0, 1),
(98, '48cfaa530ab047e6152d5f0696bffc4ac72bdee9.jpg', 96, 0, 1),
(99, 'd77392e03996715f82aef5c35b48e177428ee9f3.jpg', 97, 0, 1),
(100, '9ba4553a8cb77e58c8539275026a3ee0525d8ff4.jpg', 98, 0, 1),
(101, '40ff90ae5f833f7b0ecbcbfd8a977bb2285bb96c.jpeg', 99, 0, 1),
(102, '4f3e001bcca2076cacef199f3e6610e22342716a.jpg', 100, 0, 1),
(103, '73193b67c5114097586c7888e6f1077f23130bc0.png', 101, 0, 1),
(104, 'a20ae252d0df5fbada18a825f38feaedb49b9aca.jpg', 102, 0, 1),
(105, '10e0d01bd0998aae0fce56d9af83313d07f97190.jpg', 103, 0, 1),
(106, 'b96855f825e6a8053bbae33518af15e29223f054.jpg', 104, 0, 1),
(107, '003ba0d3b2e09e817dc2ebad1acbc6d8afa3dc5c.jpeg', 105, 0, 1),
(108, '9bd3d7df0f8d18babf790d6695d92c04d95a58da1.jpg', 106, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_ordered`
--

CREATE TABLE IF NOT EXISTS `products_ordered` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `options` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `products_ordered`
--

INSERT INTO `products_ordered` (`id`, `product_id`, `quantity`, `price`, `total_price`, `options`, `order_id`) VALUES
(3, 104, 2, 29.00, 58.00, '', 3),
(5, 104, 3, 29.00, 87.00, '', 6),
(6, 38, 2, 75.00, 150.00, 'red', 6);

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE IF NOT EXISTS `shipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `range` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`id`, `range`, `price`) VALUES
(1, 'UK Delivery', '0'),
(2, 'International Delivery', '12');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image300x100` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `internal` int(1) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `description`, `image`, `image300x100`, `url`, `internal`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 'Sample slider title 2', 'Sample slider description 2', '1400x400_772cfb3b2f659d082cd231fd1a69a7e0895228f5.png', '300x100772cfb3b2f659d082cd231fd1a69a7e0895228f5.png', 'http://rgstore.local//finishing/details/2', 1, 1, '2015-10-27 16:48:30', '2015-11-13 11:22:26'),
(2, 'Sample slider title 1', 'Sample slider description 1', '1400x400_8e295a766eb655175b09dc5d9071622e850b3ef6.jpg', '300x1008e295a766eb655175b09dc5d9071622e850b3ef6.jpg', 'http://rgstore.local/printersportfolio/printersdetail/1', 1, 0, '2015-10-27 16:48:56', '2015-11-13 11:22:17'),
(3, 'Sample slider title 3', 'Sample slider description 3', '1400x400_2f00e5e4c5a7dff03439349479cd97eacc800e9f.jpg', '300x1002f00e5e4c5a7dff03439349479cd97eacc800e9f.jpg', 'http://rgstore.local//finishing/details/15', 1, 3, '2015-10-29 09:51:36', '2015-11-13 11:22:32'),
(4, 'Sample slider title 4', 'Sample slider description 4', '1400x400_9fbb9e647552fd38635b5f054c4bb60598aa9d48.jpg', '300x1009fbb9e647552fd38635b5f054c4bb60598aa9d48.jpg', 'http://rgstore.local//printersportfolio/printersdetail/5', 1, 4, '2015-10-29 09:52:24', '2015-11-13 11:22:41');

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE IF NOT EXISTS `static_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `link_name` varchar(255) NOT NULL,
  `link_to_page` varchar(500) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `static_pages`
--

INSERT INTO `static_pages` (`id`, `title`, `slug`, `link_name`, `link_to_page`, `content`) VALUES
(1, 'Payment', 'payment', 'Payment', 'http://rgstore.local/page/payment', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>'),
(2, 'Delivery service', 'delivery', 'Delivery', 'http://rgstore.local/page/delivery', '<p>All items on iMakr.com are delivered via a courier service. If items are in stock, they will be dispatched immediately and will usually be delivered to a UK address within 2 working days and a European or Channel Island address within 3 – 5 working days. Delivery times are indicative and not guaranteed. If you think that your order has gone missing, please Contact Us and we will help you to locate it.<br />\n<br />\nIf the item you ordered is not in stock, we will notify you of the waiting period and issue an Order Dispatched email once we have sent you the item.<br />\n<br />\nIf you have chosen our free Collect in Store service, we will contact you to confirm when your order is ready for collection at the iMakr Store, 79 Clerkenwell Road, Farringdon, EC1R 5AR, London.<br />\n<br />\nIf you are interested in delivery outside of Europe, please contact us and we will try and arrange something for you.<br />\n<br />\nPlease read our Terms and Conditions for further information about Delivery.</p>'),
(3, 'Terms & Conditions of Sale', 'terms', 'Terms & Conditions', 'http://rgstore.local/page/terms', '<p>These Terms and Conditions and the Order Dispatched notification constitute the contract between iMakr.com “we/us” and the customer “you” for the supply of goods and services. The details of our Terms and Conditions are laid out clearly below. We have endeavoured to give you all the information that you may require to carry out an online transaction with us. This includes the process from order, to delivery, our returns policy, guarantees and after sales service.<br />\n<br />\nWe will ask you to accept these Terms and Conditions whenever you make a purchase. The Terms and Conditions may be amended and updated occasionally so please spend a few minutes checking that you are happy with the details below. No other terms and conditions shall apply.<br />\n<br />\nPlease note that different terms (including prices, charges for services, availability, delivery times and returns policies) apply to purchases made in the iMakr Store in New York or the iMakr Store in London.<br />\n<br />\n<strong>Placing an Order</strong><br />\nYou will be asked to register an account or sign into an existing account when you confirm an online order with us. This allows you to track your orders once they have been placed, print and download your invoices and speed up the Checkout process.<br />\n<br />\nYou can modify your order at any point during the Checkout process until you confirm your payment details. You can review the contents of your Basket, add or remove items or correct any errors in your personal details. Your Basket will show you the total cost of your order, which will include the cost of the goods and delivery charges (if applicable). You will be given the opportunity to review and accept a summary of your order details before proceeding to payment and confirming your order.<br />\n<br />\nOnce we have received your order and confirmed your payment details, we will provide you with an Order Reference Number. This Order Reference Number will also be contained within an e-mail entitled “Order Confirmation” that will automatically be sent to your registered e-mail address when your order have been successfully completed. This e-mail confirms that we have received your order, however it does not serve as a guarantee that we will be able to supply all of the items on your order. If we cannot accept or fulfil your order for any reason, we will contact you to explain the reasons why.<br />\n<br />\nOnce we have dispatched your order, we will send you an “Order Dispatched” e-mail. The Order Dispatched e-mail will confirm that we are able to fulfil your order and will also contain shipping details and a tracking number for your goods. Invoices for your orders are stored on your Account page on our website.<br />\n<br />\nIf you choose our Collect in Store service you will be given an Order Reference Number when you place your order. This Order Reference Number or the "Order Confirmation" e-mail should be brought with you when you come to the iMakr NY store or the iMakr London Store to collect your order. You will also be asked to show the card that you used to make the online payment for your order. All of our Collect in Store sales take place in our iMakr Stores. We will contact you to confirm when your order is ready to collect. Sales are subject to the normal in-store terms of sale, statutory rights and the Manager&#39;s discretion.<br />\n<br />\nAll of our correspondence with you will be sent to the e-mail address provided by you when you register an account with us. Please check that we have the correct contact details during the Checkout process.<br />\n<br />\nWe will endeavour to supply you with all of the items listed in your Order Confirmation e-mail, however sometimes this may not be possible for the following reasons:<br />\n1) A product is no longer available<br />\n2) We are unable to source relevant components or an item is out of stock<br />\n3) There was a pricing error on the iMakr.com website<br />\n<br />\nIf we are unable to supply you with the item, we will contact you to suggest alternative products that may be of interest. If you do not wish us to procure an alternative product, we will cancel your order with respect to the product that we cannot supply, and refund you the monies that you have paid us for that product.<br />\n<br />\nIn the event of a pricing error, we reserve the right to correct the error and charge the correct price. Under these circumstances, we will contact you and notify you of the mistake and offer you the opportunity for a full refund.<br />\n<br />\nThe refunds for the products and associated delivery charges will be the extent of our liability to you if we are unable to deliver the products you have ordered.</p>\n\n<p>When you receive your printer, please carefully inspect it for any shipping damage and contact us immediately via email <a href="mailto:rgatewaytest@gmail.com">support@imakr.com</a> if there are any issues—please include your unit&#39;s serial number and photos of the problem.</p>\n\n<p><br />\n<strong>Prices and Payment</strong><br />\nThe prices advertised on this site are in GBP, Euro or US Dollars as indicated. The cost of your order will be the sum of the individual products or bundles, the delivery charge and any additional services that you may have ordered. The total cost will be clearly displayed in your Basket when you check-out and before you confirm your order. Once you have confirmed your order, you will be asked to submit your payment details. Payment will be deducted from your account once you have confirmed your payment details. Payments must be made in the currency shown on your invoice.<br />\n<br />\nPayment can be made securely by most major credit and debit cards, or via Paypal. We do not store any card details once payment has been made.<br />\n<br />\nIn order to protect you and us against fraudulent activities, we may use information that you have submitted to confirm your identity. For example, we may check your name, address or other personal information provided by you during the Checkout process against suitable third party databases.  We may also securely forward your information to our accredited identity verification partner who may keep a record of your details.  All information provided by you will be treated securely and strictly in accordance with the Data Protection Act 1998. </p>\n\n<p><strong>Delivery</strong><br />\nOnce we have received confirmation of your payment details, we will dispatch your order. We will send your order to the Billing Address indicated by you during the Checkout process, unless a different Delivery Address is indicated by you. Please ensure that the delivery details we hold for you are correct. We will send you an e-mail entitled “Order Dispatched” with shipping details and a tracking number as soon as your order has left the warehouse. If you order a number of products, it is possible that your goods will arrive separately.<br />\n<br />\nIf you have chosen our Collect in Store service, we will contact you to confirm when your order is ready for collection. Just take a print-out of your Order Confirmation and/or Order Reference Number with you and the card that you made the online payment with.<br />\n<br />\nAll items on our website are delivered via a courier service. If all items are in stock, they will usually be delivered in 72 working hours. Delivery times are indicative and not guaranteed. If you do not receive your order within 30 days of placing it, you reserve the right to cancel your order and receive a full refund. We cannot deliver small or large items to BFPO (British Forces Post Office) addresses.<br />\n<br />\n<strong>Missing, Damaged, Incorrect or Faulty orders</strong><br />\nWe strive to ensure that your order arrives intact and complete. If you have ordered several products, they may not all arrive at once. You will be able to track the status of your order via a tracking code that will be sent to you in your "Order Dispatched" e-mail. If you you have not received an "Order Dispatched" e-mail, or your order has not been dispatched, please contact us via e-mail at Support@iMakr.com.<br />\n<br />\nIf you receive a damaged, incorrect or faulty order, please notify us immediately via the contact methods described above. We will confirm the most convenient process for you to return the item to us. Returns need to be carried out within 7 working days from the day after the delivery date. We will offer you the option of repair, replacement or full refund for your product.<br />\n<br />\nThe title to, and risk of loss of the goods transfers to you on delivery to the address indicated by you in the Checkout process. Whilst the goods are in your possession you must take reasonable care of them and not use them until they are returned. Please see more detail in our Returns Policy below.<br />\n<br />\n<strong>Returns and Cancellation Policy </strong><br />\nIf you decide to return you order once it has been delivered, we will refund or exchange your purchase as long as the original packaging is undamaged and the item is returned in a saleable condition. You will need to return your purchase within 7 calendar days from the date of delivery.<br />\n<br />\nIf you have opened the packages to inspect the products, but have not used them, you are still entitled to a refund under the Distance Selling Regulations, as long as you return them within 7 working days (not including Saturday, Sunday and public holidays) from the day after the day of delivery.<br />\n<br />\nIf you wish to return products please contact us via e-mail at <a href="mailto:rgatewaytest@gmail.com">support@imakr.com</a>, within 7 working days of receiving your order to notify us of your intent. Please have your Order Reference Number and delivery details to hand when you contact us.  We will issue you with a Product Return Code and provide you with instructions on how to return the item to us. You will be liable for the return delivery costs of unwanted orders.<br />\n<br />\nThe title to, and risk of loss of the goods transfers to you on delivery to the address indicated by you during the Checkout process. Whilst the goods are in your possession you must take reasonable care of them and not use them until they are returned.<br />\n<br />\nThe goods must be in a saleable condition and must be returned in the original, undamaged packaging, along with any accessories and free gifts received with them. The original packaging provides the best protection for our products. We reserve the right to reject any returned items that were damaged due to insufficient packaging. It is recommended that the returned item is posted through a recorded delivery service as we cannot be held responsible for items that go missing in the post.<br />\n<br />\nOn receipt of the returned, undamaged and unused item, we will issue a full refund for the cost of the product/s plus the original delivery charge. Any items that are returned damaged or used, or with indications that you did not take reasonable care of them whilst they were in your possession, may result in a reduced refund. Refunds will be made to the card that was used when payment was first made and will take 3-5 days to be credited to your account.<br />\n<br />\nDVDs, CDs, memory cards and software cannot be returned if the seals have been broken. We also cannot accept returns of filament, 3D Art or items that have been modified to your specification. We have the right to retain any charge paid for services which have already begun or been completed.<br />\n<br />\n<strong>Product Guarantee</strong><br />\nWe offer a 6 months to 12 months guarantee from the date of delivery, depending on 3D Printers (exact lenght is specified on each model) which covers all parts and labour for all our 3D printers. In the unlikely event that your printer develops a fault within the guarantee period, please contact us via:</p>\n\n<ul>\n <li>E-mail: <a href="mailto:rgatewaytest@gmail.com">support@imakr.com</a></li>\n</ul>\n\n<p>We will confirm the most convenient process for you to return the printer to us for repair. You will not be liable for the cost of returning the printer to us. We reserve the right to inspect the printer and confirm the fault. We do not cover any faults caused by accident, neglect, misuse or normal wear and tear. We do not accept returns on software, CD&#39;s, DVD&#39;s or memory cards if the seals have been broken. The items sold on this website are intended for normal domestic and consumer use. <br />\n<br />\nOur Returns Policy and Product Guarantee do not affect your statutory rights.<br />\n<br />\n<strong>Contacting Us</strong> <br />\nYou can contact us in the following ways:<br />\n<br />\nE-mail: Please send an e-mail to <a href="mailto:rgatewaytest@gmail.com">info@imakr.com</a> or follow the <strong><a href="http://www.imakr.com/contact">Contact Us</a></strong> link<br />\n<br />\n<br />\n<strong>Liability</strong><br />\nThe products on this website are for use by adults only and are used at the owner&#39;s own risk. Attention should be paid to the print nozzles and print-beds of printers, as they reach high temperatures during operation and can cause burns. Printers should not be left unattended and should be disconnected from the electrical supply when not in use. To the fullest extent permissable by English law, we cannot accept liability for any damages, loss, personal injury or death resulting from the use of our products.<br />\n<br />\n<strong>General</strong><br />\nThese Terms and Conditions, all transactions relating to this website and all non-contractual obligations arising from any transaction carried out on this website are governed by English law and are subject to the non-exclusive jurisdiction of the English courts. We do not accept amendments to these terms and conditions.</p>\n\n<p> </p>'),
(4, 'Instalation', 'instalation', 'Instalation', 'http://rgstore.local/page/instalation', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>'),
(5, 'Returns Policy', 'return', 'Returns', 'http://rgstore.local/page/return', '<p>f you decide to return you order after you have received it, we will refund or exchange your purchase as long as the original packaging is undamaged and the item is returned unused and in a saleable condition. You will need to return your purchase within 7 working days (this does not include Saturday, Sunday and public holidays) from the day after the date of delivery. <a href="http://www.imakr.com/contact-us">Contact Us</a> to confirm the best way of returning the item. You will be liable for the cost of returning the item to us.  Refunds will be made to the card that you used to make the original payment, and will take 3-5 working days to be credited to your account.<br />\n<br />\nIf you receive an incorrect or damaged item, please <a href="http://www.imakr.com/contact-us">Contact Us</a> within 7 working days so that we can offer you an exchange or refund. We will confirm with you the most convenient way for you to return the item to us. All refunds will be made to the card that you used to make the original payment, and will take 3-5 working days to be credited to your account.<br />\n<br />\nUnfortunately we cannot accept returns of DVDs, CDs, memory cards and software if the seals have been broken. We also cannot accept returns of filament and 3D Art or products that have been modified to your specification.<br />\n<br />\nOur Returns Policy and Product Guarantee do not affect your statutory rights.<br />\nPlease read our Terms and Conditions for further information about our Returns Policy.</p>'),
(6, 'Guarantee', 'guarantee', 'Guarantee', 'http://rgstore.local/page/guarantee', '<p>We offer at least a 6-month warranty (1 year on some 3D Printers when indicated) on all our printers from the date of delivery, which cover parts and labor for all our 3D printers. The warranty does not cover any faults caused by accident, neglect, misuse, clogging, leveling or normal wear and tear.</p>\n\n<p>In the unlikely event that your printer develops a fault within the warranty period, please contact us via e-mail at <a href="mailto:Support@iMakr.com">Support@iMakr.com</a> / <a href="mailto:info@imakr.com">info@imakr.com</a></p>\n\n<p>The Support Team will be in contact to assist you in the best possible way. If your printer needs to be repaired, we will discuss the most convenient process for you to return the printer to us.</p>\n\n<p>When you receive your printer, please carefully inspect it for any shipping damage and contact us immediately via email (<a href="mailto:support@imakr.com">support@imakr.com</a>) if there are any issues—please include your unit&#39;s serial number and photos of the problem.</p>\n\n<p>To minimize damage to the printer, you do not need to include any power supply or filament when your printer comes in for repairs.<br />\n<br />\nIf you return your printer to us for repairs in the original packaging (with protective enclosures), you will not be liable for the cost of returning the printer to us. We will also return the printer to you free of charge.<br />\n<br />\nIn the case where original packaging is NOT used to return the printer to us for repair, you are liable to arrange your own delivery and collection of your printer. Instant Makr Ltd will not cover any damage caused to the printer during this shipping procedure.<br />\n<br />\nOur Returns Policy and Product Guarantee do not affect your statutory rights.<br />\n<br />\nPlease read our Terms and Conditions for further information about our Product Guarantee.</p>'),
(7, 'News', 'news', 'News', 'http://rgstore.local/page/news', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>'),
(8, 'Careers', 'career', 'Careers', 'http://rgstore.local/page/career', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>'),
(9, 'About us', 'about', 'About Us', 'http://rgstore.local/page/about', '<p><strong>iMakr offers:</strong></p>\n\n<p> </p>\n\n<p><strong>Best in class 3D printers;</strong></p>\n\n<p> </p>\n\n<p>iMakr offers you the best quality 3D printers thanks to it’s expert team of engineers who test and approve each product that his the shelves.</p>\n\n<p> </p>\n\n<p><strong>Large range of quality consumables;</strong></p>\n\n<p> </p>\n\n<p>iMakr are able to provide you with the best and most advanced consumables thanks to regular testing of innovative new products.</p>\n\n<p> </p>\n\n<p><strong>High quality customer services before and after sales;</strong></p>\n\n<p> </p>\n\n<p>iMakr is known for providing the best standard of customer care in the industry. Our team of experts is able to handle any query, and will answer swiftly and efficiently.</p>\n\n<p>iMakr offers bi-weekly training every Tuesday and Thursday. As well as bespoke, on site on-site training should the need arise. Training is carried about by a team of skilled iMakr engineers and 3D printing consultants.</p>\n\n<p>After every sale, we follow up with purchase within 7 days and make sure every to ensure that every customer is able to adopt the technology without difficulty.</p>\n\n<p> </p>\n\n<p><strong>Maintenance/repair;</strong></p>\n\n<p> </p>\n\n<p>We are the maintenance and repair centre for all our partners. We keep stock of spare parts for all machines for immediate customer support and quick response time - usually within 24 hours.</p>\n\n<p> </p>\n\n<p><strong>My Mini Factory - world&#39;s largest curated 3D object platform;</strong></p>\n\n<p> </p>\n\n<p>All iMakr customers are benefited by free 3D printables available for free on MyMiniFactory.com</p>\n\n<p> </p>\n\n<p><strong>Events and Exhibitions;</strong></p>\n\n<p> </p>\n\n<p>iMakr participates regularly in prominent events and exhibitions where it demonstrates 3D Printing in an effort to help people understand what this technology can do for them. iMakr has been invited by world leading companies and brands to showcase 3D Printing Technology in a range of environments and spaces.</p>\n\n<p> </p>\n\n<p> If you need information or any query, please contact us at <a href="mailto:rgatewaytest@gmail.com">info@imakr.com</a></p>'),
(10, 'CUSTOMER SUPPORT', 'support', 'Customer support - Help', 'http://rgstore.local/page/support', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>'),
(11, 'Collect in Store', 'stores', 'Collect in Store', 'http://rgstore.local/page/stores', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>'),
(12, 'Money-Back Guarantee', 'money-back', 'Money-Back Guarantee', 'http://rgstore.local/page/money-back', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>'),
(13, 'Buyer''s Guide', 'buyers-guide', 'Buyer''s Guide', 'http://rgstore.local/page/buyers-guide', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>'),
(14, 'Best Price & Largest Choice', 'best-choice', 'Best Price & Largest Choice', 'http://rgstore.local/page/best-choice', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>'),
(15, 'Delivery by DHL in Europe', 'delivery-europe', 'Delivery by DHL in Europe', 'http://rgstore.local/page/delivery-europe', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `transactions_paypal`
--

CREATE TABLE IF NOT EXISTS `transactions_paypal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `complete` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `transactions_paypal`
--

INSERT INTO `transactions_paypal` (`id`, `user_id`, `payment_id`, `hash`, `complete`) VALUES
(1, 7, 'PAY-1753120380068142VKZFR2FI', '8e43ad9522951fbf90fc9e7a11d40033', 1),
(2, 7, 'PAY-4FT588981L321352VKZFR3CY', '3d45f036cdef8db8b42125a606da7e81', 1),
(3, 7, 'PAY-9SH39500TC850364SKZFR5AQ', '523dafb17e2abe9cff6ea337ab8e1a55', 1),
(4, 7, 'PAY-2CA04856G00431200KZFR6LA', '76e175b72d8307965cc9e77cadd12c27', 1),
(5, 7, 'PAY-5FV8538495239432YKZFR7AA', '11bf78b3622f2a07da90b9388a8e7f79', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `reset_token` varchar(255) NOT NULL,
  `registered_at` datetime NOT NULL,
  `reset_at` datetime NOT NULL,
  `is_admin` int(1) NOT NULL,
  `online` int(5) NOT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `google_acc_token` varchar(255) DEFAULT NULL,
  `facebook_acc_token` varchar(255) DEFAULT NULL,
  `twitter_acc_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `title`, `fname`, `lname`, `reset_token`, `registered_at`, `reset_at`, `is_admin`, `online`, `google_id`, `facebook_id`, `twitter_id`, `google_acc_token`, `facebook_acc_token`, `twitter_acc_token`) VALUES
(7, '', 'b501aaf172e9e1f15db425a98223b54e9174f032', 'user@abv.bg', 'Mr', 'User', 'Userov', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'admin', '983deae511d41e77f27360335c0b52c89e4b78b2', '', '', 'admin', '', '', '2015-11-13 09:48:40', '0000-00-00 00:00:00', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `country_id` int(1) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `phonecode` varchar(45) NOT NULL,
  `is_main` int(1) NOT NULL,
  `is_deleted` int(1) NOT NULL,
  PRIMARY KEY (`detail_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`detail_id`, `user_id`, `address`, `city`, `zip_code`, `country_id`, `phone`, `phonecode`, `is_main`, `is_deleted`) VALUES
(1, 1, 'Brezovska 55', 'Plovdiv', 4000, 33, '33456433', '359', 0, 0),
(2, 7, 'Brezovska 55', 'Plovdiv', 4000, 33, '885791348', '359', 0, 0),
(3, 7, 'Uarsd eist 143', 'Aalborg', 4124, 58, '123999332', '45', 1, 0),
(4, 7, 'Avgusta Trauyana 55', 'Stara zagora', 6000, 33, '112233', '359', 0, 0),
(5, 7, 'Uarsd eis 143', 'Plovdiv', 6000, 14, '2354545', '43', 0, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bundle`
--
ALTER TABLE `bundle`
  ADD CONSTRAINT `bundle_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bundle_items`
--
ALTER TABLE `bundle_items`
  ADD CONSTRAINT `bundle_items_ibfk_1` FOREIGN KEY (`bundle_id`) REFERENCES `bundle` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`menus_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`man_id`) REFERENCES `manufacturer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`print_id`) REFERENCES `print_tech` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_3` FOREIGN KEY (`build_id`) REFERENCES `building_size` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_4` FOREIGN KEY (`acc_id`) REFERENCES `accuracy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_5` FOREIGN KEY (`f_cat_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products_images`
--
ALTER TABLE `products_images`
  ADD CONSTRAINT `products_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products_ordered`
--
ALTER TABLE `products_ordered`
  ADD CONSTRAINT `products_ordered_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
