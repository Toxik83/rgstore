//JQuery Twitter Feed. Coded by Tom Elliott @ www.webdevdoor.com (2013) based on https://twitter.com/javascripts/blogger.js
//Requires JSON output from authenticating script: http://www.webdevdoor.com/php/authenticating-twitter-feed-timeline-oauth/
$(document).ready(function () { 
    var displaylimit = 2;
    var twitterprofile = "parkour2111";
	var screenname = "Velislav Ichev";
    var showdirecttweets = false;
    var showretweets = true;
    var showtweetlinks = true;
    var showprofilepic = false;
	var showtweetactions = false;
	var showretweetindicator = true;
	
	var headerHTML = '';
	var loadingHTML = '';

        var baseurl = window.location.origin;
        
	
	$('#twitter-feed').html(headerHTML + loadingHTML);

    $.getJSON(baseurl + '/index.php/blog/tweets',


        function(feeds) {   
		   //alert(feeds);
            var feedHTML = '';
            var displayCounter = 1;         
            for (var i=0; i<feeds.length; i++) {
				var tweetscreenname = feeds[i].user.name;
                var tweetusername = feeds[i].user.screen_name;
                var profileimage = feeds[i].user.profile_image_url_https;
                var status = feeds[i].text; 
				var isaretweet = false;
				var isdirect = false;
				var tweetid = feeds[i].id_str;
				
				//If the tweet has been retweeted, get the profile pic of the tweeter
				if(typeof feeds[i].retweeted_status != 'undefined'){
				   profileimage = feeds[i].retweeted_status.user.profile_image_url_https;
				   tweetscreenname = feeds[i].retweeted_status.user.name;
				   tweetusername = feeds[i].retweeted_status.user.screen_name;
				   tweetid = feeds[i].retweeted_status.id_str;
				   status = feeds[i].retweeted_status.text; 
				   isaretweet = true;
				 };
				 
				 
				 //Check to see if the tweet is a direct message
				 if (feeds[i].text.substr(0,1) == "@") {
					 isdirect = true;
				 }
				 
				//console.log(feeds[i]);
				 
				 //Generate twitter feed HTML based on selected options
				 if (((showretweets == true) || ((isaretweet == false) && (showretweets == false))) && ((showdirecttweets == true) || ((showdirecttweets == false) && (isdirect == false)))) { 
					if ((feeds[i].text.length > 1) && (displayCounter <= displaylimit)) {             
						if (showtweetlinks == true) {
							status = addlinks(status);
						}
						 
						if (displayCounter == 1) {
							feedHTML += headerHTML;
						}
                                                jQuery(document).ready(function () {
                                                    jQuery("abbr.timeago").timeago();
                                                });	
                                                
						feedHTML += '<li class="twitter-article" id="tw'+displayCounter+'">';
						feedHTML += '<span class="tweetprofilelink"><a href="https://twitter.com/'+tweetusername+'" target="_blank">@'+tweetusername+'</a></span><span class="tweet-time"> '+status+'<a href="https://twitter.com/'+tweetusername+'/status/'+tweetid+'" target="_blank">' + ' ' + (feeds[i].created_at)+'</a></span>';
						
						if ((isaretweet == true) && (showretweetindicator == true)) {
							feedHTML += '<div id="retweet-indicator"></div>';
						}						
						if (showtweetactions == true) {
							feedHTML += '<div id="twitter-actions"><div class="intent" id="intent-reply"><a href="https://twitter.com/intent/tweet?in_reply_to='+tweetid+'" title="Reply"></a></div><div class="intent" id="intent-retweet"><a href="https://twitter.com/intent/retweet?tweet_id='+tweetid+'" title="Retweet"></a></div><div class="intent" id="intent-fave"><a href="https://twitter.com/intent/favorite?tweet_id='+tweetid+'" title="Favourite"></a></div></div>';
						}

						feedHTML += '</div>';
						displayCounter++;
					}   
				 }
            }
            
             
            $('.twitter-feed').html(feedHTML);
			
			//Add twitter action animation and rollovers
			if (showtweetactions == true) {				
				$('.twitter-article').hover(function(){
					$(this).find('#twitter-actions').css({'display':'block', 'opacity':0, 'margin-top':-20});
					$(this).find('#twitter-actions').animate({'opacity':1, 'margin-top':0},200);
				}, function() {
					$(this).find('#twitter-actions').animate({'opacity':0, 'margin-top':-20},120, function(){
						$(this).css('display', 'none');
					});
				});			
			
				//Add new window for action clicks
			
				$('#twitter-actions a').click(function(){
					var url = $(this).attr('href');
				  window.open(url, 'tweet action window', 'width=580,height=500');
				  return false;
				});
			}
			
			
    }).error(function(jqXHR, textStatus, errorThrown) {
		var error = "";
            if (jqXHR.status == 404) {
                error = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                error = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                error = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                error = 'Time out error.';
            } else if (exception === 'abort') {
                error = 'Ajax request aborted.';
            } else {
                error = 'Uncaught Error.\n' + jqXHR.responseText;
            }	
       		alert("error: " + error);
    });
    

    //Function modified from Stack Overflow
    function addlinks(data) {
        //Add link to all http:// links within tweets
         data = data.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g, function(url) {
            return '<a href="'+url+'"  target="_blank">'+url+'</a>';
        });
             
        //Add link to @usernames used within tweets
        data = data.replace(/\B@([_a-z0-9]+)/ig, function(reply) {
            return '<a href="http://twitter.com/'+reply.substring(1)+'" style="font-weight:lighter;" target="_blank">'+reply.charAt(0)+reply.substring(1)+'</a>';
        });
		//Add link to #hastags used within tweets
        data = data.replace(/\B#([_a-z0-9]+)/ig, function(reply) {
            return '<a href="https://twitter.com/search?q='+reply.substring(1)+'" style="font-weight:lighter;" target="_blank">'+reply.charAt(0)+reply.substring(1)+'</a>';
        });
        return data;
    }
     

     
    function relative_time(time_value) {
      var values = time_value.split(" ");
      time_value = values[1] + " " + values[2] + ", " + values[5] + " " + values[3];
      var parsed_date = Date.parse(time_value);
      var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
      var delta = parseInt((relative_to.getTime() - parsed_date) / 1000);
	  var shortdate = time_value.substr(4,2) + " " + time_value.substr(0,3);
      delta = delta + (relative_to.getTimezoneOffset() * 60);
     
      if (delta < 60) {
        return '1m';
      } else if(delta < 120) {
        return '1m';
      } else if(delta < (60*60)) {
        return (parseInt(delta / 60)).toString() + 'm';
      } else if(delta < (120*60)) {
        return '1h';
      } else if(delta < (24*60*60)) {
        return (parseInt(delta / 3600)).toString() + 'h';
      } else if(delta < (48*60*60)) {
        //return '1 day';
		return shortdate;
      } else {
        return shortdate;
      }
  } 
  
  
  
    (function (factory) {
        if (typeof define === 'function' && define.amd) {
            // AMD. Register as an anonymous module.
            define(['jquery'], factory);
        } else {
            // Browser globals
            factory(jQuery);
        }
    }(function ($) {
        $.timeago = function (timestamp) {
            if (timestamp instanceof Date) {
                return inWords(timestamp);
            } else if (typeof timestamp === "string") {
                return inWords($.timeago.parse(timestamp));
            } else if (typeof timestamp === "number") {
                return inWords(new Date(timestamp));
            } else {
                return inWords($.timeago.datetime(timestamp));
            }
        };
        var $t = $.timeago;

        $.extend($.timeago, {
            settings: {
                refreshMillis: 60000,
                allowPast: true,
                allowFuture: false,
                localeTitle: false,
                cutoff: 0,
                strings: {
                    prefixAgo: null,
                    prefixFromNow: null,
                    suffixAgo: "ago",
                    suffixFromNow: "from now",
                    inPast: 'any moment now',
                    seconds: "less than a minute",
                    minute: "about a minute",
                    minutes: "%d minutes",
                    hour: "about an hour",
                    hours: "about %d hours",
                    day: "a day",
                    days: "%d days",
                    month: "about a month",
                    months: "%d months",
                    year: "about a year",
                    years: "%d years",
                    wordSeparator: " ",
                    numbers: []
                }
            },
            inWords: function (distanceMillis) {
                if (!this.settings.allowPast && !this.settings.allowFuture) {
                    throw 'timeago allowPast and allowFuture settings can not both be set to false.';
                }

                var $l = this.settings.strings;
                var prefix = $l.prefixAgo;
                var suffix = $l.suffixAgo;
                if (this.settings.allowFuture) {
                    if (distanceMillis < 0) {
                        prefix = $l.prefixFromNow;
                        suffix = $l.suffixFromNow;
                    }
                }

                if (!this.settings.allowPast && distanceMillis >= 0) {
                    return this.settings.strings.inPast;
                }

                var seconds = Math.abs(distanceMillis) / 1000;
                var minutes = seconds / 60;
                var hours = minutes / 60;
                var days = hours / 24;
                var years = days / 365;

                function substitute(stringOrFunction, number) {
                    var string = $.isFunction(stringOrFunction) ? stringOrFunction(number, distanceMillis) : stringOrFunction;
                    var value = ($l.numbers && $l.numbers[number]) || number;
                    return string.replace(/%d/i, value);
                }

                var words = seconds < 45 && substitute($l.seconds, Math.round(seconds)) ||
                        seconds < 90 && substitute($l.minute, 1) ||
                        minutes < 45 && substitute($l.minutes, Math.round(minutes)) ||
                        minutes < 90 && substitute($l.hour, 1) ||
                        hours < 24 && substitute($l.hours, Math.round(hours)) ||
                        hours < 42 && substitute($l.day, 1) ||
                        days < 30 && substitute($l.days, Math.round(days)) ||
                        days < 45 && substitute($l.month, 1) ||
                        days < 365 && substitute($l.months, Math.round(days / 30)) ||
                        years < 1.5 && substitute($l.year, 1) ||
                        substitute($l.years, Math.round(years));

                var separator = $l.wordSeparator || "";
                if ($l.wordSeparator === undefined) {
                    separator = " ";
                }
                return $.trim([prefix, words, suffix].join(separator));
            },
            parse: function (iso8601) {
                var s = $.trim(iso8601);
                s = s.replace(/\.\d+/, ""); // remove milliseconds
                s = s.replace(/-/, "/").replace(/-/, "/");
                s = s.replace(/T/, " ").replace(/Z/, " UTC");
                s = s.replace(/([\+\-]\d\d)\:?(\d\d)/, " $1$2"); // -04:00 -> -0400
                s = s.replace(/([\+\-]\d\d)$/, " $100"); // +09 -> +0900
                return new Date(s);
            },
            datetime: function (elem) {
                var iso8601 = $t.isTime(elem) ? $(elem).attr("datetime") : $(elem).attr("title");
                return $t.parse(iso8601);
            },
            isTime: function (elem) {
                // jQuery's `is()` doesn't play well with HTML5 in IE
                return $(elem).get(0).tagName.toLowerCase() === "time"; // $(elem).is("time");
            }
        });

        // functions that can be called via $(el).timeago('action')
        // init is default when no action is given
        // functions are called with context of a single element
        var functions = {
            init: function () {
                var refresh_el = $.proxy(refresh, this);
                refresh_el();
                var $s = $t.settings;
                if ($s.refreshMillis > 0) {
                    this._timeagoInterval = setInterval(refresh_el, $s.refreshMillis);
                }
            },
            update: function (time) {
                var parsedTime = $t.parse(time);
                $(this).data('timeago', {datetime: parsedTime});
                if ($t.settings.localeTitle)
                    $(this).attr("title", parsedTime.toLocaleString());
                refresh.apply(this);
            },
            updateFromDOM: function () {
                $(this).data('timeago', {datetime: $t.parse($t.isTime(this) ? $(this).attr("datetime") : $(this).attr("title"))});
                refresh.apply(this);
            },
            dispose: function () {
                if (this._timeagoInterval) {
                    window.clearInterval(this._timeagoInterval);
                    this._timeagoInterval = null;
                }
            }
        };

        $.fn.timeago = function (action, options) {
            var fn = action ? functions[action] : functions.init;
            if (!fn) {
                throw new Error("Unknown function name '" + action + "' for timeago");
            }
            // each over objects here and call the requested function
            this.each(function () {
                fn.call(this, options);
            });
            return this;
        };

        function refresh() {
            //check if it's still visible
            if (!$.contains(document.documentElement, this)) {
                //stop if it has been removed
                $(this).timeago("dispose");
                return this;
            }

            var data = prepareData(this);
            var $s = $t.settings;

            if (!isNaN(data.datetime)) {
                if ($s.cutoff == 0 || Math.abs(distance(data.datetime)) < $s.cutoff) {
                    $(this).text(inWords(data.datetime));
                }
            }
            return this;
        }

        function prepareData(element) {
            element = $(element);
            if (!element.data("timeago")) {
                element.data("timeago", {datetime: $t.datetime(element)});
                var text = $.trim(element.text());
                if ($t.settings.localeTitle) {
                    element.attr("title", element.data('timeago').datetime.toLocaleString());
                } else if (text.length > 0 && !($t.isTime(element) && element.attr("title"))) {
                    element.attr("title", text);
                }
            }
            return element.data("timeago");
        }

        function inWords(date) {
            return $t.inWords(distance(date));
        }

        function distance(date) {
            return (new Date().getTime() - date.getTime());
        }

        // fix for IE6 suckage
        document.createElement("abbr");
        document.createElement("time");
    }));

  
//  function time_Ago(time_ago) {
//    time_ago = strtotime(time_ago);
//    var cur_time = time();
//    var time_elapsed = cur_time - time_ago;
//    var seconds = time_elapsed;
//    var minutes = round(time_elapsed / 60);
//    var hours = round(time_elapsed / 3600);
//    var days = round(time_elapsed / 86400);
//    var weeks = round(time_elapsed / 604800);
//    var months = round(time_elapsed / 2600640);
//    var years = round(time_elapsed / 31207680);
//    // Seconds
//    if (seconds <= 20) {
//        return "just now";
//    }
//    else if (seconds >= 20 && seconds <= 60) {
//        return "less than a minute ago";
//    }
//    //Minutes
//    else if (minutes <= 60) {
//        if (minutes === 1) {
//            return "one minute ago";
//        } else if (minutes >= 20 && minutes <= 45) {
//            return "about half an hour ago";
//        }else if (minutes >= 45 && minutes <= 60) {
//            return "about an hour ago";
//        } 
//        else {
//            return "$minutes minutes ago";
//        }
//    }
//    //Hours
//    else if (hours <= 24) {
//        if (hours === 1) {
//            return "an hour ago";
//        }else if (hours === 1 && (minutes >= 80  && minutes <= 100)){
//            return "about hour and a half ago";
//        } else if (hours === 1 && (minutes >= 100  && minutes <= 120)){
//            return "about " + ( hours + 1 ) + "  hrs ago";
//        } else {
//            return hours +  " hrs ago";
//        }
//    }
//    //Days
//    else if (days <= 7) {
//        if (days === 1) {
//            return "yesterday";
//        } else {
//            return "$days days ago";
//        }
//    }
//    //Weeks
//    else if (weeks <= 4.3) {
//        if (weeks === 1) {
//            return "a week ago";
//        } else {
//            return "$weeks weeks ago";
//        }
//    }
//    //Months
//    else if (months <= 12) {
//        if (months === 1) {
//            return "a month ago";
//        } else {
//            return  months + " months ago";
//        }
//    }
//    //Years
//    else {
//        if (years === 1) {
//            return "one year ago";
//        } else {
//            return years + "years ago";
//        }
//    }
    
    
});