(function ($) {
    $(window).load(function () {
        $('#flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            directionNav: false,
            keyboard: true,
            startAt: 0,
            slideshow: false,
            randomize: false,
            prevText: "",
            nextText: "",
            start: function () {
                $('body').removeClass('loading');
            }
        });
    });
})(jQuery);
