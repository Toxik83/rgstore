$(document).ready(function () {
    $("#selectId").on('change', Click);

    window.keyword = '';
    var chosen = "";
    var a = $('.search_form');
    
    $("#suggesstion-box").on('click',function(){
        $('.search_form').submit();  
    });  
    
    $("#suggesstion-box").mouseover(function(){
        $('#suggesstion-box ul li:eq(' + chosen + ')').removeClass('selected_li');
    });
//    $("#suggesstion-box").mouseout(function(e){
//        e.preventDefault();
//        $('#suggesstion-box ul li:eq(' + chosen + ')').addClass('selected_li');
//    });
    
    
    $(document).on('keydown', '#autocomplete', function (e) {
        if (e.keyCode === 40) {
            if (chosen === "") {
                chosen = 0;
            } else if ((chosen + 1) < $('#suggesstion-box ul li').length) {
                chosen++;
            }
            $('#suggesstion-box ul li').removeClass('selected_li');
            $('#suggesstion-box ul li:eq(' + chosen + ')').addClass('selected_li');
            return false;
        }
        if (e.keyCode === 38) {
            if (chosen === "") {
                chosen = 0;
            } else if (chosen > 0) {
                chosen--;
            }
            $('#suggesstion-box ul li').removeClass('selected_li');
            $('#suggesstion-box ul li:eq(' + chosen + ')').addClass('selected_li');
            return false;
        }
        
        if ($("#suggesstion-box").is(':empty') === false) {
            if (e.keyCode === 13 ) {
                if(chosen === ""){
                    return;
                }else{
                    var a = $('#suggesstion-box ul li:eq(' + chosen + ')').text();
                    $('#autocomplete').val(a);
                    $("#suggesstion-box").hide();
                }
            }
        }
    });
    
    $("#autocomplete").keyup(function (e) {
        if (e.keyCode === 40 || e.keyCode === 38 || e.keyCode === 13) {
            return;
        }else{
            window.keyword = $("#autocomplete").val();
            $.ajax({
                type: "Post",
                url: siteurl + '/finishing/autoSearch',
                data: "keyword=" + keyword + "&selectId=" + $("#selectId").val(),
                beforeSend: function () {
                    $("#autocomplete").css("background", "#FFF url(/media/public/images/LoaderIcon.gif) no-repeat 190px");
                },
                success: function (data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#autocomplete").css("background", "#FFF");
                }

            });
        }
    });
    

    function Click() {
        $.ajax({
            type: "Post",
            url: siteurl + '/finishing/autoSearch',
            data: "keyword=" + keyword + "&selectId=" + $("#selectId").val(),
            beforeSend: function () {
                $("#autocomplete").css("background", "#FFF url(/media/public/images/LoaderIcon.gif) no-repeat 190px");
            },
            success: function (data) {
                $("#suggesstion-box").show();
                $("#suggesstion-box").html(data);
                $("#autocomplete").css("background", "#FFF");
            }

        });
    }

});

function select(val) {
    $("#autocomplete").val(val);
    $("#suggesstion-box").hide();
}
