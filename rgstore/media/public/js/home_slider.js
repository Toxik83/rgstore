(function ($) {
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: true,
            directionNav: false,
            manualControls: ".flex-control-nav li",
            useCSS: false /* Chrome fix*/,
            keyboard: true,
            startAt: 0,
            slideshow: false,
            randomize: false,
            prevText: "",
            nextText: "",
            start: function () {
                $('body').removeClass('loading');
            }
        });
    });
})(jQuery);