$(document).ready(function(){

    // Show hide popover
    $(".dropdownShare").click(function(){
        var share = $(this).closest('.dropdownShare');
        share.children('.dropdown-share').slideToggle();
        var parent =  share.closest('.product_details');
        parent.parent().siblings().find('.dropdown-share').fadeOut();
    });


    $(document).on("click", function(event){
        var $trigger = $(".dropdownShare");
        if($trigger !== event.target && !$trigger.has(event.target).length){
            $(".dropdown-share").fadeOut();
        }
    });

});
