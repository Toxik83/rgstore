$(function () {
    $(".radio_button").on('change', function () {
        var txt = $(this).val();
        $.ajax({
            url: '/index.php/cart/calculate_order',
            type: 'post',
            dataType: "json",
            data: {
                shipping: txt
            },
            success: function (shipping) {
                $('#total').text(shipping);
                $('#shipping_cost').text(txt);
            }
        });
    });
});
