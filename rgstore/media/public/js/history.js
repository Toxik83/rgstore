$(document).ready(function () {
    var sidebar,dropDown,fetch,fetchDropDown;

    sidebar = $('#wrap');
    dropDown = $('#wrap');

     dropDown.find('a#nothing').on('click', function (e) {
        var nothing = $(this).attr('href');
        
        history.pushState(null, null, nothing);
        
        fetchDropDown(nothing);
        e.preventDefault();

    });

    sidebar.find('a#kozaa,a#koza1').on('click', function (e) {
        var href = $(this).attr('href');

        history.pushState(null, null, href);
        
        fetch(href);
        e.preventDefault();

    });
   
   

    fetchDropDown = function (nothing) {
        $.ajax({
            url: window.location + nothing,
            method: 'GET',
            cache: false,
            success: function (data) {
                dropDown.html(data);
            }
        });
    };
    fetch = function (href) {
        $.ajax({
            url: 'http://rgstore.local/finishing/' + href,
            method: 'GET',
            cache: false,
            success: function (data) {
               
                sidebar.html(data);
            }
        });
    };

});