$(function () {
    var id = $('#userId').val();

    $('#editUserForm').validate({
       rules: {
           fname: {
               required: true,
               minlength: 3
           },
           lname: {
               required: true,
               minlength: 3
           },
           currPassword: {
               required: function() {
                    return $('#password').val() !== '';
               },
               minlength: 3,
               remote: {
                    url: "/index.php/userProfile/ajaxCheckPassword",
                    type: "post",
                    data: {
                        id: id
                    },
                }
           },
           password: {
               required: function() {
                    return $('#password').val() !== '';
               },
               minlength: 3
           },
           passconf: {
               required: function() {
                    return $('#password').val() !== '';
               },
               minlength: 3,
               equalTo: "#password"
           },
           email: {
               required: function() {
                            return $("#email").val() !== $("#emailHidden").val();
                         },
               email: true,
               remote: {
                    param: {
                      url: "/index.php/register/ajaxCheckEmail",
                    },
                    depends: function() {
                      return $("#email").val() !== $("#emailHidden").val();
                    }
               }
           },
           terms: "required"
       },
        messages: {
            fname: {
                required: "Please enter a first name",
                minlength: jQuery.validator.format("At least {0} characters required!")
            },
            lname: {
                required: "Please enter a last name",
                minlength: jQuery.validator.format("At least {0} characters required!")
            },
            currPassword: {
                required: "Please enter your current password",
                minlength: jQuery.validator.format("At least {0} characters required!"),
                remote: "This password doesn't match your current password"
            },
            password: {
                required: "Please enter a password",
                minlength: jQuery.validator.format("At least {0} characters required!")
            },
            passconf: {
                required: "Please confirm the new password",
                minlength: jQuery.validator.format("At least {0} characters required!"),
                equalTo: "The confirmation password doesn't match"
            },
            email: {
                required: "Please enter email address",
                remote: "This email is already taken."
            }
        }
    });
});
