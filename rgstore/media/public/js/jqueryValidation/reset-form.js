$(function () {
    $('#resetForm').validate({
       rules: {
           password: {
               required: true,
               minlength: 3
           },
           passconf: {
               required: true,
               minlength: 3,
               equalTo: "#password"
           }
       },
        messages: {
            password: {
                required: "Please enter a password",
                minlength: jQuery.validator.format("At least {0} characters required!")
            },
            passconf: {
                required: "Please enter a password",
                minlength: jQuery.validator.format("At least {0} characters required!"),
                equalTo: "The confirmation password doesn't match"
            }
        }
    });
});