$(function () {
    $('#registerForm').validate({
       rules: {
           title: {
               valueNotEquals: "Title"
           },
           fname: {
               required: true,
               minlength: 3
           },
           lname: {
               required: true,
               minlength: 3
           },
           password: {
               required: true,
               minlength: 3
           },
           passconf: {
               required: true,
               minlength: 3,
               equalTo: "#password"
           },
           email: {
               required: true,
               email: true,
               remote: {
                    url: "/index.php/register/ajaxCheckEmail",
                    type: "post"
                }
           },
           terms: "required"
       },
        messages: {
            title: {
               valueNotEquals: "Please select title"
            },
            fname: {
                required: "Please enter a first name",
                minlength: jQuery.validator.format("At least {0} characters required!")
            },
            lname: {
                required: "Please enter a last name",
                minlength: jQuery.validator.format("At least {0} characters required!")
            },
            password: {
                required: "Please enter a password",
                minlength: jQuery.validator.format("At least {0} characters required!")
            },
            passconf: {
                required: "Please enter a password",
                minlength: jQuery.validator.format("At least {0} characters required!"),
                equalTo: "The confirmation password doesn't match"
            },
            email: {
                required: "Please enter email address",
                remote: "This email is already taken."
            },
            terms: {
                required: "Please agree to our terms of use"
            }
        }
    });
});