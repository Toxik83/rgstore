﻿var div = document.getElementById('errors');
var title = document.getElementById('title');
var name1 = document.getElementById('name1'); 	
var name2 = document.getElementById('name2');
var email = document.getElementById('email'); 			
var password = document.getElementById('password');
var repassword=document.getElementById('repassword'); 


function formValidator() {
    
   //  if (notEmpty(username, "Username field must be filled.")) {
    if (madeSelection(title, "Title", "Please choose a title."))  {
        if (isAlphabet(name1, "Please enter only letters for your first name."))      {
            if (isAlphabet(name2, "Please enter only letters for your last name."))  {
                    if (emailValidator(email, "Please enter a valid email address") && notEmpty(email, "Email field must be filled.")) {
                        if (isAlphanumeric(password, "Password must contain only letter and numbers.")){
                            if (passwordCheck(password, repassword, "Passwords don't match."))  {
                                alert("Form successfuly submited!");
                            return true;
                            }
                         }
                     }
                }
            }
        }

   return false;

}

function returnError(errorMessage) {
    div.innerHTML = "<p>" + errorMessage + "</p>";
}
function clearError(elem) {
    elem.onfocus = function () {
        div.innerHTML = "";
    };
}

function notEmpty(elem, errorMessage) {
    if (elem.value.length == 0) {
        returnError(errorMessage);
        clearError(elem);
        return false;
    }
    return true;
}

function isNumeric(elem, errorMessage) {
    var numericExp = /^[0-9]+$/;
    if (elem.value.match(numericExp)) {
        return true;
    } else {
        returnError(errorMessage);
        clearError(elem);
        return false;

    }
}

function isAlphabet(elem, errorMessage) {
    var alphaExp = /^[a-zA-Z]+$/;
    if (elem.value.match(alphaExp)) {
        return true;
    } else {
        returnError(errorMessage);
        clearError(elem);
        return false;
    }
}

function isAlphanumeric(elem, errorMessage) {
    var alphaExp = /^[0-9a-zA-Z ]+$/;
    if (elem.value.match(alphaExp)) {
        return true;
    } else {
        returnError(errorMessage);
        clearError(elem);
        return false;
    }
}

function lengthRestriction(elem, min, max) {
    var uInput = elem.value;
    if (uInput.length >= min && uInput.length <= max) {
        return true;
    } else {
        returnError("The length of the field must be between " + min + " and " + max);
        clearError(elem);
        return false;
    }
}

function madeSelection(elem, pattern ,errorMessage) {
    if (elem.value == pattern) {
        returnError(errorMessage);
        clearError(elem);
        return false;
    } else {
        return true;
    }
}

function emailValidator(elem, errorMessage) {
    var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (elem.value.match(emailExp)) {
        return true;
    } else {
        returnError(errorMessage);
        clearError(elem);
        return false;
    }
}

function passwordCheck(elem, elem2, errorMessage) {
    if (elem.value == elem2.value) {
        return true;
    } else {
        returnError(errorMessage);
        clearError(elem);
        return false;
    }
}
