$(function () {
    $('#countries').on('change', function () {
        var countryId = $(this).val();
        $.ajax({
            url: '/index.php/userProfile/getPhoneCode',
            type: 'post',
            dataType: 'json',
            data: {
                cId: countryId
            },
            success: function (data) {
                $('#code').text('+' + data.phonecode);
            }
        });
    });
});
