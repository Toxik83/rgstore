$(document).ready(function () {
    
    $('.cat').on('click', function (e) {
        var match = document.URL.match(/catName=\d+/);
        var matchUrl = document.URL.match(/details/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'finishing') {
            var submitTo = $form.attr("action") + '?catName=' + $(this).attr('data-cat');
        }else if (window.location.href.indexOf("catName") > -1) {
            var submitTo = window.location.href.replace(match, 'catName=' + $(this).attr('data-cat'));
        }else if(matchUrl ||matchSeller || matchDeals || matchPagination) {
            var submitTo = siteurl + 'finishing/search' + '?catName=' + $(this).attr('data-cat');
        }else if( window.location == siteurl) {
            var submitTo = ("finishing/search") + ('?catName=' + $(this).attr('data-cat'));
        }else {
            var submitTo = window.location + '&catName=' + $(this).attr('data-cat');
        }

        window.location.href = submitTo;

        return false;
    });

    $('.man').on('click', function (e) {
        var match = document.URL.match(/man=\d+/);
        var matchUrl = document.URL.match(/details/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'finishing') {
            var submitTo = $form.attr("action") + '?man=' + $(this).attr('data-man');
        }else if( window.location == siteurl) {
            var submitTo = ("finishing/search") + ('?man=' + $(this).attr('data-man'));
        }else if(matchUrl || matchSeller  || matchDeals|| matchPagination) {
            var submitTo = siteurl + 'finishing/search' + '?man=' + $(this).attr('data-man');
        }else if (window.location.href.indexOf("man") > -1) {
            var submitTo = window.location.href.replace(match, 'man=' + $(this).attr('data-man'));
        }else {
            var submitTo = window.location + '&man=' + $(this).attr('data-man');
        }

        window.location.href = submitTo;

        return false;
    });

    $('.min_max').on('click', function (e) {
        var match = document.URL.match(/min=\d+&max=\d+/);
        var matchUrl = document.URL.match(/details/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'finishing') {
            var submitTo = $form.attr("action") + ('?min=' + $(this).attr('data-price') + '&max=' + $(this).attr('data-id2'));
        }else if(matchUrl || matchSeller || matchDeals || matchPagination) {
            var submitTo = siteurl + 'finishing/search' + ('?min=' + $(this).attr('data-price') + '&max=' + $(this).attr('data-id2'));
        }else if( window.location == siteurl) {
            
            var submitTo = ("finishing/search") + ('?min=' + $(this).attr('data-price') + '&max=' + $(this).attr('data-id2'));
        }
        else if (window.location.href.indexOf("min") > -1) {

            var submitTo = window.location.href.replace(match, 'min=' + $(this).attr('data-price') + '&max=' + $(this).attr('data-id2'));
        } else {
            var submitTo = window.location + '&min=' + $(this).attr('data-price') + '&max=' + $(this).attr('data-id2');
        }

        window.location.href = submitTo;

        return false;
    });
    

    
    $('.sort_result').on('change', function (e) {
        var match = document.URL.match(/orderBy=\d/);
        var matchPagination = document.URL.match(/index/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPer = document.URL.match(/per/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        
        if(matchPer && match){
            var submitTo = window.location.href.replace(match, 'orderBy=' + $(this).val());
        }else if(matchPer){
            var submitTo = window.location + '&orderBy=' + $(this).val();
        }else if(matchPagination){
            var submitTo = siteurl + $form.attr("action") +  ('?orderBy=' + $(this).val());
        }else if(matchSeller || matchDeals){
            var submitTo = 'search' +  ('?orderBy=' + $(this).val());
        }else if(window.location == siteurl + 'finishing' || window.location == siteurl + 'filamentportfolio' || window.location == siteurl + 'printersportfolio') {
            var submitTo = $form.attr("action") + ('?orderBy=' + $(this).val());
        }else if (window.location.href.indexOf("orderBy") > -1) {

            var submitTo = window.location.href.replace(match, 'orderBy=' + $(this).val());
        }else {
            var submitTo = window.location + '&orderBy=' + $(this).val();
        }

        window.location.href = submitTo;

        return false;
    });
    
    $('.per').on('change', function (e) {
        var match = document.URL.match(/per=\d+/);
        var matchPagination = document.URL.match(/index/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchOrderBy = document.URL.match(/orderBy/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        
        if(matchOrderBy && match){
            var submitTo = window.location.href.replace(match, 'per=' + $(this).val());
        }else if(matchOrderBy){
            var submitTo = window.location + '&per=' + $(this).val();
        }else if(matchPagination){
            var submitTo = siteurl + $form.attr("action")  + ('?per=' + $(this).val());
        }else if(matchSeller || matchDeals){
            var submitTo = 'search' + ('?per=' + $(this).val());
        }else if (window.location == siteurl + 'finishing' || window.location == siteurl + 'filamentportfolio' || window.location == siteurl + 'printersportfolio') {
            var submitTo = $form.attr("action") + ('?per=' + $(this).val());
        } else if (window.location.href.indexOf("per") > -1) {

            var submitTo = window.location.href.replace(match, 'per=' + $(this).val());
        } else {
            var submitTo = window.location + '&per=' + $(this).val();
        }

        window.location.href = submitTo;

        return false;
    });
    
    
    //    Filament scripts 
    
    
    $('.min_max_fil').on('click', function (e) {
        var match = document.URL.match(/min=\d+&max=\d+/);
        var matchUrl = document.URL.match(/filamentsDetail/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'filamentportfolio') {
            var submitTo = $form.attr("action") + ('?min=' + $(this).attr('data-price-fil') + '&max=' + $(this).attr('data-id2'));
        }else if(matchUrl || matchSeller || matchDeals || matchPagination) {
            var submitTo = siteurl + 'filamentportfolio/search' + ('?min=' + $(this).attr('data-price-fil') + '&max=' + $(this).attr('data-id2'));
        }else if( window.location == siteurl) {
            var submitTo = ("filamentportfolio/search") + ('?min=' + $(this).attr('data-price-fil') + '&max=' + $(this).attr('data-id2'));
        }else if (window.location.href.indexOf("min") > -1) {

            var submitTo = window.location.href.replace(match, 'min=' + $(this).attr('data-price-fil') + '&max=' + $(this).attr('data-id2'));
        } else {
            var submitTo = window.location + '&min=' + $(this).attr('data-price-fil') + '&max=' + $(this).attr('data-id2');
        }

        window.location.href = submitTo;

        return false;
    });
    
    $('.cat-fil').on('click', function (e) {
        var match = document.URL.match(/catName=\d/);
        var matchUrl = document.URL.match(/filamentsDetail/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'filamentportfolio') {
            var submitTo = $form.attr("action") + '?catName=' + $(this).attr('data-cat-fil');
        }else if(matchUrl || matchSeller || matchDeals || matchPagination) {
            var submitTo =  siteurl + 'filamentportfolio/search' + '?catName=' + $(this).attr('data-cat-fil');
        }else if( window.location == siteurl) {
            
            var submitTo = ("filamentportfolio/search") + ('?catName=' + $(this).attr('data-cat-fil'));
        } else if (window.location.href.indexOf("catName") > -1) {
            var submitTo = window.location.href.replace(match, 'catName=' + $(this).attr('data-cat-fil'));
        } else {
            var submitTo = window.location + '&catName=' + $(this).attr('data-cat-fil');
        }

        window.location.href = submitTo;

        return false;
    });
    
    $('.man-fil').on('click', function (e) {
        var match = document.URL.match(/man=\d/);
        var matchUrl = document.URL.match(/filamentsDetail/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'filamentportfolio') {
            var submitTo = $form.attr("action") + '?man=' + $(this).attr('data-man-fil');
        }else if(matchUrl || matchSeller || matchDeals || matchPagination) {
            var submitTo =  siteurl +'filamentportfolio/search' + '?man=' + $(this).attr('data-man-fil');
        }else if( window.location == siteurl) {
            
            var submitTo = ("filamentportfolio/search") + ('?man=' + $(this).attr('data-man-fil'));
        } else if (window.location.href.indexOf("man") > -1) {
            var submitTo = window.location.href.replace(match, 'man=' + $(this).attr('data-man-fil'));
        } else {
            var submitTo = window.location + '&man=' + $(this).attr('data-man-fil');
        }

        window.location.href = submitTo;

        return false;
    });
    
    // Printers filter
    
     $('.min_max_pr').on('click', function (e) {
        var match = document.URL.match(/min=\d+&max=\d+/);
        var matchUrl = document.URL.match(/printersdetail/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'printersportfolio') {
            var submitTo = $form.attr("action") + ('?min=' + $(this).attr('data-price-pr') + '&max=' + $(this).attr('data-id2'));
        }else if(matchUrl || matchSeller || matchDeals || matchPagination) {
            var submitTo = siteurl + 'printersportfolio/search' + ('?min=' + $(this).attr('data-price-pr') + '&max=' + $(this).attr('data-id2'));
        }else if( window.location == siteurl) {
            var submitTo = ("printersportfolio/search") + ('?min=' + $(this).attr('data-price-pr') + '&max=' + $(this).attr('data-id2'));
        }else if (window.location.href.indexOf("min") > -1) {

            var submitTo = window.location.href.replace(match, 'min=' + $(this).attr('data-price-pr') + '&max=' + $(this).attr('data-id2'));
        } else {
            var submitTo = window.location + '&min=' + $(this).attr('data-price-pr') + '&max=' + $(this).attr('data-id2');
        }

        window.location.href = submitTo;

        return false;
    });
    
    $('.man-pr').on('click', function (e) {
        var match = document.URL.match(/man=\d+/);
        var matchUrl = document.URL.match(/printersdetail/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'printersportfolio') {
            var submitTo = $form.attr("action") + '?man=' + $(this).attr('data-man-pr');
        }else if(matchUrl || matchSeller || matchDeals || matchPagination) {
            var submitTo = siteurl + 'printersportfolio/search' + '?man=' + $(this).attr('data-man-pr');
        }else if( window.location == siteurl) {
           var submitTo = ("printersportfolio/search") + ('?man=' + $(this).attr('data-man-pr'));
        } else if (window.location.href.indexOf("man") > -1) {
            var submitTo = window.location.href.replace(match, 'man=' + $(this).attr('data-man-pr'));
        } else {
            var submitTo = window.location + '&man=' + $(this).attr('data-man-pr');
        }

        window.location.href = submitTo;

        return false;
    });
    $('.tech').on('click', function (e) {
        var match = document.URL.match(/tech=\d+/);
        var matchUrl = document.URL.match(/printersdetail/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'printersportfolio') {
            var submitTo = $form.attr("action") + '?tech=' + $(this).attr('data-tech');
        }else if(matchUrl || matchSeller || matchDeals || matchPagination) {
            var submitTo = siteurl + 'printersportfolio/search' + '?tech=' + $(this).attr('data-tech');
        }else if( window.location == siteurl) {
            var submitTo = ("printersportfolio/search") + ('?tech=' + $(this).attr('data-tech'));
        } else if (window.location.href.indexOf("tech") > -1) {
            var submitTo = window.location.href.replace(match, 'tech=' + $(this).attr('data-tech'));
        } else {
            var submitTo = window.location + '&tech=' + $(this).attr('data-tech');
        }

        window.location.href = submitTo;

        return false;
    });
    
    $('.size').on('click', function (e) {
        var match = document.URL.match(/size=\d+/);
        var matchUrl = document.URL.match(/printersdetail/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'printersportfolio') {
            var submitTo = $form.attr("action") + '?size=' + $(this).attr('data-size');
        }else if(matchUrl || matchSeller || matchDeals || matchPagination ) {
            var submitTo = siteurl + 'printersportfolio/search' + '?size=' + $(this).attr('data-size');
        }else if( window.location == siteurl) {
            var submitTo = ("printersportfolio/search") + ('?size=' + $(this).attr('data-size'));
        }  else if (window.location.href.indexOf("size") > -1) {
            var submitTo = window.location.href.replace(match, 'size=' + $(this).attr('data-size'));
        } else {
            var submitTo = window.location + '&size=' + $(this).attr('data-size');
        }

        window.location.href = submitTo;

        return false;
    });
    $('.acc').on('click', function (e) {
        var match = document.URL.match(/acc=\d+/);
        var matchUrl = document.URL.match(/printersdetail/);
        var matchSeller = document.URL.match(/sellers/);
        var matchDeals = document.URL.match(/deals/);
        var matchPagination = document.URL.match(/index/);
        
        e.preventDefault();
        var $form = $(this).closest("form");
        if (window.location == siteurl + 'printersportfolio') {
            var submitTo = $form.attr("action") + '?acc=' + $(this).attr('data-acc');
        }else if(matchUrl || matchSeller || matchDeals || matchPagination) {
            var submitTo = siteurl + 'printersportfolio/search'  + '?acc=' + $(this).attr('data-acc');
        }else if( window.location == siteurl) {
            
            var submitTo = ("printersportfolio/search") + ('?acc=' + $(this).attr('data-acc'));
        } else if (window.location.href.indexOf("acc") > -1) {
            var submitTo = window.location.href.replace(match, 'acc=' + $(this).attr('data-acc'));
        } else {
            var submitTo = window.location + '&acc=' + $(this).attr('data-acc');
        }

        window.location.href = submitTo;

        return false;
    });
    
});
