$(document).ready(function () {

    var outerHeight = 0;
    $('.main_content').each(function() {
        outerHeight += $(this).outerHeight(true);
    });
    var panel_height = 0;
    var active_panel, panel;
    active_panel = $('.active_panel');
    panel = active_panel.siblings('.panel');
    panel_height = panel.outerHeight(true);
    var last_seen;
    last_seen=$('.last_seen');
    var last_seen_height = last_seen.outerHeight(true);
    var sidebar_height = outerHeight - panel_height;
    var filer_height = outerHeight - panel_height - last_seen_height; //margin-bottom

    $(".sidebar").css('min-height', sidebar_height + 'px');
    $(".filter").css('min-height', filer_height + 'px');
    last_seen.css('min-height', last_seen_height + 'px');

    //koza

     $('.cookie-message').cookieBar();


    /*--------broken images--------*/

    $('img').one('error', function () {

        $(this).attr("src", siteurl + "media/public/images/no-image.jpg");

    });


    /*--------responsive menu--------*/

    $('.toggle-nav').click(function(e) {
        e.preventDefault();

        $(this).toggleClass('active_nav');
        $('.menu_bottom_row').toggleClass('active_nav');
    });

    /*--------dropdown--------*/
    $('.dropdown_menu').hide(); 
    
    /*--------dropdown_check -----------*/
    $('.min_max').on('click', function (e) {
        localStorage.setItem('lastPrice', $(e.target).attr('data-price'));
    });
    $('.man').on('click', function (e) {
        localStorage.setItem('lastMan', $(e.target).attr('data-man'));
    });
    $('.cat').on('click', function (e) {
        localStorage.setItem('lastCat', $(e.target).attr('data-cat'));
    });
    
    var lastPrice = localStorage.getItem('lastPrice');
    var lastMan = localStorage.getItem('lastMan');
    var lastCat = localStorage.getItem('lastCat');


    var matchPrice = document.URL.match(/min=\d+&max=\d+/);
    var matchMan = document.URL.match(/man=\d/);
    var matchCat = document.URL.match(/catName=\d/);
    
    var price = $('.dropdown_menu').find("[data-price='" + lastPrice + "']");
    var man = $('.dropdown_menu').find("[data-man='" + lastMan + "']");
    var cat = $('.dropdown_menu').find("[data-cat='" + lastCat + "']");
    
    
    if(matchPrice){
        price.closest('.dropdown_menu').show(); 
        price.addClass('orange');
    }
    if(matchMan){
       man.closest('.dropdown_menu').show(); 
       man.addClass('orange');
    }
    if(matchCat){
       cat.closest('.dropdown_menu').show(); 
       cat.addClass('orange');
    }
    
//    filament scripts
    
    $('.min_max_fil').on('click', function (e) {
        localStorage.setItem('lastPrice-fil', $(e.target).attr('data-price-fil'));
    });
    $('.man-fil').on('click', function (e) {
        localStorage.setItem('lastMan-fil', $(e.target).attr('data-man-fil'));
    });
    $('.cat-fil').on('click', function (e) {
        localStorage.setItem('lastCat-fil', $(e.target).attr('data-cat-fil'));
    });
    
    var lastPriceFil = localStorage.getItem('lastPrice-fil');
    var lastManFil = localStorage.getItem('lastMan-fil');
    var lastCatFil = localStorage.getItem('lastCat-fil');


    var matchPriceFil = document.URL.match(/min=\d+&max=\d+/);
    var matchManFil = document.URL.match(/man=\d/);
    var matchCatFil = document.URL.match(/catName=\d/);
    
    var priceFil = $('.dropdown_menu').find("[data-price-fil='" + lastPriceFil + "']");
    var manFil = $('.dropdown_menu').find("[data-man-fil='" + lastManFil + "']");
    var catFil = $('.dropdown_menu').find("[data-cat-fil='" + lastCatFil + "']");
    
    
    if(matchPriceFil){
        priceFil.closest('.dropdown_menu').show(); 
        priceFil.addClass('orange');
    }
    if(matchManFil){
       manFil.closest('.dropdown_menu').show(); 
       manFil.addClass('orange');
    }
    if(matchCatFil){
       catFil.closest('.dropdown_menu').show(); 
       catFil.addClass('orange');
    }
    
    // Printer scripts
    
    $('.min_max_pr').on('click', function (e) {
        localStorage.setItem('lastPrice-pr', $(e.target).attr('data-price-pr'));
    });
    $('.man-pr').on('click', function (e) {
        localStorage.setItem('lastMan-pr', $(e.target).attr('data-man-pr'));
    });
    
    
    $('.tech').on('click', function (e) {
        localStorage.setItem('lastMan-tech', $(e.target).attr('data-tech'));
    });
    $('.size').on('click', function (e) {
        localStorage.setItem('lastMan-size', $(e.target).attr('data-size'));
    });
    $('.acc').on('click', function (e) {
        localStorage.setItem('lastMan-acc', $(e.target).attr('data-acc'));
    });

    
    var lastPricePr = localStorage.getItem('lastPrice-pr');
    var lastManPr = localStorage.getItem('lastMan-pr');
    
    var lastTech = localStorage.getItem('lastMan-tech');
    var lastSize = localStorage.getItem('lastMan-size');
    var lastAcc = localStorage.getItem('lastMan-acc');


    var matchPricePr = document.URL.match(/min=\d+&max=\d+/);
    var matchManPr = document.URL.match(/man=\d/);
    
    var matchTech = document.URL.match(/tech=\d/);
    var matchSize = document.URL.match(/size=\d/);
    var matchAcc = document.URL.match(/acc=\d/);
    
    var pricePr = $('.dropdown_menu').find("[data-price-pr='" + lastPricePr + "']");
    var manPr = $('.dropdown_menu').find("[data-man-pr='" + lastManPr + "']");
    
    var Tech = $('.dropdown_menu').find("[data-tech='" + lastTech + "']");
    var Size = $('.dropdown_menu').find("[data-size='" + lastSize + "']");
    var Acc = $('.dropdown_menu').find("[data-acc='" + lastAcc + "']");
    
    
    if(matchPricePr){
        pricePr.closest('.dropdown_menu').show(); 
        pricePr.addClass('orange');
    }
    if(matchManPr){
       manPr.closest('.dropdown_menu').show(); 
       manPr.addClass('orange');
    }
    
    if(matchTech){
       Tech.closest('.dropdown_menu').show(); 
       Tech.addClass('orange');
    }
    if(matchSize){
       Size.closest('.dropdown_menu').show(); 
       Size.addClass('orange');
    }
    if(matchAcc){
       Acc.closest('.dropdown_menu').show(); 
       Acc.addClass('orange');
    }

    
    /*--------end_of_checking -----------*/
        
    $('.dropdown_title').click(function () {

        $(this).toggleClass('arrow_up').siblings('ul').toggle();
    });
    
    /*--------tab box--------*/

    $('.panel').hide();
    $('.panel:first').show();

    $('.tab.tab_deals').click(function () {

        var $tabBox = $(this).closest('.tab_box');
        $tabBox.find('.tab').removeClass('active_tab');
        $(this).addClass('active_tab');

        var showTab = $(this).attr('data-tabs');
        $(this).siblings('[data-tabs]').parent('ul').siblings('[data-tabs=' + showTab + ']').fadeIn(600).siblings('[data-tabs]').hide();

        panel_height = $(this).siblings('[data-tabs]').parent('ul').siblings('[data-tabs=' + showTab + ']').siblings('[data-tabs]').outerHeight(true);

        $(".sidebar").css('min-height', outerHeight - panel_height + 'px');


    });

    /*--------current menu element--------*/

    var url = window.location;
    // Will only work if string in href matches with location
    $('ul.tabs.profile_tabs a[href="' + url + '"]');

    // Will also work for relative and absolute hrefs
    $('ul.tabs.profile_tabs a').filter(function () {
        return this.href == url;
    }).parent().addClass('active_tab');

    /*--------accordion--------*/

    $('.accordion_section > .accordion_content').hide();
    $('.accordion_section > .accordion_content:first').show();
    $('.accordion_title:first').toggleClass('current_accordion');
    $('#accordion_content').show();

    $('.accordion_title').click(function (e) {
        e.preventDefault();
        var siblingSections = $(this).parent().siblings('.accordion_section');
        $(this).toggleClass('current_accordion').siblings('.accordion_content').slideToggle(500);
        siblingSections.children('.accordion_content').slideUp(500);
        siblingSections.children('.accordion_title').removeClass('current_accordion');

    });

    var qty_num, qty_text, qty_field,
        select, selected_option, select_text,
        restricted_select, restricted_option,
        color_select, color_option, target,
        default_color_select_text;

    /*--------numeric input--------*/

    $('.qty_add').click(function () {
        qty_field = $(this).closest('.qty_field');
        qty_num = qty_field.find('.qty_num').val();
        qty_text = qty_field.find('.qty_num');
        qty_text.val(++qty_num);
    });
    $('.qty_remove').click(function () {
        qty_field = $(this).closest('.qty_field');
        qty_num = qty_field.find('.qty_num').val();
        qty_text = qty_field.find('.qty_num');
        if (qty_num > 0) qty_text.val(--qty_num);
    });

    /*--------selects----------*/

    $('select').change(function () {
        selected_option = $(this).find('option:selected');
        select_text = selected_option.text();
        $(this).siblings('.select_text').find('p').text(select_text);

    });

    $('.restricted_select').change(function () {
        restricted_option = $(this).find('option:selected');

    });

    $('.color_select').change(function () {
        color_option = $(this).find('option:selected');

    });

    /*--------filament/finishing color picker--------*/
    var colors2=[];
    $(".add_color").click(function (e) {
        e.preventDefault();

        target = $(this).closest('.buy_product');

        var color_select_value = target.find('.color_select option:selected').val();
        var color_select_text = target.find('.color_select option:selected').text();
         
        var restricted_select_value = target.find('.restricted_select option:selected').val();
        var restricted_select_text = target.find('.restricted_select option:selected').text();

        qty_num = target.find('.qty_num').val();
         colors2.push({color: color_select_value , qty: qty_num});

        if ((qty_num > 0) && (restricted_select_value != 'default') && ( color_select_value != 'default')) {

            target.find('.restricted_product').addClass('disabled_select');
            target.find('.restricted_select').children('option:selected').siblings().prop('disabled', true);
            target.find('.product_added').css('display', 'inline-block');
            target.find('h3').css('display', 'block');
            target.find('.chosen_product').css('display', 'inline-block').text(restricted_select_text + ': ');
            target.find('.qty_' + color_select_text + '').text(qty_num);
            target.find('[data-color=' + color_select_text + ']').css('display', 'inline-block');

        }
        else {

            target.find('[data-color=' + color_select_text + ']').css('display', 'none');
        }

        $('.remove_product').click(function (e) {
            e.preventDefault();

            target = $(this).closest('.buy_product');

            target.find('.restricted_product').removeClass('disabled_select');
            target.find('.restricted_select').children('option:selected').siblings().prop('disabled', false);
            target.find('.qty').text('');

            var default_restricted_select_text = restricted_option.siblings('option[value="default"]').text();
            restricted_option.siblings('option[value="default"]').prop('selected', true).prop('disabled', true);
            target.find('.restricted_text').children('p').text(default_restricted_select_text);

            default_color_select_text = color_option.siblings('option[value="default"]').text();
            color_option.siblings('option[value="default"]').prop('selected', true).prop('disabled', true);
            target.find('.color_text').children('p').text(default_color_select_text);

            target.find("input[name='quantity']").val('Qty');

            $(this).siblings().css('display', 'none').closest('.product_added').css('display', 'none');

        });

        $('.remove_color').click(function (e) {
            e.preventDefault();

            target = $(this).closest('.buy_product');

            var color = $(this).attr("data-color");

            target.find('[data-color=' + color + ']').css('display', 'none');
            target.find('.qty_' + color + '').text('');

            default_color_select_text = color_option.siblings('option[value="default"]').text();
            color_option.siblings('option[value="default"]').prop('selected', true).prop('disabled', true);
            target.find('.color_text').children('p').text(default_color_select_text);

            target.find("input[name='quantity']").val('Qty');

        });

    });

    /*--------temporary maybe? enter breaks add filament/finish color number input --------*/

    $("input[name='quantity']").keydown(function(e){

        if (e.keyCode == 10 || e.keyCode == 13)
            e.preventDefault();

    });
    
    $("button[name='addfilament']").click(function (e) {
        e.preventDefault();
        target = $(this).closest('.buy_product');

        var id = target.find('.id_input').val();
       
        var colour = target.find('.color_select option:selected').val();

        qty_num = target.find('.qty_num').val();

      

        // var obj = {};
        //obj[$(val).attr('data-color')] = parseInt($(val).find('.qty').html(), 10);
        //colors.push(obj);

        
       
        

        $.ajax({
            url: '/index.php/cart/ajaxBuyFilament',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                colors2: colors2
            },
            complete: function (data) {
                window.location.reload();
            }

            
        });

    });

    $(".basket_controls").on('click', function () {
        var val2 = $(this).closest("div.qty_field").find("input[name='qty[]']").val();

        var val = $(this).closest("div.qty_field").find("input[name='qty[]']").attr("data-id");
        
        $.ajax({
            url: '/index.php/cart/updateCart',
            type: 'post',
            dataType: 'json',
            data: {
                id: val,
                qty: val2
            },
            complete: function (data) {
                window.location.reload();
            }

        });

    });
   
   /*-------- Terms of use --------*/
   
    $('#terms').on('click', function(){
        var terms = $('#terms').is(':checked');
        if (terms === true) {
            $('#terms_hidden').attr('value', function () {
                return 1;
            });
        } else {
            $('#terms_hidden').attr('value', function () {
                return 0;
            });
        }
    });

    /*--------Compare list--------*/
    $(".compare_link").on('click', function (e) {
        e.preventDefault();                
        var id = $(this).data('id');
 
        $.ajax({
            url: '/index.php/compare/ajaxCompare',
            type: 'post',
            dataType: 'json',
            data: {
                compare: id        
                },
            success: function (data) { 
                if (data.error === 1) {
                    $('#compare_error').show().addClass('notification warning');
                    $('#compare_span').addClass('icon-times-circle');
                    $('#compare_error_p').text('You can have up to 3 items in the compare list');
                } else {
                    window.location.reload();
                }               
            }
            
        });   
    });
    
    /*--------Compare list remove item--------*/   
    $(".compare_link_remove").on('click', function (e) {
        e.preventDefault();                
        var id = $(this).data('id');
        var ele = [];
        var ele = document.getElementsByClassName('product_'+id);
        
        for(var i = 0; i < ele.length; i++) {
            $("td").remove("._"+id);
        }
        
        $.ajax({
            url: '/index.php/compare/AjaxRemoveItems',
            type: 'post',
            data: {
                remcompare: id        
                },
            success: function(){
               window.location.reload();               
            }
        });
    });
     
    $(".ct_basket").on('click', function (e) {
            e.preventDefault();                
            var id = $(this).data('id');
          $.ajax({
            url: '/index.php/cart/buyProduct',
            type: 'post',
            dataType: 'json',
            data: {
                id: id        
                },
                complete: function (data) {
                window.location.reload();
            }
            
    });
    });
    
    $(document).ready(function() {

  
	
});
    /*--------elevateZoom--------*/

    $('.zoom').elevateZoom({
        zoomType: "lens",
        lensShape: "round",
        lensSize: 150,
        borderSize: 1
    });

    /*--------Share buttons--------*/

    stLight.options({
        publisher: "6b16154e-c062-40f4-8de1-22586fab3aaa",
        doNotHash: false,
        doNotCopy: false,
        hashAddressBar: false
    });

    /*---------?--------*/


});
